(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./node_modules/moment/locale sync recursive ^\\.\\/.*$":
/*!**************************************************!*\
  !*** ./node_modules/moment/locale sync ^\.\/.*$ ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": "./node_modules/moment/locale/af.js",
	"./af.js": "./node_modules/moment/locale/af.js",
	"./ar": "./node_modules/moment/locale/ar.js",
	"./ar-dz": "./node_modules/moment/locale/ar-dz.js",
	"./ar-dz.js": "./node_modules/moment/locale/ar-dz.js",
	"./ar-kw": "./node_modules/moment/locale/ar-kw.js",
	"./ar-kw.js": "./node_modules/moment/locale/ar-kw.js",
	"./ar-ly": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ly.js": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ma": "./node_modules/moment/locale/ar-ma.js",
	"./ar-ma.js": "./node_modules/moment/locale/ar-ma.js",
	"./ar-sa": "./node_modules/moment/locale/ar-sa.js",
	"./ar-sa.js": "./node_modules/moment/locale/ar-sa.js",
	"./ar-tn": "./node_modules/moment/locale/ar-tn.js",
	"./ar-tn.js": "./node_modules/moment/locale/ar-tn.js",
	"./ar.js": "./node_modules/moment/locale/ar.js",
	"./az": "./node_modules/moment/locale/az.js",
	"./az.js": "./node_modules/moment/locale/az.js",
	"./be": "./node_modules/moment/locale/be.js",
	"./be.js": "./node_modules/moment/locale/be.js",
	"./bg": "./node_modules/moment/locale/bg.js",
	"./bg.js": "./node_modules/moment/locale/bg.js",
	"./bm": "./node_modules/moment/locale/bm.js",
	"./bm.js": "./node_modules/moment/locale/bm.js",
	"./bn": "./node_modules/moment/locale/bn.js",
	"./bn.js": "./node_modules/moment/locale/bn.js",
	"./bo": "./node_modules/moment/locale/bo.js",
	"./bo.js": "./node_modules/moment/locale/bo.js",
	"./br": "./node_modules/moment/locale/br.js",
	"./br.js": "./node_modules/moment/locale/br.js",
	"./bs": "./node_modules/moment/locale/bs.js",
	"./bs.js": "./node_modules/moment/locale/bs.js",
	"./ca": "./node_modules/moment/locale/ca.js",
	"./ca.js": "./node_modules/moment/locale/ca.js",
	"./cs": "./node_modules/moment/locale/cs.js",
	"./cs.js": "./node_modules/moment/locale/cs.js",
	"./cv": "./node_modules/moment/locale/cv.js",
	"./cv.js": "./node_modules/moment/locale/cv.js",
	"./cy": "./node_modules/moment/locale/cy.js",
	"./cy.js": "./node_modules/moment/locale/cy.js",
	"./da": "./node_modules/moment/locale/da.js",
	"./da.js": "./node_modules/moment/locale/da.js",
	"./de": "./node_modules/moment/locale/de.js",
	"./de-at": "./node_modules/moment/locale/de-at.js",
	"./de-at.js": "./node_modules/moment/locale/de-at.js",
	"./de-ch": "./node_modules/moment/locale/de-ch.js",
	"./de-ch.js": "./node_modules/moment/locale/de-ch.js",
	"./de.js": "./node_modules/moment/locale/de.js",
	"./dv": "./node_modules/moment/locale/dv.js",
	"./dv.js": "./node_modules/moment/locale/dv.js",
	"./el": "./node_modules/moment/locale/el.js",
	"./el.js": "./node_modules/moment/locale/el.js",
	"./en-au": "./node_modules/moment/locale/en-au.js",
	"./en-au.js": "./node_modules/moment/locale/en-au.js",
	"./en-ca": "./node_modules/moment/locale/en-ca.js",
	"./en-ca.js": "./node_modules/moment/locale/en-ca.js",
	"./en-gb": "./node_modules/moment/locale/en-gb.js",
	"./en-gb.js": "./node_modules/moment/locale/en-gb.js",
	"./en-ie": "./node_modules/moment/locale/en-ie.js",
	"./en-ie.js": "./node_modules/moment/locale/en-ie.js",
	"./en-il": "./node_modules/moment/locale/en-il.js",
	"./en-il.js": "./node_modules/moment/locale/en-il.js",
	"./en-nz": "./node_modules/moment/locale/en-nz.js",
	"./en-nz.js": "./node_modules/moment/locale/en-nz.js",
	"./eo": "./node_modules/moment/locale/eo.js",
	"./eo.js": "./node_modules/moment/locale/eo.js",
	"./es": "./node_modules/moment/locale/es.js",
	"./es-do": "./node_modules/moment/locale/es-do.js",
	"./es-do.js": "./node_modules/moment/locale/es-do.js",
	"./es-us": "./node_modules/moment/locale/es-us.js",
	"./es-us.js": "./node_modules/moment/locale/es-us.js",
	"./es.js": "./node_modules/moment/locale/es.js",
	"./et": "./node_modules/moment/locale/et.js",
	"./et.js": "./node_modules/moment/locale/et.js",
	"./eu": "./node_modules/moment/locale/eu.js",
	"./eu.js": "./node_modules/moment/locale/eu.js",
	"./fa": "./node_modules/moment/locale/fa.js",
	"./fa.js": "./node_modules/moment/locale/fa.js",
	"./fi": "./node_modules/moment/locale/fi.js",
	"./fi.js": "./node_modules/moment/locale/fi.js",
	"./fo": "./node_modules/moment/locale/fo.js",
	"./fo.js": "./node_modules/moment/locale/fo.js",
	"./fr": "./node_modules/moment/locale/fr.js",
	"./fr-ca": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ca.js": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ch": "./node_modules/moment/locale/fr-ch.js",
	"./fr-ch.js": "./node_modules/moment/locale/fr-ch.js",
	"./fr.js": "./node_modules/moment/locale/fr.js",
	"./fy": "./node_modules/moment/locale/fy.js",
	"./fy.js": "./node_modules/moment/locale/fy.js",
	"./gd": "./node_modules/moment/locale/gd.js",
	"./gd.js": "./node_modules/moment/locale/gd.js",
	"./gl": "./node_modules/moment/locale/gl.js",
	"./gl.js": "./node_modules/moment/locale/gl.js",
	"./gom-latn": "./node_modules/moment/locale/gom-latn.js",
	"./gom-latn.js": "./node_modules/moment/locale/gom-latn.js",
	"./gu": "./node_modules/moment/locale/gu.js",
	"./gu.js": "./node_modules/moment/locale/gu.js",
	"./he": "./node_modules/moment/locale/he.js",
	"./he.js": "./node_modules/moment/locale/he.js",
	"./hi": "./node_modules/moment/locale/hi.js",
	"./hi.js": "./node_modules/moment/locale/hi.js",
	"./hr": "./node_modules/moment/locale/hr.js",
	"./hr.js": "./node_modules/moment/locale/hr.js",
	"./hu": "./node_modules/moment/locale/hu.js",
	"./hu.js": "./node_modules/moment/locale/hu.js",
	"./hy-am": "./node_modules/moment/locale/hy-am.js",
	"./hy-am.js": "./node_modules/moment/locale/hy-am.js",
	"./id": "./node_modules/moment/locale/id.js",
	"./id.js": "./node_modules/moment/locale/id.js",
	"./is": "./node_modules/moment/locale/is.js",
	"./is.js": "./node_modules/moment/locale/is.js",
	"./it": "./node_modules/moment/locale/it.js",
	"./it.js": "./node_modules/moment/locale/it.js",
	"./ja": "./node_modules/moment/locale/ja.js",
	"./ja.js": "./node_modules/moment/locale/ja.js",
	"./jv": "./node_modules/moment/locale/jv.js",
	"./jv.js": "./node_modules/moment/locale/jv.js",
	"./ka": "./node_modules/moment/locale/ka.js",
	"./ka.js": "./node_modules/moment/locale/ka.js",
	"./kk": "./node_modules/moment/locale/kk.js",
	"./kk.js": "./node_modules/moment/locale/kk.js",
	"./km": "./node_modules/moment/locale/km.js",
	"./km.js": "./node_modules/moment/locale/km.js",
	"./kn": "./node_modules/moment/locale/kn.js",
	"./kn.js": "./node_modules/moment/locale/kn.js",
	"./ko": "./node_modules/moment/locale/ko.js",
	"./ko.js": "./node_modules/moment/locale/ko.js",
	"./ky": "./node_modules/moment/locale/ky.js",
	"./ky.js": "./node_modules/moment/locale/ky.js",
	"./lb": "./node_modules/moment/locale/lb.js",
	"./lb.js": "./node_modules/moment/locale/lb.js",
	"./lo": "./node_modules/moment/locale/lo.js",
	"./lo.js": "./node_modules/moment/locale/lo.js",
	"./lt": "./node_modules/moment/locale/lt.js",
	"./lt.js": "./node_modules/moment/locale/lt.js",
	"./lv": "./node_modules/moment/locale/lv.js",
	"./lv.js": "./node_modules/moment/locale/lv.js",
	"./me": "./node_modules/moment/locale/me.js",
	"./me.js": "./node_modules/moment/locale/me.js",
	"./mi": "./node_modules/moment/locale/mi.js",
	"./mi.js": "./node_modules/moment/locale/mi.js",
	"./mk": "./node_modules/moment/locale/mk.js",
	"./mk.js": "./node_modules/moment/locale/mk.js",
	"./ml": "./node_modules/moment/locale/ml.js",
	"./ml.js": "./node_modules/moment/locale/ml.js",
	"./mn": "./node_modules/moment/locale/mn.js",
	"./mn.js": "./node_modules/moment/locale/mn.js",
	"./mr": "./node_modules/moment/locale/mr.js",
	"./mr.js": "./node_modules/moment/locale/mr.js",
	"./ms": "./node_modules/moment/locale/ms.js",
	"./ms-my": "./node_modules/moment/locale/ms-my.js",
	"./ms-my.js": "./node_modules/moment/locale/ms-my.js",
	"./ms.js": "./node_modules/moment/locale/ms.js",
	"./mt": "./node_modules/moment/locale/mt.js",
	"./mt.js": "./node_modules/moment/locale/mt.js",
	"./my": "./node_modules/moment/locale/my.js",
	"./my.js": "./node_modules/moment/locale/my.js",
	"./nb": "./node_modules/moment/locale/nb.js",
	"./nb.js": "./node_modules/moment/locale/nb.js",
	"./ne": "./node_modules/moment/locale/ne.js",
	"./ne.js": "./node_modules/moment/locale/ne.js",
	"./nl": "./node_modules/moment/locale/nl.js",
	"./nl-be": "./node_modules/moment/locale/nl-be.js",
	"./nl-be.js": "./node_modules/moment/locale/nl-be.js",
	"./nl.js": "./node_modules/moment/locale/nl.js",
	"./nn": "./node_modules/moment/locale/nn.js",
	"./nn.js": "./node_modules/moment/locale/nn.js",
	"./pa-in": "./node_modules/moment/locale/pa-in.js",
	"./pa-in.js": "./node_modules/moment/locale/pa-in.js",
	"./pl": "./node_modules/moment/locale/pl.js",
	"./pl.js": "./node_modules/moment/locale/pl.js",
	"./pt": "./node_modules/moment/locale/pt.js",
	"./pt-br": "./node_modules/moment/locale/pt-br.js",
	"./pt-br.js": "./node_modules/moment/locale/pt-br.js",
	"./pt.js": "./node_modules/moment/locale/pt.js",
	"./ro": "./node_modules/moment/locale/ro.js",
	"./ro.js": "./node_modules/moment/locale/ro.js",
	"./ru": "./node_modules/moment/locale/ru.js",
	"./ru.js": "./node_modules/moment/locale/ru.js",
	"./sd": "./node_modules/moment/locale/sd.js",
	"./sd.js": "./node_modules/moment/locale/sd.js",
	"./se": "./node_modules/moment/locale/se.js",
	"./se.js": "./node_modules/moment/locale/se.js",
	"./si": "./node_modules/moment/locale/si.js",
	"./si.js": "./node_modules/moment/locale/si.js",
	"./sk": "./node_modules/moment/locale/sk.js",
	"./sk.js": "./node_modules/moment/locale/sk.js",
	"./sl": "./node_modules/moment/locale/sl.js",
	"./sl.js": "./node_modules/moment/locale/sl.js",
	"./sq": "./node_modules/moment/locale/sq.js",
	"./sq.js": "./node_modules/moment/locale/sq.js",
	"./sr": "./node_modules/moment/locale/sr.js",
	"./sr-cyrl": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr-cyrl.js": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr.js": "./node_modules/moment/locale/sr.js",
	"./ss": "./node_modules/moment/locale/ss.js",
	"./ss.js": "./node_modules/moment/locale/ss.js",
	"./sv": "./node_modules/moment/locale/sv.js",
	"./sv.js": "./node_modules/moment/locale/sv.js",
	"./sw": "./node_modules/moment/locale/sw.js",
	"./sw.js": "./node_modules/moment/locale/sw.js",
	"./ta": "./node_modules/moment/locale/ta.js",
	"./ta.js": "./node_modules/moment/locale/ta.js",
	"./te": "./node_modules/moment/locale/te.js",
	"./te.js": "./node_modules/moment/locale/te.js",
	"./tet": "./node_modules/moment/locale/tet.js",
	"./tet.js": "./node_modules/moment/locale/tet.js",
	"./tg": "./node_modules/moment/locale/tg.js",
	"./tg.js": "./node_modules/moment/locale/tg.js",
	"./th": "./node_modules/moment/locale/th.js",
	"./th.js": "./node_modules/moment/locale/th.js",
	"./tl-ph": "./node_modules/moment/locale/tl-ph.js",
	"./tl-ph.js": "./node_modules/moment/locale/tl-ph.js",
	"./tlh": "./node_modules/moment/locale/tlh.js",
	"./tlh.js": "./node_modules/moment/locale/tlh.js",
	"./tr": "./node_modules/moment/locale/tr.js",
	"./tr.js": "./node_modules/moment/locale/tr.js",
	"./tzl": "./node_modules/moment/locale/tzl.js",
	"./tzl.js": "./node_modules/moment/locale/tzl.js",
	"./tzm": "./node_modules/moment/locale/tzm.js",
	"./tzm-latn": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm-latn.js": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm.js": "./node_modules/moment/locale/tzm.js",
	"./ug-cn": "./node_modules/moment/locale/ug-cn.js",
	"./ug-cn.js": "./node_modules/moment/locale/ug-cn.js",
	"./uk": "./node_modules/moment/locale/uk.js",
	"./uk.js": "./node_modules/moment/locale/uk.js",
	"./ur": "./node_modules/moment/locale/ur.js",
	"./ur.js": "./node_modules/moment/locale/ur.js",
	"./uz": "./node_modules/moment/locale/uz.js",
	"./uz-latn": "./node_modules/moment/locale/uz-latn.js",
	"./uz-latn.js": "./node_modules/moment/locale/uz-latn.js",
	"./uz.js": "./node_modules/moment/locale/uz.js",
	"./vi": "./node_modules/moment/locale/vi.js",
	"./vi.js": "./node_modules/moment/locale/vi.js",
	"./x-pseudo": "./node_modules/moment/locale/x-pseudo.js",
	"./x-pseudo.js": "./node_modules/moment/locale/x-pseudo.js",
	"./yo": "./node_modules/moment/locale/yo.js",
	"./yo.js": "./node_modules/moment/locale/yo.js",
	"./zh-cn": "./node_modules/moment/locale/zh-cn.js",
	"./zh-cn.js": "./node_modules/moment/locale/zh-cn.js",
	"./zh-hk": "./node_modules/moment/locale/zh-hk.js",
	"./zh-hk.js": "./node_modules/moment/locale/zh-hk.js",
	"./zh-tw": "./node_modules/moment/locale/zh-tw.js",
	"./zh-tw.js": "./node_modules/moment/locale/zh-tw.js"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	var module = __webpack_require__(id);
	return module;
}
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) { // check for number or string
		var e = new Error('Cannot find module "' + req + '".');
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return id;
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./node_modules/moment/locale sync recursive ^\\.\\/.*$";

/***/ }),

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error('Cannot find module "' + req + '".');
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/admin/bars-page/bars-page.component.html":
/*!**********************************************************!*\
  !*** ./src/app/admin/bars-page/bars-page.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"o-page\">\r\n  <app-sidebar></app-sidebar>\r\n  <main class=\"o-page__content\">\r\n    <app-navbar></app-navbar>\r\n    <div class=\"container page-content\">\r\n      <div class=\"row\">\r\n        <div class=\"col-xl-3\">\r\n          <aside class=\"c-menu u-ml-medium u-hidden-down@wide\">\r\n            <h4 class=\"c-menu__title\">Menu</h4>\r\n            <ul class=\"u-mb-medium\">\r\n              <li class=\"c-menu__item\">\r\n                <a class=\"c-menu__link is-active\" >\r\n                  <i class=\"fa fa-trophy u-mr-xsmall\"></i>Featured Events\r\n                </a>\r\n              </li>\r\n              <li class=\"c-menu__item\">\r\n                <a class=\"c-menu__link\" >\r\n                  <i class=\"c-menu__icon fa fa-heart-o\"></i>Recommended\r\n                </a>\r\n              </li>\r\n              <li class=\"c-menu__item\">\r\n                <a class=\"c-menu__link\" >\r\n                  <i class=\"c-menu__icon fa fa-bullhorn\"></i>Live Streams\r\n                </a>\r\n              </li>\r\n              <li class=\"c-menu__item\">\r\n                <a class=\"c-menu__link\" >\r\n                  <i class=\"c-menu__icon fa fa-newspaper-o\"></i>Press\r\n                </a>\r\n              </li>\r\n              <li class=\"c-menu__item\">\r\n                <a class=\"c-menu__link\" >\r\n                  <i class=\"c-menu__icon fa fa-diamond\"></i>Favourites\r\n                </a>\r\n              </li>\r\n              <li class=\"c-menu__item\">\r\n                <a class=\"c-menu__link\" >\r\n                  <i class=\"c-menu__icon fa fa-map-o\"></i>Places\r\n                </a>\r\n              </li>\r\n            </ul>\r\n\r\n            <h4 class=\"c-menu__title\">Your Events</h4>\r\n            <ul>\r\n              <li class=\"c-menu__item\">\r\n                <a class=\"c-menu__link\" >\r\n                  <img src=\"assets/imgsidebar-icon1.png\" class=\"u-mr-xsmall\" style=\"width: 14px;\" alt=\"Icon 1\">Classes\r\n                </a>\r\n              </li>\r\n              <li class=\"c-menu__item\">\r\n                <a class=\"c-menu__link\" >\r\n                  <img src=\"assets/imgsidebar-icon2.png\" class=\"u-mr-xsmall\" style=\"width: 14px;\" alt=\"Icon 2\">People\r\n                </a>\r\n              </li>\r\n              <li class=\"c-menu__item\">\r\n                <a class=\"c-menu__link\" >\r\n                  <img src=\"assets/imgsidebar-icon3.png\" class=\"u-mr-xsmall\" style=\"width: 14px;\" alt=\"Icon 3\">Networking\r\n                </a>\r\n              </li>\r\n              <li class=\"c-menu__item\">\r\n                <a class=\"c-menu__link\" >\r\n                  <img src=\"assets/imgsidebar-icon4.png\" class=\"u-mr-xsmall\" style=\"width: 14px;\" alt=\"Icon 4\">Hi-Skill\r\n                </a>\r\n              </li>\r\n              <li class=\"c-menu__item\">\r\n                <a class=\"c-menu__link\" >\r\n                  <img src=\"assets/imgsidebar-icon5.png\" class=\"u-mr-xsmall\" style=\"width: 14px;\" alt=\"Add icon\">Going to Buy\r\n                </a>\r\n              </li>\r\n              <li class=\"c-menu__item\">\r\n                <a class=\"c-menu__link\" >\r\n                  <img src=\"assets/imgsidebar-icon6.png\" class=\"u-mr-xsmall\" style=\"width: 14px;\" alt=\"Add icon\">Add New List\r\n                </a>\r\n              </li>\r\n            </ul>\r\n          </aside>\r\n        </div>\r\n\r\n        <div class=\"col-sm-12 col-xl-9\">\r\n          <main>\r\n            <div class=\"row\">\r\n              <div class=\"col-8 col-xl-10\">\r\n                <h1 class=\"u-h3\">Featured Events</h1>\r\n              </div>\r\n              <div class=\"col-4 col-xl-2\">\r\n                <div class=\"c-field u-mb-small\">\r\n                  <label class=\"c-field__label u-hidden-visually\" for=\"select-recently\">Recently Opened</label>\r\n\r\n                  <!-- Select2 jquery plugin is used -->\r\n                  <select class=\"c-select\" name=\"recently-opened\" id=\"select-recently\">\r\n                    <option value=\"value1\">All Types</option>\r\n                    <option value=\"value2\">This day</option>\r\n                    <option value=\"value3\">Last Week</option>\r\n                  </select>\r\n                </div>\r\n              </div>\r\n            </div><!-- // .row -->\r\n            <div class=\"row\">\r\n              <div class=\"col-md-6\">\r\n                <article class=\"c-event\">\r\n                  <div class=\"c-event__img\">\r\n                    <img src=\"assets/imgevent1.jpg\" alt=\"San Francisco's Buildings\">\r\n                    <span class=\"c-event__status\">Featured</span>\r\n                  </div>\r\n                  <div class=\"c-event__meta\">\r\n                    <h3 class=\"c-event__title\">Designing in US\r\n                      <span class=\"c-event__place\">United States, San Francisco</span>\r\n                    </h3>\r\n\r\n                    <a  class=\"c-btn c-btn--success c-event__btn\">Buy for $560</a>\r\n                  </div>\r\n                </article><!-- // .c-event -->\r\n              </div>\r\n\r\n              <div class=\"col-md-6\">\r\n                <article class=\"c-event\">\r\n                  <div class=\"c-event__img\">\r\n                    <img src=\"assets/imgevent2.jpg\" alt=\"San Francisco's Buildings\">\r\n\r\n                    <span class=\"c-event__status\">Featured</span>\r\n                  </div>\r\n                  <div class=\"c-event__meta\">\r\n\r\n                    <h3 class=\"c-event__title\">Photography Class\r\n                      <span class=\"c-event__place\">United States, San Francisco</span>\r\n                    </h3>\r\n\r\n                    <a  class=\"c-btn c-btn--success c-event__btn\">Buy for $560</a>\r\n                  </div>\r\n                </article><!-- // .c-event -->\r\n              </div>\r\n            </div><!-- // .row -->\r\n\r\n            <div class=\"row\">\r\n              <div class=\"col-sm-12\">\r\n                <div class=\"c-table-responsive@tablet\">\r\n                  <table class=\"c-table u-mb-large\">\r\n                    <caption class=\"c-table__title\">\r\n                      All Events <small>32 Events</small>\r\n                    </caption>\r\n                    <thead class=\"c-table__head c-table__head--slim\">\r\n                    <tr>\r\n                      <th class=\"c-table__cell c-table__cell--head\">Conference</th>\r\n                      <th class=\"c-table__cell c-table__cell--head\">Date</th>\r\n                      <th class=\"c-table__cell c-table__cell--head\">Price</th>\r\n                      <th class=\"c-table__cell c-table__cell--head\">\r\n                        <span class=\"u-hidden-visually\">Actions</span>\r\n                      </th>\r\n                    </tr>\r\n                    </thead>\r\n\r\n                    <tbody>\r\n                    <tr class=\"c-table__row\">\r\n                      <td class=\"c-table__cell\">O’Reilly Design Conference\r\n                        <span class=\"u-block u-text-mute u-text-xsmall\">\r\n                                                        United States, San Francisco\r\n                                                    </span>\r\n                      </td>\r\n\r\n                      <td class=\"c-table__cell\">24th - 28th August 2018</td>\r\n                      <td class=\"c-table__cell\">$290\r\n                        <span class=\"u-block u-text-xsmall u-text-mute\">\r\n                                                        32 Tickets remaining\r\n                                                    </span>\r\n                      </td>\r\n                      <td class=\"c-table__cell u-text-right\">\r\n                        <a class=\"c-btn c-btn--success\" >Buy Tickets</a>\r\n                      </td>\r\n                    </tr>\r\n\r\n                    <tr class=\"c-table__row\">\r\n                      <td class=\"c-table__cell\">UX Night at General Assembly\r\n                        <span class=\"u-block u-text-mute u-text-xsmall\">\r\n                                                        United States, Boston\r\n                                                    </span>\r\n                      </td>\r\n                      <td class=\"c-table__cell\">6th - 9th July 2018</td>\r\n                      <td class=\"c-table__cell\">$290\r\n                        <span class=\"u-block u-text-xsmall u-text-mute\">\r\n                                                        22 Tickets remaining\r\n                                                    </span>\r\n                      </td>\r\n                      <td class=\"c-table__cell u-text-right\">\r\n                        <a class=\"c-btn c-btn--success\" >Buy Tickets</a>\r\n                      </td>\r\n                    </tr>\r\n\r\n                    <tr class=\"c-table__row\">\r\n                      <td class=\"c-table__cell\">TypeCon2016\r\n                        <span class=\"u-block u-text-mute u-text-xsmall\">\r\n                                                        France, Paris\r\n                                                    </span>\r\n                      </td>\r\n                      <td class=\"c-table__cell\">24th June 2018</td>\r\n                      <td class=\"c-table__cell\">$290\r\n                        <span class=\"u-block u-text-xsmall u-text-mute\">\r\n                                                        904 Tickets remaining\r\n                                                    </span>\r\n                      </td>\r\n                      <td class=\"c-table__cell u-text-right\">\r\n                        <a class=\"c-btn c-btn--success\" >Buy Tickets</a>\r\n                      </td>\r\n                    </tr>\r\n\r\n                    <tr class=\"c-table__row\">\r\n                      <td class=\"c-table__cell\">SmashingConf San Francisco 2018\r\n                        <span class=\"u-block u-text-mute u-text-xsmall\">\r\n                                                        United States, San Francisco\r\n                                                    </span>\r\n                      </td>\r\n                      <td class=\"c-table__cell\">31st May - 3rd June 2018</td>\r\n                      <td class=\"c-table__cell\">$290\r\n                        <span class=\"u-block u-text-xsmall u-text-mute\">\r\n                                                        904 Tickets remaining\r\n                                                    </span>\r\n                      </td>\r\n                      <td class=\"c-table__cell u-text-right\">\r\n                        <a class=\"c-btn c-btn--success\" >Buy Tickets</a>\r\n                      </td>\r\n                    </tr>\r\n\r\n                    <tr class=\"c-table__row\">\r\n                      <td class=\"c-table__cell\">Now What? Conference 2018\r\n                        <span class=\"u-block u-text-mute u-text-xsmall\">\r\n                                                        United States, Sioux Falls\r\n                                                    </span>\r\n                      </td>\r\n                      <td class=\"c-table__cell\">4th - 8th May 2018</td>\r\n                      <td class=\"c-table__cell\">\r\n                        <del class=\"u-text-danger\">Sold Out</del>\r\n                      </td>\r\n                      <td class=\"c-table__cell u-text-right\">\r\n                        <a class=\"c-btn is-disabled\" >Buy Tickets</a>\r\n                      </td>\r\n                    </tr>\r\n\r\n                    <tr class=\"c-table__row\">\r\n                      <td class=\"c-table__cell\">Generate Moscow 2018\r\n                        <span class=\"u-block u-text-mute u-text-xsmall\">\r\n                                                        Russia, Moscow\r\n                                                    </span>\r\n                      </td>\r\n                      <td class=\"c-table__cell\">13th - 14th April 2018</td>\r\n                      <td class=\"c-table__cell\">$390\r\n                        <span class=\"u-block u-text-xsmall u-text-danger\">\r\n                                                        2 Tickets remaining\r\n                                                    </span>\r\n                      </td>\r\n                      <td class=\"c-table__cell u-text-right\">\r\n                        <a class=\"c-btn c-btn--success\" >Buy Tickets</a>\r\n                      </td>\r\n                    </tr>\r\n\r\n                    <tr class=\"c-table__row\">\r\n                      <td class=\"c-table__cell\">REVOLVE Conference\r\n                        <span class=\"u-block u-text-mute u-text-xsmall\">\r\n                                                        United States, Charleston\r\n                                                    </span>\r\n                      </td>\r\n                      <td class=\"c-table__cell\">24th - 28th August 2016</td>\r\n                      <td class=\"c-table__cell\">$2,100\r\n                        <span class=\"u-block u-text-xsmall u-text-mute\">\r\n                                                        89 Tickets remaining\r\n                                                    </span>\r\n                      </td>\r\n                      <td class=\"c-table__cell u-text-right\">\r\n                        <a class=\"c-btn c-btn--success\" >Buy Tickets</a>\r\n                      </td>\r\n                    </tr>\r\n                    </tbody>\r\n                  </table>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </main>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </main>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/admin/bars-page/bars-page.component.scss":
/*!**********************************************************!*\
  !*** ./src/app/admin/bars-page/bars-page.component.scss ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/admin/bars-page/bars-page.component.ts":
/*!********************************************************!*\
  !*** ./src/app/admin/bars-page/bars-page.component.ts ***!
  \********************************************************/
/*! exports provided: BarsPageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BarsPageComponent", function() { return BarsPageComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var BarsPageComponent = /** @class */ (function () {
    function BarsPageComponent() {
    }
    BarsPageComponent.prototype.ngOnInit = function () {
    };
    BarsPageComponent.prototype.onScroll = function () { };
    BarsPageComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-bars-page',
            template: __webpack_require__(/*! ./bars-page.component.html */ "./src/app/admin/bars-page/bars-page.component.html"),
            styles: [__webpack_require__(/*! ./bars-page.component.scss */ "./src/app/admin/bars-page/bars-page.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], BarsPageComponent);
    return BarsPageComponent;
}());



/***/ }),

/***/ "./src/app/admin/category-page/category-page.component.html":
/*!******************************************************************!*\
  !*** ./src/app/admin/category-page/category-page.component.html ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"o-page\">\r\n  <app-sidebar></app-sidebar>\r\n  <main class=\"o-page__content\">\r\n    <app-navbar></app-navbar>\r\n    <div class=\"container page-content\">\r\n      <div class=\"row\">\r\n        <div class=\"col-xl-3\">\r\n          <app-management-dashboard-side-menu [menuConfig]=\"menuConfig\" (action)=\"sideMenuAction($event)\"></app-management-dashboard-side-menu>\r\n        </div>\r\n\r\n        <div class=\"col-sm-12 col-xl-9\">\r\n          <main>\r\n            <div class=\"row\">\r\n              <div class=\"col-sm-12\" infiniteScroll\r\n                   [infiniteScrollDistance]=\"2\"\r\n                   [infiniteScrollThrottle]=\"300\"\r\n                   (scrolled)=\"onScroll()\">\r\n                <div class=\"c-table-responsive@tablet\">\r\n                  <table class=\"c-table u-mb-large\">\r\n                    <caption class=\"c-table__title\">\r\n                      {{'ALL_CATEGORIES' | translate}} <small>{{categories.length}} {{'CATEGORIES' | translate}}</small>\r\n                      <app-filter-input class=\"text-right\" (filterEvent)=\"filterEvent($event)\" ></app-filter-input>\r\n                    </caption>\r\n                    <thead class=\"c-table__head c-table__head--slim\">\r\n                    <tr>\r\n                      <th class=\"c-table__cell c-table__cell--head\">{{'ID' |translate}}</th>\r\n                      <th class=\"c-table__cell c-table__cell--head\">{{'NAME' |translate}}</th>\r\n                      <th class=\"c-table__cell c-table__cell--head\">{{'DESCRIPTION' |translate}}</th>\r\n                    </tr>\r\n                    </thead>\r\n\r\n                    <tbody>\r\n                    <tr class=\"c-table__row\" *ngFor=\"let category of categories | filter: {name: filter}\"  app-category-list-item [data]=\"category\">\r\n\r\n                    </tr>\r\n\r\n                    </tbody>\r\n                  </table>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </main>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </main>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/admin/category-page/category-page.component.scss":
/*!******************************************************************!*\
  !*** ./src/app/admin/category-page/category-page.component.scss ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/admin/category-page/category-page.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/admin/category-page/category-page.component.ts ***!
  \****************************************************************/
/*! exports provided: CategoryPageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CategoryPageComponent", function() { return CategoryPageComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm5/ngx-bootstrap-modal.js");
/* harmony import */ var _models_category__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../models/category */ "./src/app/models/category.ts");
/* harmony import */ var _components_modals_modal_category_modal_category_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../components/modals/modal-category/modal-category.component */ "./src/app/components/modals/modal-category/modal-category.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var CategoryPageComponent = /** @class */ (function () {
    function CategoryPageComponent(modalService) {
        this.modalService = modalService;
        this.menuConfig = [{
                name: 'MENU',
                items: [{
                        name: 'ADD_NEW_CATEGORY',
                        action: { action: 'createCategory' },
                        icon: 'plus'
                    }]
            }];
        this.categories = [];
    }
    CategoryPageComponent.prototype.ngOnInit = function () {
        this.load();
    };
    CategoryPageComponent.prototype.load = function () {
        var categories = new Array(10).fill(new _models_category__WEBPACK_IMPORTED_MODULE_2__["Category"]());
        for (var i = 0; i < categories.length; i++) {
            this.categories.push(categories[i]);
        }
    };
    CategoryPageComponent.prototype.createCategory = function () {
        var _this = this;
        this.modalRef = this.modalService.show(_components_modals_modal_category_modal_category_component__WEBPACK_IMPORTED_MODULE_3__["ModalCategoryComponent"], {
            initialState: {
                name: 'Category name',
                description: 'Category description',
                callback: function (category) {
                    _this.categories.push(category);
                }
            }
        });
    };
    // COMPONENT COMUNICATION
    CategoryPageComponent.prototype.filterEvent = function (event) {
        this.filter = event;
    };
    CategoryPageComponent.prototype.sideMenuAction = function (event) {
        if (event.action && event.action.action) {
            this[event.action.action](event.action.data);
        }
    };
    CategoryPageComponent.prototype.onScroll = function () { };
    CategoryPageComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-category-page',
            template: __webpack_require__(/*! ./category-page.component.html */ "./src/app/admin/category-page/category-page.component.html"),
            styles: [__webpack_require__(/*! ./category-page.component.scss */ "./src/app/admin/category-page/category-page.component.scss")]
        }),
        __metadata("design:paramtypes", [ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_1__["BsModalService"]])
    ], CategoryPageComponent);
    return CategoryPageComponent;
}());

/* This is a component which we pass in modal*/


/***/ }),

/***/ "./src/app/admin/dashboard-page/dashboard-page.component.html":
/*!********************************************************************!*\
  !*** ./src/app/admin/dashboard-page/dashboard-page.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"o-page\">\r\n  <app-sidebar></app-sidebar>\r\n  <main class=\"o-page__content\">\r\n    <app-navbar></app-navbar>\r\n    <div class=\"container-fluid page-content\">\r\n      <div class=\"row\">\r\n        <div class=\"col-xl-4\">\r\n          <div class=\"c-graph-card\" data-mh=\"graph-cards\">\r\n            <div class=\"c-graph-card__content\">\r\n              <h3 class=\"c-graph-card__title\">Next Payout</h3>\r\n              <p class=\"c-graph-card__date\">Activity from 4 Jan 2017 to 10 Jan 2017</p>\r\n              <h4 class=\"c-graph-card__number\">$2,190</h4>\r\n              <p class=\"c-graph-card__status\">You’ve made $230 Today</p>\r\n            </div>\r\n\r\n            <div class=\"c-graph-card__chart\">\r\n              <canvas id=\"js-chart-payout\" width=\"300\" height=\"74\"></canvas>\r\n            </div>\r\n          </div>\r\n        </div>\r\n\r\n        <div class=\"col-xl-4\">\r\n          <div class=\"c-graph-card\" data-mh=\"graph-cards\">\r\n            <div class=\"c-graph-card__content\">\r\n              <h3 class=\"c-graph-card__title\">Total Earnings</h3>\r\n              <p class=\"c-graph-card__date\">In 15 Months</p>\r\n              <h4 class=\"c-graph-card__number\">$23,580</h4>\r\n              <p class=\"c-graph-card__status\">Last Month you’ve made $2,980</p>\r\n            </div>\r\n\r\n            <div class=\"c-graph-card__chart\">\r\n              <canvas id=\"js-chart-earnings\" width=\"300\" height=\"74\"></canvas>\r\n            </div>\r\n          </div>\r\n        </div>\r\n\r\n        <div class=\"col-xl-4\">\r\n          <div class=\"c-progress-card\" data-mh=\"graph-cards\">\r\n            <h3 class=\"c-progress-card__title\">All Tasks Overview</h3>\r\n            <p class=\"c-progress-card__date\">Next 4 Weeks</p>\r\n\r\n            <div class=\"c-progress-card__item\">\r\n              <div class=\"c-progress-card__label\">Week 3</div>\r\n\r\n              <div class=\"c-progress-card__progress c-progress c-progress--small\">\r\n                <div class=\"c-progress__bar u-bg-success\" style=\"width:100%;\">\r\n                  <div class=\"c-progress__bar u-bg-fancy\" style=\"width: 70%\">\r\n                    <div class=\"c-progress__bar u-bg-info\" style=\"width: 50%\">\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div><!-- // .c-progress-card__item -->\r\n\r\n            <div class=\"c-progress-card__item\">\r\n              <div class=\"c-progress-card__label\">Week 4</div>\r\n\r\n              <div class=\"c-progress-card__progress c-progress c-progress--small\">\r\n                <div class=\"c-progress__bar u-bg-success\" style=\"width:100%;\">\r\n                  <div class=\"c-progress__bar u-bg-fancy\" style=\"width: 70%\">\r\n                    <div class=\"c-progress__bar u-bg-info\" style=\"width: 50%\">\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div><!-- // .c-progress-card__item -->\r\n\r\n            <div class=\"c-progress-card__item\">\r\n              <div class=\"c-progress-card__label\">Week 5</div>\r\n\r\n              <div class=\"c-progress-card__progress c-progress c-progress--small\">\r\n                <div class=\"c-progress__bar u-bg-success\" style=\"width:100%;\">\r\n                  <div class=\"c-progress__bar u-bg-fancy\" style=\"width: 70%\">\r\n                    <div class=\"c-progress__bar u-bg-info\" style=\"width: 50%\">\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div><!-- // .c-progress-card__item -->\r\n\r\n            <div class=\"c-progress-card__item\">\r\n              <div class=\"c-progress-card__label\">Week 6</div>\r\n\r\n              <div class=\"c-progress-card__progress c-progress c-progress--small\">\r\n                <div class=\"c-progress__bar u-bg-success\" style=\"width:100%;\">\r\n                  <div class=\"c-progress__bar u-bg-fancy\" style=\"width: 70%\">\r\n                    <div class=\"c-progress__bar u-bg-info\" style=\"width: 50%\">\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div><!-- // .c-progress-card__item -->\r\n\r\n            <ul class=\"c-progress-card__legends\">\r\n              <li class=\"u-text-mute u-text-xsmall\">\r\n                <i class=\"c-progress-card__legend u-bg-fancy\"></i>Progress\r\n              </li>\r\n              <li class=\"u-text-mute u-text-xsmall\">\r\n                <i class=\"c-progress-card__legend u-bg-info\"></i>Due\r\n              </li>\r\n              <li class=\"u-text-mute u-text-xsmall\">\r\n                <i class=\"c-progress-card__legend u-bg-success\"></i>OA\r\n              </li>\r\n              <li class=\"u-text-mute u-text-xsmall\">\r\n                <i class=\"c-progress-card__legend u-bg-danger\"></i>Delegated\r\n              </li>\r\n            </ul>\r\n          </div>\r\n        </div>\r\n      </div>\r\n\r\n      <span class=\"c-divider has-text u-mb-medium\">Active Projects</span>\r\n\r\n      <div class=\"row\">\r\n        <div class=\"col-sm-6 col-lg-6 col-xl-3\">\r\n\r\n          <div class=\"c-project-card u-mb-medium\">\r\n            <img class=\"c-project-card__img\" src=\"assets/img/project-card1.jpg\" alt=\"About the image\">\r\n\r\n            <div class=\"c-project-card__content\">\r\n              <div class=\"c-project-card__head\">\r\n                <h4 class=\"c-project-card__title\">Magazine Images</h4>\r\n                <p class=\"c-project-card__info\">Kinfolk  |  Last Update: 21 Dec 2016</p>\r\n              </div>\r\n\r\n\r\n              <div class=\"c-project-card__meta\">\r\n                <p>4,870 USD\r\n                  <small class=\"u-block u-text-mute\">Budget / Salary</small>\r\n                </p>\r\n                <p>Early Dec 2017\r\n                  <small class=\"u-block u-text-danger\">10 Days Remaining</small>\r\n                </p>\r\n              </div>\r\n            </div>\r\n          </div>\r\n\r\n        </div>\r\n\r\n        <div class=\"col-sm-6 col-lg-6 col-xl-3\">\r\n\r\n          <div class=\"c-project-card u-mb-medium\">\r\n            <img class=\"c-project-card__img\" src=\"assets/img/project-card2.jpg\" alt=\"About the image\">\r\n\r\n            <div class=\"c-project-card__content\">\r\n              <div class=\"c-project-card__head\">\r\n                <h4 class=\"c-project-card__title\">Design Competition 2018</h4>\r\n                <p class=\"c-project-card__info\">Gourmet| Last Update: 29 Dec 2017</p>\r\n              </div>\r\n\r\n\r\n              <div class=\"c-project-card__meta\">\r\n                <p>9,680 USD\r\n                  <small class=\"u-block u-text-mute\">Budget / Salary</small>\r\n                </p>\r\n                <p>Late January\r\n                  <small class=\"u-block u-text-mute\">Deadline</small>\r\n                </p>\r\n              </div>\r\n            </div>\r\n          </div>\r\n\r\n        </div>\r\n\r\n        <div class=\"col-sm-6 col-lg-6 col-xl-3\">\r\n\r\n          <div class=\"c-project-card u-mb-medium\">\r\n            <img class=\"c-project-card__img\" src=\"assets/img/project-card3.jpg\" alt=\"About the image\">\r\n\r\n            <div class=\"c-project-card__content\">\r\n              <div class=\"c-project-card__head\">\r\n                <h4 class=\"c-project-card__title\">New Dashboard</h4>\r\n                <p class=\"c-project-card__info\">Tapdaq  |  Last Update: 28 Nov 2017</p>\r\n              </div>\r\n\r\n\r\n              <div class=\"c-project-card__meta\">\r\n                <p>4,870 USD\r\n                  <small class=\"u-block u-text-mute\">Budget / Salary</small>\r\n                </p>\r\n                <p>Early Dec 2017\r\n                  <small class=\"u-block u-text-danger\">15 Days Remaining</small>\r\n                </p>\r\n              </div>\r\n            </div>\r\n          </div>\r\n\r\n        </div>\r\n\r\n        <div class=\"col-sm-6 col-lg-6 col-xl-3\">\r\n\r\n          <div class=\"c-project-card u-mb-medium\">\r\n            <img class=\"c-project-card__img\" src=\"assets/img/project-card4.jpg\" alt=\"About the image\">\r\n\r\n            <div class=\"c-project-card__content\">\r\n              <div class=\"c-project-card__head\">\r\n                <h4 class=\"c-project-card__title\">Mobile App</h4>\r\n                <p class=\"c-project-card__info\">Cofee & Co.  |  Last Update: 1 Dec 2016</p>\r\n              </div>\r\n\r\n\r\n              <div class=\"c-project-card__meta\">\r\n                <p>8,760 USD\r\n                  <small class=\"u-block u-text-mute\">Budget / Salary</small>\r\n                </p>\r\n                <p>Early Dec 2017\r\n                  <small class=\"u-block u-text-mute\">6 Days Remaining</small>\r\n                </p>\r\n              </div>\r\n            </div>\r\n          </div>\r\n\r\n        </div>\r\n      </div><!-- // .row -->\r\n\r\n      <div class=\"row\">\r\n        <div class=\"col-md-6\">\r\n          <div class=\"c-card c-card--responsive u-mb-medium\">\r\n            <div class=\"c-card__header c-card__header--transparent o-line\">\r\n              <h5 class=\"c-card__title\">Invoicing</h5>\r\n              <a class=\"c-card__meta\" >View all invoices</a>\r\n            </div>\r\n\r\n            <table class=\"c-table u-border-zero\">\r\n              <tbody>\r\n              <tr class=\"c-table__row u-border-top-zero\">\r\n                <td class=\"c-table__cell u-text-mute\">00450</td>\r\n                <td class=\"c-table__cell\">Design Works</td>\r\n                <td class=\"c-table__cell u-text-mute\">Carlson Limited</td>\r\n                <td class=\"c-table__cell u-text-right\">\r\n                  <span class=\"c-badge c-badge--small c-badge--danger\">Delayed</span>\r\n                </td>\r\n                <td class=\"c-table__cell\">$2,580</td>\r\n              </tr>\r\n\r\n              <tr class=\"c-table__row\">\r\n                <td class=\"c-table__cell u-text-mute\">00569</td>\r\n                <td class=\"c-table__cell\">New Illustrations</td>\r\n                <td class=\"c-table__cell u-text-mute\">Twitter</td>\r\n                <td class=\"c-table__cell\">\r\n                  <span class=\"c-badge c-badge--small c-badge--warning\">Pending Invoice</span>\r\n                </td>\r\n                <td class=\"c-table__cell\">$2,580</td>\r\n              </tr>\r\n\r\n              <tr class=\"c-table__row\">\r\n                <td class=\"c-table__cell u-text-mute\">01875</td>\r\n                <td class=\"c-table__cell\">UX Study</td>\r\n                <td class=\"c-table__cell u-text-mute\">Re-Research</td>\r\n                <td class=\"c-table__cell u-text-right\">\r\n                  <span class=\"c-badge c-badge--small c-badge--success\">Paid Today</span>\r\n                </td>\r\n                <td class=\"c-table__cell\">$2,580</td>\r\n              </tr>\r\n\r\n              <tr class=\"c-table__row\">\r\n                <td class=\"c-table__cell u-text-mute\">00369</td>\r\n                <td class=\"c-table__cell\">Landing Page</td>\r\n                <td class=\"c-table__cell u-text-mute\">Travelsimo</td>\r\n                <td class=\"c-table__cell u-text-right\">\r\n                  <span class=\"c-badge c-badge--small c-badge--secondary\">Paid Today</span>\r\n                </td>\r\n                <td class=\"c-table__cell\">$2,580</td>\r\n              </tr>\r\n\r\n              <tr class=\"c-table__row\">\r\n                <td class=\"c-table__cell u-text-mute\">00689</td>\r\n                <td class=\"c-table__cell\">iOS App Design</td>\r\n                <td class=\"c-table__cell u-text-mute\">Silingo</td>\r\n                <td class=\"c-table__cell u-text-right\">\r\n                  <span class=\"c-badge c-badge--small c-badge--secondary\">Paid Today</span>\r\n                </td>\r\n                <td class=\"c-table__cell\">$2,580</td>\r\n              </tr>\r\n              </tbody>\r\n            </table>\r\n          </div>\r\n        </div>\r\n\r\n        <div class=\"col-md-6\">\r\n          <div class=\"c-card u-mb-medium\">\r\n            <div class=\"c-card__header c-card__header--transparent o-line\">\r\n              <h5 class=\"c-card__title\">Tasks</h5>\r\n              <a class=\"c-card__meta\" >View all all tasks</a>\r\n            </div>\r\n\r\n            <div class=\"c-todo u-border-top-zero\">\r\n              <input class=\"c-todo__input\" id=\"todo1\" type=\"checkbox\" name=\"exampe-list\">\r\n              <label class=\"c-todo__label\" for=\"todo1\">\r\n                Create new prototype for the landing page\r\n              </label>\r\n\r\n              <span class=\"c-badge c-badge--danger c-badge--small\">Due Today</span>\r\n            </div><!-- // .c-todo -->\r\n\r\n            <div class=\"c-todo\">\r\n              <input class=\"c-todo__input\" id=\"todo2\" type=\"checkbox\" name=\"exampe-list\">\r\n              <label class=\"c-todo__label\" for=\"todo2\">\r\n                Add new Google Analytics code to all main files\r\n              </label>\r\n\r\n              <span class=\"c-badge c-badge--secondary c-badge--small\">Due in 2 Days</span>\r\n            </div><!-- // .c-todo -->\r\n\r\n            <div class=\"c-todo\">\r\n              <input class=\"c-todo__input\" id=\"todo3\" type=\"checkbox\" name=\"exampe-list\">\r\n              <label class=\"c-todo__label\" for=\"todo3\">\r\n                Finish Dashboard UI Kit update\r\n              </label>\r\n\r\n              <span class=\"c-badge c-badge--secondary c-badge--small\">Due in 3 Days</span>\r\n            </div><!-- // .c-todo -->\r\n\r\n            <div class=\"c-todo is-completed\">\r\n              <input class=\"c-todo__input\" id=\"todo4\" type=\"checkbox\" name=\"exampe-list\" checked>\r\n              <label class=\"c-todo__label\" for=\"todo4\">\r\n                Update parallax scroll on team page\r\n              </label>\r\n\r\n              <span class=\"c-badge c-badge--secondary c-badge--small\">Due in 5 Days</span>\r\n            </div><!-- // .c-todo -->\r\n\r\n            <div class=\"c-todo is-completed\">\r\n              <input class=\"c-todo__input\" id=\"todo5\" type=\"checkbox\" name=\"exampe-list\" checked>\r\n              <label class=\"c-todo__label\" for=\"todo5\">\r\n                Update parallax scroll on team page\r\n              </label>\r\n\r\n              <span class=\"c-badge c-badge--secondary c-badge--small\">Due in 1 Week</span>\r\n            </div><!-- // .c-todo -->\r\n          </div>\r\n        </div>\r\n      </div><!-- // .row -->\r\n\r\n    </div><!-- // .container -->\r\n  </main>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/admin/dashboard-page/dashboard-page.component.scss":
/*!********************************************************************!*\
  !*** ./src/app/admin/dashboard-page/dashboard-page.component.scss ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/admin/dashboard-page/dashboard-page.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/admin/dashboard-page/dashboard-page.component.ts ***!
  \******************************************************************/
/*! exports provided: DashboardPageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardPageComponent", function() { return DashboardPageComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var DashboardPageComponent = /** @class */ (function () {
    function DashboardPageComponent() {
    }
    DashboardPageComponent.prototype.ngOnInit = function () {
    };
    DashboardPageComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-dashboard-page',
            template: __webpack_require__(/*! ./dashboard-page.component.html */ "./src/app/admin/dashboard-page/dashboard-page.component.html"),
            styles: [__webpack_require__(/*! ./dashboard-page.component.scss */ "./src/app/admin/dashboard-page/dashboard-page.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], DashboardPageComponent);
    return DashboardPageComponent;
}());



/***/ }),

/***/ "./src/app/admin/event-page/event-page.component.html":
/*!************************************************************!*\
  !*** ./src/app/admin/event-page/event-page.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"o-page\">\r\n  <app-sidebar></app-sidebar>\r\n  <main class=\"o-page__content\">\r\n\r\n    <app-navbar [title]=\"event.name\"></app-navbar>\r\n    <app-status-bar [data]=\"event\" (dataChange)=\"event = this.$event\"></app-status-bar>\r\n    <div class=\"container-fluid page-content\" >\r\n\r\n      <div class=\"row\">\r\n        <div class=\"col-xl-8\">\r\n          <app-event-info-management-card [data]=\"event\" (dataChange)=\"this.event = $event\"></app-event-info-management-card>\r\n\r\n          <app-event-tickets-card [data]=\"event\" (dataChange)=\"this.event = $event\"></app-event-tickets-card>\r\n\r\n          <!--<app-event-statistics-card></app-event-statistics-card>-->\r\n        </div>\r\n\r\n        <div class=\"col-xl-4\">\r\n          <app-sidebar-members [data]=\"event\" (dataChange)=\"this.event = $event\"></app-sidebar-members>\r\n\r\n          <app-sidebar-bars [data]=\"event\" (dataChange)=\"this.event = $event\"></app-sidebar-bars>\r\n\r\n          <app-sidebar-details [data]=\"event\" (dataChange)=\"this.event = $event\"></app-sidebar-details>\r\n        </div>\r\n      </div>\r\n    </div><!-- // .container -->\r\n  </main>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/admin/event-page/event-page.component.scss":
/*!************************************************************!*\
  !*** ./src/app/admin/event-page/event-page.component.scss ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/admin/event-page/event-page.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/admin/event-page/event-page.component.ts ***!
  \**********************************************************/
/*! exports provided: EventPageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EventPageComponent", function() { return EventPageComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var EventPageComponent = /** @class */ (function () {
    function EventPageComponent() {
        this.text = 'eh';
        this.event = {
            staff: []
        };
    }
    EventPageComponent.prototype.ngOnInit = function () {
        this.load();
    };
    EventPageComponent.prototype.load = function () {
        // this.event = new Event();
    };
    EventPageComponent.prototype.sampleClick = function (evt) {
        console.log('SAMPLECLICK', evt, this.name);
    };
    EventPageComponent.prototype.logEvent = function () {
        console.log(this.event, this.event.name);
    };
    EventPageComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-event-page',
            template: __webpack_require__(/*! ./event-page.component.html */ "./src/app/admin/event-page/event-page.component.html"),
            styles: [__webpack_require__(/*! ./event-page.component.scss */ "./src/app/admin/event-page/event-page.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], EventPageComponent);
    return EventPageComponent;
}());



/***/ }),

/***/ "./src/app/admin/events-page/events-page.component.html":
/*!**************************************************************!*\
  !*** ./src/app/admin/events-page/events-page.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"o-page\">\r\n  <app-sidebar></app-sidebar>\r\n  <main class=\"o-page__content\">\r\n    <app-navbar></app-navbar>\r\n    <div class=\"container page-content\">\r\n      <div class=\"row\">\r\n        <div class=\"col-xl-3\">\r\n          <app-management-dashboard-side-menu [menuConfig]=\"menuConfig\" (action)=\"sideMenuAction($event)\"></app-management-dashboard-side-menu>\r\n        </div>\r\n\r\n        <div class=\"col-sm-12 col-xl-9\">\r\n          <main>\r\n            <div class=\"row\">\r\n              <div class=\"col-sm-12\" infiniteScroll\r\n                   [infiniteScrollDistance]=\"2\"\r\n                   [infiniteScrollThrottle]=\"300\"\r\n                   (scrolled)=\"onScroll()\">\r\n                <div class=\"c-table-responsive@tablet\">\r\n                  <table class=\"c-table u-mb-large\">\r\n                    <caption class=\"c-table__title\">\r\n                      {{'ALL_EVENTS' | translate}} <small>{{events.length}} {{'EVENTS' | translate}}</small>\r\n                      <app-filter-input class=\"text-right\" (filterEvent)=\"filterEvent($event)\" ></app-filter-input>\r\n                    </caption>\r\n                    <thead class=\"c-table__head c-table__head--slim\">\r\n                    <tr>\r\n                      <th class=\"c-table__cell c-table__cell--head\">{{'NAME' |translate}}</th>\r\n                      <th class=\"c-table__cell c-table__cell--head\">{{'DATE' |translate}}</th>\r\n                      <th class=\"c-table__cell c-table__cell--head\">{{'PRICE' |translate}}</th>\r\n                      <th class=\"c-table__cell c-table__cell--head\">\r\n                        <span class=\"u-hidden-visually\">{{'ACTIONS' |translate}}</span>\r\n                      </th>\r\n                    </tr>\r\n                    </thead>\r\n\r\n                    <tbody>\r\n                    <tr class=\"c-table__row\" *ngFor=\"let event of events | filter: {name: filter}\" app-event-list-item  [data]=\"event\">\r\n\r\n                    </tr>\r\n\r\n                    </tbody>\r\n                  </table>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </main>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </main>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/admin/events-page/events-page.component.scss":
/*!**************************************************************!*\
  !*** ./src/app/admin/events-page/events-page.component.scss ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/admin/events-page/events-page.component.ts":
/*!************************************************************!*\
  !*** ./src/app/admin/events-page/events-page.component.ts ***!
  \************************************************************/
/*! exports provided: EventsPageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EventsPageComponent", function() { return EventsPageComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _models_event__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../models/event */ "./src/app/models/event.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var EventsPageComponent = /** @class */ (function () {
    function EventsPageComponent(router) {
        this.router = router;
        this.menuConfig = [{
                name: 'MENU',
                items: [{
                        name: 'ADD_NEW_EVENT',
                        action: { action: 'createEvent' },
                        icon: 'plus'
                    }]
            }];
        this.events = [];
    }
    EventsPageComponent.prototype.ngOnInit = function () {
        this.load();
    };
    EventsPageComponent.prototype.load = function () {
        this.events = Array(10).fill(new _models_event__WEBPACK_IMPORTED_MODULE_1__["Event"]());
    };
    EventsPageComponent.prototype.createEvent = function () {
        this.events.push(new _models_event__WEBPACK_IMPORTED_MODULE_1__["Event"]());
        this.openEvent(1);
    };
    EventsPageComponent.prototype.openEvent = function (id) {
        this.router.navigate(["event-management/event/" + id]);
    };
    // COMPONENT COMUNICATION
    EventsPageComponent.prototype.filterEvent = function (event) {
        this.filter = event;
    };
    EventsPageComponent.prototype.sideMenuAction = function (event) {
        if (event.action && event.action.action) {
            this[event.action.action](event.action.data);
        }
    };
    EventsPageComponent.prototype.onScroll = function () { };
    EventsPageComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-events-page',
            template: __webpack_require__(/*! ./events-page.component.html */ "./src/app/admin/events-page/events-page.component.html"),
            styles: [__webpack_require__(/*! ./events-page.component.scss */ "./src/app/admin/events-page/events-page.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], EventsPageComponent);
    return EventsPageComponent;
}());



/***/ }),

/***/ "./src/app/admin/items-page/items-page.component.html":
/*!************************************************************!*\
  !*** ./src/app/admin/items-page/items-page.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"o-page\">\r\n  <app-sidebar></app-sidebar>\r\n  <main class=\"o-page__content\">\r\n    <app-navbar></app-navbar>\r\n    <div class=\"container page-content\">\r\n      <div class=\"row\">\r\n        <div class=\"col-xl-3\">\r\n          <app-management-dashboard-side-menu [menuConfig]=\"menuConfig\" (action)=\"sideMenuAction($event)\"></app-management-dashboard-side-menu>\r\n        </div>\r\n\r\n        <div class=\"col-sm-12 col-xl-9\">\r\n          <main>\r\n            <div class=\"row\">\r\n              <!--<button (click)=\"push()\" >XXX</button>-->\r\n              <div class=\"col-sm-12\" infiniteScroll\r\n                   [infiniteScrollDistance]=\"2\"\r\n                   [infiniteScrollThrottle]=\"300\"\r\n                   (scrolled)=\"onScroll()\">\r\n                <div class=\"c-table-responsive@tablet\">\r\n                  <table class=\"c-table u-mb-large\">\r\n                    <caption class=\"c-table__title\">\r\n                      {{'ALL_ITEMS' | translate}} <small>{{items.length}} {{'ITEMS' | translate}}</small>\r\n                      <app-filter-input class=\"text-right\" (filterEvent)=\"filterEvent($event)\" ></app-filter-input>\r\n                    </caption>\r\n                    <thead class=\"c-table__head c-table__head--slim\">\r\n                    <tr>\r\n                      <th class=\"c-table__cell c-table__cell--head\">{{'ID' |translate}}</th>\r\n                      <th class=\"c-table__cell c-table__cell--head\">{{'NAME' |translate}}</th>\r\n                      <th class=\"c-table__cell c-table__cell--head\">{{'DESCRIPTION' |translate}}</th>\r\n                      <th class=\"c-table__cell c-table__cell--head\">{{'TYPE' |translate}}</th>\r\n                      <th class=\"c-table__cell c-table__cell--head\">{{'BRAND' |translate}}</th>\r\n                    </tr>\r\n                    </thead>\r\n\r\n                    <tbody>\r\n                    <tr class=\"c-table__row\" *ngFor=\"let item of items | filter: {name: filter}\" app-item-list-item >\r\n\r\n                    </tr>\r\n\r\n                    </tbody>\r\n                  </table>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </main>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </main>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/admin/items-page/items-page.component.scss":
/*!************************************************************!*\
  !*** ./src/app/admin/items-page/items-page.component.scss ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/admin/items-page/items-page.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/admin/items-page/items-page.component.ts ***!
  \**********************************************************/
/*! exports provided: ItemsPageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ItemsPageComponent", function() { return ItemsPageComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm5/ngx-bootstrap-modal.js");
/* harmony import */ var _components_modals_modal_item_modal_item_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../components/modals/modal-item/modal-item.component */ "./src/app/components/modals/modal-item/modal-item.component.ts");
/* harmony import */ var _models_item__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../models/item */ "./src/app/models/item.ts");
/* harmony import */ var _models_item_type__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../models/item-type */ "./src/app/models/item-type.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ItemsPageComponent = /** @class */ (function () {
    function ItemsPageComponent(modalService) {
        this.modalService = modalService;
        this.menuConfig = [{
                name: 'MENU',
                items: [{
                        name: 'ADD_NEW_ITEM',
                        action: { action: 'create' },
                        icon: 'plus'
                    }]
            }, {
                name: 'FILTER_BY_TYPE',
                items: [{
                        name: 'ALL_TYPES',
                        action: { action: 'filterByType' }
                    }]
            }];
        this.items = [];
        this.itemTypes = [];
        this.originalItems = [];
        this.load();
        this.loadRelated();
    }
    ItemsPageComponent.prototype.ngOnInit = function () {
    };
    ItemsPageComponent.prototype.loadRelated = function () {
        var itemTypes = new Array(10).fill(new _models_item_type__WEBPACK_IMPORTED_MODULE_4__["ItemType"]());
        for (var i = 0; i < itemTypes.length; i++) {
            this.itemTypes.push(itemTypes[i]);
            this.menuConfig[1].items.push({
                name: itemTypes[i].type,
                action: {
                    action: 'filterByType',
                    data: this.itemTypes[i]
                }
            });
        }
    };
    ItemsPageComponent.prototype.load = function () {
        var items = new Array(10).fill(new _models_item__WEBPACK_IMPORTED_MODULE_3__["Item"]());
        for (var i = 0; i < items.length; i++) {
            this.items.push(items[i]);
        }
        this.originalItems = this.items;
    };
    ItemsPageComponent.prototype.create = function () {
        var _this = this;
        this.modalRef = this.modalService.show(_components_modals_modal_item_modal_item_component__WEBPACK_IMPORTED_MODULE_2__["ModalItemComponent"], {
            initialState: {
                name: 'Item name',
                description: 'Item description',
                type: this.itemTypes[0],
                brand: '',
                itemTypes: this.itemTypes,
                callback: function (item) {
                    try {
                        if (item.type.id === _this.items[0].type.id) {
                            _this.items.push(item);
                        }
                    }
                    catch (ex) {
                        // ..
                    }
                    _this.originalItems.push(item);
                }
            }
        });
    };
    ItemsPageComponent.prototype.filterByType = function (itemType) {
        if (!itemType) {
            this.items = this.originalItems;
        }
        else {
            this.items = this.originalItems.filter(function (el) { return el.type.id === itemType.id; });
        }
    };
    // COMPONENT COMUNICATION
    ItemsPageComponent.prototype.filterEvent = function (event) {
        this.filter = event;
    };
    ItemsPageComponent.prototype.sideMenuAction = function (event) {
        if (event.action && event.action.action) {
            this[event.action.action](event.action.data);
        }
    };
    ItemsPageComponent.prototype.onScroll = function () { };
    ItemsPageComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-items-page',
            template: __webpack_require__(/*! ./items-page.component.html */ "./src/app/admin/items-page/items-page.component.html"),
            styles: [__webpack_require__(/*! ./items-page.component.scss */ "./src/app/admin/items-page/items-page.component.scss")]
        }),
        __metadata("design:paramtypes", [ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_1__["BsModalService"]])
    ], ItemsPageComponent);
    return ItemsPageComponent;
}());



/***/ }),

/***/ "./src/app/admin/menu-page/menu-page.component.html":
/*!**********************************************************!*\
  !*** ./src/app/admin/menu-page/menu-page.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"o-page\">\r\n  <app-sidebar></app-sidebar>\r\n  <main class=\"o-page__content\">\r\n    <app-navbar></app-navbar>\r\n    <div class=\"container page-content\">\r\n      <div class=\"row\">\r\n        <div class=\"col-xl-3\">\r\n          <aside class=\"c-menu u-ml-medium u-hidden-down@wide\">\r\n            <h4 class=\"c-menu__title\">Menu</h4>\r\n            <ul class=\"u-mb-medium\">\r\n              <li class=\"c-menu__item\">\r\n                <a class=\"c-menu__link is-active\" >\r\n                  <i class=\"fa fa-trophy u-mr-xsmall\"></i>Featured Events\r\n                </a>\r\n              </li>\r\n              <li class=\"c-menu__item\">\r\n                <a class=\"c-menu__link\" >\r\n                  <i class=\"c-menu__icon fa fa-heart-o\"></i>Recommended\r\n                </a>\r\n              </li>\r\n              <li class=\"c-menu__item\">\r\n                <a class=\"c-menu__link\" >\r\n                  <i class=\"c-menu__icon fa fa-bullhorn\"></i>Live Streams\r\n                </a>\r\n              </li>\r\n              <li class=\"c-menu__item\">\r\n                <a class=\"c-menu__link\" >\r\n                  <i class=\"c-menu__icon fa fa-newspaper-o\"></i>Press\r\n                </a>\r\n              </li>\r\n              <li class=\"c-menu__item\">\r\n                <a class=\"c-menu__link\" >\r\n                  <i class=\"c-menu__icon fa fa-diamond\"></i>Favourites\r\n                </a>\r\n              </li>\r\n              <li class=\"c-menu__item\">\r\n                <a class=\"c-menu__link\" >\r\n                  <i class=\"c-menu__icon fa fa-map-o\"></i>Places\r\n                </a>\r\n              </li>\r\n            </ul>\r\n\r\n            <h4 class=\"c-menu__title\">Your Events</h4>\r\n            <ul>\r\n              <li class=\"c-menu__item\">\r\n                <a class=\"c-menu__link\" >\r\n                  <img src=\"assets/imgsidebar-icon1.png\" class=\"u-mr-xsmall\" style=\"width: 14px;\" alt=\"Icon 1\">Classes\r\n                </a>\r\n              </li>\r\n              <li class=\"c-menu__item\">\r\n                <a class=\"c-menu__link\" >\r\n                  <img src=\"assets/imgsidebar-icon2.png\" class=\"u-mr-xsmall\" style=\"width: 14px;\" alt=\"Icon 2\">People\r\n                </a>\r\n              </li>\r\n              <li class=\"c-menu__item\">\r\n                <a class=\"c-menu__link\" >\r\n                  <img src=\"assets/imgsidebar-icon3.png\" class=\"u-mr-xsmall\" style=\"width: 14px;\" alt=\"Icon 3\">Networking\r\n                </a>\r\n              </li>\r\n              <li class=\"c-menu__item\">\r\n                <a class=\"c-menu__link\" >\r\n                  <img src=\"assets/imgsidebar-icon4.png\" class=\"u-mr-xsmall\" style=\"width: 14px;\" alt=\"Icon 4\">Hi-Skill\r\n                </a>\r\n              </li>\r\n              <li class=\"c-menu__item\">\r\n                <a class=\"c-menu__link\" >\r\n                  <img src=\"assets/imgsidebar-icon5.png\" class=\"u-mr-xsmall\" style=\"width: 14px;\" alt=\"Add icon\">Going to Buy\r\n                </a>\r\n              </li>\r\n              <li class=\"c-menu__item\">\r\n                <a class=\"c-menu__link\" >\r\n                  <img src=\"assets/imgsidebar-icon6.png\" class=\"u-mr-xsmall\" style=\"width: 14px;\" alt=\"Add icon\">Add New List\r\n                </a>\r\n              </li>\r\n            </ul>\r\n          </aside>\r\n        </div>\r\n\r\n        <div class=\"col-sm-12 col-xl-9\">\r\n          <main>\r\n            <div class=\"row\">\r\n              <div class=\"col-8 col-xl-10\">\r\n                <h1 class=\"u-h3\">Featured Events</h1>\r\n              </div>\r\n              <div class=\"col-4 col-xl-2\">\r\n                <div class=\"c-field u-mb-small\">\r\n                  <label class=\"c-field__label u-hidden-visually\" for=\"select-recently\">Recently Opened</label>\r\n\r\n                  <!-- Select2 jquery plugin is used -->\r\n                  <select class=\"c-select\" name=\"recently-opened\" id=\"select-recently\">\r\n                    <option value=\"value1\">All Types</option>\r\n                    <option value=\"value2\">This day</option>\r\n                    <option value=\"value3\">Last Week</option>\r\n                  </select>\r\n                </div>\r\n              </div>\r\n            </div><!-- // .row -->\r\n            <div class=\"row\">\r\n              <div class=\"col-md-6\">\r\n                <article class=\"c-event\">\r\n                  <div class=\"c-event__img\">\r\n                    <img src=\"assets/imgevent1.jpg\" alt=\"San Francisco's Buildings\">\r\n                    <span class=\"c-event__status\">Featured</span>\r\n                  </div>\r\n                  <div class=\"c-event__meta\">\r\n                    <h3 class=\"c-event__title\">Designing in US\r\n                      <span class=\"c-event__place\">United States, San Francisco</span>\r\n                    </h3>\r\n\r\n                    <a  class=\"c-btn c-btn--success c-event__btn\">Buy for $560</a>\r\n                  </div>\r\n                </article><!-- // .c-event -->\r\n              </div>\r\n\r\n              <div class=\"col-md-6\">\r\n                <article class=\"c-event\">\r\n                  <div class=\"c-event__img\">\r\n                    <img src=\"assets/imgevent2.jpg\" alt=\"San Francisco's Buildings\">\r\n\r\n                    <span class=\"c-event__status\">Featured</span>\r\n                  </div>\r\n                  <div class=\"c-event__meta\">\r\n\r\n                    <h3 class=\"c-event__title\">Photography Class\r\n                      <span class=\"c-event__place\">United States, San Francisco</span>\r\n                    </h3>\r\n\r\n                    <a  class=\"c-btn c-btn--success c-event__btn\">Buy for $560</a>\r\n                  </div>\r\n                </article><!-- // .c-event -->\r\n              </div>\r\n            </div><!-- // .row -->\r\n\r\n            <div class=\"row\">\r\n              <div class=\"col-sm-12\">\r\n                <div class=\"c-table-responsive@tablet\">\r\n                  <table class=\"c-table u-mb-large\">\r\n                    <caption class=\"c-table__title\">\r\n                      All Events <small>32 Events</small>\r\n                    </caption>\r\n                    <thead class=\"c-table__head c-table__head--slim\">\r\n                    <tr>\r\n                      <th class=\"c-table__cell c-table__cell--head\">Conference</th>\r\n                      <th class=\"c-table__cell c-table__cell--head\">Date</th>\r\n                      <th class=\"c-table__cell c-table__cell--head\">Price</th>\r\n                      <th class=\"c-table__cell c-table__cell--head\">\r\n                        <span class=\"u-hidden-visually\">Actions</span>\r\n                      </th>\r\n                    </tr>\r\n                    </thead>\r\n\r\n                    <tbody>\r\n                    <tr class=\"c-table__row\">\r\n                      <td class=\"c-table__cell\">O’Reilly Design Conference\r\n                        <span class=\"u-block u-text-mute u-text-xsmall\">\r\n                                                        United States, San Francisco\r\n                                                    </span>\r\n                      </td>\r\n\r\n                      <td class=\"c-table__cell\">24th - 28th August 2018</td>\r\n                      <td class=\"c-table__cell\">$290\r\n                        <span class=\"u-block u-text-xsmall u-text-mute\">\r\n                                                        32 Tickets remaining\r\n                                                    </span>\r\n                      </td>\r\n                      <td class=\"c-table__cell u-text-right\">\r\n                        <a class=\"c-btn c-btn--success\" >Buy Tickets</a>\r\n                      </td>\r\n                    </tr>\r\n\r\n                    <tr class=\"c-table__row\">\r\n                      <td class=\"c-table__cell\">UX Night at General Assembly\r\n                        <span class=\"u-block u-text-mute u-text-xsmall\">\r\n                                                        United States, Boston\r\n                                                    </span>\r\n                      </td>\r\n                      <td class=\"c-table__cell\">6th - 9th July 2018</td>\r\n                      <td class=\"c-table__cell\">$290\r\n                        <span class=\"u-block u-text-xsmall u-text-mute\">\r\n                                                        22 Tickets remaining\r\n                                                    </span>\r\n                      </td>\r\n                      <td class=\"c-table__cell u-text-right\">\r\n                        <a class=\"c-btn c-btn--success\" >Buy Tickets</a>\r\n                      </td>\r\n                    </tr>\r\n\r\n                    <tr class=\"c-table__row\">\r\n                      <td class=\"c-table__cell\">TypeCon2016\r\n                        <span class=\"u-block u-text-mute u-text-xsmall\">\r\n                                                        France, Paris\r\n                                                    </span>\r\n                      </td>\r\n                      <td class=\"c-table__cell\">24th June 2018</td>\r\n                      <td class=\"c-table__cell\">$290\r\n                        <span class=\"u-block u-text-xsmall u-text-mute\">\r\n                                                        904 Tickets remaining\r\n                                                    </span>\r\n                      </td>\r\n                      <td class=\"c-table__cell u-text-right\">\r\n                        <a class=\"c-btn c-btn--success\" >Buy Tickets</a>\r\n                      </td>\r\n                    </tr>\r\n\r\n                    <tr class=\"c-table__row\">\r\n                      <td class=\"c-table__cell\">SmashingConf San Francisco 2018\r\n                        <span class=\"u-block u-text-mute u-text-xsmall\">\r\n                                                        United States, San Francisco\r\n                                                    </span>\r\n                      </td>\r\n                      <td class=\"c-table__cell\">31st May - 3rd June 2018</td>\r\n                      <td class=\"c-table__cell\">$290\r\n                        <span class=\"u-block u-text-xsmall u-text-mute\">\r\n                                                        904 Tickets remaining\r\n                                                    </span>\r\n                      </td>\r\n                      <td class=\"c-table__cell u-text-right\">\r\n                        <a class=\"c-btn c-btn--success\" >Buy Tickets</a>\r\n                      </td>\r\n                    </tr>\r\n\r\n                    <tr class=\"c-table__row\">\r\n                      <td class=\"c-table__cell\">Now What? Conference 2018\r\n                        <span class=\"u-block u-text-mute u-text-xsmall\">\r\n                                                        United States, Sioux Falls\r\n                                                    </span>\r\n                      </td>\r\n                      <td class=\"c-table__cell\">4th - 8th May 2018</td>\r\n                      <td class=\"c-table__cell\">\r\n                        <del class=\"u-text-danger\">Sold Out</del>\r\n                      </td>\r\n                      <td class=\"c-table__cell u-text-right\">\r\n                        <a class=\"c-btn is-disabled\" >Buy Tickets</a>\r\n                      </td>\r\n                    </tr>\r\n\r\n                    <tr class=\"c-table__row\">\r\n                      <td class=\"c-table__cell\">Generate Moscow 2018\r\n                        <span class=\"u-block u-text-mute u-text-xsmall\">\r\n                                                        Russia, Moscow\r\n                                                    </span>\r\n                      </td>\r\n                      <td class=\"c-table__cell\">13th - 14th April 2018</td>\r\n                      <td class=\"c-table__cell\">$390\r\n                        <span class=\"u-block u-text-xsmall u-text-danger\">\r\n                                                        2 Tickets remaining\r\n                                                    </span>\r\n                      </td>\r\n                      <td class=\"c-table__cell u-text-right\">\r\n                        <a class=\"c-btn c-btn--success\" >Buy Tickets</a>\r\n                      </td>\r\n                    </tr>\r\n\r\n                    <tr class=\"c-table__row\">\r\n                      <td class=\"c-table__cell\">REVOLVE Conference\r\n                        <span class=\"u-block u-text-mute u-text-xsmall\">\r\n                                                        United States, Charleston\r\n                                                    </span>\r\n                      </td>\r\n                      <td class=\"c-table__cell\">24th - 28th August 2016</td>\r\n                      <td class=\"c-table__cell\">$2,100\r\n                        <span class=\"u-block u-text-xsmall u-text-mute\">\r\n                                                        89 Tickets remaining\r\n                                                    </span>\r\n                      </td>\r\n                      <td class=\"c-table__cell u-text-right\">\r\n                        <a class=\"c-btn c-btn--success\" >Buy Tickets</a>\r\n                      </td>\r\n                    </tr>\r\n                    </tbody>\r\n                  </table>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </main>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </main>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/admin/menu-page/menu-page.component.scss":
/*!**********************************************************!*\
  !*** ./src/app/admin/menu-page/menu-page.component.scss ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/admin/menu-page/menu-page.component.ts":
/*!********************************************************!*\
  !*** ./src/app/admin/menu-page/menu-page.component.ts ***!
  \********************************************************/
/*! exports provided: MenuPageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MenuPageComponent", function() { return MenuPageComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var MenuPageComponent = /** @class */ (function () {
    function MenuPageComponent() {
    }
    MenuPageComponent.prototype.ngOnInit = function () {
    };
    MenuPageComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-menu-page',
            template: __webpack_require__(/*! ./menu-page.component.html */ "./src/app/admin/menu-page/menu-page.component.html"),
            styles: [__webpack_require__(/*! ./menu-page.component.scss */ "./src/app/admin/menu-page/menu-page.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], MenuPageComponent);
    return MenuPageComponent;
}());



/***/ }),

/***/ "./src/app/admin/menus-page/menus-page.component.html":
/*!************************************************************!*\
  !*** ./src/app/admin/menus-page/menus-page.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"o-page\">\r\n  <app-sidebar></app-sidebar>\r\n  <main class=\"o-page__content\">\r\n    <app-navbar></app-navbar>\r\n    <div class=\"container page-content\">\r\n      <div class=\"row\">\r\n        <div class=\"col-xl-3\">\r\n          <app-management-dashboard-side-menu [menuConfig]=\"menuConfig\" (action)=\"sideMenuAction($event)\"></app-management-dashboard-side-menu>\r\n        </div>\r\n\r\n        <div class=\"col-sm-12 col-xl-9\">\r\n          <main>\r\n            <div class=\"row\">\r\n              <div class=\"col-sm-12\" infiniteScroll\r\n                   [infiniteScrollDistance]=\"2\"\r\n                   [infiniteScrollThrottle]=\"300\"\r\n                   (scrolled)=\"onScroll()\">\r\n                <div class=\"c-table-responsive@tablet\">\r\n                  <table class=\"c-table u-mb-large\">\r\n                    <caption class=\"c-table__title\">\r\n                      {{'ALL_MENUS' | translate}} <small>{{menus.length}} {{'MENUS' | translate}}</small>\r\n                      <app-filter-input class=\"text-right\" (filterEvent)=\"filterEvent($event)\" ></app-filter-input>\r\n                    </caption>\r\n                    <thead class=\"c-table__head c-table__head--slim\">\r\n                    <tr>\r\n                      <th class=\"c-table__cell c-table__cell--head\">{{'ID' |translate}}</th>\r\n                      <th class=\"c-table__cell c-table__cell--head\">{{'NAME' |translate}}</th>\r\n                      <th class=\"c-table__cell c-table__cell--head\">{{'DESCRIPTION' |translate}}</th>\r\n                      <th class=\"c-table__cell c-table__cell--head\">{{'ACTIONS' |translate}}</th>\r\n                    </tr>\r\n                    </thead>\r\n\r\n                    <tbody>\r\n                    <tr class=\"c-table__row\" *ngFor=\"let menu of menus| filter: {name: filter}\"  app-menu-list-item [data]=\"menu\">\r\n\r\n                    </tr>\r\n\r\n                    </tbody>\r\n                  </table>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </main>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </main>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/admin/menus-page/menus-page.component.scss":
/*!************************************************************!*\
  !*** ./src/app/admin/menus-page/menus-page.component.scss ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/admin/menus-page/menus-page.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/admin/menus-page/menus-page.component.ts ***!
  \**********************************************************/
/*! exports provided: MenusPageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MenusPageComponent", function() { return MenusPageComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm5/ngx-bootstrap-modal.js");
/* harmony import */ var _models_menu__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../models/menu */ "./src/app/models/menu.ts");
/* harmony import */ var _components_modals_modal_menu_modal_menu_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../components/modals/modal-menu/modal-menu.component */ "./src/app/components/modals/modal-menu/modal-menu.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var MenusPageComponent = /** @class */ (function () {
    function MenusPageComponent(modalService) {
        this.modalService = modalService;
        this.menuConfig = [{
                name: 'MENU',
                items: [{
                        name: 'ADD_NEW_MENU',
                        action: { action: 'create' },
                        icon: 'plus'
                    }]
            }];
        this.menus = [];
    }
    MenusPageComponent.prototype.ngOnInit = function () {
        this.load();
    };
    MenusPageComponent.prototype.load = function () {
        var menus = new Array(10).fill(new _models_menu__WEBPACK_IMPORTED_MODULE_2__["Menu"]());
        for (var i = 0; i < menus.length; i++) {
            this.menus.push(menus[i]);
        }
    };
    MenusPageComponent.prototype.create = function () {
        var _this = this;
        this.modalRef = this.modalService.show(_components_modals_modal_menu_modal_menu_component__WEBPACK_IMPORTED_MODULE_3__["ModalMenuComponent"], {
            initialState: {
                name: 'Menu name',
                description: 'Menu description',
                callback: function (menu) {
                    _this.menus.push(menu);
                }
            }
        });
    };
    // COMPONENT COMUNICATION
    MenusPageComponent.prototype.filterEvent = function (event) {
        this.filter = event;
    };
    MenusPageComponent.prototype.sideMenuAction = function (event) {
        if (event.action && event.action.action) {
            this[event.action.action](event.action.data);
        }
    };
    MenusPageComponent.prototype.onScroll = function () { };
    MenusPageComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-menus-page',
            template: __webpack_require__(/*! ./menus-page.component.html */ "./src/app/admin/menus-page/menus-page.component.html"),
            styles: [__webpack_require__(/*! ./menus-page.component.scss */ "./src/app/admin/menus-page/menus-page.component.scss")]
        }),
        __metadata("design:paramtypes", [ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_1__["BsModalService"]])
    ], MenusPageComponent);
    return MenusPageComponent;
}());



/***/ }),

/***/ "./src/app/admin/navbar/navbar.component.html":
/*!****************************************************!*\
  !*** ./src/app/admin/navbar/navbar.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<header class=\"c-navbar\">\r\n  <button class=\"c-sidebar-toggle u-mr-small\">\r\n    <span class=\"c-sidebar-toggle__bar\"></span>\r\n    <span class=\"c-sidebar-toggle__bar\"></span>\r\n    <span class=\"c-sidebar-toggle__bar\"></span>\r\n  </button><!-- // .c-sidebar-toggle -->\r\n\r\n  <h2 class=\"c-navbar__title u-mr-auto\">{{title}}</h2>\r\n\r\n  <div class=\"c-dropdown dropdown\">\r\n    <a  class=\"c-avatar c-avatar--xsmall\"  id=\"dropdwonMenuAvatar\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\r\n      <img class=\"c-avatar__img\" src=\"assets/img/avatar-72.jpg\" alt=\"User's Profile Picture\">\r\n    </a>\r\n\r\n    <!--<div class=\"c-dropdown__menu dropdown-menu dropdown-menu-right\" aria-labelledby=\"dropdwonMenuAvatar\">-->\r\n      <!--<a class=\"c-dropdown__item dropdown-item\" >Edit Profile</a>-->\r\n      <!--<a class=\"c-dropdown__item dropdown-item\" >View Activity</a>-->\r\n      <!--<a class=\"c-dropdown__item dropdown-item\" >Manage Roles</a>-->\r\n    <!--</div>-->\r\n  </div>\r\n</header>\r\n"

/***/ }),

/***/ "./src/app/admin/navbar/navbar.component.scss":
/*!****************************************************!*\
  !*** ./src/app/admin/navbar/navbar.component.scss ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/admin/navbar/navbar.component.ts":
/*!**************************************************!*\
  !*** ./src/app/admin/navbar/navbar.component.ts ***!
  \**************************************************/
/*! exports provided: NavbarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavbarComponent", function() { return NavbarComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var NavbarComponent = /** @class */ (function () {
    function NavbarComponent() {
        this.title = 'xxxx';
    }
    NavbarComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], NavbarComponent.prototype, "title", void 0);
    NavbarComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-navbar',
            template: __webpack_require__(/*! ./navbar.component.html */ "./src/app/admin/navbar/navbar.component.html"),
            styles: [__webpack_require__(/*! ./navbar.component.scss */ "./src/app/admin/navbar/navbar.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], NavbarComponent);
    return NavbarComponent;
}());



/***/ }),

/***/ "./src/app/admin/order-page/order-page.component.html":
/*!************************************************************!*\
  !*** ./src/app/admin/order-page/order-page.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"o-page\">\r\n  <app-sidebar></app-sidebar>\r\n  <main class=\"o-page__content\">\r\n    <app-navbar></app-navbar>\r\n    <div class=\"container page-content\">\r\n      <div class=\"row u-mb-medium u-justify-center\">\r\n        <div class=\"col-xl-9\">\r\n          <div class=\"c-invoice\">\r\n\r\n            <div class=\"c-invoice__header\">\r\n              <div class=\"c-invoice__brand\">\r\n                <img class=\"c-invoice__brand-img\" src=\"assets/img/logo.png\" alt=\"{{appName}}'s Logo\">\r\n                <h1 class=\"c-invoice__brand-name\">{{appName}}</h1>\r\n              </div>\r\n\r\n              <div class=\"c-invoice__title\">\r\n                <h4>{{'INVOICE' | translate}}</h4>\r\n                <div class=\"c-invoice__date\">{{order.created_at | amDateFormat:'DD MMM'}}</div>\r\n              </div>\r\n            </div>\r\n\r\n            <div class=\"c-invoice__details\">\r\n              <div class=\"c-invoice__company\">\r\n                <span class=\"u-text-mute u-text-uppercase u-text-xsmall\">{{'FROM' | translate}}:</span>\r\n                <div class=\"c-invoice__company-name\">\r\n                  {{order.event.company.name}}\r\n                </div>\r\n\r\n                <div class=\"c-invoice__company-address\">\r\n                  {{order.event.company.address.address}},<br>\r\n                  {{order.event.company.address.city}}, {{order.event.company.address.zip}}<br>\r\n                  {{order.event.company.address.country}}\r\n                </div>\r\n              </div>\r\n\r\n              <div class=\"c-invoice__company\">\r\n                <span class=\"u-text-mute u-text-uppercase u-text-xsmall\">{{'TO' | translate}}:</span>\r\n                <div class=\"c-invoice__company-name\">\r\n                  {{order.client.name}}\r\n                </div>\r\n\r\n                <div class=\"c-invoice__company-address\">\r\n                  {{order.client.mobile}},<br>\r\n                  {{order.client.email}}<br>\r\n                </div>\r\n              </div>\r\n            </div>\r\n\r\n            <div class=\"c-invoice__body\">\r\n              <div class=\"c-invoice__desc\">\r\n                {{'INVOICE' | translate}} <br>\r\n                <span class=\"c-invoice__number\"># {{order.id}}</span>\r\n              </div>\r\n              <div class=\"c-invoice__table\">\r\n                <table class=\"c-table\">\r\n                  <thead class=\"c-table__head c-table__head--slim\">\r\n                  <tr class=\"c-table__row\">\r\n                    <th class=\"c-table__cell c-table__cell--head\">{{'DESCRIPTION' | translate}}</th>\r\n                    <th class=\"c-table__cell c-table__cell--head\">{{'QUANTITY' | translate}}</th>\r\n                    <th class=\"c-table__cell c-table__cell--head\">{{'PRICE' | translate}}</th>\r\n                  </tr>\r\n                  </thead>\r\n                  <tbody>\r\n                  <tr class=\"c-table__row\" app-invoice-single-item-list-item *ngFor=\"let orderItem of order.order_items\" [data]=\"orderItem\">\r\n\r\n                  </tr>\r\n\r\n                  </tbody>\r\n\r\n                  <tfoot>\r\n                  <tr class=\"c-table__row\">\r\n                    <td class=\"c-table__cell\" colspan=\"2\"><strong>{{'TOTAL' | translate }}</strong></td>\r\n                    <td class=\"c-table__cell\"><strong>{{order.amount}}</strong></td>\r\n                  </tfoot>\r\n                </table>\r\n\r\n                <div class=\"c-invoice__terms\">\r\n                  {{'ALL_AMOUNTS_SHOWN_IN_INVICE_ARE_IN' |translate }} {{order.currency.symbol}} ({{order.currency.name}}).\r\n                </div>\r\n              </div>\r\n            </div>\r\n\r\n            <div class=\"c-invoice__footer\">\r\n\r\n              <div class=\"c-invoice__footer-brand\">\r\n                <img src=\"assets/img/logo.png\" alt=\"{{appName}} Logo\">\r\n                <span>{{appName}}</span>\r\n              </div>\r\n\r\n              <div class=\"c-invoice__footer-info\">\r\n                <span>{{emailInfo}}</span>\r\n              </div>\r\n\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n\r\n  </main>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/admin/order-page/order-page.component.scss":
/*!************************************************************!*\
  !*** ./src/app/admin/order-page/order-page.component.scss ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/admin/order-page/order-page.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/admin/order-page/order-page.component.ts ***!
  \**********************************************************/
/*! exports provided: OrderPageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderPageComponent", function() { return OrderPageComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _models_order__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../models/order */ "./src/app/models/order.ts");
/* harmony import */ var _defaults__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../defaults */ "./src/app/defaults.ts");
/* harmony import */ var _models_order_item__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../models/order-item */ "./src/app/models/order-item.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var OrderPageComponent = /** @class */ (function () {
    function OrderPageComponent() {
        this.appName = _defaults__WEBPACK_IMPORTED_MODULE_2__["APP"].NAME;
        this.emailInfo = _defaults__WEBPACK_IMPORTED_MODULE_2__["CONTACTS"].INFO.EMAIL;
    }
    OrderPageComponent.prototype.ngOnInit = function () {
        this.load();
    };
    OrderPageComponent.prototype.load = function () {
        this.order = new _models_order__WEBPACK_IMPORTED_MODULE_1__["Order"]();
        this.order.order_items = [new _models_order_item__WEBPACK_IMPORTED_MODULE_3__["OrderItem"](), new _models_order_item__WEBPACK_IMPORTED_MODULE_3__["OrderItem"]()];
    };
    OrderPageComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-order-page',
            template: __webpack_require__(/*! ./order-page.component.html */ "./src/app/admin/order-page/order-page.component.html"),
            styles: [__webpack_require__(/*! ./order-page.component.scss */ "./src/app/admin/order-page/order-page.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], OrderPageComponent);
    return OrderPageComponent;
}());



/***/ }),

/***/ "./src/app/admin/orders-page/orders-page.component.html":
/*!**************************************************************!*\
  !*** ./src/app/admin/orders-page/orders-page.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"o-page\">\r\n  <app-sidebar></app-sidebar>\r\n  <main class=\"o-page__content\">\r\n    <app-navbar></app-navbar>\r\n\r\n    <div class=\"container page-content\">\r\n      <div class=\"row\">\r\n        <div class=\"col-lg-12\">\r\n          <app-income-sumary></app-income-sumary>\r\n        </div>\r\n\r\n        <!--<div class=\"col-lg-4\">-->\r\n          <!--<app-profit-sumary></app-profit-sumary>-->\r\n        <!--</div>-->\r\n      </div>\r\n\r\n      <div class=\"row u-mb-large\">\r\n        <div class=\"col-md-12\" infiniteScroll\r\n             [infiniteScrollDistance]=\"1\"\r\n             [infiniteScrollThrottle]=\"300\"\r\n             (scrolled)=\"onScroll()\">\r\n          <div class=\"c-table-responsive@wide\">\r\n            <table class=\"c-table\">\r\n              <caption class=\"c-table__title\">\r\n                {{'ORDERS' |translate}}\r\n              </caption>\r\n              <thead class=\"c-table__head c-table__head--slim\">\r\n                <tr class=\"c-table__row\">\r\n                  <th class=\"c-table__cell c-table__cell--head\">{{'CARDINAL' |translate}}</th>\r\n                  <th class=\"c-table__cell c-table__cell--head\">{{'INVOICE_DESCRIPTION' |translate}}</th>\r\n                  <th class=\"c-table__cell c-table__cell--head\">{{'EVENT' |translate}}</th>\r\n                  <th class=\"c-table__cell c-table__cell--head\">{{'VAT_NUMBER' |translate}}</th>\r\n                  <th class=\"c-table__cell c-table__cell--head\">{{'DATE' |translate}}</th>\r\n                  <th class=\"c-table__cell c-table__cell--head\">{{'STATUS' |translate}}</th>\r\n                  <th class=\"c-table__cell c-table__cell--head\">{{'AMOUNT' |translate}}</th>\r\n                  <th class=\"c-table__cell c-table__cell--head\">\r\n                    <span class=\"u-hidden-visually\">{{'ACTIONS' |translate}}</span>\r\n                  </th>\r\n                </tr>\r\n              </thead>\r\n\r\n              <tbody>\r\n              <tr class=\"c-table__row\" *ngFor=\"let order of orders\" app-order-list-item  [data]=\"order\">\r\n\r\n              </tr><!-- // .table__row -->\r\n              </tbody>\r\n            </table>\r\n          </div><!-- // .c-card -->\r\n        </div>\r\n      </div><!-- // .row -->\r\n    </div>\r\n  </main>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/admin/orders-page/orders-page.component.scss":
/*!**************************************************************!*\
  !*** ./src/app/admin/orders-page/orders-page.component.scss ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/admin/orders-page/orders-page.component.ts":
/*!************************************************************!*\
  !*** ./src/app/admin/orders-page/orders-page.component.ts ***!
  \************************************************************/
/*! exports provided: OrdersPageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrdersPageComponent", function() { return OrdersPageComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _models_order__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../models/order */ "./src/app/models/order.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var OrdersPageComponent = /** @class */ (function () {
    function OrdersPageComponent() {
        this.orders = [];
        this.filters = {
            event: null
        };
        this.load();
    }
    OrdersPageComponent.prototype.ngOnInit = function () {
    };
    OrdersPageComponent.prototype.load = function () {
        var orders = new Array(10).fill(new _models_order__WEBPACK_IMPORTED_MODULE_1__["Order"]());
        for (var i = 0; i < orders.length; i++) {
            this.orders.push(orders[i]);
        }
    };
    OrdersPageComponent.prototype.onScroll = function () {
        this.load();
    };
    OrdersPageComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-orders-page',
            template: __webpack_require__(/*! ./orders-page.component.html */ "./src/app/admin/orders-page/orders-page.component.html"),
            styles: [__webpack_require__(/*! ./orders-page.component.scss */ "./src/app/admin/orders-page/orders-page.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], OrdersPageComponent);
    return OrdersPageComponent;
}());



/***/ }),

/***/ "./src/app/admin/pictures-page/pictures-page.component.html":
/*!******************************************************************!*\
  !*** ./src/app/admin/pictures-page/pictures-page.component.html ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"o-page\">\r\n  <app-sidebar></app-sidebar>\r\n  <main class=\"o-page__content\">\r\n    <app-navbar></app-navbar>\r\n    <div class=\"container page-content\">\r\n      <div class=\"row\">\r\n        <div class=\"col-sm-6 col-lg-4 col-xl-3\" *ngFor=\"let picture of pictures\">\r\n          <div class=\"c-project-card u-mb-medium\">\r\n            <img class=\"c-project-card__img\" src=\"{{picture}}\" alt=\"About the image\">\r\n\r\n          </div>\r\n\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </main>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/admin/pictures-page/pictures-page.component.scss":
/*!******************************************************************!*\
  !*** ./src/app/admin/pictures-page/pictures-page.component.scss ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/admin/pictures-page/pictures-page.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/admin/pictures-page/pictures-page.component.ts ***!
  \****************************************************************/
/*! exports provided: PicturesPageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PicturesPageComponent", function() { return PicturesPageComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var PicturesPageComponent = /** @class */ (function () {
    function PicturesPageComponent() {
        this.pictures = [];
    }
    PicturesPageComponent.prototype.ngOnInit = function () {
        this.load();
    };
    PicturesPageComponent.prototype.load = function () {
        var pictures = Array(25).fill('https://picsum.photos/300/300/?random');
        for (var i = 0; i < pictures.length; i++) {
            this.pictures.push(pictures[i]);
        }
    };
    PicturesPageComponent.prototype.onScroll = function () { };
    PicturesPageComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-pictures-page',
            template: __webpack_require__(/*! ./pictures-page.component.html */ "./src/app/admin/pictures-page/pictures-page.component.html"),
            styles: [__webpack_require__(/*! ./pictures-page.component.scss */ "./src/app/admin/pictures-page/pictures-page.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], PicturesPageComponent);
    return PicturesPageComponent;
}());



/***/ }),

/***/ "./src/app/admin/place-page/place-page.component.html":
/*!************************************************************!*\
  !*** ./src/app/admin/place-page/place-page.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"o-page\">\r\n  <app-sidebar></app-sidebar>\r\n  <main class=\"o-page__content\">\r\n    <app-navbar></app-navbar>\r\n    <div class=\"c-toolbar u-justify-between u-mb-medium page-content\">\r\n      <nav class=\"c-counter-nav\">\r\n        <p class=\"c-counter-nav__title\">Status:</p>\r\n        <div class=\"c-counter-nav__item u-hidden-down@tablet\">\r\n          <a class=\"c-counter-nav__link\" >\r\n            <span class=\"c-counter-nav__counter\"><i class=\"fa fa-check\"></i></span>Contract\r\n          </a>\r\n        </div>\r\n        <div class=\"c-counter-nav__item u-hidden-down@tablet\">\r\n          <a class=\"c-counter-nav__link\" >\r\n            <span class=\"c-counter-nav__counter\"><i class=\"fa fa-check\"></i></span>Initial Design Draft\r\n          </a>\r\n        </div>\r\n        <div class=\"c-counter-nav__item\">\r\n          <a class=\"c-counter-nav__link is-active\" >\r\n            <span class=\"c-counter-nav__counter\">3</span>Coding Sprint\r\n          </a>\r\n        </div>\r\n        <div class=\"c-counter-nav__item u-hidden-down@tablet\">\r\n          <a class=\"c-counter-nav__link\" >\r\n            <span class=\"c-counter-nav__counter\">4</span>SEO Optimization\r\n          </a>\r\n        </div>\r\n      </nav>\r\n\r\n      <span class=\"c-badge c-badge--small c-badge--success\">Active Project</span>\r\n    </div>\r\n\r\n    <div class=\"container-fluid page-content\">\r\n\r\n      <div class=\"row\">\r\n        <div class=\"col-xl-8\">\r\n          <article class=\"c-stage\" id=\"stages\">\r\n            <a class=\"c-stage__header u-flex u-justify-between\" data-toggle=\"collapse\" href=\"#stage-panel1\" aria-expanded=\"false\" aria-controls=\"stage-panel1\">\r\n              <div class=\"o-media\">\r\n                <div class=\"c-stage__header-img o-media__img\">\r\n                  <img src=\"assets/img/recent1.jpg\" alt=\"About the image\">\r\n                </div>\r\n                <div class=\"c-stage__header-title o-media__body\">\r\n                  <h6 class=\"u-mb-zero\">Landing Page</h6>\r\n                  <p class=\"u-text-xsmall u-text-mute\">Posted 2 days ago  |  Expert ($$$)  |  Est. Time: Less than 1 week</p>\r\n                </div>\r\n              </div>\r\n\r\n              <i class=\"fa fa-angle-down u-text-mute\"></i>\r\n            </a>\r\n\r\n            <div class=\"c-stage__panel c-stage__panel--mute collapse show\" id=\"stage-panel1\">\r\n              <div class=\"u-p-medium\">\r\n                <p class=\"u-text-mute u-text-uppercase u-text-small u-mb-xsmall\">Description</p>\r\n                <p class=\"u-mb-medium\">What we have done so far is brighten the colour palette, created new “easy to understand” icons, overall organization of the page and also worked on the copy! After first user tesing  we got some great results - users that went through the page could navigate easily and explain Tapdaq’s services right after.</p>\r\n\r\n                <p class=\"u-text-mute u-text-uppercase u-text-small u-mb-xsmall\">Goals</p>\r\n                <div class=\"row\">\r\n                  <div class=\"col-md-7\">\r\n                    <ul>\r\n                      <li class=\"u-mb-xsmall u-text-small u-color-primary\">\r\n                        <i class=\"fa fa-check u-color-info u-text-mute u-mr-xsmall\"></i>Launch New Portfolio within 30 days\r\n                      </li>\r\n                      <li class=\"u-mb-xsmall u-text-small u-color-primary\">\r\n                        <i class=\"fa fa-check u-color-info u-text-mute u-mr-xsmall\"></i>SEO Optimization be at least on 2nd page on Google\r\n                      </li>\r\n                      <li class=\"u-mb-xsmall u-text-small u-color-primary\">\r\n                        <i class=\"fa fa-check u-color-info u-text-mute u-mr-xsmall\"></i>All Shareable Images\r\n                      </li>\r\n                    </ul>\r\n                  </div>\r\n\r\n                  <div class=\"col-md-5\">\r\n                    <ul>\r\n                      <li class=\"u-mb-xsmall u-text-small u-color-primary\">\r\n                        <i class=\"fa fa-check u-color-info u-text-mute u-mr-xsmall\"></i>New Twitter Campaign\r\n                      </li>\r\n                      <li class=\"u-mb-xsmall u-text-small u-color-primary\">\r\n                        <i class=\"fa fa-check u-color-info u-text-mute u-mr-xsmall\"></i>Setup Facebook Profile\r\n                      </li>\r\n                    </ul>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div><!-- // .c-stage__panel -->\r\n\r\n            <a class=\"c-stage__header u-flex u-justify-between\" data-toggle=\"collapse\" href=\"#stage-panel2\" aria-expanded=\"false\" aria-controls=\"stage-panel2\">\r\n              <h6 class=\"u-text-mute u-text-uppercase u-text-small u-mb-zero\">Customer Details</h6>\r\n\r\n              <i class=\"fa fa-angle-down u-text-mute\"></i>\r\n            </a>\r\n\r\n            <div class=\"c-stage__panel c-stage__panel--mute collapse\" id=\"stage-panel2\">\r\n              <div class=\"u-p-medium\">\r\n                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita quo dicta, accusantium, nesciunt vero reiciendis deserunt voluptas ullam aperiam ipsa obcaecati. Perspiciatis est, ut quas at, eaque itaque dicta quibusdam.</p>\r\n              </div>\r\n            </div><!-- // .c-stage__panel -->\r\n\r\n            <a class=\"c-stage__header u-flex u-justify-between\" data-toggle=\"collapse\" href=\"#stage-panel3\" aria-expanded=\"false\" aria-controls=\"stage-panel3\">\r\n              <h6 class=\"u-text-mute u-text-uppercase u-text-small u-mb-zero\">Contract Files and Non-Disclosure</h6>\r\n\r\n              <i class=\"fa fa-angle-down u-text-mute\"></i>\r\n            </a>\r\n\r\n            <div class=\"c-stage__panel c-stage__panel--mute collapse\" id=\"stage-panel3\">\r\n              <div class=\"u-p-medium\">\r\n                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nemo similique illo dolore aliquid quas, saepe nostrum placeat vero, illum explicabo sed cumque dolorem iusto dolores aperiam! Veritatis quod neque excepturi!</p>\r\n              </div>\r\n            </div><!-- // .c-stage__panel -->\r\n          </article><!-- // .c-stage -->\r\n\r\n          <article class=\"c-stage\">\r\n            <div class=\"c-stage__header o-media u-justify-start\">\r\n              <div class=\"c-stage__icon o-media__img\">\r\n                <i class=\"fa fa-check\"></i>\r\n              </div>\r\n              <div class=\"c-stage__header-title o-media__body\">\r\n                <h6 class=\"u-mb-zero\">Stage 1 - Initial Design Draft</h6>\r\n                <p class=\"u-text-xsmall u-text-mute\">Started 3 days ago  |  Expected time: 14 days</p>\r\n              </div>\r\n            </div>\r\n\r\n            <div class=\"c-stage__panel u-p-medium\">\r\n\r\n              <p class=\"u-text-mute u-text-uppercase u-text-small u-mb-xsmall\">Description</p>\r\n              <p class=\"u-mb-medium\">What we have done so far is brighten the colour palette, created new “easy to understand” icons, overall organization of the page and also worked on the copy! After first user tesing  we got some great results - users that went through the page could navigate easily and explain Tapdaq’s services right after.</p>\r\n\r\n              <p class=\"u-text-mute u-text-uppercase u-text-small u-mb-xsmall\">Resources</p>\r\n              <div class=\"row u-mb-medium\">\r\n                <div class=\"col-md-6 col-lg-4\">\r\n                  <ul>\r\n                    <li class=\"u-mb-xsmall u-text-small u-color-primary\">\r\n                      <i class=\"fa fa-file-image-o u-text-mute u-mr-xsmall\"></i>Previous-design.png\r\n                    </li>\r\n                    <li class=\"u-mb-xsmall u-text-small u-color-primary\">\r\n                      <i class=\"fa fa-file-text-o u-text-mute u-mr-xsmall\"></i>Marketing-Materials-2018.zip\r\n                    </li>\r\n                    <li class=\"u-mb-xsmall u-text-small u-color-primary\">\r\n                      <i class=\"fa fa-file-text-o u-text-mute u-mr-xsmall\"></i>All-PSD-Files.zip\r\n                    </li>\r\n                  </ul>\r\n                </div>\r\n\r\n                <div class=\"col-md-6 col-lg-8\">\r\n                  <ul>\r\n                    <li class=\"u-mb-xsmall u-text-small u-color-primary\">\r\n                      <i class=\"fa fa-file-text-o u-text-mute u-mr-xsmall\"></i>Brief.docx\r\n                    </li>\r\n                    <li class=\"u-mb-xsmall u-text-small u-color-primary\">\r\n                      <i class=\"fa fa-file-text-o u-text-mute u-mr-xsmall\"></i>Copy-for-landing-page-by-Jason.docx\r\n                    </li>\r\n                  </ul>\r\n                </div>\r\n              </div>\r\n\r\n              <p class=\"u-text-mute u-text-uppercase u-text-small u-mb-xsmall\">Main goal of this stage</p>\r\n\r\n              <div class=\"row\">\r\n                <div class=\"col-md-6 col-lg-4\">\r\n                  <ul>\r\n                    <li class=\"u-mb-xsmall u-text-small u-color-primary\">\r\n                      <i class=\"fa fa-check u-color-info u-text-mute u-mr-xsmall\"></i>New colour palette\r\n                    </li>\r\n                    <li class=\"u-mb-xsmall u-text-small u-color-primary\">\r\n                      <i class=\"fa fa-check u-color-info u-text-mute u-mr-xsmall\"></i>Brand new and cool icons\r\n                    </li>\r\n                  </ul>\r\n                </div>\r\n\r\n                <div class=\"col-md-6 col-lg-4\">\r\n                  <ul>\r\n                    <li class=\"u-mb-xsmall u-text-small u-color-primary\">\r\n                      <i class=\"fa fa-check u-color-info u-text-mute u-mr-xsmall\"></i>New copy\r\n                    </li>\r\n                  </ul>\r\n                </div>\r\n              </div>\r\n\r\n            </div><!-- // .c-stage__panel -->\r\n\r\n            <div class=\"c-stage__panel u-p-medium\">\r\n              <div class=\"o-media u-mb-small\">\r\n                <div class=\"o-media__img u-mr-xsmall\">\r\n                  <div class=\"c-avatar c-avatar--xsmall\">\r\n                    <img class=\"c-avatar__img\" src=\"assets/img/avatar-72.jpg\" alt=\"Profile Title\">\r\n                  </div>\r\n\r\n                </div>\r\n                <div class=\"o-media__body\">\r\n                  <h6 class=\"u-mb-zero u-text-small\">Gerald (You)</h6>\r\n                  <p class=\"u-text-mute u-text-xsmall\">8:42AM</p>\r\n                </div>\r\n              </div><!-- // .o-media -->\r\n\r\n              <p class=\"u-mb-xsmall\">Hi Steven! <br>I’m sending you our first draft along with our comments. I just love the way this is going. At this point we still need to do some polishing but would love to hear if we’re heading the right direction from you!</p>\r\n\r\n              <p>\r\n                <i class=\"fa fa-file-image-o u-mr-xsmall u-text-mute\"></i>Landing-page-V1-png\r\n              </p>\r\n            </div>\r\n\r\n            <div class=\"c-stage__label\">\r\n              <i class=\"c-stage__label-icon fa fa-check-circle\"></i>\r\n              <p class=\"c-stage__label-title\">Stage 1 - Approved</p>\r\n            </div>\r\n\r\n            <div class=\"c-stage__panel u-p-medium\">\r\n              <div class=\"o-media u-mb-small\">\r\n                <div class=\"o-media__img u-mr-xsmall\">\r\n                  <div class=\"c-avatar c-avatar--xsmall\">\r\n                    <img class=\"c-avatar__img\" src=\"assets/img/avatar4-72.jpg\" alt=\"Profile Title\">\r\n                  </div>\r\n\r\n                </div>\r\n                <div class=\"o-media__body\">\r\n                  <h6 class=\"u-mb-zero u-text-small\">Steven Robinson</h6>\r\n                  <p class=\"u-text-mute u-text-xsmall\">8:49AM</p>\r\n                </div>\r\n              </div><!-- // .o-media -->\r\n\r\n              <p class=\"u-mb-xsmall\">Amazing Stuff! I’ve approved this stage! Keep it up. We love it with Stephen! </p>\r\n            </div>\r\n          </article><!-- // .c-stage -->\r\n        </div>\r\n\r\n        <div class=\"col-xl-4\">\r\n          <div class=\"c-card u-mb-medium\">\r\n\r\n            <div class=\"u-p-medium\">\r\n              <h5 class=\"u-h6 u-mb-medium\">Project Members</h5>\r\n              <div class=\"o-media u-mb-small\">\r\n                <div class=\"o-media__img u-mr-xsmall\">\r\n                  <div class=\"c-avatar c-avatar--xsmall\">\r\n                    <img class=\"c-avatar__img\" src=\"assets/img/avatar2-72.jpg\" alt=\"Profile Title\">\r\n                  </div>\r\n                </div>\r\n                <div class=\"o-media__body\">\r\n                  <h6 class=\"u-text-small u-mb-zero\">Steven Robinson</h6>\r\n                  <p class=\"u-text-mute u-text-xsmall\">Company CTO</p>\r\n                </div>\r\n              </div><!-- // .o-media -->\r\n\r\n              <div class=\"o-media u-mb-small\">\r\n                <div class=\"o-media__img u-mr-xsmall\">\r\n                  <div class=\"c-avatar c-avatar--xsmall\">\r\n                    <img class=\"c-avatar__img\" src=\"assets/img/avatar3-72.jpg\" alt=\"Profile Title\">\r\n                  </div>\r\n                </div>\r\n                <div class=\"o-media__body\">\r\n                  <h6 class=\"u-text-small u-mb-zero\">Julia Harvey</h6>\r\n                  <p class=\"u-text-mute u-text-xsmall\">Company CMO</p>\r\n                </div>\r\n              </div><!-- // .o-media -->\r\n\r\n              <div class=\"o-media u-mb-small\">\r\n                <div class=\"o-media__img u-mr-xsmall\">\r\n                  <div class=\"c-avatar c-avatar--xsmall\">\r\n                    <img class=\"c-avatar__img\" src=\"assets/img/avatar4-72.jpg\" alt=\"Profile Title\">\r\n                  </div>\r\n                </div>\r\n                <div class=\"o-media__body\">\r\n                  <h6 class=\"u-text-small u-mb-zero\">Fred Kelly</h6>\r\n                  <p class=\"u-text-mute u-text-xsmall\">Designer</p>\r\n                </div>\r\n              </div><!-- // .o-media -->\r\n\r\n              <div class=\"o-media\">\r\n                <div class=\"o-media__img u-mr-xsmall\">\r\n                  <div class=\"c-avatar c-avatar--xsmall\">\r\n                    <img class=\"c-avatar__img\" src=\"assets/img/avatar6-72.jpg\" alt=\"Profile Title\">\r\n                  </div>\r\n                </div>\r\n                <div class=\"o-media__body\">\r\n                  <h6 class=\"u-text-small u-mb-zero\">Rose Martinez</h6>\r\n                  <p class=\"u-text-mute u-text-xsmall\">Copywriter</p>\r\n                </div>\r\n              </div><!-- // .o-media -->\r\n            </div>\r\n\r\n            <div class=\"u-pv-small  u-border-top u-text-center\">\r\n              <a class=\"u-text-mute u-text-uppercase u-text-xsmall\" >Manage Members</a>\r\n            </div>\r\n          </div>\r\n\r\n          <div class=\"c-card u-p-medium\">\r\n            <h5 class=\"u-h6 u-mb-medium\">Project Details</h5>\r\n\r\n            <table class=\"u-width-100\">\r\n              <tbody>\r\n              <tr>\r\n                <td class=\"u-pb-xsmall u-color-primary u-text-small\">Budget</td>\r\n                <td class=\"u-pb-xsmall u-text-right u-text-mute u-text-small\">$4,670</td>\r\n              </tr>\r\n              <tr>\r\n                <td class=\"u-pb-xsmall u-color-primary u-text-small\">Paid</td>\r\n                <td class=\"u-pb-xsmall u-text-right u-text-mute u-text-small\">$1,240</td>\r\n              </tr>\r\n              <tr>\r\n                <td class=\"u-color-primary u-text-small\">Next Payment</td>\r\n                <td class=\"u-text-right u-text-mute u-text-small\">After 2nd Stage</td>\r\n              </tr>\r\n              </tbody>\r\n            </table>\r\n\r\n            <div class=\"c-divider u-mv-small\"></div>\r\n\r\n            <div class=\"u-flex u-justify-between u-align-items-center\">\r\n              <p>Job Type</p>\r\n\r\n              <div>\r\n                <span class=\"c-badge c-badge--small c-badge--warning\">Design</span>\r\n                <span class=\"c-badge c-badge--small c-badge--success\">Marketing</span>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div><!-- // .container -->\r\n  </main>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/admin/place-page/place-page.component.scss":
/*!************************************************************!*\
  !*** ./src/app/admin/place-page/place-page.component.scss ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/admin/place-page/place-page.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/admin/place-page/place-page.component.ts ***!
  \**********************************************************/
/*! exports provided: PlacePageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PlacePageComponent", function() { return PlacePageComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var PlacePageComponent = /** @class */ (function () {
    function PlacePageComponent() {
    }
    PlacePageComponent.prototype.ngOnInit = function () {
    };
    PlacePageComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-place-page',
            template: __webpack_require__(/*! ./place-page.component.html */ "./src/app/admin/place-page/place-page.component.html"),
            styles: [__webpack_require__(/*! ./place-page.component.scss */ "./src/app/admin/place-page/place-page.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], PlacePageComponent);
    return PlacePageComponent;
}());



/***/ }),

/***/ "./src/app/admin/places-page/places-page.component.html":
/*!**************************************************************!*\
  !*** ./src/app/admin/places-page/places-page.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"o-page\">\r\n  <app-sidebar></app-sidebar>\r\n  <main class=\"o-page__content\">\r\n    <app-navbar></app-navbar>\r\n    <div class=\"container page-content\">\r\n      <div class=\"row\">\r\n        <div class=\"col-xl-3\">\r\n          <app-management-dashboard-side-menu [menuConfig]=\"menuConfig\" (action)=\"sideMenuAction($event)\"></app-management-dashboard-side-menu>\r\n        </div>\r\n\r\n        <div class=\"col-sm-12 col-xl-9\" infiniteScroll\r\n             [infiniteScrollDistance]=\"2\"\r\n             [infiniteScrollThrottle]=\"300\"\r\n             (scrolled)=\"onScroll()\">\r\n          <main>\r\n            <div class=\"row\">\r\n              <div class=\"col-8 col-xl-10\">\r\n                <h1 class=\"u-h3\" *ngIf=\"ourPlaces.length > 0\">{{'OUR_PLACES' | translate}}</h1>\r\n              </div>\r\n              <div class=\"col-4 col-xl-2\">\r\n                <div class=\"c-field u-mb-small\">\r\n                  <app-filter-input class=\"text-right\" (filterEvent)=\"filterEvent($event)\" ></app-filter-input>\r\n\r\n                </div>\r\n              </div>\r\n            </div><!-- // .row -->\r\n            <div class=\"row\" >\r\n              <app-place-card class=\"col-md-6\" *ngFor=\"let place of ourPlaces | filter: {name: filter} \" [data]=\"place\">\r\n\r\n              </app-place-card>\r\n\r\n            </div><!-- // .row -->\r\n\r\n            <div class=\"row\">\r\n              <div class=\"col-8 col-xl-10\">\r\n                <h1 class=\"u-h3\">{{'PLACES_WE_USED' | translate}}</h1>\r\n              </div>\r\n            </div><!-- // .row -->\r\n            <div class=\"row\">\r\n              <app-place-card class=\"col-md-6\" *ngFor=\"let place of usedPlaces | filter: {name: filter}\" [data]=\"place\">\r\n\r\n              </app-place-card>\r\n            </div>\r\n          </main>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </main>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/admin/places-page/places-page.component.scss":
/*!**************************************************************!*\
  !*** ./src/app/admin/places-page/places-page.component.scss ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/admin/places-page/places-page.component.ts":
/*!************************************************************!*\
  !*** ./src/app/admin/places-page/places-page.component.ts ***!
  \************************************************************/
/*! exports provided: PlacesPageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PlacesPageComponent", function() { return PlacesPageComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _models_place__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../models/place */ "./src/app/models/place.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var PlacesPageComponent = /** @class */ (function () {
    function PlacesPageComponent(router) {
        this.router = router;
        this.filter = '';
        this.menuConfig = [{
                name: 'MENU',
                items: [{
                        name: 'ADD_NEW_PLACE',
                        action: { action: 'createPlace' },
                        icon: 'plus'
                    }]
            }];
        this.ourPlaces = [];
        this.usedPlaces = [];
    }
    PlacesPageComponent.prototype.ngOnInit = function () {
        this.loadOurPlaces();
        this.loadUsedPlaces();
    };
    PlacesPageComponent.prototype.loadOurPlaces = function () {
        this.ourPlaces = Array(1).fill(new _models_place__WEBPACK_IMPORTED_MODULE_1__["Place"]());
    };
    PlacesPageComponent.prototype.loadUsedPlaces = function () {
        this.usedPlaces = Array(10).fill(new _models_place__WEBPACK_IMPORTED_MODULE_1__["Place"]());
    };
    PlacesPageComponent.prototype.goToPlace = function (id) {
        this.router.navigate(["event-management/place/" + id]);
    };
    PlacesPageComponent.prototype.createPlace = function () {
        var place = new _models_place__WEBPACK_IMPORTED_MODULE_1__["Place"]();
        this.goToPlace(place.id);
    };
    // COMPONENT COMUNICATION
    PlacesPageComponent.prototype.filterEvent = function (event) {
        this.filter = event;
    };
    PlacesPageComponent.prototype.sideMenuAction = function (event) {
        if (event.action && event.action.action) {
            this[event.action.action](event.action.data);
        }
    };
    PlacesPageComponent.prototype.onScroll = function () { };
    PlacesPageComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-places-page',
            template: __webpack_require__(/*! ./places-page.component.html */ "./src/app/admin/places-page/places-page.component.html"),
            styles: [__webpack_require__(/*! ./places-page.component.scss */ "./src/app/admin/places-page/places-page.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], PlacesPageComponent);
    return PlacesPageComponent;
}());



/***/ }),

/***/ "./src/app/admin/profile-page/profile-page.component.html":
/*!****************************************************************!*\
  !*** ./src/app/admin/profile-page/profile-page.component.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"o-page\">\r\n  <app-sidebar></app-sidebar>\r\n  <main class=\"o-page__content\">\r\n    <app-navbar></app-navbar>\r\n    <div class=\"container-fluid page-content\">\r\n      <div class=\"row\">\r\n        <div class=\"col-xl-7\">\r\n\r\n          <ul class=\"c-tabs__list nav nav-tabs\" id=\"myTab\" role=\"tablist\">\r\n            <li><a class=\"c-tabs__link active\" id=\"nav-home-tab\" data-toggle=\"tab\" href=\"#nav-home\" role=\"tab\" aria-controls=\"nav-home\" aria-selected=\"true\">Activity</a></li>\r\n\r\n            <li><a class=\"c-tabs__link\" id=\"nav-profile-tab\" data-toggle=\"tab\" href=\"#nav-profile\" role=\"tab\" aria-controls=\"nav-profile\" aria-selected=\"false\">Blocked Users</a></li>\r\n\r\n            <li><a class=\"c-tabs__link\" id=\"nav-contact-tab\" data-toggle=\"tab\" href=\"#nav-contact\" role=\"tab\" aria-controls=\"nav-contact\" aria-selected=\"false\">NDAs</a></li>\r\n\r\n            <li><a class=\"c-tabs__link u-hidden-down@tablet\" id=\"nav-customer-tab\" data-toggle=\"tab\" href=\"#nav-customer\" role=\"tab\" aria-controls=\"nav-customer\" aria-selected=\"false\">Customer Invoices</a></li>\r\n          </ul>\r\n\r\n          <div class=\"c-tabs__content tab-content u-mb-large\" id=\"nav-tabContent\">\r\n            <div class=\"c-tabs__pane active u-pb-medium\" id=\"nav-home\" role=\"tabpanel\" aria-labelledby=\"nav-home-tab\">\r\n\r\n              <p class=\"u-text-mute u-text-uppercase u-text-small u-mb-xsmall\">Today</p>\r\n\r\n              <div class=\"c-feed\">\r\n                <div class=\"c-feed__item c-feed__item--fancy\">\r\n                  <p>Gerald Vaughn changed the status to QA on <strong>MA-86 - Retargeting Ads</strong></p>\r\n\r\n                  <p class=\"c-feed__comment\">I’ve prepared all sizes for you. Can you take a look tonight so we can prepare my final invoice?</p>\r\n\r\n                  <span class=\"c-feed__meta\">New Dashboard Design - 9:24PM</span>\r\n                </div>\r\n\r\n                <div class=\"c-feed__item c-feed__item--success\">\r\n                  <p>Gerald Vaughn commented on <strong>DA-459 - Mediation: Demand Source Logo Size</strong></p>\r\n\r\n                  <span class=\"u-text-mute u-text-small\">Portfolio Updates for Jason Carroll - 7:12PM</span>\r\n                </div>\r\n\r\n                <div class=\"c-feed__item c-feed__item--fancy\">\r\n                  <p>Gerald Vaughn changed the status to QA on  <strong>MA-45 - Finish Prototype</strong></p>\r\n\r\n                  <span class=\"c-feed__meta\">New Dashboard Design - 11:30AM</span>\r\n                </div>\r\n              </div><!-- // .c-feed -->\r\n\r\n              <p class=\"u-text-mute u-text-uppercase u-text-small u-mb-xsmall\">Yesterday</p>\r\n\r\n              <div class=\"c-feed\">\r\n                <div class=\"c-feed__item c-feed__item--info\">\r\n                  <p>Gerald Vaughn attached 5 files to <strong>9054 - Find good stocks for our Instagram channel</strong></p>\r\n\r\n                  <div class=\"c-feed__gallery\">\r\n\r\n                    <div class=\"c-feed__gallery-item\">\r\n                      <img src=\"assets/img/feed1.jpg\" alt=\"Images' title\">\r\n                    </div>\r\n\r\n                    <div class=\"c-feed__gallery-item\">\r\n                      <img src=\"assets/img/feed2.jpg\" alt=\"Images' title\">\r\n                    </div>\r\n\r\n                    <div class=\"c-feed__gallery-item\">\r\n                      <img src=\"assets/img/feed3.jpg\" alt=\"Images' title\">\r\n                    </div>\r\n\r\n                    <div class=\"c-feed__gallery-item\">\r\n                      <img src=\"assets/img/feed4.jpg\" alt=\"Images' title\">\r\n                    </div>\r\n\r\n                    <div class=\"c-feed__gallery-item\">\r\n                      <img src=\"assets/img/feed5.jpg\" alt=\"Images' title\">\r\n                    </div>\r\n\r\n                  </div>\r\n\r\n                  <span class=\"c-feed__meta\">Marketing Templates & Strategy - 7:59AM</span>\r\n                </div>\r\n\r\n                <div class=\"c-feed__item c-feed__item--success\">\r\n                  <p>Gerald Vaughn commented on <strong>Find good stocks for our Instagram channel</strong></p>\r\n\r\n                  <p class=\"c-feed__comment\">What do you think about these? Should I continue in this style?</p>\r\n\r\n                  <span class=\"c-feed__meta\">Marketing Templates & Strategy - 7:58AM</span>\r\n                </div>\r\n\r\n                <div class=\"c-feed__item c-feed__item--fancy\">\r\n                  <p>Gerald Vaughn changed the status to In Progress on <strong>Find good stocks for our Instagram channel</strong></p>\r\n                  <span class=\"c-feed__meta\">Marketing Templates & Strategy - 6:30AM</span>\r\n                </div>\r\n              </div><!-- // .c-feed -->\r\n\r\n              <p class=\"u-text-mute u-text-uppercase u-text-small u-mb-xsmall\">28 January</p>\r\n\r\n              <div class=\"c-feed u-mb-zero\">\r\n                <div class=\"c-feed__item c-feed__item--success\">\r\n                  <p>Gerald Vaughn attached 6 files to <strong>1007 - Background Inspiration</strong></p>\r\n\r\n                  <div class=\"c-feed__gallery\">\r\n\r\n                    <div class=\"c-feed__gallery-item\">\r\n                      <img src=\"assets/img/feed7.jpg\" alt=\"Images' title\">\r\n                    </div>\r\n\r\n                    <div class=\"c-feed__gallery-item\">\r\n                      <img src=\"assets/img/feed8.jpg\" alt=\"Images' title\">\r\n                    </div>\r\n\r\n                    <div class=\"c-feed__gallery-item\">\r\n                      <img src=\"assets/img/feed9.jpg\" alt=\"Images' title\">\r\n                    </div>\r\n\r\n                    <div class=\"c-feed__gallery-item\">\r\n                      <img src=\"assets/img/feed3.jpg\" alt=\"Images' title\">\r\n                    </div>\r\n\r\n                    <div class=\"c-feed__gallery-item\">\r\n                      <img src=\"assets/img/feed2.jpg\" alt=\"Images' title\">\r\n                    </div>\r\n                    <div class=\"c-feed__gallery-item\">\r\n                      <img src=\"assets/img/feed1.jpg\" alt=\"Images' title\">\r\n                    </div>\r\n\r\n                  </div>\r\n\r\n                  <span class=\"c-feed__meta\">Templates & Inspiration - 11:50AM</span>\r\n                </div>\r\n              </div><!-- // .c-feed -->\r\n\r\n            </div>\r\n\r\n            <div class=\"c-tabs__pane u-pb-medium\" id=\"nav-profile\" role=\"tabpanel\" aria-labelledby=\"nav-profile-tab\">\r\n              <p class=\"u-mb-small\">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et quidem modi atque at aliquid expedita nemo incidunt exercitationem nihil sit. Laudantium suscipit id amet saepe ratione, accusamus. Voluptatum in, nam.</p>\r\n\r\n              <p class=\"u-mb-small\">Lorem ipsum dolor sit amet, consectetur adipisicing elit. A voluptate nobis tenetur mollitia incidunt quod, est veniam, earum nemo! Alias rerum saepe aut sapiente minus sunt doloribus tempora corrupti in!</p>\r\n\r\n              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ea distinctio nostrum molestias assumenda, repudiandae consequuntur quae pariatur aut incidunt placeat doloremque doloribus! Recusandae nostrum dolore repudiandae libero mollitia, rem eveniet.</p>\r\n            </div>\r\n\r\n            <div class=\"c-tabs__pane u-pb-medium\" id=\"nav-contact\" role=\"tabpanel\" aria-labelledby=\"nav-contact-tab\">\r\n              <p class=\"u-mb-small\">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et quidem modi atque at aliquid expedita nemo incidunt exercitationem nihil sit. Laudantium suscipit id amet saepe ratione, accusamus. Voluptatum in, nam.</p>\r\n\r\n              <p class=\"u-mb-small\">Lorem ipsum dolor sit amet, consectetur adipisicing elit. A voluptate nobis tenetur mollitia incidunt quod, est veniam, earum nemo! Alias rerum saepe aut sapiente minus sunt doloribus tempora corrupti in!</p>\r\n            </div>\r\n\r\n            <div class=\"c-tabs__pane u-pb-medium\" id=\"nav-customer\" role=\"tabpanel\" aria-labelledby=\"nav-customer-tab\">\r\n              <p class=\"u-mb-small\">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et quidem modi atque at aliquid expedita nemo incidunt exercitationem nihil sit. Laudantium suscipit id amet saepe ratione, accusamus. Voluptatum in, nam.</p>\r\n\r\n              <p class=\"u-mb-small\">Lorem ipsum dolor sit amet, consectetur adipisicing elit. A voluptate nobis tenetur mollitia incidunt quod, est veniam, earum nemo! Alias rerum saepe aut sapiente minus sunt doloribus tempora corrupti in!</p>\r\n\r\n              <p class=\"u-mb-small\">Lorem ipsum dolor sit amet, consectetur adipisicing elit. A voluptate nobis tenetur mollitia incidunt quod, est veniam, earum nemo! Alias rerum saepe aut sapiente minus sunt doloribus tempora corrupti in!</p>\r\n\r\n              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et quidem modi atque at aliquid expedita nemo incidunt exercitationem nihil sit. Laudantium suscipit id amet saepe ratione, accusamus. Voluptatum in, nam.</p>\r\n            </div>\r\n          </div>\r\n\r\n        </div>\r\n\r\n        <div class=\"col-xl-5\">\r\n          <div class=\"c-card u-p-medium u-mb-medium\">\r\n\r\n            <div class=\"u-text-center\">\r\n              <div class=\"c-avatar c-avatar--large u-mb-small u-inline-flex\">\r\n                <img class=\"c-avatar__img\" src=\"assets/img/avatar-150.jpg\" alt=\"Adam's Face\">\r\n              </div>\r\n\r\n              <h3 class=\"u-h5\">Gerald Vaughn</h3>\r\n              <p>Freelance Designer, Previously TapQ</p>\r\n              <span class=\"u-text-mute u-text-small\">London, United Kingdom</span>\r\n            </div>\r\n\r\n            <div class=\"u-flex u-mt-medium\">\r\n              <a class=\"c-btn c-btn--info c-btn--fullwidth u-mr-xsmall\" >Edit Profile</a>\r\n              <a class=\"c-btn c-btn--secondary c-btn--fullwidth\" >View Profile As</a>\r\n            </div>\r\n\r\n            <table class=\"c-table u-text-center u-pv-small u-mt-medium u-border-right-zero u-border-left-zero\">\r\n              <thead>\r\n              <tr>\r\n                <th class=\"u-pt-small\">\r\n\r\n                  <div class=\"c-rating\">\r\n                    <i class=\"c-rating__icon is-active fa fa-star\"></i>\r\n                    <i class=\"c-rating__icon is-active fa fa-star\"></i>\r\n                    <i class=\"c-rating__icon is-active fa fa-star\"></i>\r\n                    <i class=\"c-rating__icon is-active fa fa-star\"></i>\r\n                    <i class=\"c-rating__icon fa fa-star\"></i>\r\n                  </div>\r\n\r\n                </th>\r\n                <th class=\"u-pt-small u-color-primary\">38</th>\r\n                <th class=\"u-pt-small u-color-primary\">54</th>\r\n                <th class=\"u-pt-small u-color-primary\">186</th>\r\n              </tr>\r\n              </thead>\r\n              <tbody>\r\n              <tr>\r\n                <td class=\"u-text-mute u-text-xsmall u-pb-small u-text-uppercase\">Rating</td>\r\n                <td class=\"u-text-mute u-text-xsmall u-pb-small u-text-uppercase\">Review</td>\r\n                <td class=\"u-text-mute u-text-xsmall u-pb-small u-text-uppercase\">Clients</td>\r\n                <td class=\"u-text-mute u-text-xsmall u-pb-small u-text-uppercase\">Finished Gigs</td>\r\n              </tr>\r\n              </tbody>\r\n            </table>\r\n\r\n            <div class=\"c-feed has-icons u-mt-medium\">\r\n              <div class=\"c-feed__item has-icon\">\r\n                <i class=\"c-feed__item-icon u-bg-info fa fa-tumblr\"></i>\r\n                <p>Product Designer</p>\r\n                <span class=\"c-feed__meta\">Tumblr- London, United Kingdom</span>\r\n              </div>\r\n\r\n              <div class=\"c-feed__item has-icon\">\r\n                <i class=\"c-feed__item-icon u-bg-fancy fa fa-dropbox\"></i>\r\n                <p>Intern</p>\r\n                <p class=\"c-feed__meta\">Dropbox - Berlin, Germany</p>\r\n              </div>\r\n            </div>\r\n\r\n            <div class=\"u-pt-medium u-border-top u-text-center\">\r\n              <a class=\"u-text-mute u-text-small\" href=\"https://workspace.com/geraldvaughn\">\r\n                <i class=\"fa fa-globe u-mr-xsmall\"></i>https://workspace.com/geraldvaughn\r\n              </a>\r\n            </div>\r\n          </div>\r\n\r\n          <div class=\"c-card u-p-medium u-mb-medium\">\r\n            <h5 class=\"u-mb-medium\">Billing</h5>\r\n\r\n            <table class=\"c-table u-border-zero\">\r\n              <thead class=\"c-table__head u-border-bottom\">\r\n              <tr>\r\n                <th class=\"c-table__cell u-text-left u-p-zero u-pb-medium u-flex u-align-items-center\">\r\n                  <img class=\"u-mr-xsmall\" src=\"assets/img/visa.png\" alt=\"Credit Card\">Ending **** 5896\r\n                </th>\r\n                <th class=\"c-table__cell u-text-right u-p-zero u-pb-medium\">05 / 19</th>\r\n                <th class=\"c-table__cell u-text-right u-p-zero u-pb-medium\">\r\n                  <span class=\"u-text-mute\">VISA</span>\r\n                </th>\r\n              </tr>\r\n              </thead>\r\n              <tbody class=\"u-pt-medium\">\r\n              <tr class=\"c-table__row u-border-zero\">\r\n                <td class=\"c-table__cell u-p-zero u-pb-xsmall u-pt-medium\">PRO Package</td>\r\n                <td class=\"c-table__cell u-p-zero u-pb-xsmall u-pt-medium\">\r\n                  <span class=\"u-text-mute\">Standard Payment</span>\r\n                </td>\r\n                <td class=\"c-table__cell u-p-zero u-pb-xsmall u-pt-medium u-text-right\">\r\n                  <span class=\"u-text-mute\">19 Jan 2018</span>\r\n                </td>\r\n              </tr>\r\n              <tr class=\"c-table__row u-border-zero\">\r\n                <td class=\"c-table__cell u-p-zero u-pb-xsmall\">Job Posted</td>\r\n                <td class=\"c-table__cell u-p-zero u-pb-xsmall\">\r\n                  <span class=\"u-text-mute\">30-Days Recuring</span>\r\n                </td>\r\n                <td class=\"c-table__cell u-p-zero u-pb-xsmall u-text-right\">\r\n                  <span class=\"u-text-mute\">24 Nov 2017</span>\r\n                </td>\r\n              </tr>\r\n              <tr class=\"c-table__row u-border-zero\">\r\n                <td class=\"c-table__cell u-p-zero u-pb-xsmall\">PRO Package</td>\r\n                <td class=\"c-table__cell u-p-zero u-pb-xsmall\">\r\n                  <span class=\"u-text-mute\">Gift</span>\r\n                </td>\r\n                <td class=\"c-table__cell u-p-zero u-pb-xsmall u-text-right\">\r\n                  <span class=\"u-text-mute\">7 Mar 2017</span>\r\n                </td>\r\n              </tr>\r\n              </tbody>\r\n            </table>\r\n          </div>\r\n        </div>\r\n\r\n      </div>\r\n\r\n    </div><!-- // .container-fluid -->\r\n  </main>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/admin/profile-page/profile-page.component.scss":
/*!****************************************************************!*\
  !*** ./src/app/admin/profile-page/profile-page.component.scss ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/admin/profile-page/profile-page.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/admin/profile-page/profile-page.component.ts ***!
  \**************************************************************/
/*! exports provided: ProfilePageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfilePageComponent", function() { return ProfilePageComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ProfilePageComponent = /** @class */ (function () {
    function ProfilePageComponent() {
    }
    ProfilePageComponent.prototype.ngOnInit = function () {
    };
    ProfilePageComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-profile-page',
            template: __webpack_require__(/*! ./profile-page.component.html */ "./src/app/admin/profile-page/profile-page.component.html"),
            styles: [__webpack_require__(/*! ./profile-page.component.scss */ "./src/app/admin/profile-page/profile-page.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], ProfilePageComponent);
    return ProfilePageComponent;
}());



/***/ }),

/***/ "./src/app/admin/sidebar/sidebar.component.html":
/*!******************************************************!*\
  !*** ./src/app/admin/sidebar/sidebar.component.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"o-page__sidebar js-page-sidebar\">\r\n  <div class=\"c-sidebar\">\r\n    <a class=\"c-sidebar__brand\" >\r\n      <img class=\"c-sidebar__brand-img\" src=\"assets/img/logo.png\" alt=\"Logo\"> {{'DASHBOARD' | translate}}\r\n    </a>\r\n\r\n    <h4 class=\"c-sidebar__title\">{{'PLACES&EVENTS' | translate }}</h4>\r\n    <ul class=\"c-sidebar__list\">\r\n\r\n      <li app-dashboard-sidebar-list-item *ngFor=\"let obj of places\" [data]=\"obj\"></li>\r\n\r\n    </ul>\r\n\r\n    <h4 class=\"c-sidebar__title\">{{'MANAGER' | translate }}</h4>\r\n    <ul class=\"c-sidebar__list\">\r\n      <li app-dashboard-sidebar-list-item *ngFor=\"let obj of manager\" [data]=\"obj\"></li>\r\n    </ul>\r\n\r\n    <h4 class=\"c-sidebar__title\">{{'ADMIN' | translate }}</h4>\r\n    <ul class=\"c-sidebar__list\">\r\n      <li app-dashboard-sidebar-list-item *ngFor=\"let obj of admin\" [data]=\"obj\"></li>\r\n    </ul>\r\n\r\n    <h4 class=\"c-sidebar__title\">{{'ACCOUNT' | translate }}</h4>\r\n    <ul class=\"c-sidebar__list\">\r\n      <li app-dashboard-sidebar-list-item *ngFor=\"let obj of account\" [data]=\"obj\"></li>\r\n      <li app-dashboard-sidebar-list-item [data]=\"session\" (click)=\"logout()\" ></li>\r\n    </ul>\r\n\r\n  </div><!-- // .c-sidebar -->\r\n</div><!-- // .o-page__sidebar -->\r\n"

/***/ }),

/***/ "./src/app/admin/sidebar/sidebar.component.scss":
/*!******************************************************!*\
  !*** ./src/app/admin/sidebar/sidebar.component.scss ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/admin/sidebar/sidebar.component.ts":
/*!****************************************************!*\
  !*** ./src/app/admin/sidebar/sidebar.component.ts ***!
  \****************************************************/
/*! exports provided: SidebarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SidebarComponent", function() { return SidebarComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/auth.service */ "./src/app/services/auth.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SidebarComponent = /** @class */ (function () {
    function SidebarComponent(auth) {
        this.auth = auth;
        this.places = [{
                path: 'event-management/dashboard',
                name: 'DASHBOARD',
                icon: 'home'
            }, {
                path: 'event-management/events',
                name: 'EVENTS',
                icon: 'calendar-alt'
            }, {
                path: 'event-management/places',
                name: 'PLACES',
                icon: 'map-marker'
            }];
        this.manager = [{
                path: 'event-management/users',
                name: 'TEAM',
                icon: 'user-circle'
            }, {
                path: 'event-management/orders',
                name: 'ORDERS',
                icon: 'money'
            }, {
                path: 'event-management/menus',
                name: 'MENUS',
                icon: 'list-alt'
            }, {
                path: 'event-management/pictures',
                name: 'PICTURES',
                icon: 'picture-o'
            }];
        this.admin = [{
                path: 'event-management/categories',
                name: 'CATEGORIES',
                icon: 'tags'
            }, {
                path: 'event-management/items',
                name: 'INGREDIENTS',
                icon: 'beer'
            }];
        this.account = [{
                path: 'event-management/profile',
                name: 'PROFILE',
                icon: 'user'
            }];
        this.session = {
            name: 'LOGOUT',
            icon: 'sign-out'
        };
    }
    SidebarComponent.prototype.ngOnInit = function () {
    };
    SidebarComponent.prototype.logout = function () {
        this.auth.logout();
    };
    SidebarComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-sidebar',
            template: __webpack_require__(/*! ./sidebar.component.html */ "./src/app/admin/sidebar/sidebar.component.html"),
            styles: [__webpack_require__(/*! ./sidebar.component.scss */ "./src/app/admin/sidebar/sidebar.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"]])
    ], SidebarComponent);
    return SidebarComponent;
}());



/***/ }),

/***/ "./src/app/admin/user-page/user-page.component.html":
/*!**********************************************************!*\
  !*** ./src/app/admin/user-page/user-page.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"o-page\">\r\n  <app-sidebar></app-sidebar>\r\n  <main class=\"o-page__content\">\r\n    <app-navbar></app-navbar>\r\n    <div class=\"container-fluid page-content\">\r\n      <div class=\"row\">\r\n        <div class=\"col-xl-7\">\r\n\r\n          <ul class=\"c-tabs__list nav nav-tabs\" id=\"myTab\" role=\"tablist\">\r\n            <li><a class=\"c-tabs__link active\" id=\"nav-home-tab\" data-toggle=\"tab\" href=\"#nav-home\" role=\"tab\" aria-controls=\"nav-home\" aria-selected=\"true\">Activity</a></li>\r\n\r\n            <li><a class=\"c-tabs__link\" id=\"nav-profile-tab\" data-toggle=\"tab\" href=\"#nav-profile\" role=\"tab\" aria-controls=\"nav-profile\" aria-selected=\"false\">Blocked Users</a></li>\r\n\r\n            <li><a class=\"c-tabs__link\" id=\"nav-contact-tab\" data-toggle=\"tab\" href=\"#nav-contact\" role=\"tab\" aria-controls=\"nav-contact\" aria-selected=\"false\">NDAs</a></li>\r\n\r\n            <li><a class=\"c-tabs__link u-hidden-down@tablet\" id=\"nav-customer-tab\" data-toggle=\"tab\" href=\"#nav-customer\" role=\"tab\" aria-controls=\"nav-customer\" aria-selected=\"false\">Customer Invoices</a></li>\r\n          </ul>\r\n\r\n          <div class=\"c-tabs__content tab-content u-mb-large\" id=\"nav-tabContent\">\r\n            <div class=\"c-tabs__pane active u-pb-medium\" id=\"nav-home\" role=\"tabpanel\" aria-labelledby=\"nav-home-tab\">\r\n\r\n              <p class=\"u-text-mute u-text-uppercase u-text-small u-mb-xsmall\">Today</p>\r\n\r\n              <div class=\"c-feed\">\r\n                <div class=\"c-feed__item c-feed__item--fancy\">\r\n                  <p>Gerald Vaughn changed the status to QA on <strong>MA-86 - Retargeting Ads</strong></p>\r\n\r\n                  <p class=\"c-feed__comment\">I’ve prepared all sizes for you. Can you take a look tonight so we can prepare my final invoice?</p>\r\n\r\n                  <span class=\"c-feed__meta\">New Dashboard Design - 9:24PM</span>\r\n                </div>\r\n\r\n                <div class=\"c-feed__item c-feed__item--success\">\r\n                  <p>Gerald Vaughn commented on <strong>DA-459 - Mediation: Demand Source Logo Size</strong></p>\r\n\r\n                  <span class=\"u-text-mute u-text-small\">Portfolio Updates for Jason Carroll - 7:12PM</span>\r\n                </div>\r\n\r\n                <div class=\"c-feed__item c-feed__item--fancy\">\r\n                  <p>Gerald Vaughn changed the status to QA on  <strong>MA-45 - Finish Prototype</strong></p>\r\n\r\n                  <span class=\"c-feed__meta\">New Dashboard Design - 11:30AM</span>\r\n                </div>\r\n              </div><!-- // .c-feed -->\r\n\r\n              <p class=\"u-text-mute u-text-uppercase u-text-small u-mb-xsmall\">Yesterday</p>\r\n\r\n              <div class=\"c-feed\">\r\n                <div class=\"c-feed__item c-feed__item--info\">\r\n                  <p>Gerald Vaughn attached 5 files to <strong>9054 - Find good stocks for our Instagram channel</strong></p>\r\n\r\n                  <div class=\"c-feed__gallery\">\r\n\r\n                    <div class=\"c-feed__gallery-item\">\r\n                      <img src=\"assets/img/feed1.jpg\" alt=\"Images' title\">\r\n                    </div>\r\n\r\n                    <div class=\"c-feed__gallery-item\">\r\n                      <img src=\"assets/img/feed2.jpg\" alt=\"Images' title\">\r\n                    </div>\r\n\r\n                    <div class=\"c-feed__gallery-item\">\r\n                      <img src=\"assets/img/feed3.jpg\" alt=\"Images' title\">\r\n                    </div>\r\n\r\n                    <div class=\"c-feed__gallery-item\">\r\n                      <img src=\"assets/img/feed4.jpg\" alt=\"Images' title\">\r\n                    </div>\r\n\r\n                    <div class=\"c-feed__gallery-item\">\r\n                      <img src=\"assets/img/feed5.jpg\" alt=\"Images' title\">\r\n                    </div>\r\n\r\n                  </div>\r\n\r\n                  <span class=\"c-feed__meta\">Marketing Templates & Strategy - 7:59AM</span>\r\n                </div>\r\n\r\n                <div class=\"c-feed__item c-feed__item--success\">\r\n                  <p>Gerald Vaughn commented on <strong>Find good stocks for our Instagram channel</strong></p>\r\n\r\n                  <p class=\"c-feed__comment\">What do you think about these? Should I continue in this style?</p>\r\n\r\n                  <span class=\"c-feed__meta\">Marketing Templates & Strategy - 7:58AM</span>\r\n                </div>\r\n\r\n                <div class=\"c-feed__item c-feed__item--fancy\">\r\n                  <p>Gerald Vaughn changed the status to In Progress on <strong>Find good stocks for our Instagram channel</strong></p>\r\n                  <span class=\"c-feed__meta\">Marketing Templates & Strategy - 6:30AM</span>\r\n                </div>\r\n              </div><!-- // .c-feed -->\r\n\r\n              <p class=\"u-text-mute u-text-uppercase u-text-small u-mb-xsmall\">28 January</p>\r\n\r\n              <div class=\"c-feed u-mb-zero\">\r\n                <div class=\"c-feed__item c-feed__item--success\">\r\n                  <p>Gerald Vaughn attached 6 files to <strong>1007 - Background Inspiration</strong></p>\r\n\r\n                  <div class=\"c-feed__gallery\">\r\n\r\n                    <div class=\"c-feed__gallery-item\">\r\n                      <img src=\"assets/img/feed7.jpg\" alt=\"Images' title\">\r\n                    </div>\r\n\r\n                    <div class=\"c-feed__gallery-item\">\r\n                      <img src=\"assets/img/feed8.jpg\" alt=\"Images' title\">\r\n                    </div>\r\n\r\n                    <div class=\"c-feed__gallery-item\">\r\n                      <img src=\"assets/img/feed9.jpg\" alt=\"Images' title\">\r\n                    </div>\r\n\r\n                    <div class=\"c-feed__gallery-item\">\r\n                      <img src=\"assets/img/feed3.jpg\" alt=\"Images' title\">\r\n                    </div>\r\n\r\n                    <div class=\"c-feed__gallery-item\">\r\n                      <img src=\"assets/img/feed2.jpg\" alt=\"Images' title\">\r\n                    </div>\r\n                    <div class=\"c-feed__gallery-item\">\r\n                      <img src=\"assets/img/feed1.jpg\" alt=\"Images' title\">\r\n                    </div>\r\n\r\n                  </div>\r\n\r\n                  <span class=\"c-feed__meta\">Templates & Inspiration - 11:50AM</span>\r\n                </div>\r\n              </div><!-- // .c-feed -->\r\n\r\n            </div>\r\n\r\n            <div class=\"c-tabs__pane u-pb-medium\" id=\"nav-profile\" role=\"tabpanel\" aria-labelledby=\"nav-profile-tab\">\r\n              <p class=\"u-mb-small\">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et quidem modi atque at aliquid expedita nemo incidunt exercitationem nihil sit. Laudantium suscipit id amet saepe ratione, accusamus. Voluptatum in, nam.</p>\r\n\r\n              <p class=\"u-mb-small\">Lorem ipsum dolor sit amet, consectetur adipisicing elit. A voluptate nobis tenetur mollitia incidunt quod, est veniam, earum nemo! Alias rerum saepe aut sapiente minus sunt doloribus tempora corrupti in!</p>\r\n\r\n              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ea distinctio nostrum molestias assumenda, repudiandae consequuntur quae pariatur aut incidunt placeat doloremque doloribus! Recusandae nostrum dolore repudiandae libero mollitia, rem eveniet.</p>\r\n            </div>\r\n\r\n            <div class=\"c-tabs__pane u-pb-medium\" id=\"nav-contact\" role=\"tabpanel\" aria-labelledby=\"nav-contact-tab\">\r\n              <p class=\"u-mb-small\">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et quidem modi atque at aliquid expedita nemo incidunt exercitationem nihil sit. Laudantium suscipit id amet saepe ratione, accusamus. Voluptatum in, nam.</p>\r\n\r\n              <p class=\"u-mb-small\">Lorem ipsum dolor sit amet, consectetur adipisicing elit. A voluptate nobis tenetur mollitia incidunt quod, est veniam, earum nemo! Alias rerum saepe aut sapiente minus sunt doloribus tempora corrupti in!</p>\r\n            </div>\r\n\r\n            <div class=\"c-tabs__pane u-pb-medium\" id=\"nav-customer\" role=\"tabpanel\" aria-labelledby=\"nav-customer-tab\">\r\n              <p class=\"u-mb-small\">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et quidem modi atque at aliquid expedita nemo incidunt exercitationem nihil sit. Laudantium suscipit id amet saepe ratione, accusamus. Voluptatum in, nam.</p>\r\n\r\n              <p class=\"u-mb-small\">Lorem ipsum dolor sit amet, consectetur adipisicing elit. A voluptate nobis tenetur mollitia incidunt quod, est veniam, earum nemo! Alias rerum saepe aut sapiente minus sunt doloribus tempora corrupti in!</p>\r\n\r\n              <p class=\"u-mb-small\">Lorem ipsum dolor sit amet, consectetur adipisicing elit. A voluptate nobis tenetur mollitia incidunt quod, est veniam, earum nemo! Alias rerum saepe aut sapiente minus sunt doloribus tempora corrupti in!</p>\r\n\r\n              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et quidem modi atque at aliquid expedita nemo incidunt exercitationem nihil sit. Laudantium suscipit id amet saepe ratione, accusamus. Voluptatum in, nam.</p>\r\n            </div>\r\n          </div>\r\n\r\n        </div>\r\n\r\n        <div class=\"col-xl-5\">\r\n          <div class=\"c-card u-p-medium u-mb-medium\">\r\n\r\n            <div class=\"u-text-center\">\r\n              <div class=\"c-avatar c-avatar--large u-mb-small u-inline-flex\">\r\n                <img class=\"c-avatar__img\" src=\"assets/img/avatar-150.jpg\" alt=\"Adam's Face\">\r\n              </div>\r\n\r\n              <h3 class=\"u-h5\">Gerald Vaughn</h3>\r\n              <p>Freelance Designer, Previously TapQ</p>\r\n              <span class=\"u-text-mute u-text-small\">London, United Kingdom</span>\r\n            </div>\r\n\r\n            <div class=\"u-flex u-mt-medium\">\r\n              <a class=\"c-btn c-btn--info c-btn--fullwidth u-mr-xsmall\" >Edit Profile</a>\r\n              <a class=\"c-btn c-btn--secondary c-btn--fullwidth\" >View Profile As</a>\r\n            </div>\r\n\r\n            <table class=\"c-table u-text-center u-pv-small u-mt-medium u-border-right-zero u-border-left-zero\">\r\n              <thead>\r\n              <tr>\r\n                <th class=\"u-pt-small\">\r\n\r\n                  <div class=\"c-rating\">\r\n                    <i class=\"c-rating__icon is-active fa fa-star\"></i>\r\n                    <i class=\"c-rating__icon is-active fa fa-star\"></i>\r\n                    <i class=\"c-rating__icon is-active fa fa-star\"></i>\r\n                    <i class=\"c-rating__icon is-active fa fa-star\"></i>\r\n                    <i class=\"c-rating__icon fa fa-star\"></i>\r\n                  </div>\r\n\r\n                </th>\r\n                <th class=\"u-pt-small u-color-primary\">38</th>\r\n                <th class=\"u-pt-small u-color-primary\">54</th>\r\n                <th class=\"u-pt-small u-color-primary\">186</th>\r\n              </tr>\r\n              </thead>\r\n              <tbody>\r\n              <tr>\r\n                <td class=\"u-text-mute u-text-xsmall u-pb-small u-text-uppercase\">Rating</td>\r\n                <td class=\"u-text-mute u-text-xsmall u-pb-small u-text-uppercase\">Review</td>\r\n                <td class=\"u-text-mute u-text-xsmall u-pb-small u-text-uppercase\">Clients</td>\r\n                <td class=\"u-text-mute u-text-xsmall u-pb-small u-text-uppercase\">Finished Gigs</td>\r\n              </tr>\r\n              </tbody>\r\n            </table>\r\n\r\n            <div class=\"c-feed has-icons u-mt-medium\">\r\n              <div class=\"c-feed__item has-icon\">\r\n                <i class=\"c-feed__item-icon u-bg-info fa fa-tumblr\"></i>\r\n                <p>Product Designer</p>\r\n                <span class=\"c-feed__meta\">Tumblr- London, United Kingdom</span>\r\n              </div>\r\n\r\n              <div class=\"c-feed__item has-icon\">\r\n                <i class=\"c-feed__item-icon u-bg-fancy fa fa-dropbox\"></i>\r\n                <p>Intern</p>\r\n                <p class=\"c-feed__meta\">Dropbox - Berlin, Germany</p>\r\n              </div>\r\n            </div>\r\n\r\n            <div class=\"u-pt-medium u-border-top u-text-center\">\r\n              <a class=\"u-text-mute u-text-small\" href=\"https://workspace.com/geraldvaughn\">\r\n                <i class=\"fa fa-globe u-mr-xsmall\"></i>https://workspace.com/geraldvaughn\r\n              </a>\r\n            </div>\r\n          </div>\r\n\r\n          <div class=\"c-card u-p-medium u-mb-medium\">\r\n            <h5 class=\"u-mb-medium\">Billing</h5>\r\n\r\n            <table class=\"c-table u-border-zero\">\r\n              <thead class=\"c-table__head u-border-bottom\">\r\n              <tr>\r\n                <th class=\"c-table__cell u-text-left u-p-zero u-pb-medium u-flex u-align-items-center\">\r\n                  <img class=\"u-mr-xsmall\" src=\"assets/img/visa.png\" alt=\"Credit Card\">Ending **** 5896\r\n                </th>\r\n                <th class=\"c-table__cell u-text-right u-p-zero u-pb-medium\">05 / 19</th>\r\n                <th class=\"c-table__cell u-text-right u-p-zero u-pb-medium\">\r\n                  <span class=\"u-text-mute\">VISA</span>\r\n                </th>\r\n              </tr>\r\n              </thead>\r\n              <tbody class=\"u-pt-medium\">\r\n              <tr class=\"c-table__row u-border-zero\">\r\n                <td class=\"c-table__cell u-p-zero u-pb-xsmall u-pt-medium\">PRO Package</td>\r\n                <td class=\"c-table__cell u-p-zero u-pb-xsmall u-pt-medium\">\r\n                  <span class=\"u-text-mute\">Standard Payment</span>\r\n                </td>\r\n                <td class=\"c-table__cell u-p-zero u-pb-xsmall u-pt-medium u-text-right\">\r\n                  <span class=\"u-text-mute\">19 Jan 2018</span>\r\n                </td>\r\n              </tr>\r\n              <tr class=\"c-table__row u-border-zero\">\r\n                <td class=\"c-table__cell u-p-zero u-pb-xsmall\">Job Posted</td>\r\n                <td class=\"c-table__cell u-p-zero u-pb-xsmall\">\r\n                  <span class=\"u-text-mute\">30-Days Recuring</span>\r\n                </td>\r\n                <td class=\"c-table__cell u-p-zero u-pb-xsmall u-text-right\">\r\n                  <span class=\"u-text-mute\">24 Nov 2017</span>\r\n                </td>\r\n              </tr>\r\n              <tr class=\"c-table__row u-border-zero\">\r\n                <td class=\"c-table__cell u-p-zero u-pb-xsmall\">PRO Package</td>\r\n                <td class=\"c-table__cell u-p-zero u-pb-xsmall\">\r\n                  <span class=\"u-text-mute\">Gift</span>\r\n                </td>\r\n                <td class=\"c-table__cell u-p-zero u-pb-xsmall u-text-right\">\r\n                  <span class=\"u-text-mute\">7 Mar 2017</span>\r\n                </td>\r\n              </tr>\r\n              </tbody>\r\n            </table>\r\n          </div>\r\n        </div>\r\n\r\n      </div>\r\n\r\n    </div><!-- // .container-fluid -->\r\n  </main>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/admin/user-page/user-page.component.scss":
/*!**********************************************************!*\
  !*** ./src/app/admin/user-page/user-page.component.scss ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/admin/user-page/user-page.component.ts":
/*!********************************************************!*\
  !*** ./src/app/admin/user-page/user-page.component.ts ***!
  \********************************************************/
/*! exports provided: UserPageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserPageComponent", function() { return UserPageComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var UserPageComponent = /** @class */ (function () {
    function UserPageComponent() {
    }
    UserPageComponent.prototype.ngOnInit = function () {
    };
    UserPageComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-user-page',
            template: __webpack_require__(/*! ./user-page.component.html */ "./src/app/admin/user-page/user-page.component.html"),
            styles: [__webpack_require__(/*! ./user-page.component.scss */ "./src/app/admin/user-page/user-page.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], UserPageComponent);
    return UserPageComponent;
}());



/***/ }),

/***/ "./src/app/admin/users-page/users-page.component.html":
/*!************************************************************!*\
  !*** ./src/app/admin/users-page/users-page.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"o-page\">\r\n  <app-sidebar></app-sidebar>\r\n  <main class=\"o-page__content\">\r\n    <app-navbar></app-navbar>\r\n    <div class=\"container-fluid page-content\">\r\n      <div class=\"row\">\r\n        <div class=\"col-xl-3\">\r\n          <app-management-dashboard-side-menu [menuConfig]=\"menuConfig\" (action)=\"sideMenuAction($event)\"></app-management-dashboard-side-menu>\r\n\r\n        </div>\r\n\r\n        <div class=\"col-sm-12 col-xl-9\" infiniteScroll\r\n             [infiniteScrollDistance]=\"2\"\r\n             [infiniteScrollThrottle]=\"300\"\r\n             (scrolled)=\"onScroll()\">\r\n          <main>\r\n            <div class=\"row\">\r\n              <div class=\"col-8 col-xl-10\">\r\n                <h1 class=\"u-h3\">{{'THE_STAFF' |translate}}</h1>\r\n              </div>\r\n              <div class=\"col-4 col-xl-2\">\r\n                <div class=\"c-field u-mb-small\">\r\n                  <app-filter-input class=\"text-right\" (filterEvent)=\"filterEvent($event)\" ></app-filter-input>\r\n\r\n                </div>\r\n              </div>\r\n            </div><!-- // .row -->\r\n\r\n            <div class=\"row\">\r\n              <app-card-user class=\"col-md-6\" *ngFor=\"let user of displayedUsers | filter: {name: filter}\" [data]=\"user\">\r\n\r\n              </app-card-user>\r\n            </div><!-- // .row -->\r\n\r\n          </main>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </main>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/admin/users-page/users-page.component.scss":
/*!************************************************************!*\
  !*** ./src/app/admin/users-page/users-page.component.scss ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/admin/users-page/users-page.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/admin/users-page/users-page.component.ts ***!
  \**********************************************************/
/*! exports provided: UsersPageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsersPageComponent", function() { return UsersPageComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _models_user__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../models/user */ "./src/app/models/user.ts");
/* harmony import */ var _models_user_company_role__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../models/user-company-role */ "./src/app/models/user-company-role.ts");
/* harmony import */ var _models_event__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../models/event */ "./src/app/models/event.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var UsersPageComponent = /** @class */ (function () {
    function UsersPageComponent(router) {
        this.router = router;
        this.filter = '';
        this.users = [];
        this.displayedUsers = [];
        this.menuConfig = [{
                name: 'MENU',
                items: [{
                        name: 'ADD_NEW_USER',
                        action: { action: 'createEvent' },
                        icon: 'plus'
                    }]
            }, {
                name: 'FILTERS',
                items: [{
                        name: 'ALL',
                        action: { action: 'displayAll' },
                        image: 'assets/img/sidebar-icon1.png'
                    }, {
                        name: 'ADMIN',
                        action: { action: 'displayAdmin' },
                        image: 'assets/img/sidebar-icon2.png'
                    }, {
                        name: 'BAR_ADMIN',
                        action: { action: 'displayBarManager' },
                        image: 'assets/img/sidebar-icon3.png'
                    }, {
                        name: 'DOOR',
                        action: { action: 'displayDoor' },
                        image: 'assets/img/sidebar-icon4.png'
                    }, {
                        name: 'BAR',
                        action: { action: 'displayBar' },
                        image: 'assets/img/sidebar-icon5.png'
                    }]
            }];
    }
    UsersPageComponent.prototype.ngOnInit = function () {
        this.load();
        this.displayAll();
    };
    UsersPageComponent.prototype.load = function () {
        this.users = Array(10).fill(new _models_user__WEBPACK_IMPORTED_MODULE_1__["User"]());
    };
    UsersPageComponent.prototype.displayAll = function () {
        this.displayedUsers = this.users;
    };
    UsersPageComponent.prototype.displayAdmin = function () {
        this.displayedUsers = this.users.filter(function (el) { return el.company_role.role === _models_user_company_role__WEBPACK_IMPORTED_MODULE_2__["USER_ROLES"][0]; });
    };
    UsersPageComponent.prototype.displayDoor = function () {
        this.displayedUsers = this.users.filter(function (el) { return el.company_role.role === _models_user_company_role__WEBPACK_IMPORTED_MODULE_2__["USER_ROLES"][1]; });
    };
    UsersPageComponent.prototype.displayBarManager = function () {
        this.displayedUsers = this.users.filter(function (el) { return el.company_role.role === _models_user_company_role__WEBPACK_IMPORTED_MODULE_2__["USER_ROLES"][2]; });
    };
    UsersPageComponent.prototype.displayBar = function () {
        this.displayedUsers = this.users.filter(function (el) { return el.company_role.role === _models_user_company_role__WEBPACK_IMPORTED_MODULE_2__["USER_ROLES"][3]; });
    };
    UsersPageComponent.prototype.createUser = function () {
        this.users.push(new _models_event__WEBPACK_IMPORTED_MODULE_3__["Event"]());
        this.openUser(1);
    };
    UsersPageComponent.prototype.openUser = function (id) {
        this.router.navigate(["event-management/user/" + id]);
    };
    // COMPONENT COMUNICATION
    UsersPageComponent.prototype.filterEvent = function (event) {
        this.filter = event;
    };
    UsersPageComponent.prototype.sideMenuAction = function (event) {
        if (event.action && event.action.action) {
            this[event.action.action](event.action.data);
        }
    };
    UsersPageComponent.prototype.onScroll = function () { };
    UsersPageComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-users-page',
            template: __webpack_require__(/*! ./users-page.component.html */ "./src/app/admin/users-page/users-page.component.html"),
            styles: [__webpack_require__(/*! ./users-page.component.scss */ "./src/app/admin/users-page/users-page.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])
    ], UsersPageComponent);
    return UsersPageComponent;
}());



/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _landing_home_page_home_page_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./landing/home-page/home-page.component */ "./src/app/landing/home-page/home-page.component.ts");
/* harmony import */ var _landing_login_page_login_page_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./landing/login-page/login-page.component */ "./src/app/landing/login-page/login-page.component.ts");
/* harmony import */ var _bar_management_system_kanban_page_kanban_page_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./bar-management-system/kanban-page/kanban-page.component */ "./src/app/bar-management-system/kanban-page/kanban-page.component.ts");
/* harmony import */ var _bar_management_system_event_checkin_page_event_checkin_page_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./bar-management-system/event-checkin-page/event-checkin-page.component */ "./src/app/bar-management-system/event-checkin-page/event-checkin-page.component.ts");
/* harmony import */ var _bar_management_system_bar_checkin_page_bar_checkin_page_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./bar-management-system/bar-checkin-page/bar-checkin-page.component */ "./src/app/bar-management-system/bar-checkin-page/bar-checkin-page.component.ts");
/* harmony import */ var _bar_management_system_stock_management_page_stock_management_page_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./bar-management-system/stock-management-page/stock-management-page.component */ "./src/app/bar-management-system/stock-management-page/stock-management-page.component.ts");
/* harmony import */ var _admin_user_page_user_page_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./admin/user-page/user-page.component */ "./src/app/admin/user-page/user-page.component.ts");
/* harmony import */ var _admin_dashboard_page_dashboard_page_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./admin/dashboard-page/dashboard-page.component */ "./src/app/admin/dashboard-page/dashboard-page.component.ts");
/* harmony import */ var _admin_users_page_users_page_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./admin/users-page/users-page.component */ "./src/app/admin/users-page/users-page.component.ts");
/* harmony import */ var _admin_events_page_events_page_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./admin/events-page/events-page.component */ "./src/app/admin/events-page/events-page.component.ts");
/* harmony import */ var _admin_orders_page_orders_page_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./admin/orders-page/orders-page.component */ "./src/app/admin/orders-page/orders-page.component.ts");
/* harmony import */ var _admin_items_page_items_page_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./admin/items-page/items-page.component */ "./src/app/admin/items-page/items-page.component.ts");
/* harmony import */ var _admin_menus_page_menus_page_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./admin/menus-page/menus-page.component */ "./src/app/admin/menus-page/menus-page.component.ts");
/* harmony import */ var _admin_places_page_places_page_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./admin/places-page/places-page.component */ "./src/app/admin/places-page/places-page.component.ts");
/* harmony import */ var _admin_place_page_place_page_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./admin/place-page/place-page.component */ "./src/app/admin/place-page/place-page.component.ts");
/* harmony import */ var _admin_bars_page_bars_page_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./admin/bars-page/bars-page.component */ "./src/app/admin/bars-page/bars-page.component.ts");
/* harmony import */ var _admin_event_page_event_page_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./admin/event-page/event-page.component */ "./src/app/admin/event-page/event-page.component.ts");
/* harmony import */ var _admin_order_page_order_page_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./admin/order-page/order-page.component */ "./src/app/admin/order-page/order-page.component.ts");
/* harmony import */ var _admin_menu_page_menu_page_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./admin/menu-page/menu-page.component */ "./src/app/admin/menu-page/menu-page.component.ts");
/* harmony import */ var _admin_category_page_category_page_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./admin/category-page/category-page.component */ "./src/app/admin/category-page/category-page.component.ts");
/* harmony import */ var _admin_pictures_page_pictures_page_component__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./admin/pictures-page/pictures-page.component */ "./src/app/admin/pictures-page/pictures-page.component.ts");
/* harmony import */ var _admin_profile_page_profile_page_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./admin/profile-page/profile-page.component */ "./src/app/admin/profile-page/profile-page.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
























var routes = [{
        path: '',
        component: _landing_home_page_home_page_component__WEBPACK_IMPORTED_MODULE_2__["HomePageComponent"]
    }, {
        path: 'login',
        component: _landing_login_page_login_page_component__WEBPACK_IMPORTED_MODULE_3__["LoginPageComponent"]
    }, {
        path: 'bar-management/check-in-event',
        component: _bar_management_system_event_checkin_page_event_checkin_page_component__WEBPACK_IMPORTED_MODULE_5__["EventCheckinPageComponent"]
    }, {
        path: 'bar-management/check-in-bar',
        component: _bar_management_system_bar_checkin_page_bar_checkin_page_component__WEBPACK_IMPORTED_MODULE_6__["BarCheckinPageComponent"]
    }, {
        path: 'bar-management/board',
        component: _bar_management_system_kanban_page_kanban_page_component__WEBPACK_IMPORTED_MODULE_4__["KanbanPageComponent"]
    }, {
        path: 'bar-management/stock-management',
        component: _bar_management_system_stock_management_page_stock_management_page_component__WEBPACK_IMPORTED_MODULE_7__["StockManagementPageComponent"]
    }, {
        path: 'event-management/dashboard',
        component: _admin_dashboard_page_dashboard_page_component__WEBPACK_IMPORTED_MODULE_9__["DashboardPageComponent"]
    }, {
        path: 'event-management/users',
        component: _admin_users_page_users_page_component__WEBPACK_IMPORTED_MODULE_10__["UsersPageComponent"]
    }, {
        path: 'event-management/events',
        component: _admin_events_page_events_page_component__WEBPACK_IMPORTED_MODULE_11__["EventsPageComponent"]
    }, {
        path: 'event-management/orders',
        component: _admin_orders_page_orders_page_component__WEBPACK_IMPORTED_MODULE_12__["OrdersPageComponent"]
    }, {
        path: 'event-management/items',
        component: _admin_items_page_items_page_component__WEBPACK_IMPORTED_MODULE_13__["ItemsPageComponent"]
    }, {
        path: 'event-management/menus',
        component: _admin_menus_page_menus_page_component__WEBPACK_IMPORTED_MODULE_14__["MenusPageComponent"]
    }, {
        path: 'event-management/places',
        component: _admin_places_page_places_page_component__WEBPACK_IMPORTED_MODULE_15__["PlacesPageComponent"]
    }, {
        path: 'event-management/place/:id',
        component: _admin_place_page_place_page_component__WEBPACK_IMPORTED_MODULE_16__["PlacePageComponent"]
    }, {
        path: 'event-management/bars',
        component: _admin_bars_page_bars_page_component__WEBPACK_IMPORTED_MODULE_17__["BarsPageComponent"]
    }, {
        path: 'event-management/user/:id',
        component: _admin_user_page_user_page_component__WEBPACK_IMPORTED_MODULE_8__["UserPageComponent"]
    }, {
        path: 'event-management/event/:id',
        component: _admin_event_page_event_page_component__WEBPACK_IMPORTED_MODULE_18__["EventPageComponent"]
    }, {
        path: 'event-management/order/:id',
        component: _admin_order_page_order_page_component__WEBPACK_IMPORTED_MODULE_19__["OrderPageComponent"]
    }, {
        path: 'event-management/menu/:id',
        component: _admin_menu_page_menu_page_component__WEBPACK_IMPORTED_MODULE_20__["MenuPageComponent"]
    }, {
        path: 'event-management/categories',
        component: _admin_category_page_category_page_component__WEBPACK_IMPORTED_MODULE_21__["CategoryPageComponent"]
    }, {
        path: 'event-management/pictures',
        component: _admin_pictures_page_pictures_page_component__WEBPACK_IMPORTED_MODULE_22__["PicturesPageComponent"]
    }, {
        path: 'event-management/profile',
        component: _admin_profile_page_profile_page_component__WEBPACK_IMPORTED_MODULE_23__["ProfilePageComponent"]
    }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>\r\n"

/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm5/ionic-storage.js");
/* harmony import */ var ngx_bootstrap_utils__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-bootstrap/utils */ "./node_modules/ngx-bootstrap/utils/fesm5/ngx-bootstrap-utils.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AppComponent = /** @class */ (function () {
    function AppComponent(translate, storage) {
        this.storage = storage;
        this.title = 'app';
        this.storage.ready().then(function () { return console.log('Storage ready'); });
        Object(ngx_bootstrap_utils__WEBPACK_IMPORTED_MODULE_3__["setTheme"])('bs4'); // or 'bs4'
        // this language will be used as a fallback when a translation isn't found in the current language
        translate.setDefaultLang('pt-PT');
        // the lang to use, if the lang isn't available, it will use the current loader to get them
        translate.use('pt-PT');
    }
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")]
        }),
        __metadata("design:paramtypes", [_ngx_translate_core__WEBPACK_IMPORTED_MODULE_1__["TranslateService"], _ionic_storage__WEBPACK_IMPORTED_MODULE_2__["Storage"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: createTranslateLoader, getAuthServiceConfigs, AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "createTranslateLoader", function() { return createTranslateLoader; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getAuthServiceConfigs", function() { return getAuthServiceConfigs; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ngx_webstorage__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ngx-webstorage */ "./node_modules/ngx-webstorage/dist/app.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var ng2_dragula__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ng2-dragula */ "./node_modules/ng2-dragula/dist/fesm5/ng2-dragula.js");
/* harmony import */ var angular_6_datatable__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! angular-6-datatable */ "./node_modules/angular-6-datatable/index.js");
/* harmony import */ var angular_6_datatable__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(angular_6_datatable__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm5/ngx-bootstrap-modal.js");
/* harmony import */ var ngx_bootstrap_dropdown__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ngx-bootstrap/dropdown */ "./node_modules/ngx-bootstrap/dropdown/fesm5/ngx-bootstrap-dropdown.js");
/* harmony import */ var ngx_bootstrap_typeahead__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ngx-bootstrap/typeahead */ "./node_modules/ngx-bootstrap/typeahead/fesm5/ngx-bootstrap-typeahead.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var _ngx_translate_http_loader__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @ngx-translate/http-loader */ "./node_modules/@ngx-translate/http-loader/esm5/ngx-translate-http-loader.js");
/* harmony import */ var ngx_infinite_scroll__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ngx-infinite-scroll */ "./node_modules/ngx-infinite-scroll/modules/ngx-infinite-scroll.es5.js");
/* harmony import */ var angular_froala_wysiwyg__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! angular-froala-wysiwyg */ "./node_modules/angular-froala-wysiwyg/index.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm5/ionic-storage.js");
/* harmony import */ var angular_6_social_login__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! angular-6-social-login */ "./node_modules/angular-6-social-login/angular-6-social-login.umd.js");
/* harmony import */ var angular_6_social_login__WEBPACK_IMPORTED_MODULE_17___default = /*#__PURE__*/__webpack_require__.n(angular_6_social_login__WEBPACK_IMPORTED_MODULE_17__);
/* harmony import */ var _landing_home_page_home_page_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./landing/home-page/home-page.component */ "./src/app/landing/home-page/home-page.component.ts");
/* harmony import */ var _landing_login_page_login_page_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./landing/login-page/login-page.component */ "./src/app/landing/login-page/login-page.component.ts");
/* harmony import */ var _landing_options_page_options_page_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./landing/options-page/options-page.component */ "./src/app/landing/options-page/options-page.component.ts");
/* harmony import */ var _bar_management_system_kanban_page_kanban_page_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./bar-management-system/kanban-page/kanban-page.component */ "./src/app/bar-management-system/kanban-page/kanban-page.component.ts");
/* harmony import */ var _components_browser_happy_browser_happy_component__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./components/browser-happy/browser-happy.component */ "./src/app/components/browser-happy/browser-happy.component.ts");
/* harmony import */ var _bar_management_system_bar_nav_bar_nav_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./bar-management-system/bar-nav/bar-nav.component */ "./src/app/bar-management-system/bar-nav/bar-nav.component.ts");
/* harmony import */ var _components_filter_input_filter_input_component__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./components/filter-input/filter-input.component */ "./src/app/components/filter-input/filter-input.component.ts");
/* harmony import */ var _components_profile_avatar_profile_avatar_component__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./components/profile-avatar/profile-avatar.component */ "./src/app/components/profile-avatar/profile-avatar.component.ts");
/* harmony import */ var _bar_management_system_card_bar_card_bar_component__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ./bar-management-system/card-bar/card-bar.component */ "./src/app/bar-management-system/card-bar/card-bar.component.ts");
/* harmony import */ var _bar_management_system_card_bar_user_icon_card_bar_user_icon_component__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ./bar-management-system/card-bar-user-icon/card-bar-user-icon.component */ "./src/app/bar-management-system/card-bar-user-icon/card-bar-user-icon.component.ts");
/* harmony import */ var _bar_management_system_kanban_board_item_kanban_board_item_component__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! ./bar-management-system/kanban-board-item/kanban-board-item.component */ "./src/app/bar-management-system/kanban-board-item/kanban-board-item.component.ts");
/* harmony import */ var _bar_management_system_bar_checkin_page_bar_checkin_page_component__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! ./bar-management-system/bar-checkin-page/bar-checkin-page.component */ "./src/app/bar-management-system/bar-checkin-page/bar-checkin-page.component.ts");
/* harmony import */ var _bar_management_system_event_checkin_page_event_checkin_page_component__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! ./bar-management-system/event-checkin-page/event-checkin-page.component */ "./src/app/bar-management-system/event-checkin-page/event-checkin-page.component.ts");
/* harmony import */ var _bar_management_system_card_event_card_event_component__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! ./bar-management-system/card-event/card-event.component */ "./src/app/bar-management-system/card-event/card-event.component.ts");
/* harmony import */ var _admin_users_page_users_page_component__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! ./admin/users-page/users-page.component */ "./src/app/admin/users-page/users-page.component.ts");
/* harmony import */ var _admin_events_page_events_page_component__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(/*! ./admin/events-page/events-page.component */ "./src/app/admin/events-page/events-page.component.ts");
/* harmony import */ var _admin_places_page_places_page_component__WEBPACK_IMPORTED_MODULE_34__ = __webpack_require__(/*! ./admin/places-page/places-page.component */ "./src/app/admin/places-page/places-page.component.ts");
/* harmony import */ var _admin_bars_page_bars_page_component__WEBPACK_IMPORTED_MODULE_35__ = __webpack_require__(/*! ./admin/bars-page/bars-page.component */ "./src/app/admin/bars-page/bars-page.component.ts");
/* harmony import */ var _admin_orders_page_orders_page_component__WEBPACK_IMPORTED_MODULE_36__ = __webpack_require__(/*! ./admin/orders-page/orders-page.component */ "./src/app/admin/orders-page/orders-page.component.ts");
/* harmony import */ var _admin_user_page_user_page_component__WEBPACK_IMPORTED_MODULE_37__ = __webpack_require__(/*! ./admin/user-page/user-page.component */ "./src/app/admin/user-page/user-page.component.ts");
/* harmony import */ var _admin_event_page_event_page_component__WEBPACK_IMPORTED_MODULE_38__ = __webpack_require__(/*! ./admin/event-page/event-page.component */ "./src/app/admin/event-page/event-page.component.ts");
/* harmony import */ var _admin_order_page_order_page_component__WEBPACK_IMPORTED_MODULE_39__ = __webpack_require__(/*! ./admin/order-page/order-page.component */ "./src/app/admin/order-page/order-page.component.ts");
/* harmony import */ var _admin_sidebar_sidebar_component__WEBPACK_IMPORTED_MODULE_40__ = __webpack_require__(/*! ./admin/sidebar/sidebar.component */ "./src/app/admin/sidebar/sidebar.component.ts");
/* harmony import */ var _admin_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_41__ = __webpack_require__(/*! ./admin/navbar/navbar.component */ "./src/app/admin/navbar/navbar.component.ts");
/* harmony import */ var _admin_pictures_page_pictures_page_component__WEBPACK_IMPORTED_MODULE_42__ = __webpack_require__(/*! ./admin/pictures-page/pictures-page.component */ "./src/app/admin/pictures-page/pictures-page.component.ts");
/* harmony import */ var _admin_menus_page_menus_page_component__WEBPACK_IMPORTED_MODULE_43__ = __webpack_require__(/*! ./admin/menus-page/menus-page.component */ "./src/app/admin/menus-page/menus-page.component.ts");
/* harmony import */ var _admin_menu_page_menu_page_component__WEBPACK_IMPORTED_MODULE_44__ = __webpack_require__(/*! ./admin/menu-page/menu-page.component */ "./src/app/admin/menu-page/menu-page.component.ts");
/* harmony import */ var _bar_management_system_stock_management_page_stock_management_page_component__WEBPACK_IMPORTED_MODULE_45__ = __webpack_require__(/*! ./bar-management-system/stock-management-page/stock-management-page.component */ "./src/app/bar-management-system/stock-management-page/stock-management-page.component.ts");
/* harmony import */ var _components_stock_list_item_stock_list_item_component__WEBPACK_IMPORTED_MODULE_46__ = __webpack_require__(/*! ./components/stock-list-item/stock-list-item.component */ "./src/app/components/stock-list-item/stock-list-item.component.ts");
/* harmony import */ var ngx_moment__WEBPACK_IMPORTED_MODULE_47__ = __webpack_require__(/*! ngx-moment */ "./node_modules/ngx-moment/fesm5/ngx-moment.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_48__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _pipes_filter_pipe__WEBPACK_IMPORTED_MODULE_49__ = __webpack_require__(/*! ./pipes/filter.pipe */ "./src/app/pipes/filter.pipe.ts");
/* harmony import */ var _admin_dashboard_page_dashboard_page_component__WEBPACK_IMPORTED_MODULE_50__ = __webpack_require__(/*! ./admin/dashboard-page/dashboard-page.component */ "./src/app/admin/dashboard-page/dashboard-page.component.ts");
/* harmony import */ var _admin_items_page_items_page_component__WEBPACK_IMPORTED_MODULE_51__ = __webpack_require__(/*! ./admin/items-page/items-page.component */ "./src/app/admin/items-page/items-page.component.ts");
/* harmony import */ var _admin_place_page_place_page_component__WEBPACK_IMPORTED_MODULE_52__ = __webpack_require__(/*! ./admin/place-page/place-page.component */ "./src/app/admin/place-page/place-page.component.ts");
/* harmony import */ var _admin_category_page_category_page_component__WEBPACK_IMPORTED_MODULE_53__ = __webpack_require__(/*! ./admin/category-page/category-page.component */ "./src/app/admin/category-page/category-page.component.ts");
/* harmony import */ var _components_dashboard_sidebar_list_item_dashboard_sidebar_list_item_component__WEBPACK_IMPORTED_MODULE_54__ = __webpack_require__(/*! ./components/dashboard-sidebar-list-item/dashboard-sidebar-list-item.component */ "./src/app/components/dashboard-sidebar-list-item/dashboard-sidebar-list-item.component.ts");
/* harmony import */ var _admin_profile_page_profile_page_component__WEBPACK_IMPORTED_MODULE_55__ = __webpack_require__(/*! ./admin/profile-page/profile-page.component */ "./src/app/admin/profile-page/profile-page.component.ts");
/* harmony import */ var _components_management_dashboard_side_menu_management_dashboard_side_menu_component__WEBPACK_IMPORTED_MODULE_56__ = __webpack_require__(/*! ./components/management-dashboard-side-menu/management-dashboard-side-menu.component */ "./src/app/components/management-dashboard-side-menu/management-dashboard-side-menu.component.ts");
/* harmony import */ var _components_event_list_item_event_list_item_component__WEBPACK_IMPORTED_MODULE_57__ = __webpack_require__(/*! ./components/event-list-item/event-list-item.component */ "./src/app/components/event-list-item/event-list-item.component.ts");
/* harmony import */ var _components_place_card_place_card_component__WEBPACK_IMPORTED_MODULE_58__ = __webpack_require__(/*! ./components/place-card/place-card.component */ "./src/app/components/place-card/place-card.component.ts");
/* harmony import */ var _components_card_user_card_user_component__WEBPACK_IMPORTED_MODULE_59__ = __webpack_require__(/*! ./components/card-user/card-user.component */ "./src/app/components/card-user/card-user.component.ts");
/* harmony import */ var _components_income_sumary_income_sumary_component__WEBPACK_IMPORTED_MODULE_60__ = __webpack_require__(/*! ./components/income-sumary/income-sumary.component */ "./src/app/components/income-sumary/income-sumary.component.ts");
/* harmony import */ var _components_profit_sumary_profit_sumary_component__WEBPACK_IMPORTED_MODULE_61__ = __webpack_require__(/*! ./components/profit-sumary/profit-sumary.component */ "./src/app/components/profit-sumary/profit-sumary.component.ts");
/* harmony import */ var _components_order_list_item_order_list_item_component__WEBPACK_IMPORTED_MODULE_62__ = __webpack_require__(/*! ./components/order-list-item/order-list-item.component */ "./src/app/components/order-list-item/order-list-item.component.ts");
/* harmony import */ var _ctrl_ngx_chartjs__WEBPACK_IMPORTED_MODULE_63__ = __webpack_require__(/*! @ctrl/ngx-chartjs */ "./node_modules/@ctrl/ngx-chartjs/fesm5/ctrl-ngx-chartjs.js");
/* harmony import */ var ngx_dropzone_wrapper__WEBPACK_IMPORTED_MODULE_64__ = __webpack_require__(/*! ngx-dropzone-wrapper */ "./node_modules/ngx-dropzone-wrapper/dist/ngx-dropzone-wrapper.es5.js");
/* harmony import */ var _components_image_upload_image_upload_component__WEBPACK_IMPORTED_MODULE_65__ = __webpack_require__(/*! ./components/image-upload/image-upload.component */ "./src/app/components/image-upload/image-upload.component.ts");
/* harmony import */ var _components_file_upload_file_upload_component__WEBPACK_IMPORTED_MODULE_66__ = __webpack_require__(/*! ./components/file-upload/file-upload.component */ "./src/app/components/file-upload/file-upload.component.ts");
/* harmony import */ var _components_invoice_single_item_list_item_invoice_single_item_list_item_component__WEBPACK_IMPORTED_MODULE_67__ = __webpack_require__(/*! ./components/invoice-single-item-list-item/invoice-single-item-list-item.component */ "./src/app/components/invoice-single-item-list-item/invoice-single-item-list-item.component.ts");
/* harmony import */ var _components_modals_modal_category_modal_category_component__WEBPACK_IMPORTED_MODULE_68__ = __webpack_require__(/*! ./components/modals/modal-category/modal-category.component */ "./src/app/components/modals/modal-category/modal-category.component.ts");
/* harmony import */ var _components_category_list_item_category_list_item_component__WEBPACK_IMPORTED_MODULE_69__ = __webpack_require__(/*! ./components/category-list-item/category-list-item.component */ "./src/app/components/category-list-item/category-list-item.component.ts");
/* harmony import */ var _components_modals_modal_item_modal_item_component__WEBPACK_IMPORTED_MODULE_70__ = __webpack_require__(/*! ./components/modals/modal-item/modal-item.component */ "./src/app/components/modals/modal-item/modal-item.component.ts");
/* harmony import */ var _components_item_list_item_item_list_item_component__WEBPACK_IMPORTED_MODULE_71__ = __webpack_require__(/*! ./components/item-list-item/item-list-item.component */ "./src/app/components/item-list-item/item-list-item.component.ts");
/* harmony import */ var _components_menu_list_item_menu_list_item_component__WEBPACK_IMPORTED_MODULE_72__ = __webpack_require__(/*! ./components/menu-list-item/menu-list-item.component */ "./src/app/components/menu-list-item/menu-list-item.component.ts");
/* harmony import */ var _components_modals_modal_menu_modal_menu_component__WEBPACK_IMPORTED_MODULE_73__ = __webpack_require__(/*! ./components/modals/modal-menu/modal-menu.component */ "./src/app/components/modals/modal-menu/modal-menu.component.ts");
/* harmony import */ var _components_inline_input_inline_input_component__WEBPACK_IMPORTED_MODULE_74__ = __webpack_require__(/*! ./components/inline-input/inline-input.component */ "./src/app/components/inline-input/inline-input.component.ts");
/* harmony import */ var _components_status_bar_status_bar_component__WEBPACK_IMPORTED_MODULE_75__ = __webpack_require__(/*! ./components/status-bar/status-bar.component */ "./src/app/components/status-bar/status-bar.component.ts");
/* harmony import */ var _components_sidebar_members_sidebar_members_component__WEBPACK_IMPORTED_MODULE_76__ = __webpack_require__(/*! ./components/sidebar-members/sidebar-members.component */ "./src/app/components/sidebar-members/sidebar-members.component.ts");
/* harmony import */ var _components_sidebar_bars_sidebar_bars_component__WEBPACK_IMPORTED_MODULE_77__ = __webpack_require__(/*! ./components/sidebar-bars/sidebar-bars.component */ "./src/app/components/sidebar-bars/sidebar-bars.component.ts");
/* harmony import */ var _components_sidebar_details_sidebar_details_component__WEBPACK_IMPORTED_MODULE_78__ = __webpack_require__(/*! ./components/sidebar-details/sidebar-details.component */ "./src/app/components/sidebar-details/sidebar-details.component.ts");
/* harmony import */ var _components_event_info_management_card_event_info_management_card_component__WEBPACK_IMPORTED_MODULE_79__ = __webpack_require__(/*! ./components/event-info-management-card/event-info-management-card.component */ "./src/app/components/event-info-management-card/event-info-management-card.component.ts");
/* harmony import */ var _components_event_statistics_card_event_statistics_card_component__WEBPACK_IMPORTED_MODULE_80__ = __webpack_require__(/*! ./components/event-statistics-card/event-statistics-card.component */ "./src/app/components/event-statistics-card/event-statistics-card.component.ts");
/* harmony import */ var _components_event_tickets_card_event_tickets_card_component__WEBPACK_IMPORTED_MODULE_81__ = __webpack_require__(/*! ./components/event-tickets-card/event-tickets-card.component */ "./src/app/components/event-tickets-card/event-tickets-card.component.ts");
/* harmony import */ var _components_modals_modal_mce_modal_mce_component__WEBPACK_IMPORTED_MODULE_82__ = __webpack_require__(/*! ./components/modals/modal-mce/modal-mce.component */ "./src/app/components/modals/modal-mce/modal-mce.component.ts");
/* harmony import */ var _components_inline_mce_inline_mce_component__WEBPACK_IMPORTED_MODULE_83__ = __webpack_require__(/*! ./components/inline-mce/inline-mce.component */ "./src/app/components/inline-mce/inline-mce.component.ts");
/* harmony import */ var _components_no_content_panel_no_content_panel_component__WEBPACK_IMPORTED_MODULE_84__ = __webpack_require__(/*! ./components/no-content-panel/no-content-panel.component */ "./src/app/components/no-content-panel/no-content-panel.component.ts");
/* harmony import */ var _directives_add_user_add_user_directive__WEBPACK_IMPORTED_MODULE_85__ = __webpack_require__(/*! ./directives/add-user/add-user.directive */ "./src/app/directives/add-user/add-user.directive.ts");
/* harmony import */ var _directives_add_place_add_place_directive__WEBPACK_IMPORTED_MODULE_86__ = __webpack_require__(/*! ./directives/add-place/add-place.directive */ "./src/app/directives/add-place/add-place.directive.ts");
/* harmony import */ var _directives_add_photo_add_photo_directive__WEBPACK_IMPORTED_MODULE_87__ = __webpack_require__(/*! ./directives/add-photo/add-photo.directive */ "./src/app/directives/add-photo/add-photo.directive.ts");
/* harmony import */ var _components_image_grid_image_grid_component__WEBPACK_IMPORTED_MODULE_88__ = __webpack_require__(/*! ./components/image-grid/image-grid.component */ "./src/app/components/image-grid/image-grid.component.ts");
/* harmony import */ var _components_image_gallery_image_gallery_component__WEBPACK_IMPORTED_MODULE_89__ = __webpack_require__(/*! ./components/image-gallery/image-gallery.component */ "./src/app/components/image-gallery/image-gallery.component.ts");
/* harmony import */ var _directives_add_bar_add_bar_directive__WEBPACK_IMPORTED_MODULE_90__ = __webpack_require__(/*! ./directives/add-bar/add-bar.directive */ "./src/app/directives/add-bar/add-bar.directive.ts");
/* harmony import */ var _components_modals_modal_add_staff_modal_add_staff_component__WEBPACK_IMPORTED_MODULE_91__ = __webpack_require__(/*! ./components/modals/modal-add-staff/modal-add-staff.component */ "./src/app/components/modals/modal-add-staff/modal-add-staff.component.ts");
/* harmony import */ var _components_modals_modal_add_place_modal_add_place_component__WEBPACK_IMPORTED_MODULE_92__ = __webpack_require__(/*! ./components/modals/modal-add-place/modal-add-place.component */ "./src/app/components/modals/modal-add-place/modal-add-place.component.ts");
/* harmony import */ var _directives_add_staff_add_staff_directive__WEBPACK_IMPORTED_MODULE_93__ = __webpack_require__(/*! ./directives/add-staff/add-staff.directive */ "./src/app/directives/add-staff/add-staff.directive.ts");
/* harmony import */ var ngx_google_places_autocomplete__WEBPACK_IMPORTED_MODULE_94__ = __webpack_require__(/*! ngx-google-places-autocomplete */ "./node_modules/ngx-google-places-autocomplete/bundles/ngx-google-places-autocomplete.umd.js");
/* harmony import */ var ngx_google_places_autocomplete__WEBPACK_IMPORTED_MODULE_94___default = /*#__PURE__*/__webpack_require__.n(ngx_google_places_autocomplete__WEBPACK_IMPORTED_MODULE_94__);
/* harmony import */ var _components_place_info_place_info_component__WEBPACK_IMPORTED_MODULE_95__ = __webpack_require__(/*! ./components/place-info/place-info.component */ "./src/app/components/place-info/place-info.component.ts");
/* harmony import */ var _defaults__WEBPACK_IMPORTED_MODULE_96__ = __webpack_require__(/*! ./defaults */ "./src/app/defaults.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


































































































var DEFAULT_DROPZONE_CONFIG = {
    // Change this to your upload POST address:
    url: 'https://httpbin.org/post',
    maxFilesize: 50,
    acceptedFiles: 'image/*'
};
// AoT requires an exported function for factories
function createTranslateLoader(http) {
    return new _ngx_translate_http_loader__WEBPACK_IMPORTED_MODULE_13__["TranslateHttpLoader"](http, './assets/i18n/', '.json');
}
function getAuthServiceConfigs() {
    var config = new angular_6_social_login__WEBPACK_IMPORTED_MODULE_17__["AuthServiceConfig"]([
        {
            id: angular_6_social_login__WEBPACK_IMPORTED_MODULE_17__["FacebookLoginProvider"].PROVIDER_ID,
            provider: new angular_6_social_login__WEBPACK_IMPORTED_MODULE_17__["FacebookLoginProvider"](_defaults__WEBPACK_IMPORTED_MODULE_96__["KEYS"].FACEBOOK_APP_ID)
        },
        {
            id: angular_6_social_login__WEBPACK_IMPORTED_MODULE_17__["GoogleLoginProvider"].PROVIDER_ID,
            provider: new angular_6_social_login__WEBPACK_IMPORTED_MODULE_17__["GoogleLoginProvider"]("Your-Google-Client-Id")
        },
    ]);
    return config;
}
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_6__["AppComponent"],
                _landing_home_page_home_page_component__WEBPACK_IMPORTED_MODULE_18__["HomePageComponent"],
                _landing_login_page_login_page_component__WEBPACK_IMPORTED_MODULE_19__["LoginPageComponent"],
                _landing_options_page_options_page_component__WEBPACK_IMPORTED_MODULE_20__["OptionsPageComponent"],
                _bar_management_system_kanban_page_kanban_page_component__WEBPACK_IMPORTED_MODULE_21__["KanbanPageComponent"],
                _components_browser_happy_browser_happy_component__WEBPACK_IMPORTED_MODULE_22__["BrowserHappyComponent"],
                _bar_management_system_bar_nav_bar_nav_component__WEBPACK_IMPORTED_MODULE_23__["BarNavComponent"],
                _components_filter_input_filter_input_component__WEBPACK_IMPORTED_MODULE_24__["FilterInputComponent"],
                _components_profile_avatar_profile_avatar_component__WEBPACK_IMPORTED_MODULE_25__["ProfileAvatarComponent"],
                _bar_management_system_card_bar_card_bar_component__WEBPACK_IMPORTED_MODULE_26__["CardBarComponent"],
                _bar_management_system_card_bar_user_icon_card_bar_user_icon_component__WEBPACK_IMPORTED_MODULE_27__["CardBarUserIconComponent"],
                _bar_management_system_kanban_board_item_kanban_board_item_component__WEBPACK_IMPORTED_MODULE_28__["KanbanBoardItemComponent"],
                _bar_management_system_bar_checkin_page_bar_checkin_page_component__WEBPACK_IMPORTED_MODULE_29__["BarCheckinPageComponent"],
                _bar_management_system_event_checkin_page_event_checkin_page_component__WEBPACK_IMPORTED_MODULE_30__["EventCheckinPageComponent"],
                _bar_management_system_card_event_card_event_component__WEBPACK_IMPORTED_MODULE_31__["CardEventComponent"],
                _admin_users_page_users_page_component__WEBPACK_IMPORTED_MODULE_32__["UsersPageComponent"],
                _admin_events_page_events_page_component__WEBPACK_IMPORTED_MODULE_33__["EventsPageComponent"],
                _admin_places_page_places_page_component__WEBPACK_IMPORTED_MODULE_34__["PlacesPageComponent"],
                _admin_bars_page_bars_page_component__WEBPACK_IMPORTED_MODULE_35__["BarsPageComponent"],
                _admin_orders_page_orders_page_component__WEBPACK_IMPORTED_MODULE_36__["OrdersPageComponent"],
                _admin_user_page_user_page_component__WEBPACK_IMPORTED_MODULE_37__["UserPageComponent"],
                _admin_event_page_event_page_component__WEBPACK_IMPORTED_MODULE_38__["EventPageComponent"],
                _admin_order_page_order_page_component__WEBPACK_IMPORTED_MODULE_39__["OrderPageComponent"],
                _admin_sidebar_sidebar_component__WEBPACK_IMPORTED_MODULE_40__["SidebarComponent"],
                _admin_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_41__["NavbarComponent"],
                _admin_pictures_page_pictures_page_component__WEBPACK_IMPORTED_MODULE_42__["PicturesPageComponent"],
                _admin_menus_page_menus_page_component__WEBPACK_IMPORTED_MODULE_43__["MenusPageComponent"],
                _admin_menu_page_menu_page_component__WEBPACK_IMPORTED_MODULE_44__["MenuPageComponent"],
                _bar_management_system_stock_management_page_stock_management_page_component__WEBPACK_IMPORTED_MODULE_45__["StockManagementPageComponent"],
                _components_stock_list_item_stock_list_item_component__WEBPACK_IMPORTED_MODULE_46__["StockListItemComponent"],
                _pipes_filter_pipe__WEBPACK_IMPORTED_MODULE_49__["FilterPipe"],
                _admin_dashboard_page_dashboard_page_component__WEBPACK_IMPORTED_MODULE_50__["DashboardPageComponent"],
                _admin_items_page_items_page_component__WEBPACK_IMPORTED_MODULE_51__["ItemsPageComponent"],
                _admin_place_page_place_page_component__WEBPACK_IMPORTED_MODULE_52__["PlacePageComponent"],
                _admin_category_page_category_page_component__WEBPACK_IMPORTED_MODULE_53__["CategoryPageComponent"],
                _components_dashboard_sidebar_list_item_dashboard_sidebar_list_item_component__WEBPACK_IMPORTED_MODULE_54__["DashboardSidebarListItemComponent"],
                _admin_profile_page_profile_page_component__WEBPACK_IMPORTED_MODULE_55__["ProfilePageComponent"],
                _components_management_dashboard_side_menu_management_dashboard_side_menu_component__WEBPACK_IMPORTED_MODULE_56__["ManagementDashboardSideMenuComponent"],
                _components_event_list_item_event_list_item_component__WEBPACK_IMPORTED_MODULE_57__["EventListItemComponent"],
                _components_place_card_place_card_component__WEBPACK_IMPORTED_MODULE_58__["PlaceCardComponent"],
                _components_card_user_card_user_component__WEBPACK_IMPORTED_MODULE_59__["CardUserComponent"],
                _components_income_sumary_income_sumary_component__WEBPACK_IMPORTED_MODULE_60__["IncomeSumaryComponent"],
                _components_profit_sumary_profit_sumary_component__WEBPACK_IMPORTED_MODULE_61__["ProfitSumaryComponent"],
                _components_order_list_item_order_list_item_component__WEBPACK_IMPORTED_MODULE_62__["OrderListItemComponent"],
                _components_image_upload_image_upload_component__WEBPACK_IMPORTED_MODULE_65__["ImageUploadComponent"],
                _components_file_upload_file_upload_component__WEBPACK_IMPORTED_MODULE_66__["FileUploadComponent"],
                _components_invoice_single_item_list_item_invoice_single_item_list_item_component__WEBPACK_IMPORTED_MODULE_67__["InvoiceSingleItemListItemComponent"],
                _components_modals_modal_category_modal_category_component__WEBPACK_IMPORTED_MODULE_68__["ModalCategoryComponent"],
                _components_category_list_item_category_list_item_component__WEBPACK_IMPORTED_MODULE_69__["CategoryListItemComponent"],
                _components_modals_modal_item_modal_item_component__WEBPACK_IMPORTED_MODULE_70__["ModalItemComponent"],
                _components_item_list_item_item_list_item_component__WEBPACK_IMPORTED_MODULE_71__["ItemListItemComponent"],
                _components_menu_list_item_menu_list_item_component__WEBPACK_IMPORTED_MODULE_72__["MenuListItemComponent"],
                _components_modals_modal_menu_modal_menu_component__WEBPACK_IMPORTED_MODULE_73__["ModalMenuComponent"],
                _components_inline_input_inline_input_component__WEBPACK_IMPORTED_MODULE_74__["InlineInputComponent"],
                _components_status_bar_status_bar_component__WEBPACK_IMPORTED_MODULE_75__["StatusBarComponent"],
                _components_sidebar_members_sidebar_members_component__WEBPACK_IMPORTED_MODULE_76__["SidebarMembersComponent"],
                _components_sidebar_bars_sidebar_bars_component__WEBPACK_IMPORTED_MODULE_77__["SidebarBarsComponent"],
                _components_sidebar_details_sidebar_details_component__WEBPACK_IMPORTED_MODULE_78__["SidebarDetailsComponent"],
                _components_event_info_management_card_event_info_management_card_component__WEBPACK_IMPORTED_MODULE_79__["EventInfoManagementCardComponent"],
                _components_event_statistics_card_event_statistics_card_component__WEBPACK_IMPORTED_MODULE_80__["EventStatisticsCardComponent"],
                _components_event_tickets_card_event_tickets_card_component__WEBPACK_IMPORTED_MODULE_81__["EventTicketsCardComponent"],
                _components_modals_modal_mce_modal_mce_component__WEBPACK_IMPORTED_MODULE_82__["ModalMceComponent"],
                _components_inline_mce_inline_mce_component__WEBPACK_IMPORTED_MODULE_83__["InlineMceComponent"],
                _components_no_content_panel_no_content_panel_component__WEBPACK_IMPORTED_MODULE_84__["NoContentPanelComponent"],
                _directives_add_user_add_user_directive__WEBPACK_IMPORTED_MODULE_85__["AddUserDirective"],
                _directives_add_place_add_place_directive__WEBPACK_IMPORTED_MODULE_86__["AddPlaceDirective"],
                _directives_add_photo_add_photo_directive__WEBPACK_IMPORTED_MODULE_87__["AddPhotoDirective"],
                _components_image_grid_image_grid_component__WEBPACK_IMPORTED_MODULE_88__["ImageGridComponent"],
                _components_image_gallery_image_gallery_component__WEBPACK_IMPORTED_MODULE_89__["ImageGalleryComponent"],
                _directives_add_bar_add_bar_directive__WEBPACK_IMPORTED_MODULE_90__["AddBarDirective"],
                _components_modals_modal_add_staff_modal_add_staff_component__WEBPACK_IMPORTED_MODULE_91__["ModalAddStaffComponent"],
                _components_modals_modal_add_place_modal_add_place_component__WEBPACK_IMPORTED_MODULE_92__["ModalAddPlaceComponent"],
                _directives_add_staff_add_staff_directive__WEBPACK_IMPORTED_MODULE_93__["AddStaffDirective"],
                _components_place_info_place_info_component__WEBPACK_IMPORTED_MODULE_95__["PlaceInfoComponent"],
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                ngx_infinite_scroll__WEBPACK_IMPORTED_MODULE_14__["InfiniteScrollModule"],
                ngx_webstorage__WEBPACK_IMPORTED_MODULE_2__["Ng2Webstorage"],
                ngx_moment__WEBPACK_IMPORTED_MODULE_47__["MomentModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_5__["AppRoutingModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"],
                angular_6_datatable__WEBPACK_IMPORTED_MODULE_8__["DataTableModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_48__["FormsModule"],
                _ctrl_ngx_chartjs__WEBPACK_IMPORTED_MODULE_63__["ChartjsModule"],
                ngx_dropzone_wrapper__WEBPACK_IMPORTED_MODULE_64__["DropzoneModule"],
                ngx_google_places_autocomplete__WEBPACK_IMPORTED_MODULE_94__["GooglePlaceModule"],
                angular_6_social_login__WEBPACK_IMPORTED_MODULE_17__["SocialLoginModule"],
                _ionic_storage__WEBPACK_IMPORTED_MODULE_16__["IonicStorageModule"].forRoot(),
                _ngx_translate_core__WEBPACK_IMPORTED_MODULE_12__["TranslateModule"].forRoot({
                    loader: {
                        provide: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_12__["TranslateLoader"],
                        useFactory: (createTranslateLoader),
                        deps: [_angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClient"]]
                    }
                }),
                ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_9__["ModalModule"].forRoot(),
                ngx_bootstrap_dropdown__WEBPACK_IMPORTED_MODULE_10__["BsDropdownModule"].forRoot(),
                ngx_bootstrap_typeahead__WEBPACK_IMPORTED_MODULE_11__["TypeaheadModule"].forRoot(),
                ng2_dragula__WEBPACK_IMPORTED_MODULE_7__["DragulaModule"].forRoot(),
                angular_froala_wysiwyg__WEBPACK_IMPORTED_MODULE_15__["FroalaEditorModule"].forRoot(),
                angular_froala_wysiwyg__WEBPACK_IMPORTED_MODULE_15__["FroalaViewModule"].forRoot()
            ],
            providers: [
                {
                    provide: ngx_dropzone_wrapper__WEBPACK_IMPORTED_MODULE_64__["DROPZONE_CONFIG"],
                    useValue: DEFAULT_DROPZONE_CONFIG
                },
                {
                    provide: angular_6_social_login__WEBPACK_IMPORTED_MODULE_17__["AuthServiceConfig"],
                    useFactory: getAuthServiceConfigs
                }
            ],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_6__["AppComponent"]],
            entryComponents: [
                _components_modals_modal_category_modal_category_component__WEBPACK_IMPORTED_MODULE_68__["ModalCategoryComponent"],
                _components_modals_modal_item_modal_item_component__WEBPACK_IMPORTED_MODULE_70__["ModalItemComponent"],
                _components_modals_modal_menu_modal_menu_component__WEBPACK_IMPORTED_MODULE_73__["ModalMenuComponent"],
                _components_modals_modal_mce_modal_mce_component__WEBPACK_IMPORTED_MODULE_82__["ModalMceComponent"],
                _components_modals_modal_add_staff_modal_add_staff_component__WEBPACK_IMPORTED_MODULE_91__["ModalAddStaffComponent"],
                _components_modals_modal_add_place_modal_add_place_component__WEBPACK_IMPORTED_MODULE_92__["ModalAddPlaceComponent"]
            ]
        })
    ], AppModule);
    return AppModule;
}());

Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_3__["platformBrowserDynamic"])().bootstrapModule(AppModule);


/***/ }),

/***/ "./src/app/bar-management-system/bar-checkin-page/bar-checkin-page.component.html":
/*!****************************************************************************************!*\
  !*** ./src/app/bar-management-system/bar-checkin-page/bar-checkin-page.component.html ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<div>\r\n  <app-browser-happy></app-browser-happy>\r\n\r\n  <app-bar-nav></app-bar-nav>\r\n\r\n  <div class=\"c-toolbar u-mb-medium\">\r\n\r\n    <nav class=\"c-toolbar__nav u-mr-auto\">\r\n      <a class=\"c-toolbar__nav-item is-active\">{{event.name }} bars</a>\r\n      <!--<a class=\"c-toolbar__nav-item\" href=\"#tab2\">Synced Projects</a>-->\r\n    </nav>\r\n\r\n  </div>\r\n\r\n  <div class=\"container u-mb-medium\">\r\n    <div class=\"row u-justify-center\">\r\n      <app-card-bar *ngFor=\"let bar of bars\" class=\"col-sm-6 col-lg-3\" [data]=\"bar\"></app-card-bar>\r\n\r\n  </div><!-- // .container -->\r\n\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/bar-management-system/bar-checkin-page/bar-checkin-page.component.scss":
/*!****************************************************************************************!*\
  !*** ./src/app/bar-management-system/bar-checkin-page/bar-checkin-page.component.scss ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/bar-management-system/bar-checkin-page/bar-checkin-page.component.ts":
/*!**************************************************************************************!*\
  !*** ./src/app/bar-management-system/bar-checkin-page/bar-checkin-page.component.ts ***!
  \**************************************************************************************/
/*! exports provided: BarCheckinPageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BarCheckinPageComponent", function() { return BarCheckinPageComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm5/ionic-storage.js");
/* harmony import */ var _defaults__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../defaults */ "./src/app/defaults.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var BarCheckinPageComponent = /** @class */ (function () {
    function BarCheckinPageComponent(storage) {
        this.storage = storage;
        this.event = {};
        this.bars = [];
    }
    BarCheckinPageComponent.prototype.ngOnInit = function () {
        this.loadEvent();
        this.loadBars();
    };
    BarCheckinPageComponent.prototype.loadEvent = function () {
        var _this = this;
        this.event = this.storage.get(_defaults__WEBPACK_IMPORTED_MODULE_2__["STORAGE"].CHECKIN).then(function (event) {
            _this.event = event;
        });
    };
    BarCheckinPageComponent.prototype.loadBars = function () {
        var _this = this;
        this.event = this.storage.get(_defaults__WEBPACK_IMPORTED_MODULE_2__["STORAGE"].CHECKIN).then(function (event) {
            _this.bars = event.bars;
        });
    };
    BarCheckinPageComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-bar-checkin-page',
            template: __webpack_require__(/*! ./bar-checkin-page.component.html */ "./src/app/bar-management-system/bar-checkin-page/bar-checkin-page.component.html"),
            styles: [__webpack_require__(/*! ./bar-checkin-page.component.scss */ "./src/app/bar-management-system/bar-checkin-page/bar-checkin-page.component.scss")]
        }),
        __metadata("design:paramtypes", [_ionic_storage__WEBPACK_IMPORTED_MODULE_1__["Storage"]])
    ], BarCheckinPageComponent);
    return BarCheckinPageComponent;
}());



/***/ }),

/***/ "./src/app/bar-management-system/bar-nav/bar-nav.component.html":
/*!**********************************************************************!*\
  !*** ./src/app/bar-management-system/bar-nav/bar-nav.component.html ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<header class=\"c-navbar\">\r\n  <a class=\"c-navbar__brand\" href=\"#!\">\r\n    <img src=\"assets/img/logo.png\" alt=\"Dashboard's Logo\">\r\n  </a>\r\n\r\n  <!-- Navigation items that will be collapes and toggle in small viewports -->\r\n  <nav class=\"c-nav collapse\" id=\"main-nav\">\r\n    <ul class=\"c-nav__list\">\r\n      <li class=\"c-nav__item\">\r\n        <a class=\"c-nav__link\" routerLink=\"/bar-management/check-in-event\">Events</a>\r\n      </li>\r\n      <li class=\"c-nav__item\">\r\n        <a class=\"c-nav__link\" routerLink=\"/bar-management/check-in-bar\">Bar</a>\r\n      </li>\r\n      <li class=\"c-nav__item\">\r\n        <a class=\"c-nav__link\"  routerLink=\"/bar-management/board\">Board</a>\r\n      </li>\r\n    </ul>\r\n  </nav>\r\n  <!-- // Navigation items  -->\r\n\r\n  <div class=\"c-dropdown u-ml-medium dropdown\" dropdown id=\"avatar-dropdown\">\r\n    <a  class=\"c-avatar c-avatar--xsmall has-dropdown dropdown-toggle\"  id=\"dropdwonMenuAvatar\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\" dropdownToggle >\r\n      <img class=\"c-avatar__img\" [src]=\"avatar\" alt=\"User's Profile Picture\">\r\n    </a>\r\n\r\n    <div class=\"c-dropdown__menu dropdown-menu dropdown-menu-right\" aria-labelledby=\"dropdwonMenuAvatar\" *dropdownMenu>\r\n      <a class=\"c-dropdown__item dropdown-item\" (click)=\"logout()\">Logout</a>\r\n      <!--<a class=\"c-dropdown__item dropdown-item\" >View Activity</a>-->\r\n      <!--<a class=\"c-dropdown__item dropdown-item\" >Manage Roles</a>-->\r\n    </div>\r\n  </div>\r\n\r\n  <button class=\"c-nav-toggle\" type=\"button\" data-toggle=\"collapse\" data-target=\"#main-nav\">\r\n    <span class=\"c-nav-toggle__bar\"></span>\r\n    <span class=\"c-nav-toggle__bar\"></span>\r\n    <span class=\"c-nav-toggle__bar\"></span>\r\n  </button><!-- // .c-nav-toggle -->\r\n</header>\r\n"

/***/ }),

/***/ "./src/app/bar-management-system/bar-nav/bar-nav.component.scss":
/*!**********************************************************************!*\
  !*** ./src/app/bar-management-system/bar-nav/bar-nav.component.scss ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#avatar-dropdown {\n  position: absolute;\n  right: 30px; }\n"

/***/ }),

/***/ "./src/app/bar-management-system/bar-nav/bar-nav.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/bar-management-system/bar-nav/bar-nav.component.ts ***!
  \********************************************************************/
/*! exports provided: BarNavComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BarNavComponent", function() { return BarNavComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm5/ionic-storage.js");
/* harmony import */ var _defaults__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../defaults */ "./src/app/defaults.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var BarNavComponent = /** @class */ (function () {
    function BarNavComponent(auth, storage) {
        this.auth = auth;
        this.storage = storage;
        this.avatar = 'assets/img/avatar-72.jpg';
    }
    BarNavComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.storage.get(_defaults__WEBPACK_IMPORTED_MODULE_3__["STORAGE"].AUTH).then(function (data) {
            _this.avatar = data.image;
        });
    };
    BarNavComponent.prototype.logout = function () {
        this.auth.logout();
    };
    BarNavComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-bar-nav',
            template: __webpack_require__(/*! ./bar-nav.component.html */ "./src/app/bar-management-system/bar-nav/bar-nav.component.html"),
            styles: [__webpack_require__(/*! ./bar-nav.component.scss */ "./src/app/bar-management-system/bar-nav/bar-nav.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"], _ionic_storage__WEBPACK_IMPORTED_MODULE_2__["Storage"]])
    ], BarNavComponent);
    return BarNavComponent;
}());



/***/ }),

/***/ "./src/app/bar-management-system/card-bar-user-icon/card-bar-user-icon.component.html":
/*!********************************************************************************************!*\
  !*** ./src/app/bar-management-system/card-bar-user-icon/card-bar-user-icon.component.html ***!
  \********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<a class=\"c-project__profile c-tooltip c-tooltip--top\" aria-label=\"Dylan Shelton\" >\r\n  <img src=\"assets/img/avatar4-72.jpg\" alt=\"Adam's Face\">\r\n</a>\r\n"

/***/ }),

/***/ "./src/app/bar-management-system/card-bar-user-icon/card-bar-user-icon.component.scss":
/*!********************************************************************************************!*\
  !*** ./src/app/bar-management-system/card-bar-user-icon/card-bar-user-icon.component.scss ***!
  \********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/bar-management-system/card-bar-user-icon/card-bar-user-icon.component.ts":
/*!******************************************************************************************!*\
  !*** ./src/app/bar-management-system/card-bar-user-icon/card-bar-user-icon.component.ts ***!
  \******************************************************************************************/
/*! exports provided: CardBarUserIconComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CardBarUserIconComponent", function() { return CardBarUserIconComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var CardBarUserIconComponent = /** @class */ (function () {
    function CardBarUserIconComponent() {
    }
    CardBarUserIconComponent.prototype.ngOnInit = function () {
    };
    CardBarUserIconComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: '[app-card-bar-user-icon]',
            template: __webpack_require__(/*! ./card-bar-user-icon.component.html */ "./src/app/bar-management-system/card-bar-user-icon/card-bar-user-icon.component.html"),
            styles: [__webpack_require__(/*! ./card-bar-user-icon.component.scss */ "./src/app/bar-management-system/card-bar-user-icon/card-bar-user-icon.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], CardBarUserIconComponent);
    return CardBarUserIconComponent;
}());



/***/ }),

/***/ "./src/app/bar-management-system/card-bar/card-bar.component.html":
/*!************************************************************************!*\
  !*** ./src/app/bar-management-system/card-bar/card-bar.component.html ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n  <div class=\"c-project\">\r\n    <div class=\"c-project__img\">\r\n      <img [src]=\"data.photo || 'assets/img/bar1.png'\" alt=\"Bar photo\">\r\n    </div>\r\n\r\n    <h3 class=\"c-project__title\">{{data.title}}\r\n      <span class=\"c-project__status\">{{'LAST_UPDATED_AT' | translate }}: <span class=\"u-text-bold\">{{data.updated_at | amTimeAgo}}</span></span>\r\n    </h3>\r\n\r\n    <div class=\"c-project__team\">\r\n\r\n      <a class=\"c-project__profile c-tooltip c-tooltip&#45;&#45;top\" aria-label=\"Joseph Mullins\"  *ngFor=\"let user of data.users\">\r\n        <img src=\"{{user.avatar}}\" alt=\"{{user.name}}'s avatar\">\r\n      </a>\r\n\r\n      <a class=\"c-project__profile c-project__profile--btn c-tooltip c-tooltip--top\" [attr.aria-label]=\"'CHECKIN_AT_BAR' | translate\" (click)=\"join()\">\r\n        <i class=\"fa fa-plus\"></i>\r\n      </a>\r\n    </div>\r\n\r\n  </div><!-- // .c-project -->\r\n"

/***/ }),

/***/ "./src/app/bar-management-system/card-bar/card-bar.component.scss":
/*!************************************************************************!*\
  !*** ./src/app/bar-management-system/card-bar/card-bar.component.scss ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/bar-management-system/card-bar/card-bar.component.ts":
/*!**********************************************************************!*\
  !*** ./src/app/bar-management-system/card-bar/card-bar.component.ts ***!
  \**********************************************************************/
/*! exports provided: CardBarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CardBarComponent", function() { return CardBarComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm5/ionic-storage.js");
/* harmony import */ var _defaults__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../defaults */ "./src/app/defaults.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var CardBarComponent = /** @class */ (function () {
    function CardBarComponent(router, storage) {
        this.router = router;
        this.storage = storage;
        this.data = {};
    }
    CardBarComponent.prototype.ngOnInit = function () {
    };
    CardBarComponent.prototype.join = function () {
        this.storage.set(_defaults__WEBPACK_IMPORTED_MODULE_3__["STORAGE"].BAR, this.data);
        this.router.navigate(['bar-management/board'], { queryParams: { bar: this.data.id } });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], CardBarComponent.prototype, "data", void 0);
    CardBarComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-card-bar',
            template: __webpack_require__(/*! ./card-bar.component.html */ "./src/app/bar-management-system/card-bar/card-bar.component.html"),
            styles: [__webpack_require__(/*! ./card-bar.component.scss */ "./src/app/bar-management-system/card-bar/card-bar.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _ionic_storage__WEBPACK_IMPORTED_MODULE_2__["Storage"]])
    ], CardBarComponent);
    return CardBarComponent;
}());



/***/ }),

/***/ "./src/app/bar-management-system/card-event/card-event.component.html":
/*!****************************************************************************!*\
  !*** ./src/app/bar-management-system/card-event/card-event.component.html ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<div class=\"c-project\">\r\n  <div class=\"c-project__img\">\r\n    <img [src]=\"photo\" alt=\"Event {{data.name}} image\">\r\n  </div>\r\n\r\n  <h3 class=\"c-project__title\">{{data.name}}\r\n    <span class=\"c-project__status\">Last updated: <span class=\"u-text-bold\">{{ data.updated_at | amTimeAgo }}</span></span>\r\n  </h3>\r\n\r\n  <div class=\"c-project__team\">\r\n    <a class=\"c-project__profile c-project__profile--btn c-tooltip c-tooltip--top\" [attr.aria-label]=\"'CHECKIN_AT_EVENT' | translate\" (click)=\"join(data)\">\r\n      <i class=\"fa fa-plus\"></i>\r\n    </a>\r\n  </div>\r\n\r\n</div><!-- // .c-project -->\r\n"

/***/ }),

/***/ "./src/app/bar-management-system/card-event/card-event.component.scss":
/*!****************************************************************************!*\
  !*** ./src/app/bar-management-system/card-event/card-event.component.scss ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/bar-management-system/card-event/card-event.component.ts":
/*!**************************************************************************!*\
  !*** ./src/app/bar-management-system/card-event/card-event.component.ts ***!
  \**************************************************************************/
/*! exports provided: CardEventComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CardEventComponent", function() { return CardEventComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm5/ionic-storage.js");
/* harmony import */ var _defaults__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../defaults */ "./src/app/defaults.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var CardEventComponent = /** @class */ (function () {
    // photo:string = 'assets/img/event1.jpg';
    function CardEventComponent(router, storage) {
        this.router = router;
        this.storage = storage;
    }
    CardEventComponent.prototype.ngOnInit = function () {
    };
    CardEventComponent.prototype.join = function () {
        this.storage.set(_defaults__WEBPACK_IMPORTED_MODULE_3__["STORAGE"].CHECKIN, this.data);
        this.router.navigate(['bar-management/check-in-bar'], { queryParams: { event: this.data.id } });
    };
    Object.defineProperty(CardEventComponent.prototype, "photo", {
        get: function () {
            var photo;
            try {
                photo = this.data.photos[0];
            }
            catch (ex) {
                photo = 'assets/img/event1.jpg';
            }
            return photo;
        },
        enumerable: true,
        configurable: true
    });
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], CardEventComponent.prototype, "data", void 0);
    CardEventComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-card-event',
            template: __webpack_require__(/*! ./card-event.component.html */ "./src/app/bar-management-system/card-event/card-event.component.html"),
            styles: [__webpack_require__(/*! ./card-event.component.scss */ "./src/app/bar-management-system/card-event/card-event.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _ionic_storage__WEBPACK_IMPORTED_MODULE_2__["Storage"]])
    ], CardEventComponent);
    return CardEventComponent;
}());



/***/ }),

/***/ "./src/app/bar-management-system/event-checkin-page/event-checkin-page.component.html":
/*!********************************************************************************************!*\
  !*** ./src/app/bar-management-system/event-checkin-page/event-checkin-page.component.html ***!
  \********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<div>\r\n  <app-browser-happy></app-browser-happy>\r\n\r\n  <app-bar-nav></app-bar-nav>\r\n\r\n  <div class=\"c-toolbar u-mb-medium\">\r\n\r\n    <!--<div class=\"c-btn-group u-mr-medium u-hidden-down@tablet\">-->\r\n      <!--<a class=\"c-btn c-btn&#45;&#45;secondary\" href=\"#!\">-->\r\n        <!--<i class=\"fa fa-check-square-o u-opacity-medium\"></i>-->\r\n      <!--</a>-->\r\n      <!--<a class=\"c-btn c-btn&#45;&#45;secondary\" href=\"#!\">-->\r\n        <!--<i class=\"fa fa-trash-o u-opacity-medium\"></i>-->\r\n      <!--</a>-->\r\n    <!--</div>-->\r\n\r\n    <!--<a class=\"c-toolbar__icon has-divider u-hidden-down@mobile\" ><i class=\"fa fa-th-large\"></i></a>-->\r\n    <!--<a class=\"c-toolbar__icon has-divider u-hidden-down@mobile\"><i class=\"fa fa-navicon\"></i></a>-->\r\n\r\n    <!--<input type=\"range\" class=\"c-range c-range&#45;&#45;inline u-mr-auto u-hidden-down@mobile\">-->\r\n\r\n    <nav class=\"c-toolbar__nav u-mr-auto\">\r\n      <a class=\"c-toolbar__nav-item is-active\" href=\"#tab1\">{{'YOUR_EVENTS' | translate}}</a>\r\n      <!--<a class=\"c-toolbar__nav-item\" href=\"#tab2\">Synced Projects</a>-->\r\n    </nav>\r\n\r\n    <!--<a class=\"c-btn c-btn&#45;&#45;success u-ml-auto u-hidden-down@mobile\" >-->\r\n      <!--<i class=\"fa fa-plus u-mr-xsmall u-opacity-medium\"></i>New Project-->\r\n    <!--</a>-->\r\n  </div>\r\n\r\n  <div class=\"container u-mb-medium\">\r\n    <div class=\"row u-justify-center\">\r\n      <app-card-event *ngFor=\"let event of events\" class=\"col-sm-6 col-lg-3\" [data]=\"event\"></app-card-event>\r\n\r\n  </div><!-- // .container -->\r\n\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/bar-management-system/event-checkin-page/event-checkin-page.component.scss":
/*!********************************************************************************************!*\
  !*** ./src/app/bar-management-system/event-checkin-page/event-checkin-page.component.scss ***!
  \********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/bar-management-system/event-checkin-page/event-checkin-page.component.ts":
/*!******************************************************************************************!*\
  !*** ./src/app/bar-management-system/event-checkin-page/event-checkin-page.component.ts ***!
  \******************************************************************************************/
/*! exports provided: EventCheckinPageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EventCheckinPageComponent", function() { return EventCheckinPageComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_api_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/api.service */ "./src/app/services/api.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var EventCheckinPageComponent = /** @class */ (function () {
    function EventCheckinPageComponent(api) {
        this.api = api;
        this.events = [];
    }
    EventCheckinPageComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.api.getUserEvents().then(function (events) {
            _this.events = events;
        });
    };
    EventCheckinPageComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-event-checkin-page',
            template: __webpack_require__(/*! ./event-checkin-page.component.html */ "./src/app/bar-management-system/event-checkin-page/event-checkin-page.component.html"),
            styles: [__webpack_require__(/*! ./event-checkin-page.component.scss */ "./src/app/bar-management-system/event-checkin-page/event-checkin-page.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_api_service__WEBPACK_IMPORTED_MODULE_1__["ApiService"]])
    ], EventCheckinPageComponent);
    return EventCheckinPageComponent;
}());



/***/ }),

/***/ "./src/app/bar-management-system/kanban-board-item/kanban-board-item.component.html":
/*!******************************************************************************************!*\
  !*** ./src/app/bar-management-system/kanban-board-item/kanban-board-item.component.html ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"c-task\">\r\n  <small class=\"c-task__content\">\r\n    <p><b>{{userName}}</b> @ {{data.created_at | amTimeAgo }}</p>\r\n    <ul>\r\n      <li *ngFor=\"let menuItem of orderItems\">\r\n        {{menuItem._string}} x{{menuItem._count}}\r\n      </li>\r\n    </ul>\r\n  </small>\r\n</div><!-- // .c-task -->\r\n"

/***/ }),

/***/ "./src/app/bar-management-system/kanban-board-item/kanban-board-item.component.scss":
/*!******************************************************************************************!*\
  !*** ./src/app/bar-management-system/kanban-board-item/kanban-board-item.component.scss ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "p, li {\n  font-weight: 100; }\n\np {\n  padding: 0 10px; }\n\nul {\n  margin: 0 15px;\n  list-style-type: disc;\n  padding: 0 15px; }\n"

/***/ }),

/***/ "./src/app/bar-management-system/kanban-board-item/kanban-board-item.component.ts":
/*!****************************************************************************************!*\
  !*** ./src/app/bar-management-system/kanban-board-item/kanban-board-item.component.ts ***!
  \****************************************************************************************/
/*! exports provided: KanbanBoardItemComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "KanbanBoardItemComponent", function() { return KanbanBoardItemComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var KanbanBoardItemComponent = /** @class */ (function () {
    function KanbanBoardItemComponent(translate) {
        this.translate = translate;
        this.data = {};
        this.orderItems = [];
    }
    KanbanBoardItemComponent.prototype.ngOnInit = function () {
        this.orderItems = this.arrangeOrderItems(this.data.shoppingCart.menuItems);
        // this.orderItems = this.data.shoppingCart.menuItems;
    };
    KanbanBoardItemComponent.prototype.arrangeOrderItems = function (orderItems) {
        var _this = this;
        var arrangedOrderItems = [];
        var arrangedStrings = [];
        orderItems.map(function (el) {
            el._options = [];
            el._string = '';
            Object.keys(el.options).forEach(function (key) {
                var option = el.options[key];
                var type = typeof option;
                switch (type) {
                    case 'boolean':
                        if (option)
                            el._options.push(_this.translate.instant(key));
                        break;
                    case 'string':
                        el._options.push(option);
                        break;
                    case 'object':
                        try {
                            el.options.push(option.menuItem.name || option.menuItem.item.name);
                        }
                        catch (ex) {
                            el.options.push("Error getting item for " + key);
                        }
                        break;
                }
            });
            el._string += el.name || el.item.name;
            el._string += (el._options.length > 0) ? " (" + el.options.toString() + ")" : '';
            el._count = 1;
            var index = arrangedStrings.indexOf(el._string);
            if (-1 === index) {
                arrangedStrings.push(el._string);
                arrangedOrderItems.push(el);
            }
            else {
                arrangedOrderItems[index]._count++;
            }
            return el;
        });
        return arrangedOrderItems;
        /*
  
        /*
        el._string += el.name || el.item.name;
        el._string += (el._options.length > 0)? ` (${el.options.toString()})`: '';
        el._count = 0;
  
        let _index = arrangedStrings.indexOf(el._string);
        if(-1 === _index) {
          arrangedStrings.push(el._string);
          arrangedOrderItems.push(el);
        }else {
          arrangedOrderItems[_index]._count++;
        }
        console.log(arrangedOrderItems);
        return arrangedOrderItems;
        */
    };
    Object.defineProperty(KanbanBoardItemComponent.prototype, "userName", {
        get: function () {
            if (this.data) {
                if (this.data.user && this.data.user.name)
                    return this.data.user.name;
            }
            return this.translate.instant('NO_NAME_YET');
        },
        enumerable: true,
        configurable: true
    });
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], KanbanBoardItemComponent.prototype, "data", void 0);
    KanbanBoardItemComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-kanban-board-item',
            template: __webpack_require__(/*! ./kanban-board-item.component.html */ "./src/app/bar-management-system/kanban-board-item/kanban-board-item.component.html"),
            styles: [__webpack_require__(/*! ./kanban-board-item.component.scss */ "./src/app/bar-management-system/kanban-board-item/kanban-board-item.component.scss")]
        }),
        __metadata("design:paramtypes", [_ngx_translate_core__WEBPACK_IMPORTED_MODULE_1__["TranslateService"]])
    ], KanbanBoardItemComponent);
    return KanbanBoardItemComponent;
}());



/***/ }),

/***/ "./src/app/bar-management-system/kanban-page/kanban-page.component.html":
/*!******************************************************************************!*\
  !*** ./src/app/bar-management-system/kanban-page/kanban-page.component.html ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div>\r\n  <app-browser-happy></app-browser-happy>\r\n  <!-- Add your site or application content here -->\r\n  <app-bar-nav></app-bar-nav>\r\n\r\n  <div class=\"c-toolbar u-justify-space-between u-mb-medium\">\r\n    <h4 class=\"c-toolbar__title u-mr-auto\">{{'ORDERS' | translate}}: {{event.name}} - {{bar.name}} #{{bar.id}}</h4>\r\n\r\n    <a class=\"c-btn c-btn--info\" (click)=\"goToStockManagement()\">{{'STOCK_MANAGEMENT' | translate}}</a>\r\n  </div>\r\n\r\n  <div class=\"container-fluid\">\r\n    <div class=\"row\">\r\n      <div class=\"col-sm-6 col-md-4 col-xl\">\r\n        <div class=\"c-board c-board--info\">\r\n          <div class=\"c-board__header\">\r\n            <h4 class=\"c-board__title\">{{'RECIEVED' | translate}}\r\n              <span class=\"u-text-mute\"> {{newOrders.length}}</span>\r\n            </h4>\r\n          </div>\r\n          <div class=\"c-board__content\" dragula=\"DRAGULA_EVENTS\" id=\"kanban-1\" [(dragulaModel)]='newOrders'>\r\n            <div *ngFor=\"let order of newOrders; let i = index;\" [attr.data-index]=\"i\">\r\n              <app-kanban-board-item [data]=\"order\"></app-kanban-board-item>\r\n            </div>\r\n          </div><!-- // .c-board__content -->\r\n        </div><!-- // .c-board -->\r\n      </div>\r\n\r\n      <div class=\"col-sm-6 col-md-4 col-xl\">\r\n        <div class=\"c-board c-board--danger\">\r\n          <div class=\"c-board__header\">\r\n            <h4 class=\"c-board__title\">{{'PREPARING' | translate}}\r\n              <span class=\"u-text-mute\">{{ongoingOrders.length}}</span>\r\n            </h4>\r\n          </div>\r\n\r\n          <div class=\"c-board__content\" dragula=\"DRAGULA_EVENTS\" id=\"kanban-2\" [(dragulaModel)]='ongoingOrders'>\r\n            <div *ngFor=\"let order of ongoingOrders; let i = index;\" [attr.data-index]=\"i\">\r\n              <app-kanban-board-item [data]=\"order\"></app-kanban-board-item>\r\n            </div>\r\n          </div><!-- // .c-board__content -->\r\n        </div><!-- // .c-board -->\r\n      </div>\r\n\r\n      <div class=\"col-sm-6 col-md-4 col-xl\">\r\n        <div class=\"c-board c-board--success\">\r\n          <div class=\"c-board__header\">\r\n            <h4 class=\"c-board__title\">{{'COMPLETE' | translate}}\r\n              <span class=\"u-text-mute\"> {{completeOrders.length}}</span>\r\n            </h4>\r\n          </div>\r\n\r\n          <div class=\"c-board__content\" dragula=\"DRAGULA_EVENTS\" id=\"kanban-3\" [(dragulaModel)]=\"completeOrders\">\r\n            <div *ngFor=\"let order of completeOrders; let i = index;\" [attr.data-index]=\"i\">\r\n              <app-kanban-board-item [data]=\"order\"></app-kanban-board-item>\r\n            </div>\r\n          </div><!-- // .c-board__content -->\r\n        </div><!-- // .c-board -->\r\n      </div>\r\n\r\n      <div class=\"col-sm-6 col-md-4 col-xl\">\r\n        <div class=\"c-board c-board--info\">\r\n          <div class=\"c-board__header\">\r\n            <h4 class=\"c-board__title\">{{'DELIVERED' | translate}}\r\n              <span class=\"u-text-mute\"> {{doneOrders.length}}</span>\r\n            </h4>\r\n          </div>\r\n\r\n          <div class=\"c-board__content\" dragula=\"DRAGULA_EVENTS\" id=\"kanban-4\" [(dragulaModel)]=\"doneOrders\">\r\n            <div *ngFor=\"let order of doneOrders; let i = index;\" [attr.data-index]=\"i\">\r\n              <app-kanban-board-item [data]=\"order\"></app-kanban-board-item>\r\n            </div>\r\n\r\n          </div><!-- // .c-board__content -->\r\n\r\n        </div><!-- // .c-board -->\r\n      </div>\r\n\r\n      <!--<div class=\"col-sm-6 col-md-4 col-xl\">-->\r\n        <!--<a class=\"c-add-board\" ><i class=\"fa fa-plus\"></i>Add a board</a>-->\r\n      <!--</div>&lt;!&ndash; // .c-board &ndash;&gt;-->\r\n    </div>\r\n  </div>\r\n\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/bar-management-system/kanban-page/kanban-page.component.scss":
/*!******************************************************************************!*\
  !*** ./src/app/bar-management-system/kanban-page/kanban-page.component.scss ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/bar-management-system/kanban-page/kanban-page.component.ts":
/*!****************************************************************************!*\
  !*** ./src/app/bar-management-system/kanban-page/kanban-page.component.ts ***!
  \****************************************************************************/
/*! exports provided: KanbanPageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "KanbanPageComponent", function() { return KanbanPageComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ng2_dragula__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ng2-dragula */ "./node_modules/ng2-dragula/dist/fesm5/ng2-dragula.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_api_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/api.service */ "./src/app/services/api.service.ts");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm5/ionic-storage.js");
/* harmony import */ var _defaults__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../defaults */ "./src/app/defaults.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var KanbanPageComponent = /** @class */ (function () {
    function KanbanPageComponent(dragulaService, router, storage, api) {
        this.dragulaService = dragulaService;
        this.router = router;
        this.storage = storage;
        this.api = api;
        this.event = { name: '' };
        this.bar = {};
        this.newOrders = [];
        this.ongoingOrders = [];
        this.completeOrders = [];
        this.doneOrders = [];
        this.BAG = 'DRAGULA_EVENTS';
        this.subs = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subscription"]();
        this.orders = [];
    }
    KanbanPageComponent.prototype.bootstrap = function () {
        var _this = this;
        this.dragulaService.createGroup(this.BAG, {
            accepts: function (el, target, source, sibling) {
                // To avoid dragging from right to left container
                var targetId = Number((target.id).match(/\d+$/));
                var sourceId = Number((source.id).match(/\d+$/));
                return targetId > sourceId && targetId === sourceId + 1;
            }
        });
        // SINCE THE FUCKERS CAN ONLY GO ONE LEVEL -> need to do this sequentially
        this.subs.add(this.dragulaService.dropModel(this.BAG)
            .subscribe(function (_a) {
            var el = _a.el, target = _a.target, source = _a.source, sourceModel = _a.sourceModel, targetModel = _a.targetModel, item = _a.item;
            // console.log('dropModel:');
            // console.log(el);
            // console.log(source);
            // console.log(target);
            // console.log(sourceModel);
            // console.log(targetModel);
            // console.log(item);
            _this.updateOrderStatus(item);
            var step = Number((target.id).match(/\d+$/));
        }));
    };
    KanbanPageComponent.prototype.setStatus = function (index, statusId) {
        this.orders[index].status = statusId;
    };
    KanbanPageComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.bootstrap();
        this.event = this.storage.get(_defaults__WEBPACK_IMPORTED_MODULE_6__["STORAGE"].CHECKIN).then(function (event) {
            _this.event = event;
            _this.bar = _this.storage.get(_defaults__WEBPACK_IMPORTED_MODULE_6__["STORAGE"].BAR).then(function (bar) {
                _this.bar = bar;
                _this.loadOrders();
            });
        });
    };
    KanbanPageComponent.prototype.ngOnDestroy = function () {
        this.subs.unsubscribe();
        this.dragulaService.destroy(this.BAG);
    };
    KanbanPageComponent.prototype.loadOrders = function () {
        var _this = this;
        this.api.getEventBarOrders(this.event.id, this.bar.id).then(function (orders) {
            _this.orders = orders;
            _this.filterOrders(orders);
            _this.openSocket();
        });
    };
    KanbanPageComponent.prototype.openSocket = function () {
        //alert('NEEDS TO OPEN WEBSOCKET');
    };
    KanbanPageComponent.prototype.filterOrders = function (orders) {
        var _this = this;
        this.orders.forEach(function (order) {
            var STATUS = order.status;
            switch (STATUS) {
                case 'RECIEVED':
                    _this.newOrders.push(order);
                    break;
                case 'PREPARING':
                    _this.ongoingOrders.push(order);
                    break;
                case 'READY':
                    _this.doneOrders.push(order);
                    break;
            }
        });
    };
    KanbanPageComponent.prototype.updateOrderStatus = function (order) {
        var STATUS_LIST = ['RECIEVED', 'PREPARING', 'READY', 'COMPLETE'];
        var STATUS_INDEX = STATUS_LIST.indexOf(order.status) + 1;
        order.status = STATUS_LIST[STATUS_INDEX];
        this.api.updateOrderStatus(order.id, order.status);
    };
    KanbanPageComponent.prototype.goToStockManagement = function () {
        this.router.navigate(['bar-management/stock-management']);
    };
    KanbanPageComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-kanban-page',
            template: __webpack_require__(/*! ./kanban-page.component.html */ "./src/app/bar-management-system/kanban-page/kanban-page.component.html"),
            styles: [__webpack_require__(/*! ./kanban-page.component.scss */ "./src/app/bar-management-system/kanban-page/kanban-page.component.scss")]
        }),
        __metadata("design:paramtypes", [ng2_dragula__WEBPACK_IMPORTED_MODULE_1__["DragulaService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"], _services_api_service__WEBPACK_IMPORTED_MODULE_4__["ApiService"]])
    ], KanbanPageComponent);
    return KanbanPageComponent;
}());



/***/ }),

/***/ "./src/app/bar-management-system/stock-management-page/stock-management-page.component.html":
/*!**************************************************************************************************!*\
  !*** ./src/app/bar-management-system/stock-management-page/stock-management-page.component.html ***!
  \**************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div>\r\n  <!-- NAV BARS -->\r\n  <app-browser-happy></app-browser-happy>\r\n  <app-bar-nav></app-bar-nav>\r\n\r\n  <div class=\"c-toolbar u-justify-space-between u-mb-medium\">\r\n    <h4 class=\"c-toolbar__title u-mr-auto\">{{'BAR_BACKLOG' | translate}} - bar name </h4>\r\n  </div>\r\n\r\n  <!-- CONTENT -->\r\n  <div class=\"container u-mb-large\">\r\n    <div class=\"row\">\r\n      <div class=\"col-sm-12\">\r\n        <div class=\"c-table-responsive@desktop\">\r\n          <caption class=\"c-table__title\">\r\n            {{'BAR_ITEM_COUNT' | translate }} <small>{{items.length}} </small>\r\n            <app-filter-input class=\"text-right\" (filterEvent)=\"filterEvent($event)\" ></app-filter-input>\r\n          </caption>\r\n          <table class=\"c-table\" datatable >\r\n            <thead class=\"c-table__head c-table__head--slim\">\r\n            <tr class=\"c-table__row\">\r\n              <th class=\"c-table__cell c-table__cell--head\">{{'PHOTO' | translate}}</th>\r\n              <th class=\"c-table__cell c-table__cell--head\">{{'NAME_&_BRAND' | translate}}</th>\r\n              <th class=\"c-table__cell c-table__cell--head\">{{'TYPE' | translate}}</th>\r\n              <th class=\"c-table__cell c-table__cell--head\">{{'STATUS' | translate}}</th>\r\n              <th class=\"c-table__cell c-table__cell--head\">{{'ACTIONS' | translate}}</th>\r\n            </tr>\r\n            </thead>\r\n\r\n            <tbody>\r\n              <tr app-stock-list-item *ngFor=\"let item of items | filter: {name: filter}\" [data]=\"item\">\r\n\r\n              </tr>\r\n            </tbody>\r\n          </table><!-- // .c-table -->\r\n        </div>\r\n      </div>\r\n    </div><!-- // .row -->\r\n  </div><!-- // .container -->\r\n\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/bar-management-system/stock-management-page/stock-management-page.component.scss":
/*!**************************************************************************************************!*\
  !*** ./src/app/bar-management-system/stock-management-page/stock-management-page.component.scss ***!
  \**************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "caption {\n  display: block; }\n\ntable {\n  display: table; }\n"

/***/ }),

/***/ "./src/app/bar-management-system/stock-management-page/stock-management-page.component.ts":
/*!************************************************************************************************!*\
  !*** ./src/app/bar-management-system/stock-management-page/stock-management-page.component.ts ***!
  \************************************************************************************************/
/*! exports provided: StockManagementPageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StockManagementPageComponent", function() { return StockManagementPageComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_api_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/api.service */ "./src/app/services/api.service.ts");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm5/ionic-storage.js");
/* harmony import */ var _defaults__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../defaults */ "./src/app/defaults.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var StockManagementPageComponent = /** @class */ (function () {
    function StockManagementPageComponent(api, storage) {
        this.api = api;
        this.storage = storage;
        this.event = {};
        this.bar = {};
        this.filter = '';
        this.items = [];
    }
    StockManagementPageComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.event = this.storage.get(_defaults__WEBPACK_IMPORTED_MODULE_3__["STORAGE"].CHECKIN).then(function (event) {
            _this.event = event;
            _this.bar = _this.storage.get(_defaults__WEBPACK_IMPORTED_MODULE_3__["STORAGE"].BAR).then(function (bar) {
                _this.bar = bar;
                _this.loadItems(bar.id);
            });
        });
    };
    StockManagementPageComponent.prototype.loadItems = function (barId) {
        var _this = this;
        /*
         this.items = [ new MenuItem(1, 'ITEM NAME 1'), new MenuItem(2, 'ITEM NAME 2'),
                        new MenuItem(3, 'ITEM NAME 3'), new MenuItem(4, 'ITEM NAME 4')];
        */
        this.api.getBarMenu(barId).then(function (bar) {
            try {
                _this.items = bar.menu.menuItems || [];
            }
            catch (ex) {
                _this.items = [];
            }
        });
    };
    StockManagementPageComponent.prototype.filterEvent = function (event) {
        this.filter = event;
    };
    StockManagementPageComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-stock-management-page',
            template: __webpack_require__(/*! ./stock-management-page.component.html */ "./src/app/bar-management-system/stock-management-page/stock-management-page.component.html"),
            styles: [__webpack_require__(/*! ./stock-management-page.component.scss */ "./src/app/bar-management-system/stock-management-page/stock-management-page.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_api_service__WEBPACK_IMPORTED_MODULE_1__["ApiService"], _ionic_storage__WEBPACK_IMPORTED_MODULE_2__["Storage"]])
    ], StockManagementPageComponent);
    return StockManagementPageComponent;
}());



/***/ }),

/***/ "./src/app/components/browser-happy/browser-happy.component.html":
/*!***********************************************************************!*\
  !*** ./src/app/components/browser-happy/browser-happy.component.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--[if lte IE 9]>\r\n<p class=\"browserupgrade\">You are using an <strong>outdated</strong> browser. Please <a href=\"http://browsehappy.com/\">upgrade your browser</a> to improve your experience and security.</p>\r\n<![endif]-->\r\n"

/***/ }),

/***/ "./src/app/components/browser-happy/browser-happy.component.scss":
/*!***********************************************************************!*\
  !*** ./src/app/components/browser-happy/browser-happy.component.scss ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/browser-happy/browser-happy.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/components/browser-happy/browser-happy.component.ts ***!
  \*********************************************************************/
/*! exports provided: BrowserHappyComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BrowserHappyComponent", function() { return BrowserHappyComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var BrowserHappyComponent = /** @class */ (function () {
    function BrowserHappyComponent() {
    }
    BrowserHappyComponent.prototype.ngOnInit = function () {
    };
    BrowserHappyComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-browser-happy',
            template: __webpack_require__(/*! ./browser-happy.component.html */ "./src/app/components/browser-happy/browser-happy.component.html"),
            styles: [__webpack_require__(/*! ./browser-happy.component.scss */ "./src/app/components/browser-happy/browser-happy.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], BrowserHappyComponent);
    return BrowserHappyComponent;
}());



/***/ }),

/***/ "./src/app/components/card-user/card-user.component.html":
/*!***************************************************************!*\
  !*** ./src/app/components/card-user/card-user.component.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"c-candidate\">\r\n  <div class=\"c-candidate__cover\">\r\n    <img src=\"{{data.cover}}\" alt=\"{{data.name}} Cover photo\">\r\n  </div>\r\n\r\n  <div class=\"c-candidate__info\">\r\n    <div class=\"c-candidate__avatar\">\r\n      <img src=\"{{data.avatar}}\" alt=\"{{data.name}} Avatar\">\r\n    </div>\r\n\r\n    <div class=\"c-candidate__meta\">\r\n      <h3 class=\"c-candidate__title\">{{data.name}}\r\n        <span class=\"c-candidate__country\">\r\n          <i class=\"fa fa-mobile\"></i>{{data.mobile}}<br/>\r\n          <!--<i class=\"fa fa-envelope-o\"></i>{{data.email}}<br/>-->\r\n        </span>\r\n      </h3>\r\n\r\n      <div class=\"c-candidate__actions\">\r\n        <!--<a ><i class=\"fa fa-trash-o\"></i></a>-->\r\n        <a routerLink=\"/event-management/user/{{data.id}}\"><i class=\"fa fa-cog\"></i></a>\r\n      </div>\r\n    </div>\r\n  </div>\r\n\r\n  <div class=\"c-candidate__footer\">\r\n    <!--<a  class=\"c-btn c-btn&#45;&#45;info\">-->\r\n      <!--<i class=\"fa fa-envelope-o u-mr-xsmall u-opacity-heavy\"></i>New Message-->\r\n    <!--</a>-->\r\n\r\n    <div class=\"c-candidate__status u-color-success\">\r\n      <i class=\"fa fa-check u-mr-xsmall\"></i>{{data.company_role.role}}\r\n    </div>\r\n  </div><!-- // .c-candidate__footer -->\r\n\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/components/card-user/card-user.component.scss":
/*!***************************************************************!*\
  !*** ./src/app/components/card-user/card-user.component.scss ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/card-user/card-user.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/components/card-user/card-user.component.ts ***!
  \*************************************************************/
/*! exports provided: CardUserComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CardUserComponent", function() { return CardUserComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _models_user__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../models/user */ "./src/app/models/user.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var CardUserComponent = /** @class */ (function () {
    function CardUserComponent() {
        this.data = new _models_user__WEBPACK_IMPORTED_MODULE_1__["User"]();
    }
    CardUserComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], CardUserComponent.prototype, "data", void 0);
    CardUserComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-card-user',
            template: __webpack_require__(/*! ./card-user.component.html */ "./src/app/components/card-user/card-user.component.html"),
            styles: [__webpack_require__(/*! ./card-user.component.scss */ "./src/app/components/card-user/card-user.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], CardUserComponent);
    return CardUserComponent;
}());



/***/ }),

/***/ "./src/app/components/category-list-item/category-list-item.component.html":
/*!*********************************************************************************!*\
  !*** ./src/app/components/category-list-item/category-list-item.component.html ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<td class=\"c-table__cell\"># {{data.id}}</td>\r\n<td class=\"c-table__cell\">{{data.name}}</td>\r\n<td class=\"c-table__cell\">{{data.description}}</td>\r\n\r\n"

/***/ }),

/***/ "./src/app/components/category-list-item/category-list-item.component.scss":
/*!*********************************************************************************!*\
  !*** ./src/app/components/category-list-item/category-list-item.component.scss ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/category-list-item/category-list-item.component.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/components/category-list-item/category-list-item.component.ts ***!
  \*******************************************************************************/
/*! exports provided: CategoryListItemComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CategoryListItemComponent", function() { return CategoryListItemComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _models_category__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../models/category */ "./src/app/models/category.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var CategoryListItemComponent = /** @class */ (function () {
    function CategoryListItemComponent() {
        this.data = new _models_category__WEBPACK_IMPORTED_MODULE_1__["Category"]();
        this.class = 'c-table__row';
    }
    CategoryListItemComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", _models_category__WEBPACK_IMPORTED_MODULE_1__["Category"])
    ], CategoryListItemComponent.prototype, "data", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"])('class'),
        __metadata("design:type", Object)
    ], CategoryListItemComponent.prototype, "class", void 0);
    CategoryListItemComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-category-list-item,[app-category-list-item]',
            template: __webpack_require__(/*! ./category-list-item.component.html */ "./src/app/components/category-list-item/category-list-item.component.html"),
            styles: [__webpack_require__(/*! ./category-list-item.component.scss */ "./src/app/components/category-list-item/category-list-item.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], CategoryListItemComponent);
    return CategoryListItemComponent;
}());



/***/ }),

/***/ "./src/app/components/dashboard-sidebar-list-item/dashboard-sidebar-list-item.component.html":
/*!***************************************************************************************************!*\
  !*** ./src/app/components/dashboard-sidebar-list-item/dashboard-sidebar-list-item.component.html ***!
  \***************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<a routerLink=\"/{{data.path}}\" class=\"c-sidebar__link\" *ngIf=\"data.path\">\r\n  <i class=\"fa fa-{{data.icon}} u-mr-xsmall\"></i>{{data.name | translate}}\r\n</a>\r\n\r\n<a class=\"c-sidebar__link\" *ngIf=\"!data.path\">\r\n  <i class=\"fa fa-{{data.icon}} u-mr-xsmall\"></i>{{data.name | translate}}\r\n</a>\r\n"

/***/ }),

/***/ "./src/app/components/dashboard-sidebar-list-item/dashboard-sidebar-list-item.component.scss":
/*!***************************************************************************************************!*\
  !*** ./src/app/components/dashboard-sidebar-list-item/dashboard-sidebar-list-item.component.scss ***!
  \***************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/dashboard-sidebar-list-item/dashboard-sidebar-list-item.component.ts":
/*!*************************************************************************************************!*\
  !*** ./src/app/components/dashboard-sidebar-list-item/dashboard-sidebar-list-item.component.ts ***!
  \*************************************************************************************************/
/*! exports provided: DashboardSidebarListItemComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardSidebarListItemComponent", function() { return DashboardSidebarListItemComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var DashboardSidebarListItemComponent = /** @class */ (function () {
    function DashboardSidebarListItemComponent() {
        this.data = {
            name: 'name',
            path: 'event-management/dashboard',
            icon: 'home'
        };
        this.class = 'c-sidebar__item';
    }
    DashboardSidebarListItemComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], DashboardSidebarListItemComponent.prototype, "data", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"])('class'),
        __metadata("design:type", Object)
    ], DashboardSidebarListItemComponent.prototype, "class", void 0);
    DashboardSidebarListItemComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-dashboard-sidebar-list-item,[app-dashboard-sidebar-list-item]',
            template: __webpack_require__(/*! ./dashboard-sidebar-list-item.component.html */ "./src/app/components/dashboard-sidebar-list-item/dashboard-sidebar-list-item.component.html"),
            styles: [__webpack_require__(/*! ./dashboard-sidebar-list-item.component.scss */ "./src/app/components/dashboard-sidebar-list-item/dashboard-sidebar-list-item.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], DashboardSidebarListItemComponent);
    return DashboardSidebarListItemComponent;
}());



/***/ }),

/***/ "./src/app/components/event-info-management-card/event-info-management-card.component.html":
/*!*************************************************************************************************!*\
  !*** ./src/app/components/event-info-management-card/event-info-management-card.component.html ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<article class=\"c-stage\" id=\"stages\">\r\n  <a class=\"c-stage__header u-flex u-justify-between\" data-toggle=\"collapse\"  aria-expanded=\"false\" aria-controls=\"stage-panel1\">\r\n    <div class=\"o-media\">\r\n      <div class=\"c-stage__header-img o-media__img\">\r\n        <img src=\"assets/img/recent1.jpg\" alt=\"About the image\">\r\n      </div>\r\n      <div class=\"c-stage__header-title o-media__body\">\r\n        <h6 class=\"u-mb-zero\">\r\n          <app-inline-input [data]=\"data.name\" (dataChange)=\"this.data.name = $event\"></app-inline-input>\r\n        </h6>\r\n        <!--<app-inline-input [data]=\"text\"></app-inline-input>-->\r\n        <p class=\"u-text-xsmall u-text-mute\">{{'POSTED'|translate}} {{data.created_at | amTimeAgo}}  |  {{'EST_TIME' |translate}}: Less than 1 week</p>\r\n      </div>\r\n    </div>\r\n\r\n    <i class=\"fa fa-angle-down u-text-mute\"></i>\r\n  </a>\r\n\r\n  <div class=\"c-stage__panel c-stage__panel--mute collapse show\" id=\"stage-panel1\">\r\n    <div class=\"u-p-medium\">\r\n      <p class=\"u-text-mute u-text-uppercase u-text-small u-mb-xsmall\">{{'DESCRIPTION' |translate}}</p>\r\n      <app-inline-mce [data]=\"data.description\" (dataChange)=\"this.data.description = $event\"></app-inline-mce>\r\n    </div>\r\n    <div class=\"u-p-medium\">\r\n      <p class=\"u-text-mute u-text-uppercase u-text-small u-mb-xsmall\">{{'TAGS' |translate}}</p>\r\n\r\n      <ul class=\"row\">\r\n        <li class=\"u-mb-xsmall u-text-small u-color-primary col-lg-3 pull-left mt-4\" *ngFor=\"let tag of data.tags\">\r\n          <i class=\"fa fa-check u-color-info u-text-mute u-mr-xsmall\"></i>\r\n          {{tag}}\r\n        </li>\r\n        <li class=\"u-mb-xsmall u-text-small u-color-primary col-lg-12\">\r\n          <i class=\"fa fa-plus u-color-info u-text-mute u-mr-xsmall pull-left mt-4\"></i>\r\n          <app-inline-input [data]=\"tag\" (dataChange)=\"addTag($event)\" [placeholder]=\"'ADD_NEW_TAG'\"></app-inline-input>\r\n        </li>\r\n      </ul>\r\n    </div>\r\n  </div><!-- // .c-stage__panel -->\r\n\r\n  <a class=\"c-stage__header u-flex u-justify-between\" data-toggle=\"collapse\" aria-expanded=\"true\">\r\n    <h6 class=\"u-text-mute u-text-uppercase u-text-small u-mb-zero\">{{'LOCATION' |translate}}</h6>\r\n  </a>\r\n\r\n  <div class=\"c-stage__panel\" id=\"stage-panel2\">\r\n    <div class=\"u-p-medium\">\r\n      <app-no-content-panel [title]=\"'NO_LOCATION_YET'\" [description]=\"'CLICK_TO_ADD_LOCATION'\"  *ngIf=\"!data.place || !data.place.address\" appAddPlace [data]=\"data.place\" (dataChange)=\"this.data.place = $event\" ></app-no-content-panel>\r\n      <div *ngIf=\"data.place && data.place.address\">\r\n\r\n        <app-place-info [data]=\"data.place\"></app-place-info>\r\n\r\n        <a appAddPlace [data]=\"data.place\" (dataChange)=\"this.data.place = $event\" >{{'UPDATE_LOCATION' | translate}}</a>\r\n      </div>\r\n    </div>\r\n  </div>\r\n\r\n  <a class=\"c-stage__header u-flex u-justify-between\" data-toggle=\"collapse\" aria-expanded=\"true\">\r\n    <h6 class=\"u-text-mute u-text-uppercase u-text-small u-mb-zero\">{{'GALLERY' |translate}}</h6>\r\n  </a>\r\n\r\n  <div class=\"c-stage__panel\">\r\n    <div class=\"u-p-medium\">\r\n      <app-image-gallery [data]=\"data\" (dataChange)=\"this.data = $event\" [canAddPhotos]=\"true\"></app-image-gallery>\r\n    </div>\r\n  </div>\r\n\r\n</article><!-- // .c-stage -->\r\n"

/***/ }),

/***/ "./src/app/components/event-info-management-card/event-info-management-card.component.scss":
/*!*************************************************************************************************!*\
  !*** ./src/app/components/event-info-management-card/event-info-management-card.component.scss ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/event-info-management-card/event-info-management-card.component.ts":
/*!***********************************************************************************************!*\
  !*** ./src/app/components/event-info-management-card/event-info-management-card.component.ts ***!
  \***********************************************************************************************/
/*! exports provided: EventInfoManagementCardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EventInfoManagementCardComponent", function() { return EventInfoManagementCardComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _models_event__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../models/event */ "./src/app/models/event.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var EventInfoManagementCardComponent = /** @class */ (function () {
    function EventInfoManagementCardComponent() {
        this.data = {
            status: _models_event__WEBPACK_IMPORTED_MODULE_1__["EVENT_STATUS"][0],
            place: {
                photos: []
            },
            tags: [],
            photos: []
        };
        this.dataChange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
    }
    EventInfoManagementCardComponent.prototype.ngOnInit = function () {
        // alert(JSON.stringify(this.data));
    };
    EventInfoManagementCardComponent.prototype.addTag = function (event) {
        if (!this.data.tags) {
            this.data.tags = [];
        }
        if (event.length > 2 && this.data.tags.indexOf(event) === -1) {
            this.data.tags.push(event);
        }
        this.tag = '';
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], EventInfoManagementCardComponent.prototype, "data", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], EventInfoManagementCardComponent.prototype, "dataChange", void 0);
    EventInfoManagementCardComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-event-info-management-card',
            template: __webpack_require__(/*! ./event-info-management-card.component.html */ "./src/app/components/event-info-management-card/event-info-management-card.component.html"),
            styles: [__webpack_require__(/*! ./event-info-management-card.component.scss */ "./src/app/components/event-info-management-card/event-info-management-card.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], EventInfoManagementCardComponent);
    return EventInfoManagementCardComponent;
}());



/***/ }),

/***/ "./src/app/components/event-list-item/event-list-item.component.html":
/*!***************************************************************************!*\
  !*** ./src/app/components/event-list-item/event-list-item.component.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<td class=\"c-table__cell\">{{data.name}}\r\n  <span class=\"u-block u-text-mute u-text-xsmall\" *ngIf=\"data.place && data.place.name\">\r\n    {{data.place.name}} <span *ngIf=\"data.place.address\"> - {{data.place.address.address}}</span>\r\n  </span>\r\n</td>\r\n\r\n<td class=\"c-table__cell\">{{data.date_from | amDateFormat:'DD MMM'}} - {{data.date_to | amDateFormat:'DD MMM'}}</td>\r\n<td class=\"c-table__cell\">{{data.price}}\r\n  <span class=\"u-block u-text-xsmall u-text-mute\">\r\n    {{data.available_tickets}} {{'TICKETS_REMAINING' | translate}}\r\n  </span>\r\n</td>\r\n<td class=\"c-table__cell u-text-right\">\r\n  <div class=\"c-dropdown dropdown\">\r\n    <div class=\"c-dropdown u-ml-medium dropdown\" dropdown>\r\n      <a  class=\"c-btn c-btn--secondary has-dropdown dropdown-toggle has-dropdown dropdown-toggle button\"  id=\"dropdwonMenuAvatar\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\" dropdownToggle >\r\n        {{'ACTIONS' |translate}}\r\n      </a>\r\n\r\n      <div class=\"c-dropdown__menu dropdown-menu dropdown-menu-right\" aria-labelledby=\"dropdwonMenuAvatar\" *dropdownMenu>\r\n        <a class=\"c-dropdown__item dropdown-item\" routerLink=\"/event-management/event/{{data.id}}\">{{'EDIT' |translate}}</a>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</td>\r\n"

/***/ }),

/***/ "./src/app/components/event-list-item/event-list-item.component.scss":
/*!***************************************************************************!*\
  !*** ./src/app/components/event-list-item/event-list-item.component.scss ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/event-list-item/event-list-item.component.ts":
/*!*************************************************************************!*\
  !*** ./src/app/components/event-list-item/event-list-item.component.ts ***!
  \*************************************************************************/
/*! exports provided: EventListItemComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EventListItemComponent", function() { return EventListItemComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _models_event__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../models/event */ "./src/app/models/event.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var EventListItemComponent = /** @class */ (function () {
    function EventListItemComponent() {
        this.data = new _models_event__WEBPACK_IMPORTED_MODULE_1__["Event"]();
        this.class = 'c-table__row';
    }
    EventListItemComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], EventListItemComponent.prototype, "data", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"])('class'),
        __metadata("design:type", Object)
    ], EventListItemComponent.prototype, "class", void 0);
    EventListItemComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-event-list-item,[app-event-list-item]',
            template: __webpack_require__(/*! ./event-list-item.component.html */ "./src/app/components/event-list-item/event-list-item.component.html"),
            styles: [__webpack_require__(/*! ./event-list-item.component.scss */ "./src/app/components/event-list-item/event-list-item.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], EventListItemComponent);
    return EventListItemComponent;
}());



/***/ }),

/***/ "./src/app/components/event-statistics-card/event-statistics-card.component.html":
/*!***************************************************************************************!*\
  !*** ./src/app/components/event-statistics-card/event-statistics-card.component.html ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<article class=\"c-stage\">\r\n  <div class=\"c-stage__header o-media u-justify-start\">\r\n    <div class=\"c-stage__icon o-media__img\">\r\n      <i class=\"fa fa-check\"></i>\r\n    </div>\r\n    <div class=\"c-stage__header-title o-media__body\">\r\n      <h6 class=\"u-mb-zero\">Stage 1 - Initial Design Draft</h6>\r\n      <p class=\"u-text-xsmall u-text-mute\">Started 3 days ago  |  Expected time: 14 days</p>\r\n    </div>\r\n  </div>\r\n\r\n  <div class=\"c-stage__panel u-p-medium\">\r\n\r\n    <p class=\"u-text-mute u-text-uppercase u-text-small u-mb-xsmall\">Description</p>\r\n    <p class=\"u-mb-medium\">What we have done so far is brighten the colour palette, created new “easy to understand” icons, overall organization of the page and also worked on the copy! After first user tesing  we got some great results - users that went through the page could navigate easily and explain Tapdaq’s services right after.</p>\r\n\r\n    <p class=\"u-text-mute u-text-uppercase u-text-small u-mb-xsmall\">Resources</p>\r\n    <div class=\"row u-mb-medium\">\r\n      <div class=\"col-md-6 col-lg-4\">\r\n        <ul>\r\n          <li class=\"u-mb-xsmall u-text-small u-color-primary\">\r\n            <i class=\"fa fa-file-image-o u-text-mute u-mr-xsmall\"></i>Previous-design.png\r\n          </li>\r\n          <li class=\"u-mb-xsmall u-text-small u-color-primary\">\r\n            <i class=\"fa fa-file-text-o u-text-mute u-mr-xsmall\"></i>Marketing-Materials-2018.zip\r\n          </li>\r\n          <li class=\"u-mb-xsmall u-text-small u-color-primary\">\r\n            <i class=\"fa fa-file-text-o u-text-mute u-mr-xsmall\"></i>All-PSD-Files.zip\r\n          </li>\r\n        </ul>\r\n      </div>\r\n\r\n      <div class=\"col-md-6 col-lg-8\">\r\n        <ul>\r\n          <li class=\"u-mb-xsmall u-text-small u-color-primary\">\r\n            <i class=\"fa fa-file-text-o u-text-mute u-mr-xsmall\"></i>Brief.docx\r\n          </li>\r\n          <li class=\"u-mb-xsmall u-text-small u-color-primary\">\r\n            <i class=\"fa fa-file-text-o u-text-mute u-mr-xsmall\"></i>Copy-for-landing-page-by-Jason.docx\r\n          </li>\r\n        </ul>\r\n      </div>\r\n    </div>\r\n\r\n    <p class=\"u-text-mute u-text-uppercase u-text-small u-mb-xsmall\">Main goal of this stage</p>\r\n\r\n    <div class=\"row\">\r\n      <div class=\"col-md-6 col-lg-4\">\r\n        <ul>\r\n          <li class=\"u-mb-xsmall u-text-small u-color-primary\">\r\n            <i class=\"fa fa-check u-color-info u-text-mute u-mr-xsmall\"></i>New colour palette\r\n          </li>\r\n          <li class=\"u-mb-xsmall u-text-small u-color-primary\">\r\n            <i class=\"fa fa-check u-color-info u-text-mute u-mr-xsmall\"></i>Brand new and cool icons\r\n          </li>\r\n        </ul>\r\n      </div>\r\n\r\n      <div class=\"col-md-6 col-lg-4\">\r\n        <ul>\r\n          <li class=\"u-mb-xsmall u-text-small u-color-primary\">\r\n            <i class=\"fa fa-check u-color-info u-text-mute u-mr-xsmall\"></i>New copy\r\n          </li>\r\n        </ul>\r\n      </div>\r\n    </div>\r\n\r\n  </div><!-- // .c-stage__panel -->\r\n\r\n  <div class=\"c-stage__panel u-p-medium\">\r\n    <div class=\"o-media u-mb-small\">\r\n      <div class=\"o-media__img u-mr-xsmall\">\r\n        <div class=\"c-avatar c-avatar--xsmall\">\r\n          <img class=\"c-avatar__img\" src=\"assets/img/avatar-72.jpg\" alt=\"Profile Title\">\r\n        </div>\r\n\r\n      </div>\r\n      <div class=\"o-media__body\">\r\n        <h6 class=\"u-mb-zero u-text-small\">Gerald (You)</h6>\r\n        <p class=\"u-text-mute u-text-xsmall\">8:42AM</p>\r\n      </div>\r\n    </div><!-- // .o-media -->\r\n\r\n    <p class=\"u-mb-xsmall\">Hi Steven! <br>I’m sending you our first draft along with our comments. I just love the way this is going. At this point we still need to do some polishing but would love to hear if we’re heading the right direction from you!</p>\r\n\r\n    <p>\r\n      <i class=\"fa fa-file-image-o u-mr-xsmall u-text-mute\"></i>Landing-page-V1-png\r\n    </p>\r\n  </div>\r\n\r\n  <div class=\"c-stage__label\">\r\n    <i class=\"c-stage__label-icon fa fa-check-circle\"></i>\r\n    <p class=\"c-stage__label-title\">Stage 1 - Approved</p>\r\n  </div>\r\n\r\n  <div class=\"c-stage__panel u-p-medium\">\r\n    <div class=\"o-media u-mb-small\">\r\n      <div class=\"o-media__img u-mr-xsmall\">\r\n        <div class=\"c-avatar c-avatar--xsmall\">\r\n          <img class=\"c-avatar__img\" src=\"assets/img/avatar4-72.jpg\" alt=\"Profile Title\">\r\n        </div>\r\n\r\n      </div>\r\n      <div class=\"o-media__body\">\r\n        <h6 class=\"u-mb-zero u-text-small\">Steven Robinson</h6>\r\n        <p class=\"u-text-mute u-text-xsmall\">8:49AM</p>\r\n      </div>\r\n    </div><!-- // .o-media -->\r\n\r\n    <p class=\"u-mb-xsmall\">Amazing Stuff! I’ve approved this stage! Keep it up. We love it with Stephen! </p>\r\n  </div>\r\n</article><!-- // .c-stage -->\r\n"

/***/ }),

/***/ "./src/app/components/event-statistics-card/event-statistics-card.component.scss":
/*!***************************************************************************************!*\
  !*** ./src/app/components/event-statistics-card/event-statistics-card.component.scss ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/event-statistics-card/event-statistics-card.component.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/components/event-statistics-card/event-statistics-card.component.ts ***!
  \*************************************************************************************/
/*! exports provided: EventStatisticsCardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EventStatisticsCardComponent", function() { return EventStatisticsCardComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var EventStatisticsCardComponent = /** @class */ (function () {
    function EventStatisticsCardComponent() {
    }
    EventStatisticsCardComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], EventStatisticsCardComponent.prototype, "data", void 0);
    EventStatisticsCardComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-event-statistics-card',
            template: __webpack_require__(/*! ./event-statistics-card.component.html */ "./src/app/components/event-statistics-card/event-statistics-card.component.html"),
            styles: [__webpack_require__(/*! ./event-statistics-card.component.scss */ "./src/app/components/event-statistics-card/event-statistics-card.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], EventStatisticsCardComponent);
    return EventStatisticsCardComponent;
}());



/***/ }),

/***/ "./src/app/components/event-tickets-card/event-tickets-card.component.html":
/*!*********************************************************************************!*\
  !*** ./src/app/components/event-tickets-card/event-tickets-card.component.html ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<article class=\"c-stage\">\r\n  <div class=\"c-stage__header o-media u-justify-start\">\r\n    <div class=\"c-stage__icon o-media__img\">\r\n      <i class=\"fa fa-check\"></i>\r\n    </div>\r\n    <div class=\"c-stage__header-title o-media__body\">\r\n      <h6 class=\"u-mb-zero\">Stage 1 - Initial Design Draft</h6>\r\n      <p class=\"u-text-xsmall u-text-mute\">Started 3 days ago  |  Expected time: 14 days</p>\r\n    </div>\r\n  </div>\r\n\r\n  <div class=\"c-stage__panel u-p-medium\">\r\n\r\n    <p class=\"u-text-mute u-text-uppercase u-text-small u-mb-xsmall\">Description</p>\r\n    <p class=\"u-mb-medium\">What we have done so far is brighten the colour palette, created new “easy to understand” icons, overall organization of the page and also worked on the copy! After first user tesing  we got some great results - users that went through the page could navigate easily and explain Tapdaq’s services right after.</p>\r\n\r\n    <p class=\"u-text-mute u-text-uppercase u-text-small u-mb-xsmall\">Resources</p>\r\n    <div class=\"row u-mb-medium\">\r\n      <div class=\"col-md-6 col-lg-4\">\r\n        <ul>\r\n          <li class=\"u-mb-xsmall u-text-small u-color-primary\">\r\n            <i class=\"fa fa-file-image-o u-text-mute u-mr-xsmall\"></i>Previous-design.png\r\n          </li>\r\n          <li class=\"u-mb-xsmall u-text-small u-color-primary\">\r\n            <i class=\"fa fa-file-text-o u-text-mute u-mr-xsmall\"></i>Marketing-Materials-2018.zip\r\n          </li>\r\n          <li class=\"u-mb-xsmall u-text-small u-color-primary\">\r\n            <i class=\"fa fa-file-text-o u-text-mute u-mr-xsmall\"></i>All-PSD-Files.zip\r\n          </li>\r\n        </ul>\r\n      </div>\r\n\r\n      <div class=\"col-md-6 col-lg-8\">\r\n        <ul>\r\n          <li class=\"u-mb-xsmall u-text-small u-color-primary\">\r\n            <i class=\"fa fa-file-text-o u-text-mute u-mr-xsmall\"></i>Brief.docx\r\n          </li>\r\n          <li class=\"u-mb-xsmall u-text-small u-color-primary\">\r\n            <i class=\"fa fa-file-text-o u-text-mute u-mr-xsmall\"></i>Copy-for-landing-page-by-Jason.docx\r\n          </li>\r\n        </ul>\r\n      </div>\r\n    </div>\r\n\r\n    <p class=\"u-text-mute u-text-uppercase u-text-small u-mb-xsmall\">Main goal of this stage</p>\r\n\r\n    <div class=\"row\">\r\n      <div class=\"col-md-6 col-lg-4\">\r\n        <ul>\r\n          <li class=\"u-mb-xsmall u-text-small u-color-primary\">\r\n            <i class=\"fa fa-check u-color-info u-text-mute u-mr-xsmall\"></i>New colour palette\r\n          </li>\r\n          <li class=\"u-mb-xsmall u-text-small u-color-primary\">\r\n            <i class=\"fa fa-check u-color-info u-text-mute u-mr-xsmall\"></i>Brand new and cool icons\r\n          </li>\r\n        </ul>\r\n      </div>\r\n\r\n      <div class=\"col-md-6 col-lg-4\">\r\n        <ul>\r\n          <li class=\"u-mb-xsmall u-text-small u-color-primary\">\r\n            <i class=\"fa fa-check u-color-info u-text-mute u-mr-xsmall\"></i>New copy\r\n          </li>\r\n        </ul>\r\n      </div>\r\n    </div>\r\n\r\n  </div><!-- // .c-stage__panel -->\r\n\r\n  <div class=\"c-stage__panel u-p-medium\">\r\n    <div class=\"o-media u-mb-small\">\r\n      <div class=\"o-media__img u-mr-xsmall\">\r\n        <div class=\"c-avatar c-avatar--xsmall\">\r\n          <img class=\"c-avatar__img\" src=\"assets/img/avatar-72.jpg\" alt=\"Profile Title\">\r\n        </div>\r\n\r\n      </div>\r\n      <div class=\"o-media__body\">\r\n        <h6 class=\"u-mb-zero u-text-small\">Gerald (You)</h6>\r\n        <p class=\"u-text-mute u-text-xsmall\">8:42AM</p>\r\n      </div>\r\n    </div><!-- // .o-media -->\r\n\r\n    <p class=\"u-mb-xsmall\">Hi Steven! <br>I’m sending you our first draft along with our comments. I just love the way this is going. At this point we still need to do some polishing but would love to hear if we’re heading the right direction from you!</p>\r\n\r\n    <p>\r\n      <i class=\"fa fa-file-image-o u-mr-xsmall u-text-mute\"></i>Landing-page-V1-png\r\n    </p>\r\n  </div>\r\n\r\n  <div class=\"c-stage__label\">\r\n    <i class=\"c-stage__label-icon fa fa-check-circle\"></i>\r\n    <p class=\"c-stage__label-title\">Stage 1 - Approved</p>\r\n  </div>\r\n\r\n  <div class=\"c-stage__panel u-p-medium\">\r\n    <div class=\"o-media u-mb-small\">\r\n      <div class=\"o-media__img u-mr-xsmall\">\r\n        <div class=\"c-avatar c-avatar--xsmall\">\r\n          <img class=\"c-avatar__img\" src=\"assets/img/avatar4-72.jpg\" alt=\"Profile Title\">\r\n        </div>\r\n\r\n      </div>\r\n      <div class=\"o-media__body\">\r\n        <h6 class=\"u-mb-zero u-text-small\">Steven Robinson</h6>\r\n        <p class=\"u-text-mute u-text-xsmall\">8:49AM</p>\r\n      </div>\r\n    </div><!-- // .o-media -->\r\n\r\n    <p class=\"u-mb-xsmall\">Amazing Stuff! I’ve approved this stage! Keep it up. We love it with Stephen! </p>\r\n  </div>\r\n</article><!-- // .c-stage -->\r\n"

/***/ }),

/***/ "./src/app/components/event-tickets-card/event-tickets-card.component.scss":
/*!*********************************************************************************!*\
  !*** ./src/app/components/event-tickets-card/event-tickets-card.component.scss ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/event-tickets-card/event-tickets-card.component.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/components/event-tickets-card/event-tickets-card.component.ts ***!
  \*******************************************************************************/
/*! exports provided: EventTicketsCardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EventTicketsCardComponent", function() { return EventTicketsCardComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var EventTicketsCardComponent = /** @class */ (function () {
    function EventTicketsCardComponent() {
    }
    EventTicketsCardComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], EventTicketsCardComponent.prototype, "data", void 0);
    EventTicketsCardComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-event-tickets-card',
            template: __webpack_require__(/*! ./event-tickets-card.component.html */ "./src/app/components/event-tickets-card/event-tickets-card.component.html"),
            styles: [__webpack_require__(/*! ./event-tickets-card.component.scss */ "./src/app/components/event-tickets-card/event-tickets-card.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], EventTicketsCardComponent);
    return EventTicketsCardComponent;
}());



/***/ }),

/***/ "./src/app/components/file-upload/file-upload.component.html":
/*!*******************************************************************!*\
  !*** ./src/app/components/file-upload/file-upload.component.html ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\r\n  file-upload works!\r\n</p>\r\n"

/***/ }),

/***/ "./src/app/components/file-upload/file-upload.component.scss":
/*!*******************************************************************!*\
  !*** ./src/app/components/file-upload/file-upload.component.scss ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/file-upload/file-upload.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/components/file-upload/file-upload.component.ts ***!
  \*****************************************************************/
/*! exports provided: FileUploadComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FileUploadComponent", function() { return FileUploadComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var FileUploadComponent = /** @class */ (function () {
    function FileUploadComponent() {
    }
    FileUploadComponent.prototype.ngOnInit = function () {
    };
    FileUploadComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-file-upload',
            template: __webpack_require__(/*! ./file-upload.component.html */ "./src/app/components/file-upload/file-upload.component.html"),
            styles: [__webpack_require__(/*! ./file-upload.component.scss */ "./src/app/components/file-upload/file-upload.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], FileUploadComponent);
    return FileUploadComponent;
}());



/***/ }),

/***/ "./src/app/components/filter-input/filter-input.component.html":
/*!*********************************************************************!*\
  !*** ./src/app/components/filter-input/filter-input.component.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div _ngcontent-c3=\"\" class=\"c-field has-icon-right c-navbar__search u-ml-auto u-hidden-down@tablet pull-right\">\r\n  <span _ngcontent-c3=\"\" class=\"c-field__icon\">\r\n    <i _ngcontent-c3=\"\" class=\"fa fa-search\"></i>\r\n  </span>\r\n  <label _ngcontent-c3=\"\" class=\"u-hidden-visually\" for=\"navbar-search\">{{'SEARCH' |translate}}</label>\r\n  <input _ngcontent-c3=\"\" class=\"c-input\" id=\"navbar-search\" placeholder=\"{{ placeholder | translate}}\" type=\"text\" [(ngModel)]=\"text\" (ngModelChange)=\"onChange($event, selection)\">\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/components/filter-input/filter-input.component.scss":
/*!*********************************************************************!*\
  !*** ./src/app/components/filter-input/filter-input.component.scss ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/filter-input/filter-input.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/components/filter-input/filter-input.component.ts ***!
  \*******************************************************************/
/*! exports provided: FilterInputComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FilterInputComponent", function() { return FilterInputComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var FilterInputComponent = /** @class */ (function () {
    function FilterInputComponent() {
        this.placeholder = 'SEARCH_BY_NAME';
        this.filterEvent = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
    }
    FilterInputComponent.prototype.onChange = function (event) {
        this.filterEvent.emit(this.text);
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], FilterInputComponent.prototype, "filterEvent", void 0);
    FilterInputComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-filter-input',
            template: __webpack_require__(/*! ./filter-input.component.html */ "./src/app/components/filter-input/filter-input.component.html"),
            styles: [__webpack_require__(/*! ./filter-input.component.scss */ "./src/app/components/filter-input/filter-input.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], FilterInputComponent);
    return FilterInputComponent;
}());



/***/ }),

/***/ "./src/app/components/image-gallery/image-gallery.component.html":
/*!***********************************************************************!*\
  !*** ./src/app/components/image-gallery/image-gallery.component.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"c-feed__gallery Grid\">\r\n\r\n  <div class=\"c-feed__gallery-item Grid-item\" *ngFor=\"let image of data.photos\">\r\n    <img [src]=\"image || 'assets/img/feed7.jpg'\" alt=\"photos' title\">\r\n  </div>\r\n  <app-image-upload [data]=\"data\" (dataChange)=\"this.data = $event\" *ngIf=\"canAddPhoto\">\r\n    <div class=\"c-feed__gallery-item Grid-item\" >\r\n      <img src=\"assets/img/add-photo.png\" alt=\"photos' title\">\r\n    </div>\r\n  </app-image-upload>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/components/image-gallery/image-gallery.component.scss":
/*!***********************************************************************!*\
  !*** ./src/app/components/image-gallery/image-gallery.component.scss ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".Grid {\n  display: -ms-grid;\n  display: grid;\n  grid-template-columns: repeat(auto-fill, minmax(5rem, 1fr));\n  grid-auto-rows: 1fr; }\n  .Grid div {\n    width: 100%;\n    height: 100%;\n    padding: 2px;\n    border-radius: 5px; }\n  .Grid img {\n    border-radius: 5px; }\n  .Grid .c-feed__gallery-item {\n    margin: 0 auto; }\n  .Grid::before {\n  content: '';\n  width: 0;\n  padding-bottom: 100%;\n  -ms-grid-row: 1;\n  grid-row: 1 / 1;\n  -ms-grid-column: 1;\n  grid-column: 1 / 1; }\n  .Grid > *:first-child {\n  -ms-grid-row: 1;\n  grid-row: 1 / 1;\n  -ms-grid-column: 1;\n  grid-column: 1 / 1; }\n  /* Just to make the grid visible */\n"

/***/ }),

/***/ "./src/app/components/image-gallery/image-gallery.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/components/image-gallery/image-gallery.component.ts ***!
  \*********************************************************************/
/*! exports provided: ImageGalleryComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ImageGalleryComponent", function() { return ImageGalleryComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ImageGalleryComponent = /** @class */ (function () {
    function ImageGalleryComponent() {
        this.data = {
            images: []
        };
        this.canAddPhotos = true;
        this.dataChange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
    }
    ImageGalleryComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], ImageGalleryComponent.prototype, "data", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], ImageGalleryComponent.prototype, "canAddPhotos", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], ImageGalleryComponent.prototype, "dataChange", void 0);
    ImageGalleryComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-image-gallery',
            template: __webpack_require__(/*! ./image-gallery.component.html */ "./src/app/components/image-gallery/image-gallery.component.html"),
            styles: [__webpack_require__(/*! ./image-gallery.component.scss */ "./src/app/components/image-gallery/image-gallery.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], ImageGalleryComponent);
    return ImageGalleryComponent;
}());



/***/ }),

/***/ "./src/app/components/image-grid/image-grid.component.html":
/*!*****************************************************************!*\
  !*** ./src/app/components/image-grid/image-grid.component.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\r\n  image-grid works!\r\n</p>\r\n"

/***/ }),

/***/ "./src/app/components/image-grid/image-grid.component.scss":
/*!*****************************************************************!*\
  !*** ./src/app/components/image-grid/image-grid.component.scss ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/image-grid/image-grid.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/components/image-grid/image-grid.component.ts ***!
  \***************************************************************/
/*! exports provided: ImageGridComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ImageGridComponent", function() { return ImageGridComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ImageGridComponent = /** @class */ (function () {
    function ImageGridComponent() {
    }
    ImageGridComponent.prototype.ngOnInit = function () {
    };
    ImageGridComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-image-grid',
            template: __webpack_require__(/*! ./image-grid.component.html */ "./src/app/components/image-grid/image-grid.component.html"),
            styles: [__webpack_require__(/*! ./image-grid.component.scss */ "./src/app/components/image-grid/image-grid.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], ImageGridComponent);
    return ImageGridComponent;
}());



/***/ }),

/***/ "./src/app/components/image-upload/image-upload.component.html":
/*!*********************************************************************!*\
  !*** ./src/app/components/image-upload/image-upload.component.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<input type=\"file\" accept=\"image/*\" (change)=\"onFileChange($event)\" multiple #myInput style=\"height: 1px; width: 1px; \">\r\n<span (click)=\"onClickAddPhoto()\">\r\n  <ng-content ></ng-content>\r\n</span>\r\n"

/***/ }),

/***/ "./src/app/components/image-upload/image-upload.component.scss":
/*!*********************************************************************!*\
  !*** ./src/app/components/image-upload/image-upload.component.scss ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/image-upload/image-upload.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/components/image-upload/image-upload.component.ts ***!
  \*******************************************************************/
/*! exports provided: ImageUploadComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ImageUploadComponent", function() { return ImageUploadComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _defaults__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../defaults */ "./src/app/defaults.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ImageUploadComponent = /** @class */ (function () {
    function ImageUploadComponent(http) {
        this.http = http;
        this.data = {
            images: []
        };
        this.dataChange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
    }
    ImageUploadComponent.prototype.ngOnInit = function () {
    };
    ImageUploadComponent.prototype.onFileChange = function (event) {
        for (var i = 0; i < event.target.files.length; i++) {
            this.upload(event.target.files[i]);
        }
    };
    ImageUploadComponent.prototype.upload = function (file) {
        var _this = this;
        var url = _defaults__WEBPACK_IMPORTED_MODULE_2__["API"].URL + "upload";
        var fd = new FormData();
        console.log(url);
        fd.append('image', file, file.name);
        this.http.post(url, fd, {
            reportProgress: true,
            observe: 'events'
        }).subscribe(function (event) {
            if (event.type === _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpEventType"].UploadProgress) {
                console.log("Upload progress: " + Math.round(event.loaded / event.total) * 100);
            }
            if (event.type === _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpEventType"].Response) {
                _this.data.photos.push('assets/img/feed7.jpg');
                _this.dataChange.emit(_this.data);
            }
        }, function (error) {
            console.log('Failed to upload file', error);
            _this.data.photos.push('assets/img/feed7.jpg');
            _this.dataChange.emit(_this.data);
        }, function () {
            console.log('Complete');
        });
    };
    ImageUploadComponent.prototype.onClickAddPhoto = function () {
        this.inputEl.nativeElement.click();
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], ImageUploadComponent.prototype, "data", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], ImageUploadComponent.prototype, "dataChange", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('myInput'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], ImageUploadComponent.prototype, "inputEl", void 0);
    ImageUploadComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-image-upload',
            template: __webpack_require__(/*! ./image-upload.component.html */ "./src/app/components/image-upload/image-upload.component.html"),
            styles: [__webpack_require__(/*! ./image-upload.component.scss */ "./src/app/components/image-upload/image-upload.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], ImageUploadComponent);
    return ImageUploadComponent;
}());



/***/ }),

/***/ "./src/app/components/income-sumary/income-sumary.component.html":
/*!***********************************************************************!*\
  !*** ./src/app/components/income-sumary/income-sumary.component.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"c-card u-mb-medium\" data-mh=\"invoices-cards\">\r\n  <div class=\"c-card__header c-card__header--transparent o-line u-border-bottom-zero\">\r\n    <h3 class=\"c-card__title\">Monthly Summary\r\n      <span class=\"u-block u-text-mute u-text-xsmall\">\r\n        February 2017 (12 Days Remaining)\r\n      </span>\r\n    </h3>\r\n\r\n    <div class=\"c-card__meta u-relative\">\r\n      <a class=\"dropdown-toggle\"  id=\"dropdwonMenuCard1\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\r\n        <i class=\"u-text-mute u-opacity-heavy fa fa-ellipsis-h\"></i>\r\n      </a>\r\n\r\n      <div class=\"c-dropdown__menu dropdown-menu dropdown-menu-right fade\" aria-labelledby=\"dropdwonMenuCard1\">\r\n        <a class=\"c-dropdown__item\" >Link 1</a>\r\n        <a class=\"c-dropdown__item\" >Link 2</a>\r\n      </div>\r\n    </div>\r\n\r\n  </div>\r\n\r\n  <div class=\"row\">\r\n    <div class=\"col-md-4\">\r\n      <div class=\"c-summary has-divider\">\r\n        <h5 class=\"c-summary__title\">Invoiced</h5>\r\n        <h5 class=\"c-summary__number\">$2,190\r\n          <span class=\"c-summary__status u-bg-success\">\r\n            <i class=\"fa fa-caret-up\"></i>\r\n          </span>\r\n        </h5>\r\n        <p class=\"c-summary__meta\">Last Month: $2,890</p>\r\n      </div>\r\n    </div>\r\n\r\n    <div class=\"col-md-4\">\r\n      <div class=\"c-summary has-divider\">\r\n        <h5 class=\"c-summary__title\">Profit</h5>\r\n        <h5 class=\"c-summary__number\">$2,190\r\n          <span class=\"c-summary__status u-bg-danger\">\r\n            <i class=\"fa fa-caret-down\"></i>\r\n          </span>\r\n        </h5>\r\n        <p class=\"c-summary__meta\">Last Month: $2,890</p>\r\n      </div>\r\n    </div>\r\n\r\n    <div class=\"col-md-4\">\r\n      <div class=\"c-summary\">\r\n        <h5 class=\"c-summary__title\">Expenses</h5>\r\n        <h5 class=\"c-summary__number\">$2,190\r\n          <span class=\"c-summary__status u-bg-success\">\r\n            <i class=\"fa fa-caret-up\"></i>\r\n          </span>\r\n        </h5>\r\n        <p class=\"c-summary__meta\">Last Month: $2,890</p>\r\n      </div>\r\n    </div>\r\n  </div><!-- // .row -->\r\n</div><!-- // .c-card -->\r\n"

/***/ }),

/***/ "./src/app/components/income-sumary/income-sumary.component.scss":
/*!***********************************************************************!*\
  !*** ./src/app/components/income-sumary/income-sumary.component.scss ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/income-sumary/income-sumary.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/components/income-sumary/income-sumary.component.ts ***!
  \*********************************************************************/
/*! exports provided: IncomeSumaryComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IncomeSumaryComponent", function() { return IncomeSumaryComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var IncomeSumaryComponent = /** @class */ (function () {
    function IncomeSumaryComponent() {
    }
    IncomeSumaryComponent.prototype.ngOnInit = function () {
    };
    IncomeSumaryComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-income-sumary',
            template: __webpack_require__(/*! ./income-sumary.component.html */ "./src/app/components/income-sumary/income-sumary.component.html"),
            styles: [__webpack_require__(/*! ./income-sumary.component.scss */ "./src/app/components/income-sumary/income-sumary.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], IncomeSumaryComponent);
    return IncomeSumaryComponent;
}());



/***/ }),

/***/ "./src/app/components/inline-input/inline-input.component.html":
/*!*********************************************************************!*\
  !*** ./src/app/components/inline-input/inline-input.component.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div [ngClass]=\"(data && data.length > 0) ? '' : 'bordered'\">\r\n  <span *ngIf=\"!isEditing\" class=\"click-to-edit-text form-control\" (click)=\"startEdit(myInput)\">{{data || placeholder |translate}}</span>\r\n  <input class=\"c-input\" type=\"text\" [(ngModel)]=\"data\" (ngModelChange)=\"data=$event\"  (blur)=\"endEdit()\" (keyup.enter)=\"endEdit()\" *ngIf=\"isEditing\"  #myInput >\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/components/inline-input/inline-input.component.scss":
/*!*********************************************************************!*\
  !*** ./src/app/components/inline-input/inline-input.component.scss ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".click-to-edit-text.form-control, input.c-input {\n  height: 22px;\n  padding: 2px 2px;\n  margin-left: -3px;\n  margin-top: -4px;\n  border-left: 0px;\n  border-right: 0px;\n  border-top: 0px; }\n\n.click-to-edit-text {\n  border: 1px solid transparent;\n  background: transparent;\n  cursor: text;\n  box-shadow: none; }\n\n.bordered {\n  border-bottom: 1px solid rgba(0, 0, 0, 0.03); }\n"

/***/ }),

/***/ "./src/app/components/inline-input/inline-input.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/components/inline-input/inline-input.component.ts ***!
  \*******************************************************************/
/*! exports provided: InlineInputComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InlineInputComponent", function() { return InlineInputComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var InlineInputComponent = /** @class */ (function () {
    function InlineInputComponent() {
        this.data = '';
        this.placeholder = '';
        this.dataChange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
    }
    InlineInputComponent.prototype.ngOnInit = function () {
    };
    InlineInputComponent.prototype.endEdit = function () {
        this.isEditing = false;
        this.dataChange.emit(this.data);
    };
    InlineInputComponent.prototype.startEdit = function (el) {
        var _this = this;
        this.isEditing = true;
        setTimeout(function () {
            _this.inputEl.nativeElement.focus();
        }, 0);
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], InlineInputComponent.prototype, "data", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], InlineInputComponent.prototype, "placeholder", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('myInput'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], InlineInputComponent.prototype, "inputEl", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], InlineInputComponent.prototype, "dataChange", void 0);
    InlineInputComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-inline-input',
            template: __webpack_require__(/*! ./inline-input.component.html */ "./src/app/components/inline-input/inline-input.component.html"),
            styles: [__webpack_require__(/*! ./inline-input.component.scss */ "./src/app/components/inline-input/inline-input.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], InlineInputComponent);
    return InlineInputComponent;
}());



/***/ }),

/***/ "./src/app/components/inline-mce/inline-mce.component.html":
/*!*****************************************************************!*\
  !*** ./src/app/components/inline-mce/inline-mce.component.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div (click)=\"openEditor()\" [ngClass]=\"(data && data.length > 0) ? '' : 'bordered'\">\r\n  <p class=\"u-mb-medium\" [innerHTML]=\"data\" ></p>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/components/inline-mce/inline-mce.component.scss":
/*!*****************************************************************!*\
  !*** ./src/app/components/inline-mce/inline-mce.component.scss ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "div {\n  min-height: 100px;\n  cursor: text; }\n\n.bordered {\n  border: 1px solid rgba(0, 0, 0, 0.03);\n  border-radius: 5px; }\n"

/***/ }),

/***/ "./src/app/components/inline-mce/inline-mce.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/components/inline-mce/inline-mce.component.ts ***!
  \***************************************************************/
/*! exports provided: InlineMceComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InlineMceComponent", function() { return InlineMceComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm5/ngx-bootstrap-modal.js");
/* harmony import */ var _modals_modal_mce_modal_mce_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../modals/modal-mce/modal-mce.component */ "./src/app/components/modals/modal-mce/modal-mce.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var InlineMceComponent = /** @class */ (function () {
    function InlineMceComponent(modalService) {
        this.modalService = modalService;
        this.data = '';
        this.dataChange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
    }
    InlineMceComponent.prototype.ngOnInit = function () {
    };
    InlineMceComponent.prototype.openEditor = function () {
        var _this = this;
        console.log('XXXXXXXXXXX');
        this.modalRef = this.modalService.show(_modals_modal_mce_modal_mce_component__WEBPACK_IMPORTED_MODULE_2__["ModalMceComponent"], {
            initialState: {
                data: this.data,
                callback: function (data) {
                    _this.data = data;
                    _this.dataChange.emit(data);
                }
            },
            class: 'modal-lg'
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], InlineMceComponent.prototype, "data", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], InlineMceComponent.prototype, "dataChange", void 0);
    InlineMceComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-inline-mce',
            template: __webpack_require__(/*! ./inline-mce.component.html */ "./src/app/components/inline-mce/inline-mce.component.html"),
            styles: [__webpack_require__(/*! ./inline-mce.component.scss */ "./src/app/components/inline-mce/inline-mce.component.scss")]
        }),
        __metadata("design:paramtypes", [ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_1__["BsModalService"]])
    ], InlineMceComponent);
    return InlineMceComponent;
}());



/***/ }),

/***/ "./src/app/components/invoice-single-item-list-item/invoice-single-item-list-item.component.html":
/*!*******************************************************************************************************!*\
  !*** ./src/app/components/invoice-single-item-list-item/invoice-single-item-list-item.component.html ***!
  \*******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n  <td class=\"c-table__cell\">\r\n    {{data.item.name}}\r\n    <span *ngIf=\"data.ticket && data.ticket.promotion\"> {{data.ticket.event.name}}- {{data.ticket.promotion}}</span>\r\n  </td>\r\n  <td class=\"c-table__cell\">{{data.amount}}</td>\r\n  <td class=\"c-table__cell\">\r\n    <span *ngIf=\"data.ticket\">{{data.ticket.price}}</span>\r\n    <span *ngIf=\"data.item\">{{data.item.price}}</span>\r\n  </td>\r\n\r\n"

/***/ }),

/***/ "./src/app/components/invoice-single-item-list-item/invoice-single-item-list-item.component.scss":
/*!*******************************************************************************************************!*\
  !*** ./src/app/components/invoice-single-item-list-item/invoice-single-item-list-item.component.scss ***!
  \*******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/invoice-single-item-list-item/invoice-single-item-list-item.component.ts":
/*!*****************************************************************************************************!*\
  !*** ./src/app/components/invoice-single-item-list-item/invoice-single-item-list-item.component.ts ***!
  \*****************************************************************************************************/
/*! exports provided: InvoiceSingleItemListItemComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InvoiceSingleItemListItemComponent", function() { return InvoiceSingleItemListItemComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _models_order_item__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../models/order-item */ "./src/app/models/order-item.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var InvoiceSingleItemListItemComponent = /** @class */ (function () {
    function InvoiceSingleItemListItemComponent() {
        this.data = new _models_order_item__WEBPACK_IMPORTED_MODULE_1__["OrderItem"]();
        this.class = 'c-table__row';
    }
    InvoiceSingleItemListItemComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], InvoiceSingleItemListItemComponent.prototype, "data", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"])('class'),
        __metadata("design:type", Object)
    ], InvoiceSingleItemListItemComponent.prototype, "class", void 0);
    InvoiceSingleItemListItemComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-invoice-single-item-list-item, [app-invoice-single-item-list-item]',
            template: __webpack_require__(/*! ./invoice-single-item-list-item.component.html */ "./src/app/components/invoice-single-item-list-item/invoice-single-item-list-item.component.html"),
            styles: [__webpack_require__(/*! ./invoice-single-item-list-item.component.scss */ "./src/app/components/invoice-single-item-list-item/invoice-single-item-list-item.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], InvoiceSingleItemListItemComponent);
    return InvoiceSingleItemListItemComponent;
}());



/***/ }),

/***/ "./src/app/components/item-list-item/item-list-item.component.html":
/*!*************************************************************************!*\
  !*** ./src/app/components/item-list-item/item-list-item.component.html ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<td class=\"c-table__cell\"><span class=\"u-text-mute\">{{data.id}}</span></td>\r\n<td class=\"c-table__cell\">{{data.name}}</td>\r\n<td class=\"c-table__cell\">{{data.description}}</td>\r\n<td class=\"c-table__cell\">\r\n  <span class=\"u-text-mute\">{{data.type.type |translate}}</span>\r\n</td>\r\n<td class=\"c-table__cell\">\r\n  <span class=\"u-text-mute\" *ngIf=\"data.brand && data.brand.name\">{{data.brand.name}}</span>\r\n</td>\r\n"

/***/ }),

/***/ "./src/app/components/item-list-item/item-list-item.component.scss":
/*!*************************************************************************!*\
  !*** ./src/app/components/item-list-item/item-list-item.component.scss ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/item-list-item/item-list-item.component.ts":
/*!***********************************************************************!*\
  !*** ./src/app/components/item-list-item/item-list-item.component.ts ***!
  \***********************************************************************/
/*! exports provided: ItemListItemComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ItemListItemComponent", function() { return ItemListItemComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _models_item__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../models/item */ "./src/app/models/item.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ItemListItemComponent = /** @class */ (function () {
    function ItemListItemComponent() {
        this.data = new _models_item__WEBPACK_IMPORTED_MODULE_1__["Item"]();
        this.class = 'c-table__row';
    }
    ItemListItemComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], ItemListItemComponent.prototype, "data", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"])('class'),
        __metadata("design:type", Object)
    ], ItemListItemComponent.prototype, "class", void 0);
    ItemListItemComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-item-list-item,[app-item-list-item]',
            template: __webpack_require__(/*! ./item-list-item.component.html */ "./src/app/components/item-list-item/item-list-item.component.html"),
            styles: [__webpack_require__(/*! ./item-list-item.component.scss */ "./src/app/components/item-list-item/item-list-item.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], ItemListItemComponent);
    return ItemListItemComponent;
}());



/***/ }),

/***/ "./src/app/components/management-dashboard-side-menu/management-dashboard-side-menu.component.html":
/*!*********************************************************************************************************!*\
  !*** ./src/app/components/management-dashboard-side-menu/management-dashboard-side-menu.component.html ***!
  \*********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<aside class=\"c-menu u-ml-medium u-hidden-down@wide\">\r\n  <div *ngFor=\"let m of menuConfig\">\r\n  <h4 class=\"c-menu__title\">{{m.name |translate}}</h4>\r\n    <ul class=\"u-mb-medium\">\r\n      <li class=\"c-menu__item\" *ngFor=\"let i of m.items\">\r\n        <a class=\"c-menu__link is_active\" *ngIf=\"i.action\" (click)=\"broadcast(i)\">\r\n          <i class=\"fa fa-{{i.icon}} u-mr-xsmall\" *ngIf=\"i.icon\"></i>\r\n          <img src=\"{{i.image}}\" class=\"u-mr-xsmall\" style=\"width: 14px;\" *ngIf=\"i.image\">\r\n          {{i.name | translate}}\r\n        </a>\r\n        <a [routerLink]=\"i.path\" class=\"c-menu__link \" *ngIf=\"i.path\">\r\n          <i class=\"fa fa-{{i.icon}} u-mr-xsmall\" *ngIf=\"i.icon\"></i>\r\n          <img src=\"{{i.image}}\" class=\"u-mr-xsmall\" style=\"width: 14px;\" *ngIf=\"i.image\">\r\n          {{i.name | translate}}\r\n        </a>\r\n      </li>\r\n\r\n    </ul>\r\n  </div>\r\n</aside>\r\n"

/***/ }),

/***/ "./src/app/components/management-dashboard-side-menu/management-dashboard-side-menu.component.scss":
/*!*********************************************************************************************************!*\
  !*** ./src/app/components/management-dashboard-side-menu/management-dashboard-side-menu.component.scss ***!
  \*********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/management-dashboard-side-menu/management-dashboard-side-menu.component.ts":
/*!*******************************************************************************************************!*\
  !*** ./src/app/components/management-dashboard-side-menu/management-dashboard-side-menu.component.ts ***!
  \*******************************************************************************************************/
/*! exports provided: ManagementDashboardSideMenuComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ManagementDashboardSideMenuComponent", function() { return ManagementDashboardSideMenuComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ManagementDashboardSideMenuComponent = /** @class */ (function () {
    function ManagementDashboardSideMenuComponent(differs) {
        this.menuConfig = []; // [{title:string, items: [{name, path?, action?, icon]]
        this.action = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.differ = differs.find([]).create(null);
    }
    ManagementDashboardSideMenuComponent.prototype.ngOnInit = function () {
    };
    ManagementDashboardSideMenuComponent.prototype.broadcast = function (item) {
        console.log(item);
        item.active = true;
        this.action.emit(item);
    };
    ManagementDashboardSideMenuComponent.prototype.ngDoCheck = function () {
        var change = this.differ.diff(this.menuConfig);
        // console.log(change);
        // here you can do what you want on array change
        // you can check for forEachAddedItem or forEachRemovedItem on change object to see the added/removed items
        // Attention: ngDoCheck() is triggered at each binded variable on componenet; if you have more than one in your component, make sure you filter here the one you want.
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], ManagementDashboardSideMenuComponent.prototype, "menuConfig", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], ManagementDashboardSideMenuComponent.prototype, "action", void 0);
    ManagementDashboardSideMenuComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-management-dashboard-side-menu',
            template: __webpack_require__(/*! ./management-dashboard-side-menu.component.html */ "./src/app/components/management-dashboard-side-menu/management-dashboard-side-menu.component.html"),
            styles: [__webpack_require__(/*! ./management-dashboard-side-menu.component.scss */ "./src/app/components/management-dashboard-side-menu/management-dashboard-side-menu.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_0__["IterableDiffers"]])
    ], ManagementDashboardSideMenuComponent);
    return ManagementDashboardSideMenuComponent;
}());



/***/ }),

/***/ "./src/app/components/menu-list-item/menu-list-item.component.html":
/*!*************************************************************************!*\
  !*** ./src/app/components/menu-list-item/menu-list-item.component.html ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<td class=\"c-table__cell\"># {{data.id}}</td>\r\n<td class=\"c-table__cell\">{{data.name}}</td>\r\n<td class=\"c-table__cell\">{{data.description}}</td>\r\n<td class=\"c-table__cell\">\r\n\r\n  <div class=\"c-dropdown dropdown\" dropdown>\r\n    <a  class=\"c-btn c-btn--secondary has-dropdown dropdown-toggle has-dropdown dropdown-toggle button\"  id=\"dropdwonMenuOrder\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\" dropdownToggle >\r\n      {{'ACTIONS' |translate}}\r\n    </a>\r\n\r\n    <div class=\"c-dropdown__menu dropdown-menu dropdown-menu-right\" aria-labelledby=\"dropdwonMenuOrder\" *dropdownMenu>\r\n      <a class=\"c-dropdown__item dropdown-item\" routerLink=\"/event-management/menu/{{data.id}}\">{{'EDIT' |translate}}</a>\r\n      <a class=\"c-dropdown__item dropdown-item\" (click)=\"exportCSV()\">{{'EXPORT_CSV' |translate}}</a>\r\n    </div>\r\n\r\n  </div>\r\n</td>\r\n"

/***/ }),

/***/ "./src/app/components/menu-list-item/menu-list-item.component.scss":
/*!*************************************************************************!*\
  !*** ./src/app/components/menu-list-item/menu-list-item.component.scss ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/menu-list-item/menu-list-item.component.ts":
/*!***********************************************************************!*\
  !*** ./src/app/components/menu-list-item/menu-list-item.component.ts ***!
  \***********************************************************************/
/*! exports provided: MenuListItemComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MenuListItemComponent", function() { return MenuListItemComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _models_item__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../models/item */ "./src/app/models/item.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var MenuListItemComponent = /** @class */ (function () {
    function MenuListItemComponent() {
        this.data = new _models_item__WEBPACK_IMPORTED_MODULE_1__["Item"]();
        this.class = 'c-table__row';
    }
    MenuListItemComponent.prototype.ngOnInit = function () {
    };
    MenuListItemComponent.prototype.exportCSV = function () {
        alert('TODO');
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], MenuListItemComponent.prototype, "data", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"])('class'),
        __metadata("design:type", Object)
    ], MenuListItemComponent.prototype, "class", void 0);
    MenuListItemComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-menu-list-item,[app-menu-list-item]',
            template: __webpack_require__(/*! ./menu-list-item.component.html */ "./src/app/components/menu-list-item/menu-list-item.component.html"),
            styles: [__webpack_require__(/*! ./menu-list-item.component.scss */ "./src/app/components/menu-list-item/menu-list-item.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], MenuListItemComponent);
    return MenuListItemComponent;
}());



/***/ }),

/***/ "./src/app/components/modals/modal-add-place/modal-add-place.component.html":
/*!**********************************************************************************!*\
  !*** ./src/app/components/modals/modal-add-place/modal-add-place.component.html ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"modal-header\">\r\n  <h4 class=\"modal-title pull-left\">{{'ADD_NEW_LOCATION' | translate}}</h4>\r\n  <button type=\"button\" class=\"close pull-right\" aria-label=\"Close\" (click)=\"discard()\">\r\n    <span aria-hidden=\"true\">&times;</span>\r\n  </button>\r\n</div>\r\n<div class=\"modal-body\">\r\n  <h5 _ngcontent-c23=\"\" class=\"u-h6 u-mb-medium\">{{'SELECT_ONE_PLACE' | translate}}</h5>\r\n  <input [(ngModel)]=\"asyncSelected\"\r\n         [typeahead]=\"dataSource\"\r\n         (typeaheadLoading)=\"changeTypeaheadLoading($event)\"\r\n         (typeaheadOnSelect)=\"typeaheadOnSelect($event)\"\r\n         [typeaheadOptionsLimit]=\"7\"\r\n         typeaheadOptionField=\"name\"\r\n         placeholder=\"Locations loaded with timeout\"\r\n         class=\"form-control\">\r\n  <div *ngIf=\"typeaheadLoading\">Loading</div>\r\n\r\n  <hr [attr.data-content]=\"'OR' |translate\" class=\"hr-text\">\r\n\r\n  <h5 _ngcontent-c23=\"\" class=\"u-h6 u-mb-medium\">{{'OR_CREATE_NEW' | translate}}</h5>\r\n  <div>\r\n    <div class=\"c-field u-mb-small\">\r\n      <label class=\"c-field__label\" for=\"input1\">{{'LOCATION' | translate}}</label>\r\n      <input class=\"c-input\" ngx-google-places-autocomplete [options]='options' #placesRef=\"ngx-places\" (onAddressChange)=\"handleAddressChange($event)\" id=\"input1\"/>\r\n    </div>\r\n    <div *ngIf=\"data && data.address\">\r\n      <app-place-info [data]=\"data\"></app-place-info>\r\n    </div>\r\n  </div>\r\n</div>\r\n<div class=\"modal-footer\">\r\n  <button type=\"button\" class=\"btn btn-default\" (click)=\"save()\">{{'SAVE' | translate}}</button>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/components/modals/modal-add-place/modal-add-place.component.scss":
/*!**********************************************************************************!*\
  !*** ./src/app/components/modals/modal-add-place/modal-add-place.component.scss ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "hr {\n  margin-top: 20px;\n  margin-bottom: 20px; }\n"

/***/ }),

/***/ "./src/app/components/modals/modal-add-place/modal-add-place.component.ts":
/*!********************************************************************************!*\
  !*** ./src/app/components/modals/modal-add-place/modal-add-place.component.ts ***!
  \********************************************************************************/
/*! exports provided: ModalAddPlaceComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalAddPlaceComponent", function() { return ModalAddPlaceComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm5/ngx-bootstrap-modal.js");
/* harmony import */ var ngx_google_places_autocomplete__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ngx-google-places-autocomplete */ "./node_modules/ngx-google-places-autocomplete/bundles/ngx-google-places-autocomplete.umd.js");
/* harmony import */ var ngx_google_places_autocomplete__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(ngx_google_places_autocomplete__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var ngx_google_places_autocomplete_objects_address__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-google-places-autocomplete/objects/address */ "./node_modules/ngx-google-places-autocomplete/objects/address.js");
/* harmony import */ var _models_place__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../models/place */ "./src/app/models/place.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var ModalAddPlaceComponent = /** @class */ (function () {
    function ModalAddPlaceComponent(modalService, modalRef) {
        var _this = this;
        this.modalService = modalService;
        this.modalRef = modalRef;
        this.options = {
            types: [],
        };
        this.data = new _models_place__WEBPACK_IMPORTED_MODULE_4__["Place"]();
        this.statesComplex = [
            { id: 1, name: 'Alabama', region: 'South' },
            { id: 2, name: 'Alaska', region: 'West' },
            { id: 3, name: 'Arizona', region: 'West' },
            { id: 4, name: 'Arkansas', region: 'South' },
            { id: 5, name: 'California', region: 'West' },
            { id: 6, name: 'Colorado', region: 'West' },
            { id: 7, name: 'Connecticut', region: 'Northeast' },
            { id: 8, name: 'Delaware', region: 'South' },
            { id: 9, name: 'Florida', region: 'South' },
            { id: 10, name: 'Georgia', region: 'South' },
            { id: 11, name: 'Hawaii', region: 'West' },
            { id: 12, name: 'Idaho', region: 'West' },
            { id: 13, name: 'Illinois', region: 'Midwest' },
            { id: 14, name: 'Indiana', region: 'Midwest' },
            { id: 15, name: 'Iowa', region: 'Midwest' },
            { id: 16, name: 'Kansas', region: 'Midwest' },
            { id: 17, name: 'Kentucky', region: 'South' },
            { id: 18, name: 'Louisiana', region: 'South' },
            { id: 19, name: 'Maine', region: 'Northeast' },
            { id: 21, name: 'Maryland', region: 'South' },
            { id: 22, name: 'Massachusetts', region: 'Northeast' },
            { id: 23, name: 'Michigan', region: 'Midwest' },
            { id: 24, name: 'Minnesota', region: 'Midwest' },
            { id: 25, name: 'Mississippi', region: 'South' },
            { id: 26, name: 'Missouri', region: 'Midwest' },
            { id: 27, name: 'Montana', region: 'West' },
            { id: 28, name: 'Nebraska', region: 'Midwest' },
            { id: 29, name: 'Nevada', region: 'West' },
            { id: 30, name: 'New Hampshire', region: 'Northeast' },
            { id: 31, name: 'New Jersey', region: 'Northeast' },
            { id: 32, name: 'New Mexico', region: 'West' },
            { id: 33, name: 'New York', region: 'Northeast' },
            { id: 34, name: 'North Dakota', region: 'Midwest' },
            { id: 35, name: 'North Carolina', region: 'South' },
            { id: 36, name: 'Ohio', region: 'Midwest' },
            { id: 37, name: 'Oklahoma', region: 'South' },
            { id: 38, name: 'Oregon', region: 'West' },
            { id: 39, name: 'Pennsylvania', region: 'Northeast' },
            { id: 40, name: 'Rhode Island', region: 'Northeast' },
            { id: 41, name: 'South Carolina', region: 'South' },
            { id: 42, name: 'South Dakota', region: 'Midwest' },
            { id: 43, name: 'Tennessee', region: 'South' },
            { id: 44, name: 'Texas', region: 'South' },
            { id: 45, name: 'Utah', region: 'West' },
            { id: 46, name: 'Vermont', region: 'Northeast' },
            { id: 47, name: 'Virginia', region: 'South' },
            { id: 48, name: 'Washington', region: 'South' },
            { id: 49, name: 'West Virginia', region: 'South' },
            { id: 50, name: 'Wisconsin', region: 'Midwest' },
            { id: 51, name: 'Wyoming', region: 'West' }
        ];
        this.dataSource = rxjs__WEBPACK_IMPORTED_MODULE_5__["Observable"].create(function (observer) {
            // Runs on every search
            observer.next(_this.asyncSelected);
        }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["mergeMap"])(function (token) { return _this.getStatesAsObservable(token); }));
    }
    ModalAddPlaceComponent.prototype.getStatesAsObservable = function (token) {
        var query = new RegExp(token, 'ig');
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_5__["of"])(this.statesComplex.filter(function (state) {
            return query.test(state.name);
        }));
    };
    ModalAddPlaceComponent.prototype.changeTypeaheadLoading = function (e) {
        this.typeaheadLoading = e;
    };
    ModalAddPlaceComponent.prototype.typeaheadOnSelect = function (e) {
        console.log('Selected value: ', e.value);
    };
    ModalAddPlaceComponent.prototype.ngOnInit = function () { };
    ModalAddPlaceComponent.prototype.save = function () {
        this.callback(this.data);
        this.reset();
        this.close();
    };
    ModalAddPlaceComponent.prototype.discard = function () {
        this.reset();
        this.close();
    };
    ModalAddPlaceComponent.prototype.handleAddressChange = function (address) {
        console.log({ address: address });
        if (!this.data) {
            this.data = new _models_place__WEBPACK_IMPORTED_MODULE_4__["Place"]();
            this.data.name = address.name || address.formatted_address;
        }
        this.data.address = new ngx_google_places_autocomplete_objects_address__WEBPACK_IMPORTED_MODULE_3__["Address"]();
        this.data.address.address = address.formatted_address;
        this.data.address.url = address.url;
        this.data.address.location = {};
        this.data.address.location.latitude = address.geometry.location.lat();
        this.data.address.location.longitude = address.geometry.location.lng();
        this.data.photos = [];
        if (address.photos) {
            for (var i = 0; i < address.photos.length; i++) {
                this.data.photos.push(address.photos[i].getUrl());
            }
        }
    };
    ModalAddPlaceComponent.prototype.reset = function () {
        // this.modalRef.content.name = '';
        // this.modalRef.content.description = '';
    };
    ModalAddPlaceComponent.prototype.close = function () {
        this.modalRef.hide();
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('placesRef'),
        __metadata("design:type", ngx_google_places_autocomplete__WEBPACK_IMPORTED_MODULE_2__["GooglePlaceDirective"])
    ], ModalAddPlaceComponent.prototype, "placesRef", void 0);
    ModalAddPlaceComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-modal-add-place',
            template: __webpack_require__(/*! ./modal-add-place.component.html */ "./src/app/components/modals/modal-add-place/modal-add-place.component.html"),
            styles: [__webpack_require__(/*! ./modal-add-place.component.scss */ "./src/app/components/modals/modal-add-place/modal-add-place.component.scss")]
        }),
        __metadata("design:paramtypes", [ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_1__["BsModalService"], ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_1__["BsModalRef"]])
    ], ModalAddPlaceComponent);
    return ModalAddPlaceComponent;
}());



/***/ }),

/***/ "./src/app/components/modals/modal-add-staff/modal-add-staff.component.html":
/*!**********************************************************************************!*\
  !*** ./src/app/components/modals/modal-add-staff/modal-add-staff.component.html ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"modal-header\">\r\n  <h4 class=\"modal-title pull-left\">{{'CHOOSE_STAFF' | translate}}</h4>\r\n  <button type=\"button\" class=\"close pull-right\" aria-label=\"Close\" (click)=\"discard()\">\r\n    <span aria-hidden=\"true\">&times;</span>\r\n  </button>\r\n</div>\r\n<div class=\"\">\r\n  <div class=\"c-card\">\r\n    <div class=\"c-todo u-border-top-zero\" *ngFor=\"let member of staff\">\r\n      <!--[(ngModel)]=\"cat.id\" name=\"{{ cat.name }}\" type=\"checkbox\" id=\"{{cat.name}}\" (change)=\"onChangeCategory($event, cat)\">-->\r\n      <input class=\"c-todo__input\" name=\"staff-member-{{ member.id}}\" type=\"checkbox\" id=\"staff-member-{{ member.id }}\"  type=\"checkbox\" [(ngModel)]=\"member.selected\" (change)=\"onChangeStaff($event, member)\">\r\n      <label class=\"c-todo__label\" for=\"staff-member-{{member.id}}\">\r\n        <div class=\"o-media\">\r\n          <div class=\"o-media__img u-mr-xsmall\">\r\n            <div class=\"c-avatar c-avatar--xsmall\">\r\n              <img class=\"c-avatar__img\" [src]=\"member.avatar\" alt=\"{{member.name}}'s Face\">\r\n            </div>\r\n          </div>\r\n          <div class=\"o-media__body\">\r\n            {{member.name}}<small class=\"u-block u-text-mute\">{{member.email}}</small>\r\n          </div>\r\n        </div>\r\n      </label>\r\n\r\n      <!--<span class=\"c-badge c-badge&#45;&#45;danger c-badge&#45;&#45;small\">Due Today</span>-->\r\n    </div>\r\n  </div>\r\n\r\n</div>\r\n<div class=\"modal-footer\">\r\n  <button type=\"button\" class=\"btn btn-default\" (click)=\"save()\">{{'SAVE' | translate}}</button>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/components/modals/modal-add-staff/modal-add-staff.component.scss":
/*!**********************************************************************************!*\
  !*** ./src/app/components/modals/modal-add-staff/modal-add-staff.component.scss ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/modals/modal-add-staff/modal-add-staff.component.ts":
/*!********************************************************************************!*\
  !*** ./src/app/components/modals/modal-add-staff/modal-add-staff.component.ts ***!
  \********************************************************************************/
/*! exports provided: ModalAddStaffComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalAddStaffComponent", function() { return ModalAddStaffComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm5/ngx-bootstrap-modal.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ModalAddStaffComponent = /** @class */ (function () {
    function ModalAddStaffComponent(modalService, modalRef) {
        this.modalService = modalService;
        this.modalRef = modalRef;
    }
    ModalAddStaffComponent.prototype.ngOnInit = function () {
        console.log(this.staff);
    };
    ModalAddStaffComponent.prototype.save = function () {
        var stack = [];
        for (var i = 0; i < this.staff.length; i++) {
            var member = this.staff[i];
            if (member.selected) {
                stack.push(member);
                member.selected = false;
            }
        }
        this.callback(stack);
        this.reset();
        this.close();
    };
    ModalAddStaffComponent.prototype.onChangeStaff = function (event, member) {
        // console.log(event, member);
    };
    ModalAddStaffComponent.prototype.discard = function () {
        this.reset();
        this.close();
    };
    ModalAddStaffComponent.prototype.reset = function () {
        // this.modalRef.content.name = '';
        // this.modalRef.content.description = '';
    };
    ModalAddStaffComponent.prototype.close = function () {
        this.modalRef.hide();
    };
    ModalAddStaffComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-modal-add-staff',
            template: __webpack_require__(/*! ./modal-add-staff.component.html */ "./src/app/components/modals/modal-add-staff/modal-add-staff.component.html"),
            styles: [__webpack_require__(/*! ./modal-add-staff.component.scss */ "./src/app/components/modals/modal-add-staff/modal-add-staff.component.scss")]
        }),
        __metadata("design:paramtypes", [ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_1__["BsModalService"], ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_1__["BsModalRef"]])
    ], ModalAddStaffComponent);
    return ModalAddStaffComponent;
}());



/***/ }),

/***/ "./src/app/components/modals/modal-category/modal-category.component.html":
/*!********************************************************************************!*\
  !*** ./src/app/components/modals/modal-category/modal-category.component.html ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"modal-header\">\r\n  <h4 class=\"modal-title pull-left\">{{'ADD_NEW_CATEGORY' | translate}}</h4>\r\n  <button type=\"button\" class=\"close pull-right\" aria-label=\"Close\" (click)=\"discard()\">\r\n    <span aria-hidden=\"true\">&times;</span>\r\n  </button>\r\n</div>\r\n<div class=\"modal-body\">\r\n  <div class=\"c-field u-mb-small\">\r\n    <label class=\"c-field__label\" for=\"input1\">{{'NAME' | translate}}</label>\r\n    <input class=\"c-input\" type=\"email\" id=\"input1\" [(ngModel)]=\"name\">\r\n  </div>\r\n  <div class=\"c-field u-mb-small\">\r\n    <label class=\"c-field__label\" for=\"input2\">{{'DESCRIPTION' | translate}}</label>\r\n    <input class=\"c-input\" type=\"email\" id=\"input2\" [(ngModel)]=\"description\">\r\n  </div>\r\n\r\n</div>\r\n<div class=\"modal-footer\">\r\n  <button type=\"button\" class=\"btn btn-default\" (click)=\"save()\">{{'SAVE' | translate}}</button>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/components/modals/modal-category/modal-category.component.scss":
/*!********************************************************************************!*\
  !*** ./src/app/components/modals/modal-category/modal-category.component.scss ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/modals/modal-category/modal-category.component.ts":
/*!******************************************************************************!*\
  !*** ./src/app/components/modals/modal-category/modal-category.component.ts ***!
  \******************************************************************************/
/*! exports provided: ModalCategoryComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalCategoryComponent", function() { return ModalCategoryComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm5/ngx-bootstrap-modal.js");
/* harmony import */ var _models_category__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../models/category */ "./src/app/models/category.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ModalCategoryComponent = /** @class */ (function () {
    function ModalCategoryComponent(modalService, modalRef) {
        this.modalService = modalService;
        this.modalRef = modalRef;
    }
    ModalCategoryComponent.prototype.ngOnInit = function () {
    };
    ModalCategoryComponent.prototype.save = function () {
        if (!this.validate()) {
            return;
        }
        var category = new _models_category__WEBPACK_IMPORTED_MODULE_2__["Category"]();
        category.name = this.modalRef.content.name;
        category.description = this.modalRef.content.description;
        this.callback(category);
        this.reset();
        this.close();
    };
    ModalCategoryComponent.prototype.discard = function () {
        this.reset();
        this.close();
    };
    ModalCategoryComponent.prototype.validate = function () {
        var attributes = ['name', 'description',];
        var valid = true;
        for (var i = 0; i < attributes.length; i++) {
            if (this[attributes[i]].length < 3) {
                valid = false;
                break;
            }
        }
        return valid;
    };
    ModalCategoryComponent.prototype.reset = function () {
        this.modalRef.content.name = '';
        this.modalRef.content.description = '';
    };
    ModalCategoryComponent.prototype.close = function () {
        this.modalRef.hide();
    };
    ModalCategoryComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-modal-category',
            template: __webpack_require__(/*! ./modal-category.component.html */ "./src/app/components/modals/modal-category/modal-category.component.html"),
            styles: [__webpack_require__(/*! ./modal-category.component.scss */ "./src/app/components/modals/modal-category/modal-category.component.scss")]
        }),
        __metadata("design:paramtypes", [ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_1__["BsModalService"], ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_1__["BsModalRef"]])
    ], ModalCategoryComponent);
    return ModalCategoryComponent;
}());



/***/ }),

/***/ "./src/app/components/modals/modal-item/modal-item.component.html":
/*!************************************************************************!*\
  !*** ./src/app/components/modals/modal-item/modal-item.component.html ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"modal-header\">\r\n  <h4 class=\"modal-title pull-left\">{{'ADD_NEW_CATEGORY' | translate}}</h4>\r\n  <button type=\"button\" class=\"close pull-right\" aria-label=\"Close\" (click)=\"discard()\">\r\n    <span aria-hidden=\"true\">&times;</span>\r\n  </button>\r\n</div>\r\n<div class=\"modal-body\">\r\n  <div class=\"c-field u-mb-small\">\r\n    <label class=\"c-field__label\" for=\"input1\">{{'NAME' | translate}}</label>\r\n    <input class=\"c-input\" type=\"email\" id=\"input1\" [(ngModel)]=\"name\">\r\n  </div>\r\n  <div class=\"c-field u-mb-small\">\r\n    <label class=\"c-field__label\" for=\"input2\">{{'DESCRIPTION' | translate}}</label>\r\n    <input class=\"c-input\" type=\"email\" id=\"input2\" [(ngModel)]=\"description\">\r\n  </div>\r\n  <div class=\"c-field u-mb-small\">\r\n    <label class=\"c-field__label\" for=\"input2\">{{'BRAND' | translate}}</label>\r\n    <input class=\"c-input\" type=\"email\" id=\"input3\" [(ngModel)]=\"brand\">\r\n  </div>\r\n  <div class=\"c-field u-mb-small\">\r\n    <label class=\"c-field__label\" for=\"input4\">{{'ITEM_TYPE' | translate}}</label>\r\n    <select class=\"c-input\" type=\"email\" id=\"input4\" [(ngModel)]=\"type\" >\r\n      <option *ngFor=\"let itemType of itemTypes\" [attr.value]=\"itemType\">{{itemType.type |translate}}</option>\r\n    </select>\r\n  </div>\r\n\r\n</div>\r\n<div class=\"modal-footer\">\r\n  <button type=\"button\" class=\"btn btn-default\" (click)=\"save()\">{{'SAVE' | translate}}</button>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/components/modals/modal-item/modal-item.component.scss":
/*!************************************************************************!*\
  !*** ./src/app/components/modals/modal-item/modal-item.component.scss ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/modals/modal-item/modal-item.component.ts":
/*!**********************************************************************!*\
  !*** ./src/app/components/modals/modal-item/modal-item.component.ts ***!
  \**********************************************************************/
/*! exports provided: ModalItemComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalItemComponent", function() { return ModalItemComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm5/ngx-bootstrap-modal.js");
/* harmony import */ var _models_brand__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../models/brand */ "./src/app/models/brand.ts");
/* harmony import */ var _models_item__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../models/item */ "./src/app/models/item.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ModalItemComponent = /** @class */ (function () {
    function ModalItemComponent(modalService, modalRef) {
        this.modalService = modalService;
        this.modalRef = modalRef;
        this.itemTypes = [];
        this.brand = '';
    }
    ModalItemComponent.prototype.ngOnInit = function () {
    };
    ModalItemComponent.prototype.save = function () {
        var brand = false;
        if (!this.validate()) {
            return;
        }
        if (this.brand) {
            brand = new _models_brand__WEBPACK_IMPORTED_MODULE_2__["Brand"]();
            brand.name = this.brand;
        }
        var item = new _models_item__WEBPACK_IMPORTED_MODULE_3__["Item"]();
        item.name = this.name;
        item.description = this.description;
        item.type = this.type;
        item.brand = brand;
        this.callback(item);
        this.reset();
        this.close();
    };
    ModalItemComponent.prototype.discard = function () {
        this.reset();
        this.close();
    };
    ModalItemComponent.prototype.setType = function (itemType) {
        this.type = itemType;
    };
    ModalItemComponent.prototype.validate = function () {
        var attributes = ['name', 'description'];
        var valid = true;
        for (var i = 0; i < attributes.length; i++) {
            if (this[attributes[i]].length < 3) {
                valid = false;
                break;
            }
        }
        return valid;
    };
    ModalItemComponent.prototype.reset = function () {
        this.modalRef.content.name = '';
        this.modalRef.content.description = '';
    };
    ModalItemComponent.prototype.close = function () {
        this.modalRef.hide();
    };
    ModalItemComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-modal-item',
            template: __webpack_require__(/*! ./modal-item.component.html */ "./src/app/components/modals/modal-item/modal-item.component.html"),
            styles: [__webpack_require__(/*! ./modal-item.component.scss */ "./src/app/components/modals/modal-item/modal-item.component.scss")]
        }),
        __metadata("design:paramtypes", [ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_1__["BsModalService"], ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_1__["BsModalRef"]])
    ], ModalItemComponent);
    return ModalItemComponent;
}());



/***/ }),

/***/ "./src/app/components/modals/modal-mce/modal-mce.component.html":
/*!**********************************************************************!*\
  !*** ./src/app/components/modals/modal-mce/modal-mce.component.html ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"modal-header\">\r\n  <h4 class=\"modal-title pull-left\">{{'EDIT_TEXT' | translate}}</h4>\r\n  <button type=\"button\" class=\"close pull-right\" aria-label=\"Close\" (click)=\"discard()\">\r\n    <span aria-hidden=\"true\">&times;</span>\r\n  </button>\r\n</div>\r\n<div class=\"modal-body\">\r\n<div [froalaEditor]=\"froalaOptions\" [(ngModel)]=\"data\"></div>\r\n\r\n</div>\r\n<div class=\"modal-footer\">\r\n  <button type=\"button\" class=\"btn btn-default\" (click)=\"save()\">{{'SAVE' | translate}}</button>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/components/modals/modal-mce/modal-mce.component.scss":
/*!**********************************************************************!*\
  !*** ./src/app/components/modals/modal-mce/modal-mce.component.scss ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/modals/modal-mce/modal-mce.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/components/modals/modal-mce/modal-mce.component.ts ***!
  \********************************************************************/
/*! exports provided: ModalMceComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalMceComponent", function() { return ModalMceComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm5/ngx-bootstrap-modal.js");
/* harmony import */ var _defaults__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../defaults */ "./src/app/defaults.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ModalMceComponent = /** @class */ (function () {
    function ModalMceComponent(modalService, modalRef) {
        this.modalService = modalService;
        this.modalRef = modalRef;
        this.froalaOptions = _defaults__WEBPACK_IMPORTED_MODULE_2__["FROALA_OPTIONS"];
    }
    ModalMceComponent.prototype.ngOnInit = function () {
    };
    ModalMceComponent.prototype.save = function () {
        //
        if (this.data && this.data.trim().length > 0) {
            this.callback(this.data);
            this.reset();
            this.close();
        }
    };
    ModalMceComponent.prototype.discard = function () {
        this.reset();
        this.close();
    };
    ModalMceComponent.prototype.reset = function () {
        this.modalRef.content.data = '';
    };
    ModalMceComponent.prototype.close = function () {
        this.modalRef.hide();
    };
    ModalMceComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-modal-mce',
            template: __webpack_require__(/*! ./modal-mce.component.html */ "./src/app/components/modals/modal-mce/modal-mce.component.html"),
            styles: [__webpack_require__(/*! ./modal-mce.component.scss */ "./src/app/components/modals/modal-mce/modal-mce.component.scss")]
        }),
        __metadata("design:paramtypes", [ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_1__["BsModalService"], ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_1__["BsModalRef"]])
    ], ModalMceComponent);
    return ModalMceComponent;
}());



/***/ }),

/***/ "./src/app/components/modals/modal-menu/modal-menu.component.html":
/*!************************************************************************!*\
  !*** ./src/app/components/modals/modal-menu/modal-menu.component.html ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"modal-header\">\r\n  <h4 class=\"modal-title pull-left\">{{'ADD_NEW_CATEGORY' | translate}}</h4>\r\n  <button type=\"button\" class=\"close pull-right\" aria-label=\"Close\" (click)=\"discard()\">\r\n    <span aria-hidden=\"true\">&times;</span>\r\n  </button>\r\n</div>\r\n<div class=\"modal-body\">\r\n  <div class=\"c-field u-mb-small\">\r\n    <label class=\"c-field__label\" for=\"input1\">{{'NAME' | translate}}</label>\r\n    <input class=\"c-input\" type=\"email\" id=\"input1\" [(ngModel)]=\"name\">\r\n  </div>\r\n  <div class=\"c-field u-mb-small\">\r\n    <label class=\"c-field__label\" for=\"input2\">{{'DESCRIPTION' | translate}}</label>\r\n    <input class=\"c-input\" type=\"email\" id=\"input2\" [(ngModel)]=\"description\">\r\n  </div>\r\n\r\n</div>\r\n<div class=\"modal-footer\">\r\n  <button type=\"button\" class=\"btn btn-default\" (click)=\"save()\">{{'SAVE' | translate}}</button>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/components/modals/modal-menu/modal-menu.component.scss":
/*!************************************************************************!*\
  !*** ./src/app/components/modals/modal-menu/modal-menu.component.scss ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/modals/modal-menu/modal-menu.component.ts":
/*!**********************************************************************!*\
  !*** ./src/app/components/modals/modal-menu/modal-menu.component.ts ***!
  \**********************************************************************/
/*! exports provided: ModalMenuComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalMenuComponent", function() { return ModalMenuComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm5/ngx-bootstrap-modal.js");
/* harmony import */ var _models_category__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../models/category */ "./src/app/models/category.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ModalMenuComponent = /** @class */ (function () {
    function ModalMenuComponent(modalService, modalRef) {
        this.modalService = modalService;
        this.modalRef = modalRef;
    }
    ModalMenuComponent.prototype.ngOnInit = function () {
    };
    ModalMenuComponent.prototype.save = function () {
        if (!this.validate()) {
            return;
        }
        var category = new _models_category__WEBPACK_IMPORTED_MODULE_2__["Category"]();
        category.name = this.modalRef.content.name;
        category.description = this.modalRef.content.description;
        this.callback(category);
        this.reset();
        this.close();
    };
    ModalMenuComponent.prototype.discard = function () {
        this.reset();
        this.close();
    };
    ModalMenuComponent.prototype.validate = function () {
        var attributes = ['name', 'description',];
        var valid = true;
        for (var i = 0; i < attributes.length; i++) {
            if (this[attributes[i]].length < 3) {
                valid = false;
                break;
            }
        }
        return valid;
    };
    ModalMenuComponent.prototype.reset = function () {
        this.modalRef.content.name = '';
        this.modalRef.content.description = '';
    };
    ModalMenuComponent.prototype.close = function () {
        this.modalRef.hide();
    };
    ModalMenuComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-modal-menu',
            template: __webpack_require__(/*! ./modal-menu.component.html */ "./src/app/components/modals/modal-menu/modal-menu.component.html"),
            styles: [__webpack_require__(/*! ./modal-menu.component.scss */ "./src/app/components/modals/modal-menu/modal-menu.component.scss")]
        }),
        __metadata("design:paramtypes", [ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_1__["BsModalService"], ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_1__["BsModalRef"]])
    ], ModalMenuComponent);
    return ModalMenuComponent;
}());



/***/ }),

/***/ "./src/app/components/no-content-panel/no-content-panel.component.html":
/*!*****************************************************************************!*\
  !*** ./src/app/components/no-content-panel/no-content-panel.component.html ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"no-content-container\">\r\n  <div class=\"no-content-panel\" >\r\n    <h4>{{title | translate}}</h4>\r\n    <p> {{description | translate}}</p>\r\n  </div>\r\n</div>\r\n\r\n"

/***/ }),

/***/ "./src/app/components/no-content-panel/no-content-panel.component.scss":
/*!*****************************************************************************!*\
  !*** ./src/app/components/no-content-panel/no-content-panel.component.scss ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".no-content-container {\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  border: dashed 2px #E0E0E0;\n  min-height: 200px;\n  background: #F9F9F9;\n  text-align: center;\n  cursor: pointer; }\n  .no-content-container h4, .no-content-container p {\n    color: #979797 !important; }\n"

/***/ }),

/***/ "./src/app/components/no-content-panel/no-content-panel.component.ts":
/*!***************************************************************************!*\
  !*** ./src/app/components/no-content-panel/no-content-panel.component.ts ***!
  \***************************************************************************/
/*! exports provided: NoContentPanelComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NoContentPanelComponent", function() { return NoContentPanelComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var NoContentPanelComponent = /** @class */ (function () {
    function NoContentPanelComponent() {
        this.title = 'NO_CONTENT_TO_SHOW';
        this.description = '';
    }
    NoContentPanelComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], NoContentPanelComponent.prototype, "title", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], NoContentPanelComponent.prototype, "description", void 0);
    NoContentPanelComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-no-content-panel',
            template: __webpack_require__(/*! ./no-content-panel.component.html */ "./src/app/components/no-content-panel/no-content-panel.component.html"),
            styles: [__webpack_require__(/*! ./no-content-panel.component.scss */ "./src/app/components/no-content-panel/no-content-panel.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], NoContentPanelComponent);
    return NoContentPanelComponent;
}());



/***/ }),

/***/ "./src/app/components/order-list-item/order-list-item.component.html":
/*!***************************************************************************!*\
  !*** ./src/app/components/order-list-item/order-list-item.component.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n  <td class=\"c-table__cell\"><span class=\"u-text-mute\">{{data.id}}</span></td>\r\n  <td class=\"c-table__cell\">{{data.description}}</td>\r\n  <td class=\"c-table__cell\">\r\n    <span class=\"u-text-mute\">{{data.event.name}}</span>\r\n  </td>\r\n  <td class=\"c-table__cell\">\r\n    <span class=\"u-text-mute\">{{data.vat}}</span>\r\n  </td>\r\n  <td class=\"c-table__cell\">\r\n    <span class=\"u-text-mute\">{{data.created_at | amDateFormat:'DD MMM'}}</span>\r\n  </td>\r\n  <td class=\"c-table__cell\">\r\n    <span class=\"c-badge c-badge--small c-badge--success\">{{data.status}}</span>\r\n  </td>\r\n  <td class=\"c-table__cell\">{{data.amount}} {{data.currency.symbol}}</td>\r\n\r\n  <td class=\"c-table__cell u-text-right\">\r\n\r\n    <div class=\"c-dropdown dropdown\" dropdown>\r\n      <a  class=\"c-btn c-btn--secondary has-dropdown dropdown-toggle has-dropdown dropdown-toggle button\"  id=\"dropdwonMenuOrder\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\" dropdownToggle >\r\n        {{'ACTIONS' |translate}}\r\n      </a>\r\n\r\n      <div class=\"c-dropdown__menu dropdown-menu dropdown-menu-right\" aria-labelledby=\"dropdwonMenuOrder\" *dropdownMenu>\r\n        <a class=\"c-dropdown__item dropdown-item\" routerLink=\"/event-management/order/{{data.id}}\">{{'VIEW_INVOICE' |translate}}</a>\r\n        <a class=\"c-dropdown__item dropdown-item\" (click)=\"download()\">{{'DOWNLOAD' |translate}}</a>\r\n      </div>\r\n\r\n    </div>\r\n  </td>\r\n\r\n"

/***/ }),

/***/ "./src/app/components/order-list-item/order-list-item.component.scss":
/*!***************************************************************************!*\
  !*** ./src/app/components/order-list-item/order-list-item.component.scss ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/order-list-item/order-list-item.component.ts":
/*!*************************************************************************!*\
  !*** ./src/app/components/order-list-item/order-list-item.component.ts ***!
  \*************************************************************************/
/*! exports provided: OrderListItemComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderListItemComponent", function() { return OrderListItemComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _models_order__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../models/order */ "./src/app/models/order.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var OrderListItemComponent = /** @class */ (function () {
    function OrderListItemComponent() {
        this.data = new _models_order__WEBPACK_IMPORTED_MODULE_1__["Order"]();
        this.class = 'c-table__row';
    }
    OrderListItemComponent.prototype.ngOnInit = function () {
    };
    OrderListItemComponent.prototype.download = function () {
        alert('TODO DOWNLOAD INVOICE');
        console.log(this.data);
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], OrderListItemComponent.prototype, "data", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"])('class'),
        __metadata("design:type", Object)
    ], OrderListItemComponent.prototype, "class", void 0);
    OrderListItemComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-order-list-item,[app-order-list-item]',
            template: __webpack_require__(/*! ./order-list-item.component.html */ "./src/app/components/order-list-item/order-list-item.component.html"),
            styles: [__webpack_require__(/*! ./order-list-item.component.scss */ "./src/app/components/order-list-item/order-list-item.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], OrderListItemComponent);
    return OrderListItemComponent;
}());



/***/ }),

/***/ "./src/app/components/place-card/place-card.component.html":
/*!*****************************************************************!*\
  !*** ./src/app/components/place-card/place-card.component.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n  <article class=\"c-event\">\r\n    <div class=\"c-event__img\">\r\n      <img src=\"{{data.image}}\" alt=\"{{data.name}}s Picture\">\r\n      <!--<span class=\"c-event__status\">{{'FEATURED'}}</span>-->\r\n    </div>\r\n    <div class=\"c-event__meta\">\r\n      <h3 class=\"c-event__title\">{{data.name}}\r\n        <span class=\"c-event__place\">{{data.address.address}}</span>\r\n      </h3>\r\n\r\n      <a routerLink=\"/event-management/place/{{data.id}}\" class=\"c-btn c-btn--success c-event__btn\">{{'GO_TO_PAGE' | translate}}</a>\r\n    </div>\r\n  </article><!-- // .c-event -->\r\n\r\n"

/***/ }),

/***/ "./src/app/components/place-card/place-card.component.scss":
/*!*****************************************************************!*\
  !*** ./src/app/components/place-card/place-card.component.scss ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/place-card/place-card.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/components/place-card/place-card.component.ts ***!
  \***************************************************************/
/*! exports provided: PlaceCardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PlaceCardComponent", function() { return PlaceCardComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _models_place__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../models/place */ "./src/app/models/place.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var PlaceCardComponent = /** @class */ (function () {
    function PlaceCardComponent() {
        this.data = new _models_place__WEBPACK_IMPORTED_MODULE_1__["Place"]();
    }
    PlaceCardComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", _models_place__WEBPACK_IMPORTED_MODULE_1__["Place"])
    ], PlaceCardComponent.prototype, "data", void 0);
    PlaceCardComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-place-card',
            template: __webpack_require__(/*! ./place-card.component.html */ "./src/app/components/place-card/place-card.component.html"),
            styles: [__webpack_require__(/*! ./place-card.component.scss */ "./src/app/components/place-card/place-card.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], PlaceCardComponent);
    return PlaceCardComponent;
}());



/***/ }),

/***/ "./src/app/components/place-info/place-info.component.html":
/*!*****************************************************************!*\
  !*** ./src/app/components/place-info/place-info.component.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"c-field u-mb-small\">\r\n  <label class=\"c-field__label\" for=\"input-name\">{{'NAME' | translate}}</label>\r\n  <span id=\"input-name\">{{data.name}}</span>\r\n</div>\r\n<div class=\"c-field u-mb-small\">\r\n  <label class=\"c-field__label\" for=\"input-address\">{{'ADDRESS' | translate}}</label>\r\n  <span id=\"input-address\">{{data.address.address}}</span>\r\n</div>\r\n<div class=\"c-field u-mb-small\">\r\n  <label class=\"c-field__label\" for=\"input-address\">{{'PHOTOS' | translate}}</label>\r\n  <app-image-gallery [data]=\"data\" [canAddPhotos]=\"false\"></app-image-gallery>\r\n</div>\r\n<div class=\"c-field u-mb-small\">\r\n  <label class=\"c-field__label\" for=\"input-url\">{{'VIEW_ON_GOOGLE_MAPS' | translate}}</label>\r\n  <a id=\"input-url\" href=\"{{data.address.url}}\" target=\"_blank\">{{data.address.url}}</a>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/components/place-info/place-info.component.scss":
/*!*****************************************************************!*\
  !*** ./src/app/components/place-info/place-info.component.scss ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/place-info/place-info.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/components/place-info/place-info.component.ts ***!
  \***************************************************************/
/*! exports provided: PlaceInfoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PlaceInfoComponent", function() { return PlaceInfoComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var PlaceInfoComponent = /** @class */ (function () {
    function PlaceInfoComponent() {
        this.data = {};
    }
    PlaceInfoComponent.prototype.ngOnInit = function () { };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], PlaceInfoComponent.prototype, "data", void 0);
    PlaceInfoComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-place-info',
            template: __webpack_require__(/*! ./place-info.component.html */ "./src/app/components/place-info/place-info.component.html"),
            styles: [__webpack_require__(/*! ./place-info.component.scss */ "./src/app/components/place-info/place-info.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], PlaceInfoComponent);
    return PlaceInfoComponent;
}());



/***/ }),

/***/ "./src/app/components/profile-avatar/profile-avatar.component.html":
/*!*************************************************************************!*\
  !*** ./src/app/components/profile-avatar/profile-avatar.component.html ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\r\n  profile-avatar works!\r\n</p>\r\n"

/***/ }),

/***/ "./src/app/components/profile-avatar/profile-avatar.component.scss":
/*!*************************************************************************!*\
  !*** ./src/app/components/profile-avatar/profile-avatar.component.scss ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/profile-avatar/profile-avatar.component.ts":
/*!***********************************************************************!*\
  !*** ./src/app/components/profile-avatar/profile-avatar.component.ts ***!
  \***********************************************************************/
/*! exports provided: ProfileAvatarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfileAvatarComponent", function() { return ProfileAvatarComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ProfileAvatarComponent = /** @class */ (function () {
    function ProfileAvatarComponent() {
    }
    ProfileAvatarComponent.prototype.ngOnInit = function () {
    };
    ProfileAvatarComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-profile-avatar',
            template: __webpack_require__(/*! ./profile-avatar.component.html */ "./src/app/components/profile-avatar/profile-avatar.component.html"),
            styles: [__webpack_require__(/*! ./profile-avatar.component.scss */ "./src/app/components/profile-avatar/profile-avatar.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], ProfileAvatarComponent);
    return ProfileAvatarComponent;
}());



/***/ }),

/***/ "./src/app/components/profit-sumary/profit-sumary.component.html":
/*!***********************************************************************!*\
  !*** ./src/app/components/profit-sumary/profit-sumary.component.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"c-card u-p-medium u-mb-medium\" data-mh=\"invoices-cards\">\r\n\r\n  <div class=\"o-line\">\r\n    <h4 class=\"c-card__title\">Percentual Profit\r\n      <span class=\"u-block u-text-mute u-text-xsmall\">Quarterly (3 Months)</span>\r\n    </h4>\r\n\r\n    <div class=\"c-card__meta\">\r\n      <a class=\"dropdown-toggle\"  id=\"dropdwonMenuCard2\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\r\n        <i class=\"u-text-mute u-opacity-heavy fa fa-ellipsis-h\"></i>\r\n      </a>\r\n\r\n      <div class=\"c-dropdown__menu dropdown-menu dropdown-menu-right fade\" aria-labelledby=\"dropdwonMenuCard2\">\r\n        <a class=\"c-dropdown__item\" >Link 1</a>\r\n        <a class=\"c-dropdown__item\" >Link 2</a>\r\n      </div>\r\n    </div>\r\n  </div>\r\n\r\n\r\n  <div class=\"row\">\r\n    <div class=\"col-12 u-pr-zero\">\r\n      <div class=\"c-chart-container\">\r\n        <ngx-chartjs [data]='chartData' type='line' height=\"300px\"></ngx-chartjs>\r\n      </div>\r\n      <!--<h5 class=\"c-chart__title\">Q2 - 17</h5>-->\r\n    </div>\r\n    <!--<div class=\"col-2 u-pr-zero\">-->\r\n      <!--<div class=\"c-chart-container\">-->\r\n        <!--<ngx-chartjs [data]='chartData' type='line' height=\"75\"></ngx-chartjs>-->\r\n      <!--</div>-->\r\n      <!--<h5 class=\"c-chart__title\">Q2 - 17</h5>-->\r\n    <!--</div>-->\r\n\r\n    <!--<div class=\"col-2 u-pr-zero\">-->\r\n      <!--<div class=\"c-chart-container\">-->\r\n        <!--<ngx-chartjs [data]='chartData' type='line' height=\"75\"></ngx-chartjs>-->\r\n      <!--</div>-->\r\n      <!--<h5 class=\"c-chart__title\">Q2 - 17</h5>-->\r\n    <!--</div>-->\r\n\r\n    <!--<div class=\"col-2 u-pr-zero\">-->\r\n      <!--<div class=\"c-chart-container\">-->\r\n        <!--<ngx-chartjs [data]='chartData' type='line' height=\"75\"></ngx-chartjs>-->\r\n      <!--</div>-->\r\n      <!--<h5 class=\"c-chart__title\">Q2 - 17</h5>-->\r\n    <!--</div>-->\r\n\r\n    <!--<div class=\"col-2 u-pr-zero\">-->\r\n      <!--<div class=\"c-chart-container\">-->\r\n        <!--<ngx-chartjs [data]='chartData' type='line' height=\"75\"></ngx-chartjs>-->\r\n      <!--</div>-->\r\n      <!--<h5 class=\"c-chart__title\">Q2 - 17</h5>-->\r\n    <!--</div>-->\r\n\r\n    <!--<div class=\"col-2 u-pr-zero\">-->\r\n      <!--<div class=\"c-chart-container\">-->\r\n        <!--<ngx-chartjs [data]='chartData' type='line' height=\"75\"></ngx-chartjs>-->\r\n      <!--</div>-->\r\n      <!--<h5 class=\"c-chart__title\">Q2 - 17</h5>-->\r\n    <!--</div>-->\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/components/profit-sumary/profit-sumary.component.scss":
/*!***********************************************************************!*\
  !*** ./src/app/components/profit-sumary/profit-sumary.component.scss ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/profit-sumary/profit-sumary.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/components/profit-sumary/profit-sumary.component.ts ***!
  \*********************************************************************/
/*! exports provided: ProfitSumaryComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfitSumaryComponent", function() { return ProfitSumaryComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _models_dummy_data_line_chart__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../models/dummy-data/line-chart */ "./src/app/models/dummy-data/line-chart.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ProfitSumaryComponent = /** @class */ (function () {
    function ProfitSumaryComponent() {
        this.chartData = [];
    }
    ProfitSumaryComponent.prototype.ngOnInit = function () {
        this.chartData = _models_dummy_data_line_chart__WEBPACK_IMPORTED_MODULE_1__["LINE_CHART_DATA"];
    };
    ProfitSumaryComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-profit-sumary',
            template: __webpack_require__(/*! ./profit-sumary.component.html */ "./src/app/components/profit-sumary/profit-sumary.component.html"),
            styles: [__webpack_require__(/*! ./profit-sumary.component.scss */ "./src/app/components/profit-sumary/profit-sumary.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], ProfitSumaryComponent);
    return ProfitSumaryComponent;
}());



/***/ }),

/***/ "./src/app/components/sidebar-bars/sidebar-bars.component.html":
/*!*********************************************************************!*\
  !*** ./src/app/components/sidebar-bars/sidebar-bars.component.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"c-card u-mb-medium\">\r\n\r\n  <div class=\"u-p-medium\">\r\n    <h5 class=\"u-h6 u-mb-medium\">{{'EVENT_PLACE_BARS' |translate}}</h5>\r\n    <div class=\"o-media u-mb-small\" *ngIf=\"data.place && data.place.bars && 0 !== data.place.bars.length\">\r\n      <div class=\"o-media__img u-mr-xsmall\">\r\n        <div class=\"c-avatar c-avatar--xsmall\">\r\n          <img class=\"c-avatar__img\" src=\"assets/img/avatar2-72.jpg\" alt=\"Profile Title\">\r\n        </div>\r\n      </div>\r\n      <div class=\"o-media__body\">\r\n        <h6 class=\"u-text-small u-mb-zero\">Bar Name</h6>\r\n        <p class=\"u-text-mute u-text-xsmall\"><input type=\"checkbox\"></p>\r\n      </div>\r\n    </div><!-- // .o-media -->\r\n    <app-no-content-panel [title]=\"'NO_LOCATION_YET'\" [description]=\"'NO_BARS_NO_PLACE'\" appAddPlace *ngIf=\"!data.place || !data.place.bars.length\"></app-no-content-panel>\r\n\r\n  </div>\r\n\r\n  <div class=\"u-pv-small  u-border-top u-text-center\" appAddBar *ngIf=\"data.place\" [data]=\"data\">\r\n    <a class=\"u-text-mute u-text-uppercase u-text-xsmall\" >{{'ADD_BAR_TO_EVENT' | translate}}</a>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/components/sidebar-bars/sidebar-bars.component.scss":
/*!*********************************************************************!*\
  !*** ./src/app/components/sidebar-bars/sidebar-bars.component.scss ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/sidebar-bars/sidebar-bars.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/components/sidebar-bars/sidebar-bars.component.ts ***!
  \*******************************************************************/
/*! exports provided: SidebarBarsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SidebarBarsComponent", function() { return SidebarBarsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var SidebarBarsComponent = /** @class */ (function () {
    function SidebarBarsComponent() {
        this.data = {};
        this.dataChange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
    }
    SidebarBarsComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], SidebarBarsComponent.prototype, "data", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], SidebarBarsComponent.prototype, "dataChange", void 0);
    SidebarBarsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-sidebar-bars',
            template: __webpack_require__(/*! ./sidebar-bars.component.html */ "./src/app/components/sidebar-bars/sidebar-bars.component.html"),
            styles: [__webpack_require__(/*! ./sidebar-bars.component.scss */ "./src/app/components/sidebar-bars/sidebar-bars.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], SidebarBarsComponent);
    return SidebarBarsComponent;
}());



/***/ }),

/***/ "./src/app/components/sidebar-details/sidebar-details.component.html":
/*!***************************************************************************!*\
  !*** ./src/app/components/sidebar-details/sidebar-details.component.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"c-card u-p-medium\">\r\n  <h5 class=\"u-h6 u-mb-medium\">Social Networks</h5>\r\n\r\n  <table class=\"u-width-100\">\r\n    <tbody>\r\n    <tr *ngFor=\"let social of socials\">\r\n      <td class=\"u-pb-xsmall u-color-primary u-text-small\">\r\n\r\n        <span class=\"u-pb-xsmall u-text-right u-text-mute u-text-small\">\r\n          <i [ngClass]=\"social.icon\" class=\"u-color-primary u-text-small u-float-left\"></i>\r\n          <app-inline-input [data]=\"data[social.name]\" (dataChange)=\"this.data[social.name] = $event\"></app-inline-input>\r\n        </span>\r\n      </td>\r\n    </tr>\r\n    </tbody>\r\n  </table>\r\n\r\n  <div class=\"c-divider u-mv-small\"></div>\r\n\r\n  <div class=\"u-flex u-justify-between u-align-items-center\">\r\n    <!--<p>Job Type</p>-->\r\n\r\n    <!--<div>-->\r\n      <!--<span class=\"c-badge c-badge&#45;&#45;small c-badge&#45;&#45;warning\">Design</span>-->\r\n      <!--<span class=\"c-badge c-badge&#45;&#45;small c-badge&#45;&#45;success\">Marketing</span>-->\r\n    <!--</div>-->\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/components/sidebar-details/sidebar-details.component.scss":
/*!***************************************************************************!*\
  !*** ./src/app/components/sidebar-details/sidebar-details.component.scss ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/sidebar-details/sidebar-details.component.ts":
/*!*************************************************************************!*\
  !*** ./src/app/components/sidebar-details/sidebar-details.component.ts ***!
  \*************************************************************************/
/*! exports provided: SidebarDetailsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SidebarDetailsComponent", function() { return SidebarDetailsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _defaults__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../defaults */ "./src/app/defaults.ts");
/* harmony import */ var _models_default_social__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../models/default/social */ "./src/app/models/default/social.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var SidebarDetailsComponent = /** @class */ (function () {
    function SidebarDetailsComponent() {
        this.data = { social: _models_default_social__WEBPACK_IMPORTED_MODULE_2__["DEFAULT_SOCIALS"] };
        this.dataChange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.socials = _defaults__WEBPACK_IMPORTED_MODULE_1__["SOCIAL_NETWORKS"];
    }
    SidebarDetailsComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], SidebarDetailsComponent.prototype, "data", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], SidebarDetailsComponent.prototype, "dataChange", void 0);
    SidebarDetailsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-sidebar-details',
            template: __webpack_require__(/*! ./sidebar-details.component.html */ "./src/app/components/sidebar-details/sidebar-details.component.html"),
            styles: [__webpack_require__(/*! ./sidebar-details.component.scss */ "./src/app/components/sidebar-details/sidebar-details.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], SidebarDetailsComponent);
    return SidebarDetailsComponent;
}());



/***/ }),

/***/ "./src/app/components/sidebar-members/sidebar-members.component.html":
/*!***************************************************************************!*\
  !*** ./src/app/components/sidebar-members/sidebar-members.component.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"c-card u-mb-medium\">\r\n\r\n  <div class=\"u-p-medium\">\r\n    <h5 class=\"u-h6 u-mb-medium\">{{'EVENT_STAFF_MEMBERS' |translate}}</h5>\r\n    <div *ngIf=\"data.staff && 0 !== data.staff.length\">\r\n      <div class=\"o-media u-mb-small\" *ngFor=\"let member of data.staff\">\r\n        <div class=\"o-media__img u-mr-xsmall\">\r\n          <div class=\"c-avatar c-avatar--xsmall\">\r\n            <img class=\"c-avatar__img\" [src]=\"member.avatar\" alt=\"{{member.name}} avatar\">\r\n          </div>\r\n        </div>\r\n        <div class=\"o-media__body\">\r\n          <h6 class=\"u-text-small u-mb-zero\">{{member.name}}</h6>\r\n          <p class=\"u-text-mute u-text-xsmall\">{{member.email}} - {{member.mobile}}</p>\r\n        </div>\r\n      </div><!-- // .o-media -->\r\n    </div>\r\n    <app-no-content-panel [title]=\"'NO_STAFF'\" [description]=\"'CLICK_TO_ADD_STAFF'\" appAddStaff [data]=\"data.staff\" [staff]=\"allStaff\" (dataChange)=\"this.data.staff = $event\" *ngIf=\"data.staff && 0 === data.staff.length\"></app-no-content-panel>\r\n\r\n  </div>\r\n\r\n  <div class=\"u-pv-small  u-border-top u-text-center\" appAddStaff [data]=\"data.staff\" [staff]=\"allStaff\" (dataChange)=\"this.data.staff = $event\">\r\n    <a class=\"u-text-mute u-text-uppercase u-text-xsmall\" >{{'ADD_STAFF_MEMBER' | translate}}</a>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/components/sidebar-members/sidebar-members.component.scss":
/*!***************************************************************************!*\
  !*** ./src/app/components/sidebar-members/sidebar-members.component.scss ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/sidebar-members/sidebar-members.component.ts":
/*!*************************************************************************!*\
  !*** ./src/app/components/sidebar-members/sidebar-members.component.ts ***!
  \*************************************************************************/
/*! exports provided: SidebarMembersComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SidebarMembersComponent", function() { return SidebarMembersComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _models_user__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../models/user */ "./src/app/models/user.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SidebarMembersComponent = /** @class */ (function () {
    function SidebarMembersComponent() {
        this.data = {
            staff: []
        };
        this.dataChange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.allStaff = [];
        var obj1 = JSON.parse(JSON.stringify(new _models_user__WEBPACK_IMPORTED_MODULE_1__["User"](1)));
        obj1.selected = false;
        var obj2 = JSON.parse(JSON.stringify(new _models_user__WEBPACK_IMPORTED_MODULE_1__["User"](2)));
        obj2.selected = false;
        var obj3 = JSON.parse(JSON.stringify(new _models_user__WEBPACK_IMPORTED_MODULE_1__["User"](3)));
        obj3.selected = false;
        this.allStaff.push(obj1);
        this.allStaff.push(obj2);
        this.allStaff.push(obj3);
    }
    SidebarMembersComponent.prototype.ngOnInit = function () { };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], SidebarMembersComponent.prototype, "data", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], SidebarMembersComponent.prototype, "dataChange", void 0);
    SidebarMembersComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-sidebar-members',
            template: __webpack_require__(/*! ./sidebar-members.component.html */ "./src/app/components/sidebar-members/sidebar-members.component.html"),
            styles: [__webpack_require__(/*! ./sidebar-members.component.scss */ "./src/app/components/sidebar-members/sidebar-members.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], SidebarMembersComponent);
    return SidebarMembersComponent;
}());



/***/ }),

/***/ "./src/app/components/status-bar/status-bar.component.html":
/*!*****************************************************************!*\
  !*** ./src/app/components/status-bar/status-bar.component.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"c-toolbar u-justify-between u-mb-medium\">\r\n  <nav class=\"c-counter-nav\">\r\n    <p class=\"c-counter-nav__title\">{{'STATUS' | translate}}:</p>\r\n    <div class=\"c-counter-nav__item u-hidden-down@tablet\" *ngFor=\"let status of availableStatus\">\r\n      <a class=\"c-counter-nav__link\" (click)=\"statusUpdate(status)\" [ngClass]=\"data.status === status ? 'is-active' : ''\">\r\n        <span class=\"c-counter-nav__counter\">\r\n          <i class=\"fa fa-check\"></i>\r\n        </span>{{status | translate}}\r\n      </a>\r\n    </div>\r\n    <!--<div class=\"c-counter-nav__item u-hidden-down@tablet\">-->\r\n      <!--<a class=\"c-counter-nav__link\" >-->\r\n        <!--<span class=\"c-counter-nav__counter\">-->\r\n          <!--<i class=\"fa fa-check\"></i>-->\r\n        <!--</span>Initial Design Draft-->\r\n      <!--</a>-->\r\n    <!--</div>-->\r\n    <!--<div class=\"c-counter-nav__item\">-->\r\n      <!--<a class=\"c-counter-nav__link is-active\" >-->\r\n        <!--<span class=\"c-counter-nav__counter\">-->\r\n          <!--3-->\r\n        <!--</span>Coding Sprint-->\r\n      <!--</a>-->\r\n    <!--</div>-->\r\n    <!--<div class=\"c-counter-nav__item u-hidden-down@tablet\">-->\r\n      <!--<a class=\"c-counter-nav__link\" >-->\r\n        <!--<span class=\"c-counter-nav__counter\">-->\r\n          <!--4-->\r\n        <!--</span>SEO Optimization-->\r\n      <!--</a>-->\r\n    <!--</div>-->\r\n  </nav>\r\n\r\n  <!--<span class=\"c-badge c-badge&#45;&#45;small c-badge&#45;&#45;success\">Active Project</span>-->\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/components/status-bar/status-bar.component.scss":
/*!*****************************************************************!*\
  !*** ./src/app/components/status-bar/status-bar.component.scss ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/status-bar/status-bar.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/components/status-bar/status-bar.component.ts ***!
  \***************************************************************/
/*! exports provided: StatusBarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StatusBarComponent", function() { return StatusBarComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _models_event__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../models/event */ "./src/app/models/event.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var StatusBarComponent = /** @class */ (function () {
    function StatusBarComponent() {
        this.data = new _models_event__WEBPACK_IMPORTED_MODULE_1__["Event"]();
        this.dataChange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.availableStatus = _models_event__WEBPACK_IMPORTED_MODULE_1__["EVENT_STATUS"];
    }
    StatusBarComponent.prototype.ngOnInit = function () { };
    StatusBarComponent.prototype.statusUpdate = function (status) {
        this.data.status = status;
        this.dataChange.emit(this.data);
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", _models_event__WEBPACK_IMPORTED_MODULE_1__["Event"])
    ], StatusBarComponent.prototype, "data", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], StatusBarComponent.prototype, "dataChange", void 0);
    StatusBarComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-status-bar',
            template: __webpack_require__(/*! ./status-bar.component.html */ "./src/app/components/status-bar/status-bar.component.html"),
            styles: [__webpack_require__(/*! ./status-bar.component.scss */ "./src/app/components/status-bar/status-bar.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], StatusBarComponent);
    return StatusBarComponent;
}());



/***/ }),

/***/ "./src/app/components/stock-list-item/stock-list-item.component.html":
/*!***************************************************************************!*\
  !*** ./src/app/components/stock-list-item/stock-list-item.component.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n  <td class=\"c-table__cell c-table__cell--img o-media\">\r\n\r\n    <div class=\"o-media__img u-mr-xsmall\">\r\n      <img [src]=\"photo\" style=\"width:56px;\" alt=\"{{name}} Icon\">\r\n    </div>\r\n\r\n    <div class=\"o-media__body\">\r\n    </div>\r\n  </td>\r\n\r\n  <td class=\"c-table__cell\">\r\n    {{name}}\r\n    <span class=\"u-block u-text-mute u-text-xsmall\" *ngIf=\"data.brand\">{{brand.name}}</span>\r\n    <!--<span class=\"u-block u-text-mute u-text-xsmall\">Daily: 453</span>-->\r\n  </td>\r\n  <td class=\"c-table__cell\">\r\n    <span *ngIf=\"category &&category.parent\">\r\n      {{category.parent.name}}\r\n      <span class=\"u-block u-text-mute u-text-xsmall\">{{category.name}}</span>\r\n    </span>\r\n    <span *ngIf=\"category && !category.parent\">\r\n      {{category.name}}\r\n    </span>\r\n  </td>\r\n  <td class=\"c-table__cell\">\r\n    <span class=\"c-badge\" [ngClass]=\"{\r\n      'c-badge--success': data.status === existingStatus[0],\r\n      'c-badge--warning': data.status === existingStatus[1],\r\n      'c-badge--danger': data.status === existingStatus[2]}\">{{data.status | translate}}</span>\r\n  </td>\r\n\r\n  <td class=\"c-table__cell\">\r\n    <ul class=\"u-flex u-justify-between\">\r\n      <li class=\"u-text-large\" >\r\n\r\n\r\n        <div class=\"c-dropdown u-ml-medium dropdown\" dropdown>\r\n          <a  class=\"c-btn c-btn--secondary  has-dropdown dropdown-toggle button\"  id=\"dropdwonMenuAvatar\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\" dropdownToggle >\r\n            {{'ACTIONS'}}\r\n          </a>\r\n\r\n          <div class=\"c-dropdown__menu dropdown-menu dropdown-menu-right\" aria-labelledby=\"dropdwonMenuAvatar\" *dropdownMenu>\r\n            <a class=\"c-dropdown__item dropdown-item\" *ngFor=\"let status of existingStatus\" (click)=\"statusUpdate(status)\">{{'SET_STATUS' |translate}} {{status | translate}}</a>\r\n          </div>\r\n        </div>\r\n\r\n      </li>\r\n    </ul>\r\n  </td>\r\n"

/***/ }),

/***/ "./src/app/components/stock-list-item/stock-list-item.component.scss":
/*!***************************************************************************!*\
  !*** ./src/app/components/stock-list-item/stock-list-item.component.scss ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".o-media__img u-mr-xsmall img {\n  width: 56px !important; }\n\n.c-dropdown.show .c-dropdown__menu, .dropdown.show .c-dropdown__menu {\n  min-width: 205px; }\n"

/***/ }),

/***/ "./src/app/components/stock-list-item/stock-list-item.component.ts":
/*!*************************************************************************!*\
  !*** ./src/app/components/stock-list-item/stock-list-item.component.ts ***!
  \*************************************************************************/
/*! exports provided: StockListItemComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StockListItemComponent", function() { return StockListItemComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_api_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/api.service */ "./src/app/services/api.service.ts");
/* harmony import */ var _defaults__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../defaults */ "./src/app/defaults.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var StockListItemComponent = /** @class */ (function () {
    function StockListItemComponent(api) {
        this.api = api;
        this.data = { name: '' };
        this.class = 'c-table__row';
        this.existingStatus = _defaults__WEBPACK_IMPORTED_MODULE_2__["MODEL_ENUMS"].STATUS.ITEM;
    }
    StockListItemComponent.prototype.ngOnInit = function () {
        console.log('data', this.data);
    };
    StockListItemComponent.prototype.statusUpdate = function (status) {
        this.data.status = status;
        this.api.updateMenuItemStatus(this.data.id, status);
    };
    Object.defineProperty(StockListItemComponent.prototype, "name", {
        get: function () {
            if (this.data) {
                if (this.data.name)
                    return this.data.name;
                if (this.data.item.name)
                    return this.data.item.name;
            }
            return 'MISSING_NAME';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StockListItemComponent.prototype, "photo", {
        get: function () {
            if (this.data) {
                if (this.data.photo)
                    return this.data.photo;
                if (this.data.item.photo)
                    return this.data.item.photo;
            }
            return 'assets/img/item-fallback.jpg';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StockListItemComponent.prototype, "category", {
        get: function () {
            if (this.data.category)
                return this.data.category;
            return {
                name: '',
                parent: {
                    name: ''
                }
            };
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StockListItemComponent.prototype, "brand", {
        get: function () {
            if (this.data && this.data.brand)
                return this.data.brand;
            return {
                name: ''
            };
        },
        enumerable: true,
        configurable: true
    });
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], StockListItemComponent.prototype, "data", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"])('class'),
        __metadata("design:type", Object)
    ], StockListItemComponent.prototype, "class", void 0);
    StockListItemComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-stock-list-item,[app-stock-list-item]',
            template: __webpack_require__(/*! ./stock-list-item.component.html */ "./src/app/components/stock-list-item/stock-list-item.component.html"),
            styles: [__webpack_require__(/*! ./stock-list-item.component.scss */ "./src/app/components/stock-list-item/stock-list-item.component.scss")],
        }),
        __metadata("design:paramtypes", [_services_api_service__WEBPACK_IMPORTED_MODULE_1__["ApiService"]])
    ], StockListItemComponent);
    return StockListItemComponent;
}());



/***/ }),

/***/ "./src/app/defaults.ts":
/*!*****************************!*\
  !*** ./src/app/defaults.ts ***!
  \*****************************/
/*! exports provided: APP, API, CONTACTS, STORE, GOOGLE, FROALA_OPTIONS, SOCIAL_NETWORKS, STORAGE, KEYS, MODEL_ENUMS */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "APP", function() { return APP; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "API", function() { return API; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CONTACTS", function() { return CONTACTS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "STORE", function() { return STORE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GOOGLE", function() { return GOOGLE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FROALA_OPTIONS", function() { return FROALA_OPTIONS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SOCIAL_NETWORKS", function() { return SOCIAL_NETWORKS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "STORAGE", function() { return STORAGE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "KEYS", function() { return KEYS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MODEL_ENUMS", function() { return MODEL_ENUMS; });
var APP = {
    NAME: 'CONSTS_APP_NAME',
    VERSION: '0.0.1'
};
var API = {
    HOST: 'https://drinks-api-dev.eu-gb.mybluemix.net',
    BASE_URL: 'https://drinks-api-dev.eu-gb.mybluemix.net',
    URL: 'https://drinks-api-dev.eu-gb.mybluemix.net/api',
    VERSION: 'v1'
};
var CONTACTS = {
    INFO: {
        EMAIL: 'INFO@APP.COM'
    }
};
var STORE = {
    ANDROID: {
        URL: 'https://play.google.com/store/apps'
    },
    IOS: {
        URL: 'http://itunes.com/apps/'
    }
};
var GOOGLE = {
    MAPS: 'AIzaSyBeH5korK8drYUV4BiQ93g4ecjRF_VYIRE'
};
var FROALA_OPTIONS = {
    charCounterCount: true,
    toolbarButtons: ['undo', 'redo', '|', 'bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', 'outdent', 'indent', 'clearFormatting', 'insertTable', 'html', 'insertLink', 'insertImage'],
    toolbarButtonsXS: ['undo', 'redo', '|', 'bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', 'outdent', 'indent', 'clearFormatting', 'insertTable', 'html', 'insertLink', 'insertImage'],
    toolbarButtonsSM: ['undo', 'redo', '|', 'bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', 'outdent', 'indent', 'clearFormatting', 'insertTable', 'html', 'insertLink', 'insertImage'],
    toolbarButtonsMD: ['undo', 'redo', '|', 'bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', 'outdent', 'indent', 'clearFormatting', 'insertTable', 'html', 'insertLink', 'insertImage'],
};
var SOCIAL_NETWORKS = [{
        name: 'facebook',
        icon: 'fa fa-facebook'
    }, {
        name: 'twitter',
        icon: 'fa fa-twitter'
    }, {
        name: 'instagram',
        icon: 'fa fa-instagram'
    }, {
        name: 'pinterest',
        icon: 'fa fa-pinterest'
    }, {
        name: 'linkedin',
        icon: 'fa fa-linkedin'
    }];
var STORAGE = {
    AUTH: 'Auth',
    TOKEN: 'Token',
    CHECKIN: 'Checkin',
    BAR: 'Bar',
    PROFILE: 'Profile'
};
var KEYS = {
    FACEBOOK_APP_ID: '534569243666282',
    GOOGLE_APP_ID: ''
};
var MODEL_ENUMS = {
    STATUS: {
        ITEM: ['AVAILABLE', 'ENDING', 'OUT_OF_STOCK'],
        ORDER: []
    }
};


/***/ }),

/***/ "./src/app/directives/add-bar/add-bar.directive.ts":
/*!*********************************************************!*\
  !*** ./src/app/directives/add-bar/add-bar.directive.ts ***!
  \*********************************************************/
/*! exports provided: AddBarDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddBarDirective", function() { return AddBarDirective; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AddBarDirective = /** @class */ (function () {
    function AddBarDirective() {
        this.data = false;
    }
    AddBarDirective.prototype.onClick = function ($event) {
        console.log('clicked: ' + $event);
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], AddBarDirective.prototype, "data", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"])('click', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object]),
        __metadata("design:returntype", void 0)
    ], AddBarDirective.prototype, "onClick", null);
    AddBarDirective = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"])({
            selector: '[appAddBar]'
        }),
        __metadata("design:paramtypes", [])
    ], AddBarDirective);
    return AddBarDirective;
}());



/***/ }),

/***/ "./src/app/directives/add-photo/add-photo.directive.ts":
/*!*************************************************************!*\
  !*** ./src/app/directives/add-photo/add-photo.directive.ts ***!
  \*************************************************************/
/*! exports provided: AddPhotoDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddPhotoDirective", function() { return AddPhotoDirective; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AddPhotoDirective = /** @class */ (function () {
    function AddPhotoDirective() {
        this.data = false;
    }
    AddPhotoDirective.prototype.onClick = function ($event) {
        console.log('clicked: ' + $event);
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], AddPhotoDirective.prototype, "data", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"])('click', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object]),
        __metadata("design:returntype", void 0)
    ], AddPhotoDirective.prototype, "onClick", null);
    AddPhotoDirective = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"])({
            selector: '[appAddPhoto]'
        }),
        __metadata("design:paramtypes", [])
    ], AddPhotoDirective);
    return AddPhotoDirective;
}());



/***/ }),

/***/ "./src/app/directives/add-place/add-place.directive.ts":
/*!*************************************************************!*\
  !*** ./src/app/directives/add-place/add-place.directive.ts ***!
  \*************************************************************/
/*! exports provided: AddPlaceDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddPlaceDirective", function() { return AddPlaceDirective; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm5/ngx-bootstrap-modal.js");
/* harmony import */ var _components_modals_modal_add_place_modal_add_place_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../components/modals/modal-add-place/modal-add-place.component */ "./src/app/components/modals/modal-add-place/modal-add-place.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AddPlaceDirective = /** @class */ (function () {
    function AddPlaceDirective(modalService) {
        this.modalService = modalService;
        this.data = false;
        this.dataChange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
    }
    AddPlaceDirective.prototype.onClick = function ($event) {
        this.openModal();
    };
    // ngOnInit() {}
    AddPlaceDirective.prototype.openModal = function () {
        var _this = this;
        this.modalRef = this.modalService.show(_components_modals_modal_add_place_modal_add_place_component__WEBPACK_IMPORTED_MODULE_2__["ModalAddPlaceComponent"], {
            initialState: {
                name: '',
                data: this.data,
                callback: function (data) {
                    _this.data = data;
                    _this.dataChange.emit(data);
                }
            }
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], AddPlaceDirective.prototype, "data", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], AddPlaceDirective.prototype, "dataChange", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"])('click', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object]),
        __metadata("design:returntype", void 0)
    ], AddPlaceDirective.prototype, "onClick", null);
    AddPlaceDirective = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"])({
            selector: '[appAddPlace]'
        }),
        __metadata("design:paramtypes", [ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_1__["BsModalService"]])
    ], AddPlaceDirective);
    return AddPlaceDirective;
}());



/***/ }),

/***/ "./src/app/directives/add-staff/add-staff.directive.ts":
/*!*************************************************************!*\
  !*** ./src/app/directives/add-staff/add-staff.directive.ts ***!
  \*************************************************************/
/*! exports provided: AddStaffDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddStaffDirective", function() { return AddStaffDirective; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _components_modals_modal_add_staff_modal_add_staff_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../components/modals/modal-add-staff/modal-add-staff.component */ "./src/app/components/modals/modal-add-staff/modal-add-staff.component.ts");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm5/ngx-bootstrap-modal.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AddStaffDirective = /** @class */ (function () {
    function AddStaffDirective(modalService) {
        this.modalService = modalService;
        this.staff = [];
        this.data = false;
        this.dataChange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
    }
    AddStaffDirective.prototype.onClick = function ($event) {
        this.openModal();
    };
    // ngOnInit() {}
    AddStaffDirective.prototype.openModal = function () {
        var _this = this;
        this.modalRef = this.modalService.show(_components_modals_modal_add_staff_modal_add_staff_component__WEBPACK_IMPORTED_MODULE_1__["ModalAddStaffComponent"], {
            initialState: {
                staff: this.staff,
                data: this.data,
                callback: function (data) {
                    _this.data = data;
                    _this.dataChange.emit(data);
                }
            }
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Array)
    ], AddStaffDirective.prototype, "staff", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], AddStaffDirective.prototype, "data", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], AddStaffDirective.prototype, "dataChange", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"])('click', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object]),
        __metadata("design:returntype", void 0)
    ], AddStaffDirective.prototype, "onClick", null);
    AddStaffDirective = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"])({
            selector: '[appAddStaff]'
        }),
        __metadata("design:paramtypes", [ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_2__["BsModalService"]])
    ], AddStaffDirective);
    return AddStaffDirective;
}());



/***/ }),

/***/ "./src/app/directives/add-user/add-user.directive.ts":
/*!***********************************************************!*\
  !*** ./src/app/directives/add-user/add-user.directive.ts ***!
  \***********************************************************/
/*! exports provided: AddUserDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddUserDirective", function() { return AddUserDirective; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm5/ngx-bootstrap-modal.js");
/* harmony import */ var _components_modals_modal_add_staff_modal_add_staff_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../components/modals/modal-add-staff/modal-add-staff.component */ "./src/app/components/modals/modal-add-staff/modal-add-staff.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AddUserDirective = /** @class */ (function () {
    function AddUserDirective(modalService) {
        this.modalService = modalService;
        this.data = false;
        this.dataChange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
    }
    AddUserDirective.prototype.onClick = function ($event) {
        this.openModal();
    };
    AddUserDirective.prototype.ngOnInit = function () { };
    AddUserDirective.prototype.openModal = function () {
        var _this = this;
        this.modalRef = this.modalService.show(_components_modals_modal_add_staff_modal_add_staff_component__WEBPACK_IMPORTED_MODULE_2__["ModalAddStaffComponent"], {
            initialState: {
                data: this.data,
                callback: function (data) {
                    _this.data = data;
                    _this.dataChange.emit(data);
                }
            }
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], AddUserDirective.prototype, "data", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], AddUserDirective.prototype, "dataChange", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"])('click', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object]),
        __metadata("design:returntype", void 0)
    ], AddUserDirective.prototype, "onClick", null);
    AddUserDirective = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"])({
            selector: '[appAddUser]'
        }),
        __metadata("design:paramtypes", [ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_1__["BsModalService"]])
    ], AddUserDirective);
    return AddUserDirective;
}());



/***/ }),

/***/ "./src/app/landing/home-page/home-page.component.html":
/*!************************************************************!*\
  !*** ./src/app/landing/home-page/home-page.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<header class=\"header-area overlay full-height relative v-center\" id=\"home-page\" style=\"background-position: 50% 0px;\">\r\n  <nav class=\"navbar navbar-default navbar-fixed-top\">\r\n    <ul class=\"nav navbar-nav float-right\">\r\n      <!--<li><a routerLink=\"bar-management/check-in-event\">Worker</a></li>-->\r\n      <li><a routerLink=\"/event-management/dashboard\">Manager</a></li>\r\n      <li><a routerLink=\"/bar-management/check-in-event\">Worker</a></li>\r\n      <li><a routerLink=\"/login\">Login Page</a></li>\r\n\r\n    </ul>\r\n  </nav>\r\n  <div class=\"absolute anlge-bg\"></div>\r\n  <div class=\"container\">\r\n    <div class=\"row v-center\">\r\n      <div class=\"col-xs-12 col-md-7 header-text\">\r\n        <h2>{{'HOME_PAGE_TITLE' | translate }}</h2>\r\n        <p>{{'HOME_PAGE_DESCRIPTION' | translate}}</p>\r\n        <br>\r\n        <a  class=\"button white\">Watch video</a>\r\n      </div>\r\n      <div class=\"hidden-xs hidden-sm col-md-5 text-right\">\r\n        <img src=\"assets/img/mobile.png\">\r\n     </div>\r\n    </div>\r\n  </div>\r\n  <nav class=\"navbar navbar-default navbar-fixed-bottom text-center\">\r\n    <a [href]=\"url.android\" target=\"_blank\">\r\n      <img src=\"assets/img/badge_google.png\" class=\"app-store-badge\">\r\n    </a>\r\n    <a [href]=\"url.android\" target=\"_blank\">\r\n      <img src=\"assets/img/badge_ios.png\" class=\"app-store-badge\">\r\n    </a>\r\n  </nav>\r\n</header>\r\n"

/***/ }),

/***/ "./src/app/landing/home-page/home-page.component.scss":
/*!************************************************************!*\
  !*** ./src/app/landing/home-page/home-page.component.scss ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".app-store-badge {\n  height: 50px;\n  margin-left: 5px;\n  margin-right: 5px; }\n\na {\n  font-style: italic; }\n\n.button {\n  border-radius: 24px;\n  padding: 10px; }\n\n.button.white {\n    background: #FFF;\n    color: #138fc2; }\n\nheader {\n  height: 100vh;\n  background: url('parallax-2.jpg');\n  background-size: cover;\n  color: #FFF !important; }\n\n.header-text h2, .header-text p {\n  color: #FFF !important; }\n\n.header-text h2 {\n  font-weight: bolder; }\n\n.fixed, .absolute {\n  position: absolute;\n  left: 0;\n  top: 0;\n  width: 100%;\n  height: 100%; }\n\n.anlge-bg {\n  background: url('angle-bg.png') no-repeat scroll center bottom -120px/100% auto; }\n\n.overlay:before {\n  content: \"\";\n  position: absolute;\n  left: 0;\n  top: 0;\n  width: 100%;\n  height: 100%;\n  opacity: 0.8; }\n\n.overlay:before, .sky-bg {\n  background: #75cbe7;\n  background: linear-gradient(to bottom, #75cbe7 0%, #138fc2 100%);\n  filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#75cbe7', endColorstr='#138fc2', GradientType=0);\n  color: #ffffff; }\n\n.v-center {\n  display: flex;\n  align-items: center;\n  align-content: center;\n  flex-wrap: wrap; }\n\n.owl-stage-outer {\n  transition: all 0s ease 0s;\n  width: 900px;\n  -webkit-transform: translate3d(-600px, 0px, 0px);\n          transform: translate3d(-600px, 0px, 0px); }\n\n.owl-item {\n  width: 100px;\n  margin-right: 0px; }\n\n.navbar {\n  position: fixed;\n  width: 100vw;\n  background: transparent;\n  min-height: 75px;\n  z-index: 1; }\n\n.navbar a {\n    color: #FFF;\n    font-style: italic; }\n\n.navbar.navbar-fixed-top {\n    top: 0; }\n\n.navbar.navbar-fixed-bottom {\n    bottom: 0; }\n\n.navbar ul {\n    display: inline; }\n\n.navbar ul li {\n      float: right;\n      padding: 20px; }\n"

/***/ }),

/***/ "./src/app/landing/home-page/home-page.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/landing/home-page/home-page.component.ts ***!
  \**********************************************************/
/*! exports provided: HomePageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageComponent", function() { return HomePageComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _defaults__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../defaults */ "./src/app/defaults.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var HomePageComponent = /** @class */ (function () {
    function HomePageComponent() {
        this.url = {
            android: _defaults__WEBPACK_IMPORTED_MODULE_1__["STORE"].ANDROID.URL,
            ios: _defaults__WEBPACK_IMPORTED_MODULE_1__["STORE"].IOS.URL
        };
    }
    HomePageComponent.prototype.ngOnInit = function () {
    };
    HomePageComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-home-page',
            template: __webpack_require__(/*! ./home-page.component.html */ "./src/app/landing/home-page/home-page.component.html"),
            styles: [__webpack_require__(/*! ./home-page.component.scss */ "./src/app/landing/home-page/home-page.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], HomePageComponent);
    return HomePageComponent;
}());



/***/ }),

/***/ "./src/app/landing/login-page/login-page.component.html":
/*!**************************************************************!*\
  !*** ./src/app/landing/login-page/login-page.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--<form (submit)=\"login()\">-->\r\n  <!--<input type=\"text\" placeholder=\"email\">-->\r\n  <!--<input type=\"password\" placeholder=\"password\">-->\r\n  <!--<input type=\"submit\" value=\"submit\">-->\r\n<!--</form>-->\r\n\r\n<div class=\"o-page o-page--center\">\r\n<!--[if lte IE 9]>\r\n<p class=\"browserupgrade\">You are using an <strong>outdated</strong> browser. Please <a href=\"http://browsehappy.com/\">upgrade your browser</a> to improve your experience and security.</p>\r\n<![endif]-->\r\n\r\n<div class=\"o-page__card\">\r\n  <div class=\"c-card u-mb-xsmall\">\r\n    <header class=\"c-card__header u-pt-large\">\r\n      <a class=\"c-card__icon\" >\r\n        <img src=\"assets/img/logo.svg\" alt=\"Dashboard UI Kit\">\r\n      </a>\r\n      <h1 class=\"u-h3 u-text-center u-mb-zero\">{{'PAGE.LOGIN.TITLE' | translate}}</h1>\r\n    </header>\r\n\r\n    <form class=\"c-card__body\" (submit)=\"login()\">\r\n      <div class=\"c-field u-mb-small\">\r\n        <label class=\"c-field__label\" for=\"input1\">{{'SIGN_IN' | translate}}</label>\r\n        <input class=\"c-input\" type=\"email\" id=\"input1\" name=\"email\" placeholder=\"pihh@drinks.com\" [(ngModel)]=\"email\">\r\n      </div>\r\n\r\n      <div class=\"c-field u-mb-small\">\r\n        <label class=\"c-field__label\" for=\"input2\">{{'PASSWORD' | translate}}</label>\r\n        <input class=\"c-input\" type=\"password\" id=\"input2\" name=\"password\" placeholder=\"Numbers, Letters...\" [(ngModel)]=\"password\">\r\n      </div>\r\n\r\n      <button class=\"c-btn c-btn--info c-btn--fullwidth\" type=\"button\" (click)=\"login($event)\">{{'SIGN_IN' | translate}}</button>\r\n      <br><br>\r\n      <button class=\"c-btn c-btn--info c-btn--fullwidth\" type=\"button\" (click)=\"loginFb($event)\">{{'SIGN_IN_FACEBOOK' | translate}}</button>\r\n\r\n\r\n      <span class=\"c-divider c-divider--small has-text u-mv-medium\">{{'SIGN_IN' | translate}}</span>\r\n\r\n      <div class=\"o-line\">\r\n        <a class=\"c-icon u-bg-twitter\" >\r\n          <i class=\"fa fa-twitter\"></i>\r\n        </a>\r\n\r\n        <a class=\"c-icon u-bg-facebook\" >\r\n          <i class=\"fa fa-facebook\"></i>\r\n        </a>\r\n\r\n        <a class=\"c-icon u-bg-pinterest\" >\r\n          <i class=\"fa fa-pinterest\"></i>\r\n        </a>\r\n\r\n        <a class=\"c-icon u-bg-dribbble\" >\r\n          <i class=\"fa fa-dribbble\"></i>\r\n        </a>\r\n      </div>\r\n    </form>\r\n  </div>\r\n\r\n  <div class=\"o-line\">\r\n    <a class=\"u-text-mute u-text-small\" href=\"register.html\">{{'NO_ACCOUNT_SIGNUP' | translate}}</a>\r\n    <a class=\"u-text-mute u-text-small\" href=\"forgot-password.html\">{{ 'FORGOT_PASSWORD' | translate}}</a>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/landing/login-page/login-page.component.scss":
/*!**************************************************************!*\
  !*** ./src/app/landing/login-page/login-page.component.scss ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/landing/login-page/login-page.component.ts":
/*!************************************************************!*\
  !*** ./src/app/landing/login-page/login-page.component.ts ***!
  \************************************************************/
/*! exports provided: LoginPageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageComponent", function() { return LoginPageComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/auth.service */ "./src/app/services/auth.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var LoginPageComponent = /** @class */ (function () {
    function LoginPageComponent(auth, router) {
        this.auth = auth;
        this.router = router;
    }
    LoginPageComponent.prototype.ngOnInit = function () {
    };
    LoginPageComponent.prototype.login = function ($event) {
        var _this = this;
        $event.preventDefault();
        this.auth.login({ email: this.email, password: this.password }).then(function (data) {
            _this.router.navigate(['bar-management/check-in-event']);
        }).catch(function (ex) {
            console.warn(ex);
            alert('FAILED TO LOGIN');
        });
    };
    LoginPageComponent.prototype.loginFb = function () {
        var _this = this;
        this.auth.loginFb().then(function (data) {
            _this.router.navigate(['bar-management/check-in-event']);
        });
    };
    LoginPageComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-login-page',
            template: __webpack_require__(/*! ./login-page.component.html */ "./src/app/landing/login-page/login-page.component.html"),
            styles: [__webpack_require__(/*! ./login-page.component.scss */ "./src/app/landing/login-page/login-page.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], LoginPageComponent);
    return LoginPageComponent;
}());



/***/ }),

/***/ "./src/app/landing/options-page/options-page.component.html":
/*!******************************************************************!*\
  !*** ./src/app/landing/options-page/options-page.component.html ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<div>\r\n<app-browser-happy></app-browser-happy>\r\n\r\n<app-bar-nav></app-bar-nav>\r\n\r\n<div class=\"c-toolbar u-mb-medium\">\r\n\r\n  <div class=\"c-btn-group u-mr-medium u-hidden-down@tablet\">\r\n    <a class=\"c-btn c-btn--secondary\" >\r\n      <i class=\"fa fa-check-square-o u-opacity-medium\"></i>\r\n    </a>\r\n    <a class=\"c-btn c-btn--secondary\" >\r\n      <i class=\"fa fa-trash-o u-opacity-medium\"></i>\r\n    </a>\r\n  </div>\r\n\r\n  <a class=\"c-toolbar__icon has-divider u-hidden-down@mobile\" ><i class=\"fa fa-th-large\"></i></a>\r\n  <a class=\"c-toolbar__icon has-divider u-hidden-down@mobile\" ><i class=\"fa fa-navicon\"></i></a>\r\n\r\n  <input type=\"range\" class=\"c-range c-range--inline u-mr-auto u-hidden-down@mobile\">\r\n\r\n  <nav class=\"c-toolbar__nav u-mr-auto\">\r\n    <a class=\"c-toolbar__nav-item is-active\" href=\"#tab1\">All Projects</a>\r\n    <a class=\"c-toolbar__nav-item\" href=\"#tab2\">Synced Projects</a>\r\n  </nav>\r\n\r\n  <a class=\"c-btn c-btn--success u-ml-auto u-hidden-down@mobile\" >\r\n    <i class=\"fa fa-plus u-mr-xsmall u-opacity-medium\"></i>New Project\r\n  </a>\r\n</div>\r\n\r\n<div class=\"container u-mb-medium\">\r\n  <div class=\"row u-justify-center\">\r\n    <app-card-bar *ngFor=\"let bar of bars\" class=\"col-sm-6 col-lg-3\"></app-card-bar>\r\n   <!-- <div class=\"col-sm-6 col-lg-3\">\r\n      <div class=\"c-project\">\r\n        <div class=\"c-project__img\">\r\n          <img src=\"assets/img/project-board1.jpg\" alt=\"Waves\">\r\n        </div>\r\n\r\n        <h3 class=\"c-project__title\">Beach Stories\r\n          <span class=\"c-project__status\">Last updated: <span class=\"u-text-bold\">Yesterday 3:41 AM</span></span>\r\n        </h3>\r\n\r\n        <div class=\"c-project__team\">\r\n\r\n          <a class=\"c-project__profile c-tooltip c-tooltip&#45;&#45;top\" aria-label=\"Joseph Mullins\" >\r\n            <img src=\"assets/img/avatar1-72.jpg\" alt=\"Adam's Face\">\r\n          </a>\r\n\r\n          <a class=\"c-project__profile c-tooltip c-tooltip&#45;&#45;top\" aria-label=\"Stella Munoz\" >\r\n            <img src=\"assets/img/avatar2-72.jpg\" alt=\"Adam's Face\">\r\n          </a>\r\n\r\n          <a class=\"c-project__profile c-tooltip c-tooltip&#45;&#45;top\" aria-label=\"Una Higgins\" >\r\n            <img src=\"assets/img/avatar3-72.jpg\" alt=\"Adam's Face\">\r\n          </a>\r\n\r\n          <a class=\"c-project__profile c-tooltip c-tooltip&#45;&#45;top\" aria-label=\"Dylan Shelton\" >\r\n            <img src=\"assets/img/avatar4-72.jpg\" alt=\"Adam's Face\">\r\n          </a>\r\n\r\n          <a class=\"c-project__profile c-project__profile&#45;&#45;btn c-tooltip c-tooltip&#45;&#45;top\" aria-label=\"Add a new member\" >\r\n            <i class=\"fa fa-plus\"></i>\r\n          </a>\r\n        </div>\r\n\r\n      </div>&lt;!&ndash; // .c-project &ndash;&gt;\r\n    </div>-->\r\n\r\n<!--    <div class=\"col-sm-6 col-lg-3\">\r\n      <div class=\"c-project\">\r\n        <div class=\"c-project__img\">\r\n          <img src=\"assets/img/project-board2.jpg\" alt=\"Waves\">\r\n        </div>\r\n\r\n        <h3 class=\"c-project__title\">Mountains\r\n          <span class=\"c-project__status\">Last updated: <span class=\"u-text-bold\">Monday 1:41 AM</span></span>\r\n        </h3>\r\n\r\n        <div class=\"c-project__team\">\r\n\r\n          <a class=\"c-project__profile c-tooltip c-tooltip&#45;&#45;top\" aria-label=\"Joseph Mullins\" >\r\n            <img src=\"assets/img/avatar5-72.jpg\" alt=\"Adam's Face\">\r\n          </a>\r\n\r\n          <a class=\"c-project__profile c-tooltip c-tooltip&#45;&#45;top\" aria-label=\"Stella Munoz\" >\r\n            <img src=\"assets/img/avatar6-72.jpg\" alt=\"Adam's Face\">\r\n          </a>\r\n\r\n          <a class=\"c-project__profile c-tooltip c-tooltip&#45;&#45;top\" aria-label=\"Una Higgins\" >\r\n            <img src=\"assets/img/avatar7-72.jpg\" alt=\"Adam's Face\">\r\n          </a>\r\n\r\n          <a class=\"c-project__profile c-tooltip c-tooltip&#45;&#45;top\" aria-label=\"Dylan Shelton\" >\r\n            <img src=\"assets/img/avatar8-72.jpg\" alt=\"Adam's Face\">\r\n          </a>\r\n\r\n          <a class=\"c-project__profile c-project__profile&#45;&#45;btn c-tooltip c-tooltip&#45;&#45;top\" aria-label=\"Add a new member\" >\r\n            <i class=\"fa fa-plus\"></i>\r\n          </a>\r\n        </div>\r\n\r\n      </div>&lt;!&ndash; // .c-project &ndash;&gt;\r\n    </div>\r\n\r\n    <div class=\"col-sm-6 col-lg-3\">\r\n      <div class=\"c-project\">\r\n        <div class=\"c-project__img\">\r\n          <img src=\"assets/img/project-board3.jpg\" alt=\"Waves\">\r\n        </div>\r\n\r\n        <h3 class=\"c-project__title\">Between Clouds\r\n          <span class=\"c-project__status\">Last updated: <span class=\"u-text-bold\">Monday 2:12 AM</span></span>\r\n        </h3>\r\n\r\n        <div class=\"c-project__team\">\r\n          <a class=\"c-project__profile c-tooltip c-tooltip&#45;&#45;top\" aria-label=\"Joseph Mullins\" >\r\n            <img src=\"assets/img/avatar1-72.jpg\" alt=\"Adam's Face\">\r\n          </a>\r\n\r\n          <a class=\"c-project__profile c-tooltip c-tooltip&#45;&#45;top\" aria-label=\"Stella Munoz\" >\r\n            <img src=\"assets/img/avatar2-72.jpg\" alt=\"Adam's Face\">\r\n          </a>\r\n\r\n          <a class=\"c-project__profile c-tooltip c-tooltip&#45;&#45;top\" aria-label=\"Una Higgins\" >\r\n            <img src=\"assets/img/avatar3-72.jpg\" alt=\"Adam's Face\">\r\n          </a>\r\n\r\n          <a class=\"c-project__profile c-tooltip c-tooltip&#45;&#45;top\" aria-label=\"Dylan Shelton\" >\r\n            <img src=\"assets/img/avatar4-72.jpg\" alt=\"Adam's Face\">\r\n          </a>\r\n\r\n          <a class=\"c-project__profile c-project__profile&#45;&#45;btn c-tooltip c-tooltip&#45;&#45;top\" aria-label=\"Add a new member\" >\r\n            <i class=\"fa fa-plus\"></i>\r\n          </a>\r\n        </div>\r\n\r\n      </div>&lt;!&ndash; // .c-project &ndash;&gt;\r\n    </div>\r\n\r\n    <div class=\"col-sm-6 col-lg-3\">\r\n      <div class=\"c-project\">\r\n        <div class=\"c-project__img\">\r\n          <img src=\"assets/img/project-board4.jpg\" alt=\"Waves\">\r\n        </div>\r\n\r\n        <h3 class=\"c-project__title\">The underground\r\n          <span class=\"c-project__status\">Last updated: <span class=\"u-text-bold\">Sunday 3:41 PM</span></span>\r\n        </h3>\r\n\r\n        <div class=\"c-project__team\">\r\n\r\n          <a class=\"c-project__profile c-tooltip c-tooltip&#45;&#45;top\" aria-label=\"Joseph Mullins\" >\r\n            <img src=\"assets/img/avatar5-72.jpg\" alt=\"Adam's Face\">\r\n          </a>\r\n\r\n          <a class=\"c-project__profile c-tooltip c-tooltip&#45;&#45;top\" aria-label=\"Stella Munoz\" >\r\n            <img src=\"assets/img/avatar6-72.jpg\" alt=\"Adam's Face\">\r\n          </a>\r\n\r\n          <a class=\"c-project__profile c-tooltip c-tooltip&#45;&#45;top\" aria-label=\"Una Higgins\" >\r\n            <img src=\"assets/img/avatar7-72.jpg\" alt=\"Adam's Face\">\r\n          </a>\r\n\r\n          <a class=\"c-project__profile c-tooltip c-tooltip&#45;&#45;top\" aria-label=\"Dylan Shelton\" >\r\n            <img src=\"assets/img/avatar8-72.jpg\" alt=\"Adam's Face\">\r\n          </a>\r\n\r\n          <a class=\"c-project__profile c-project__profile&#45;&#45;btn c-tooltip c-tooltip&#45;&#45;top\" aria-label=\"Add a new member\" >\r\n            <i class=\"fa fa-plus\"></i>\r\n          </a>\r\n        </div>\r\n\r\n      </div>&lt;!&ndash; // .c-project &ndash;&gt;\r\n    </div>-->\r\n  </div>\r\n\r\n  <!--<div class=\"row u-justify-center\">\r\n    <div class=\"col-sm-6 col-lg-3\">\r\n      <div class=\"c-project\">\r\n        <div class=\"c-project__img\">\r\n          <img src=\"assets/img/project-board5.jpg\" alt=\"Waves\">\r\n        </div>\r\n\r\n        <h3 class=\"c-project__title\">Sunrise\r\n          <span class=\"c-project__status\">Last updated: <span class=\"u-text-bold\">Sunday 1:19 PM</span></span>\r\n        </h3>\r\n\r\n        <div class=\"c-project__team\">\r\n\r\n          <a class=\"c-project__profile c-tooltip c-tooltip&#45;&#45;top\" aria-label=\"Joseph Mullins\" >\r\n            <img src=\"assets/img/avatar1-72.jpg\" alt=\"Adam's Face\">\r\n          </a>\r\n\r\n          <a class=\"c-project__profile c-tooltip c-tooltip&#45;&#45;top\" aria-label=\"Stella Munoz\" >\r\n            <img src=\"assets/img/avatar2-72.jpg\" alt=\"Adam's Face\">\r\n          </a>\r\n\r\n          <a class=\"c-project__profile c-tooltip c-tooltip&#45;&#45;top\" aria-label=\"Una Higgins\" >\r\n            <img src=\"assets/img/avatar3-72.jpg\" alt=\"Adam's Face\">\r\n          </a>\r\n\r\n          <a class=\"c-project__profile c-tooltip c-tooltip&#45;&#45;top\" aria-label=\"Dylan Shelton\" >\r\n            <img src=\"assets/img/avatar4-72.jpg\" alt=\"Adam's Face\">\r\n          </a>\r\n\r\n          <a class=\"c-project__profile c-project__profile&#45;&#45;btn c-tooltip c-tooltip&#45;&#45;top\" aria-label=\"Add a new member\" >\r\n            <i class=\"fa fa-plus\"></i>\r\n          </a>\r\n        </div>\r\n\r\n      </div>&lt;!&ndash; // .c-project &ndash;&gt;\r\n    </div>\r\n\r\n    <div class=\"col-sm-6 col-lg-3\">\r\n      <div class=\"c-project\">\r\n        <div class=\"c-project__img\">\r\n          <img src=\"assets/img/project-board6.jpg\" alt=\"Waves\">\r\n        </div>\r\n\r\n        <h3 class=\"c-project__title\">Big Ocean\r\n          <span class=\"c-project__status\">Last updated: <span class=\"u-text-bold\">Saturday 9:54 PM</span></span>\r\n        </h3>\r\n\r\n        <div class=\"c-project__team\">\r\n          <a class=\"c-project__profile c-tooltip c-tooltip&#45;&#45;top\" aria-label=\"Joseph Mullins\" >\r\n            <img src=\"assets/img/avatar5-72.jpg\" alt=\"Adam's Face\">\r\n          </a>\r\n\r\n          <a class=\"c-project__profile c-tooltip c-tooltip&#45;&#45;top\" aria-label=\"Stella Munoz\" >\r\n            <img src=\"assets/img/avatar6-72.jpg\" alt=\"Adam's Face\">\r\n          </a>\r\n\r\n          <a class=\"c-project__profile c-tooltip c-tooltip&#45;&#45;top\" aria-label=\"Una Higgins\" >\r\n            <img src=\"assets/img/avatar7-72.jpg\" alt=\"Adam's Face\">\r\n          </a>\r\n\r\n          <a class=\"c-project__profile c-tooltip c-tooltip&#45;&#45;top\" aria-label=\"Dylan Shelton\" >\r\n            <img src=\"assets/img/avatar8-72.jpg\" alt=\"Adam's Face\">\r\n          </a>\r\n\r\n          <a class=\"c-project__profile c-project__profile&#45;&#45;btn c-tooltip c-tooltip&#45;&#45;top\" aria-label=\"Add a new member\" >\r\n            <i class=\"fa fa-plus\"></i>\r\n          </a>\r\n        </div>\r\n\r\n      </div>&lt;!&ndash; // .c-project &ndash;&gt;\r\n    </div>\r\n\r\n    <div class=\"col-sm-6 col-lg-3\">\r\n      <div class=\"c-project\">\r\n        <div class=\"c-project__img\">\r\n          <img src=\"assets/img/project-board7.jpg\" alt=\"Waves\">\r\n        </div>\r\n\r\n        <h3 class=\"c-project__title\">Sunset\r\n          <span class=\"c-project__status\">Last updated: <span class=\"u-text-bold\">Saturday 5:24 PM</span></span>\r\n        </h3>\r\n\r\n        <div class=\"c-project__team\">\r\n\r\n          <a class=\"c-project__profile c-tooltip c-tooltip&#45;&#45;top\" aria-label=\"Joseph Mullins\" >\r\n            <img src=\"assets/img/avatar1-72.jpg\" alt=\"Adam's Face\">\r\n          </a>\r\n\r\n          <a class=\"c-project__profile c-tooltip c-tooltip&#45;&#45;top\" aria-label=\"Stella Munoz\" >\r\n            <img src=\"assets/img/avatar2-72.jpg\" alt=\"Adam's Face\">\r\n          </a>\r\n\r\n          <a class=\"c-project__profile c-tooltip c-tooltip&#45;&#45;top\" aria-label=\"Una Higgins\" >\r\n            <img src=\"assets/img/avatar3-72.jpg\" alt=\"Adam's Face\">\r\n          </a>\r\n\r\n          <a class=\"c-project__profile c-tooltip c-tooltip&#45;&#45;top\" aria-label=\"Dylan Shelton\" >\r\n            <img src=\"assets/img/avatar4-72.jpg\" alt=\"Adam's Face\">\r\n          </a>\r\n\r\n          <a class=\"c-project__profile c-project__profile&#45;&#45;btn c-tooltip c-tooltip&#45;&#45;top\" aria-label=\"Add a new member\" >\r\n            <i class=\"fa fa-plus\"></i>\r\n          </a>\r\n        </div>\r\n\r\n      </div>&lt;!&ndash; // .c-project &ndash;&gt;\r\n    </div>\r\n  </div>&lt;!&ndash; // .row &ndash;&gt;-->\r\n</div><!-- // .container -->\r\n\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/landing/options-page/options-page.component.scss":
/*!******************************************************************!*\
  !*** ./src/app/landing/options-page/options-page.component.scss ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/landing/options-page/options-page.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/landing/options-page/options-page.component.ts ***!
  \****************************************************************/
/*! exports provided: OptionsPageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OptionsPageComponent", function() { return OptionsPageComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var OptionsPageComponent = /** @class */ (function () {
    function OptionsPageComponent() {
        this.bars = [1, 2, 3, 4, 5, 6];
    }
    OptionsPageComponent.prototype.ngOnInit = function () {
    };
    OptionsPageComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-options-page',
            template: __webpack_require__(/*! ./options-page.component.html */ "./src/app/landing/options-page/options-page.component.html"),
            styles: [__webpack_require__(/*! ./options-page.component.scss */ "./src/app/landing/options-page/options-page.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], OptionsPageComponent);
    return OptionsPageComponent;
}());



/***/ }),

/***/ "./src/app/models/address.ts":
/*!***********************************!*\
  !*** ./src/app/models/address.ts ***!
  \***********************************/
/*! exports provided: Address */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Address", function() { return Address; });
var Address = /** @class */ (function () {
    function Address(address, url, city, country, location) {
        this.address = address;
        this.url = url;
        this.city = city;
        this.country = country;
        this.location = location;
        if (!address) {
            this.address = 'ADDRESS';
        }
        if (!url) {
            this.url = 'ADDRESS_URL';
        }
        if (!city) {
            this.city = 'CITY';
        }
        if (!country) {
            this.country = 'COUNTRY';
        }
        if (!location) {
            this.location = {
                latitude: 0,
                longitude: 0
            };
        }
    }
    return Address;
}());



/***/ }),

/***/ "./src/app/models/brand.ts":
/*!*********************************!*\
  !*** ./src/app/models/brand.ts ***!
  \*********************************/
/*! exports provided: Brand */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Brand", function() { return Brand; });
var Brand = /** @class */ (function () {
    function Brand(id, name, image) {
        this.id = id;
        this.name = name;
        this.image = image;
        if (!id) {
            this.id = 1;
        }
        if (!name) {
            this.name = 'BRAND_NAME';
        }
    }
    return Brand;
}());



/***/ }),

/***/ "./src/app/models/category.ts":
/*!************************************!*\
  !*** ./src/app/models/category.ts ***!
  \************************************/
/*! exports provided: Category */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Category", function() { return Category; });
var Category = /** @class */ (function () {
    function Category(id, name, description, parent) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.parent = parent;
        if (!id) {
            this.id = 1;
        }
        if (!name) {
            this.name = 'CATEGORY_NAME';
        }
        if (!name) {
            this.name = 'CATEGORY_DECRIPTION';
        }
    }
    return Category;
}());



/***/ }),

/***/ "./src/app/models/company.ts":
/*!***********************************!*\
  !*** ./src/app/models/company.ts ***!
  \***********************************/
/*! exports provided: Company */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Company", function() { return Company; });
/* harmony import */ var _address__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./address */ "./src/app/models/address.ts");

var Company = /** @class */ (function () {
    function Company(id, name, places, users, events, location, address) {
        this.id = id;
        this.name = name;
        this.places = places;
        this.users = users;
        this.events = events;
        this.location = location;
        this.address = address;
        if (!id) {
            this.id = 1;
        }
        if (!name) {
            this.name = 'COMPANY_NAME';
        }
        if (!places) {
            this.places = [];
        }
        if (!users) {
            this.users = [];
        }
        if (!events) {
            this.events = [];
        }
        if (!location) {
            this.location = { latitude: 0, longitude: 0 };
        }
        if (!address) {
            this.address = new _address__WEBPACK_IMPORTED_MODULE_0__["Address"]();
        }
    }
    return Company;
}());



/***/ }),

/***/ "./src/app/models/currency.ts":
/*!************************************!*\
  !*** ./src/app/models/currency.ts ***!
  \************************************/
/*! exports provided: Currency */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Currency", function() { return Currency; });
var Currency = /** @class */ (function () {
    function Currency(name, short_name, symbol) {
        this.name = name;
        this.short_name = short_name;
        this.symbol = symbol;
        if (!name) {
            this.name = 'Euro';
        }
        if (!short_name) {
            this.short_name = 'EUR';
        }
        if (!symbol) {
            this.symbol = '€';
        }
    }
    return Currency;
}());



/***/ }),

/***/ "./src/app/models/default/social.ts":
/*!******************************************!*\
  !*** ./src/app/models/default/social.ts ***!
  \******************************************/
/*! exports provided: DEFAULT_SOCIALS */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DEFAULT_SOCIALS", function() { return DEFAULT_SOCIALS; });
var DEFAULT_SOCIALS = {
    facebook: '',
    twitter: '',
    instagram: '',
    pinterest: '',
    linkedin: ''
};


/***/ }),

/***/ "./src/app/models/dummy-data/line-chart.ts":
/*!*************************************************!*\
  !*** ./src/app/models/dummy-data/line-chart.ts ***!
  \*************************************************/
/*! exports provided: LINE_CHART_DATA */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LINE_CHART_DATA", function() { return LINE_CHART_DATA; });
var LINE_CHART_DATA = {
    labels: [1500, 1600, 1700, 1750, 1800, 1850, 1900, 1950, 1999, 2050],
    datasets: [{
            data: [86, 114, 106, 106, 107, 111, 133, 221, 783, 2478],
            label: 'Africa',
            borderColor: '#3e95cd',
            fill: false
        }, {
            data: [282, 350, 411, 502, 635, 809, 947, 1402, 3700, 5267],
            label: 'Asia',
            borderColor: '#8e5ea2',
            fill: false
        }, {
            data: [168, 170, 178, 190, 203, 276, 408, 547, 675, 734],
            label: 'Europe',
            borderColor: '#3cba9f',
            fill: false
        }, {
            data: [40, 20, 10, 16, 24, 38, 74, 167, 508, 784],
            label: 'Latin America',
            borderColor: '#e8c3b9',
            fill: false
        }, {
            data: [6, 3, 2, 2, 7, 26, 82, 172, 312, 433],
            label: 'North America',
            borderColor: '#c45850',
            fill: false
        }]
};
/*
{
  type: 'line',
    data: {
  labels: [1500,1600,1700,1750,1800,1850,1900,1950,1999,2050],
    datasets: [{
    data: [86,114,106,106,107,111,133,221,783,2478],
    label: 'Africa',
    borderColor: '#3e95cd',
    fill: false
  }, {
    data: [282,350,411,502,635,809,947,1402,3700,5267],
    label: 'Asia',
    borderColor: '#8e5ea2',
    fill: false
  }, {
    data: [168,170,178,190,203,276,408,547,675,734],
    label: 'Europe',
    borderColor: '#3cba9f',
    fill: false
  }, {
    data: [40,20,10,16,24,38,74,167,508,784],
    label: 'Latin America',
    borderColor: '#e8c3b9',
    fill: false
  }, {
    data: [6,3,2,2,7,26,82,172,312,433],
    label: 'North America',
    borderColor: '#c45850',
    fill: false
  }
  ]
},
  options: {
    title: {
      display: true,
        text: 'World population per region (in millions)'
    }
  }
});
*/
// <ngx-chartjs [data]='data' type='bar'></ngx-chartjs>
//


/***/ }),

/***/ "./src/app/models/event.ts":
/*!*********************************!*\
  !*** ./src/app/models/event.ts ***!
  \*********************************/
/*! exports provided: EVENT_STATUS, TICKET_STATUS, Event */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EVENT_STATUS", function() { return EVENT_STATUS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TICKET_STATUS", function() { return TICKET_STATUS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Event", function() { return Event; });
/* harmony import */ var _place__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./place */ "./src/app/models/place.ts");
/* harmony import */ var _company__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./company */ "./src/app/models/company.ts");
/* harmony import */ var _default_social__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./default/social */ "./src/app/models/default/social.ts");



var EVENT_STATUS = [
    'DRAFT',
    'PUBLISHED',
    'ONGOING',
    'COMPLETE'
];
var TICKET_STATUS = [
    'AVAILABLE',
    'LAST_TICKETS',
    'SOLD_OUT'
];
var Event = /** @class */ (function () {
    function Event(id, name, description, about, photo, price, place, status, ticket_total, ticket_status, ticket_available, date_from, date_to, created_at, updated_at, company, photos, tags, staff, social) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.about = about;
        this.photo = photo;
        this.price = price;
        this.place = place;
        this.status = status;
        this.ticket_total = ticket_total;
        this.ticket_status = ticket_status;
        this.ticket_available = ticket_available;
        this.date_from = date_from;
        this.date_to = date_to;
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.company = company;
        this.photos = photos;
        this.tags = tags;
        this.staff = staff;
        this.social = social;
        if (!id) {
            this.id = 1;
        }
        if (!name) {
            this.name = 'EVENT_NAME';
        }
        if (!description) {
            this.description = 'EVENT_DESCRIPTION';
        }
        if (!about) {
            this.about = 'EVENT_ABOUT';
        }
        if (!photo) {
            this.photo = 'assets/img/project-board1.jpg';
        }
        if (!price) {
            this.price = 10.00;
        }
        if (!place) {
            this.place = new _place__WEBPACK_IMPORTED_MODULE_0__["Place"]();
        }
        if (!status) {
            this.status = EVENT_STATUS[0];
        }
        if (!ticket_total) {
            this.ticket_total = 100;
        }
        if (!ticket_status) {
            this.ticket_status = TICKET_STATUS[0];
        }
        if (!ticket_available) {
            this.ticket_available = 100;
        }
        if (!updated_at) {
            this.updated_at = new Date().toString();
        }
        if (!created_at) {
            this.updated_at = new Date().toString();
        }
        if (!date_from) {
            this.date_from = new Date().toString();
        }
        if (!date_to) {
            this.date_to = new Date().toString();
        }
        if (!company) {
            this.company = new _company__WEBPACK_IMPORTED_MODULE_1__["Company"]();
        }
        if (!photos) {
            this.photos = [];
        }
        if (!tags) {
            this.tags = [];
        }
        if (!staff) {
            this.staff = [];
        }
        if (!social) {
            this.social = _default_social__WEBPACK_IMPORTED_MODULE_2__["DEFAULT_SOCIALS"];
        }
    }
    return Event;
}());



/***/ }),

/***/ "./src/app/models/item-type.ts":
/*!*************************************!*\
  !*** ./src/app/models/item-type.ts ***!
  \*************************************/
/*! exports provided: ItemType */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ItemType", function() { return ItemType; });
var ItemType = /** @class */ (function () {
    function ItemType(id, type) {
        this.id = id;
        this.type = type;
        if (!id) {
            this.id = 1;
        }
        if (!type) {
            this.type = 'SOFT_DRINK';
        }
    }
    return ItemType;
}());



/***/ }),

/***/ "./src/app/models/item.ts":
/*!********************************!*\
  !*** ./src/app/models/item.ts ***!
  \********************************/
/*! exports provided: Item */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Item", function() { return Item; });
/* harmony import */ var _item_type__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./item-type */ "./src/app/models/item-type.ts");
/* harmony import */ var _brand__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./brand */ "./src/app/models/brand.ts");


var Item = /** @class */ (function () {
    function Item(id, name, description, type, brand) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.type = type;
        this.brand = brand;
        if (!id) {
            this.id = 1;
        }
        if (!name) {
            this.name = 'ITEM';
        }
        if (!description) {
            this.description = 'ITEM_DESCRIPTION';
        }
        if (!type) {
            this.type = new _item_type__WEBPACK_IMPORTED_MODULE_0__["ItemType"]();
        }
        if (!brand) {
            this.brand = new _brand__WEBPACK_IMPORTED_MODULE_1__["Brand"]();
        }
    }
    return Item;
}());



/***/ }),

/***/ "./src/app/models/menu-item-status.ts":
/*!********************************************!*\
  !*** ./src/app/models/menu-item-status.ts ***!
  \********************************************/
/*! exports provided: MENU_ITEM_STATUS */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MENU_ITEM_STATUS", function() { return MENU_ITEM_STATUS; });
var MENU_ITEM_STATUS = ['AVAILABLE', 'ENDING', 'OUT_OF_STOCK'];


/***/ }),

/***/ "./src/app/models/menu-item.ts":
/*!*************************************!*\
  !*** ./src/app/models/menu-item.ts ***!
  \*************************************/
/*! exports provided: MenuItem */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MenuItem", function() { return MenuItem; });
/* harmony import */ var _menu_item_status__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./menu-item-status */ "./src/app/models/menu-item-status.ts");
/* harmony import */ var _currency__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./currency */ "./src/app/models/currency.ts");


var MenuItem = /** @class */ (function () {
    function MenuItem(id, name, item, image, status, price, currency, available_single_doses) {
        this.id = id;
        this.name = name;
        this.item = item;
        this.image = image;
        this.status = status;
        this.price = price;
        this.currency = currency;
        this.available_single_doses = available_single_doses;
        if (!id) {
            this.id = 1;
        }
        if (!name) {
            this.name = 'MENU_ITEM_NAME';
        }
        if (!image) {
            this.image = 'assets/img/app-icon3.png';
        }
        if (!status) {
            this.status = _menu_item_status__WEBPACK_IMPORTED_MODULE_0__["MENU_ITEM_STATUS"][0];
        }
        if (!price) {
            this.price = 10.00;
        }
        if (!currency) {
            this.currency = new _currency__WEBPACK_IMPORTED_MODULE_1__["Currency"]();
        }
        if (!available_single_doses) {
            this.available_single_doses = 10;
        }
    }
    return MenuItem;
}());



/***/ }),

/***/ "./src/app/models/menu.ts":
/*!********************************!*\
  !*** ./src/app/models/menu.ts ***!
  \********************************/
/*! exports provided: Menu */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Menu", function() { return Menu; });
/* harmony import */ var _menu_item__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./menu-item */ "./src/app/models/menu-item.ts");

var Menu = /** @class */ (function () {
    function Menu(id, name, photo, description, menu_items) {
        this.id = id;
        this.name = name;
        this.photo = photo;
        this.description = description;
        this.menu_items = menu_items;
        if (!id) {
            this.id = 1;
        }
        if (!name) {
            this.name = 'MENU_ITEM_NAME';
        }
        if (!photo) {
            this.photo = 'assets/img/app-icon3.png';
        }
        if (!description) {
            this.description = 'MENU_DESCRIPTION';
        }
        if (!menu_items) {
            this.menu_items = [new _menu_item__WEBPACK_IMPORTED_MODULE_0__["MenuItem"](), new _menu_item__WEBPACK_IMPORTED_MODULE_0__["MenuItem"]()];
        }
    }
    return Menu;
}());



/***/ }),

/***/ "./src/app/models/order-item.ts":
/*!**************************************!*\
  !*** ./src/app/models/order-item.ts ***!
  \**************************************/
/*! exports provided: OrderItem */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderItem", function() { return OrderItem; });
/* harmony import */ var _menu_item__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./menu-item */ "./src/app/models/menu-item.ts");

var OrderItem = /** @class */ (function () {
    function OrderItem(id, amount, item, ticket) {
        this.id = id;
        this.amount = amount;
        this.item = item;
        this.ticket = ticket;
        if (!id) {
            this.id = 1;
        }
        if (!amount) {
            this.amount = 2;
        }
        if (!item) {
            this.item = new _menu_item__WEBPACK_IMPORTED_MODULE_0__["MenuItem"]();
        }
    }
    return OrderItem;
}());



/***/ }),

/***/ "./src/app/models/order.ts":
/*!*********************************!*\
  !*** ./src/app/models/order.ts ***!
  \*********************************/
/*! exports provided: Order */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Order", function() { return Order; });
/* harmony import */ var _user__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./user */ "./src/app/models/user.ts");
/* harmony import */ var _event__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./event */ "./src/app/models/event.ts");
/* harmony import */ var _currency__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./currency */ "./src/app/models/currency.ts");
/* harmony import */ var _order_item__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./order-item */ "./src/app/models/order-item.ts");




var ORDER_STATUS = [
    'OPEN',
    'PAID',
    'USED'
];
var Order = /** @class */ (function () {
    function Order(id, description, client, event, amount, currency, status, vat, order_items, updated_at, created_at) {
        this.id = id;
        this.description = description;
        this.client = client;
        this.event = event;
        this.amount = amount;
        this.currency = currency;
        this.status = status;
        this.vat = vat;
        this.order_items = order_items;
        this.updated_at = updated_at;
        this.created_at = created_at;
        if (!id) {
            this.id = 1;
        }
        if (!description) {
            this.description = 'ORDER_DESCRIPTION';
        }
        if (!client) {
            this.client = new _user__WEBPACK_IMPORTED_MODULE_0__["User"]();
        }
        if (!event) {
            this.event = new _event__WEBPACK_IMPORTED_MODULE_1__["Event"]();
        }
        if (!currency) {
            this.currency = new _currency__WEBPACK_IMPORTED_MODULE_2__["Currency"]();
        }
        if (!amount) {
            this.amount = 10.00;
        }
        if (!status) {
            this.status = ORDER_STATUS[2];
        }
        if (!order_items) {
            this.order_items = [new _order_item__WEBPACK_IMPORTED_MODULE_3__["OrderItem"](), new _order_item__WEBPACK_IMPORTED_MODULE_3__["OrderItem"]()];
        }
        if (!vat) {
            this.vat = 244196796;
        }
        if (!created_at) {
            this.created_at = new Date().toString();
        }
        if (!updated_at) {
            this.updated_at = new Date().toString();
        }
    }
    return Order;
}());



/***/ }),

/***/ "./src/app/models/place.ts":
/*!*********************************!*\
  !*** ./src/app/models/place.ts ***!
  \*********************************/
/*! exports provided: Place */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Place", function() { return Place; });
/* harmony import */ var _company__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./company */ "./src/app/models/company.ts");
/* harmony import */ var _address__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./address */ "./src/app/models/address.ts");
/* harmony import */ var _default_social__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./default/social */ "./src/app/models/default/social.ts");



var Place = /** @class */ (function () {
    function Place(id, name, address, image, bars, events, managers, owner, confirmed, photos, bluePrint, social, maps_url) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.image = image;
        this.bars = bars;
        this.events = events;
        this.managers = managers;
        this.owner = owner;
        this.confirmed = confirmed;
        this.photos = photos;
        this.bluePrint = bluePrint;
        this.social = social;
        this.maps_url = maps_url;
        if (!id) {
            this.id = 1;
        }
        if (!name) {
            this.name = 'PLACE_NAME';
        }
        if (!address) {
            this.address = new _address__WEBPACK_IMPORTED_MODULE_1__["Address"]();
        }
        if (!image) {
            this.image = 'assets/img/event2.jpg';
        }
        if (!bars) {
            this.bars = [];
        }
        if (!managers) {
            this.managers = [];
        }
        if (!owner) {
            this.owner = new _company__WEBPACK_IMPORTED_MODULE_0__["Company"]();
        }
        if (!photos) {
            this.photos = [];
        }
        if (!bluePrint) {
            this.bluePrint = '';
        } // path for a image that shows where all the bars are
        if (!social) {
            this.social = _default_social__WEBPACK_IMPORTED_MODULE_2__["DEFAULT_SOCIALS"];
        }
        if (!confirmed) {
            this.confirmed = false;
        }
        if (!maps_url) {
            this.maps_url = 'MAPS_URL';
        }
    }
    return Place;
}());



/***/ }),

/***/ "./src/app/models/user-company-role.ts":
/*!*********************************************!*\
  !*** ./src/app/models/user-company-role.ts ***!
  \*********************************************/
/*! exports provided: USER_ROLES, UserCompanyRole */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "USER_ROLES", function() { return USER_ROLES; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserCompanyRole", function() { return UserCompanyRole; });
var USER_ROLES = ['MANAGER', 'DOOR', 'BAR_MANAGER', 'BAR'];
var UserCompanyRole = /** @class */ (function () {
    function UserCompanyRole(id, user, company, role) {
        this.id = id;
        this.user = user;
        this.company = company;
        this.role = role;
        if (!id) {
            this.id = 1;
        }
        // if (!user) { this.user = new User(); }
        // if (!company) { this.company = new Company(); }
        if (!role) {
            this.role = USER_ROLES[3];
        }
    }
    return UserCompanyRole;
}());



/***/ }),

/***/ "./src/app/models/user.ts":
/*!********************************!*\
  !*** ./src/app/models/user.ts ***!
  \********************************/
/*! exports provided: User */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "User", function() { return User; });
/* harmony import */ var _event__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./event */ "./src/app/models/event.ts");
/* harmony import */ var _user_company_role__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./user-company-role */ "./src/app/models/user-company-role.ts");


var User = /** @class */ (function () {
    function User(id, email, name, mobile, cover, avatar, events, companies, company_role, devices) {
        this.id = id;
        this.email = email;
        this.name = name;
        this.mobile = mobile;
        this.cover = cover;
        this.avatar = avatar;
        this.events = events;
        this.companies = companies;
        this.company_role = company_role;
        this.devices = devices;
        if (!id) {
            this.id = 1;
        }
        if (!name) {
            this.name = 'USER_NAME';
        }
        if (!email) {
            this.email = 'USER_EMAIL';
        }
        if (!mobile) {
            this.mobile = 'USER_MOBILE_NUMBER';
        }
        if (!avatar) {
            this.avatar = 'assets/img/avatar-72.jpg';
        }
        if (!cover) {
            this.cover = 'assets/img/candidate1.jpg';
        }
        if (!events) {
            this.events = [new _event__WEBPACK_IMPORTED_MODULE_0__["Event"]()];
        }
        if (!companies) {
            this.companies = [];
        }
        if (!company_role) {
            this.company_role = new _user_company_role__WEBPACK_IMPORTED_MODULE_1__["UserCompanyRole"]();
        }
    }
    return User;
}());



/***/ }),

/***/ "./src/app/pipes/filter.pipe.ts":
/*!**************************************!*\
  !*** ./src/app/pipes/filter.pipe.ts ***!
  \**************************************/
/*! exports provided: FilterPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FilterPipe", function() { return FilterPipe; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var FilterPipe = /** @class */ (function () {
    function FilterPipe() {
    }
    FilterPipe.prototype.transform = function (items, filter, defaultFilter) {
        if (!filter || filter === '') {
            return items;
        }
        if (!Array.isArray(items)) {
            return items;
        }
        if (filter && Array.isArray(items)) {
            var filterKeys_1 = Object.keys(filter);
            if (defaultFilter) {
                return items.filter(function (item) {
                    return filterKeys_1.reduce(function (x, keyName) {
                        return (x && new RegExp(filter[keyName], 'gi').test(item[keyName])) || filter[keyName] === "";
                    }, true);
                });
            }
            else {
                return items.filter(function (item) {
                    return filterKeys_1.some(function (keyName) {
                        return new RegExp(filter[keyName], 'gi').test(item[keyName]) || filter[keyName] === "";
                    });
                });
            }
        }
    };
    FilterPipe = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Pipe"])({
            name: 'filter',
            pure: false
        })
    ], FilterPipe);
    return FilterPipe;
}());



/***/ }),

/***/ "./src/app/services/api.service.ts":
/*!*****************************************!*\
  !*** ./src/app/services/api.service.ts ***!
  \*****************************************/
/*! exports provided: ApiService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ApiService", function() { return ApiService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _defaults__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../defaults */ "./src/app/defaults.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ApiService = /** @class */ (function () {
    function ApiService(http) {
        this.http = http;
        this.base = _defaults__WEBPACK_IMPORTED_MODULE_2__["API"].URL;
        this.availableMethods = ['get', 'post', 'patch'];
    }
    ApiService.prototype.buildUrl = function (path, query) {
        if (path === void 0) { path = ''; }
        if (query === void 0) { query = ''; }
        return this.base + "/" + path + "?access_token=" + this.token + query;
    };
    ApiService.prototype.fetch = function (path, method, query) {
        if (path === void 0) { path = ''; }
        if (method === void 0) { method = 'get'; }
        if (-1 === this.availableMethods.indexOf(method)) {
            return Promise.reject('Unavailable method');
        }
        if ('get' === method) {
            path = this.buildUrl(path, query);
            return this.http[method](path).toPromise();
        }
        else {
            path = this.buildUrl(path);
            return this.http[method](path, query || {}).toPromise();
        }
    };
    // AUTH
    ApiService.prototype.facebookCallback = function (access_token) {
        return this.http.get(_defaults__WEBPACK_IMPORTED_MODULE_2__["API"].BASE_URL + "/auth/facebook-token/callback?access_token=" + access_token).toPromise();
    };
    // GET PAGES INFO
    ApiService.prototype.getUserEvents = function () {
        return this.fetch('Events', 'get', '&filter={"include":["promoter",{"bars":{"menu":{"menuItems":["currency",{"item":["brand","category"]}]}}}]}');
    };
    ApiService.prototype.getEventBars = function (eventId) {
        return this.fetch("Events/" + eventId + "/bars");
    };
    ApiService.prototype.getEventBarOrders = function (eventId, barId) {
        return this.fetch("Orders", 'get', "&filter={\"where\":{\"and\":[{\"eventId\":" + eventId + "},{\"barId\":" + barId + "}]},\"include\":{\"shoppingCart\":[{\"menuItems\":\"item\"}]}}");
    };
    ApiService.prototype.getBarMenu = function (barId) {
        return this.fetch("Bars/" + barId, 'get', "&filter={\"include\":{\"menu\":{\"menuItems\":[\"currency\",{\"item\":[\"brand\",\"category\"]}]}}}");
    };
    // UPDATE INFO
    ApiService.prototype.updateOrderStatus = function (orderId, status) {
        return this.fetch("Orders/" + orderId, 'patch', {
            status: status
        });
    };
    ApiService.prototype.updateMenuItemStatus = function (menuItemId, status) {
        return this.fetch("menuItems/" + menuItemId, 'patch', {
            status: status
        });
    };
    ApiService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], ApiService);
    return ApiService;
}());



/***/ }),

/***/ "./src/app/services/auth.service.ts":
/*!******************************************!*\
  !*** ./src/app/services/auth.service.ts ***!
  \******************************************/
/*! exports provided: AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return AuthService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm5/ionic-storage.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var angular_6_social_login__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! angular-6-social-login */ "./node_modules/angular-6-social-login/angular-6-social-login.umd.js");
/* harmony import */ var angular_6_social_login__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(angular_6_social_login__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _api_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./api.service */ "./src/app/services/api.service.ts");
/* harmony import */ var _defaults__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../defaults */ "./src/app/defaults.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var AuthService = /** @class */ (function () {
    function AuthService(storage, router, socialAuthService, api) {
        this.storage = storage;
        this.router = router;
        this.socialAuthService = socialAuthService;
        this.api = api;
        this.userData = {};
        this.authData = {};
    }
    AuthService.prototype.login = function (credentials) {
        var _this = this;
        return new Promise(function (res, rej) {
            _this.api.fetch('Users/login', 'post', credentials).then(function (data) {
                _this.storage.set(_defaults__WEBPACK_IMPORTED_MODULE_5__["STORAGE"].TOKEN, data);
                res(data);
            }).catch(function (ex) {
                rej(ex);
            });
        });
    };
    AuthService.prototype.loginFb = function () {
        var _this = this;
        return new Promise(function (res, rej) {
            _this.socialAuthService.signIn(angular_6_social_login__WEBPACK_IMPORTED_MODULE_3__["FacebookLoginProvider"].PROVIDER_ID).then(function (fbData) {
                _this.createAccessTokenWithFB(fbData.token).then(function (authData) {
                    _this.storage.set(_defaults__WEBPACK_IMPORTED_MODULE_5__["STORAGE"].AUTH, fbData);
                    _this.storage.set(_defaults__WEBPACK_IMPORTED_MODULE_5__["STORAGE"].TOKEN, authData);
                    res(authData);
                }).catch(function (ex) {
                    rej(ex);
                });
            }).catch(function (ex) {
                rej(ex);
            });
        });
    };
    AuthService.prototype.createAccessTokenWithFB = function (accessToken) {
        var _this = this;
        return new Promise(function (res, rej) {
            _this.api.facebookCallback(accessToken).then(function (authData) {
                _this.storage.set(_defaults__WEBPACK_IMPORTED_MODULE_5__["STORAGE"].TOKEN, authData).then(function () {
                    // this.events.publish(EVENTS.TOKEN, authData);
                    res(authData);
                }).catch(function (ex) {
                    // this.error.handle(ex,rej, 'ERROR.STORAGE');
                    rej(ex);
                });
            }).catch(function (ex) {
                // this.error.handle(ex,rej,'ERROR.FACEBOOK_TOKEN');
                rej(ex);
            });
        });
    };
    AuthService.prototype.getUserInfoFB = function (userID) {
        var _this = this;
        return new Promise(function (res, rej) {
            _this.api.fetch('https://graph.facebook.com/me?fields=id,name,email,gender,first_name,picture.width(720).height(720).as(picture_large)').then(function (profile) {
                profile.picture = "https://graph.facebook.com/" + userID + "/picture?type=large";
                _this.userData = profile;
                // {"data":{"height":540,"is_silhouette":false,"url":"https://platform-lookaside.fbsbx.com/platform/profilepic/?asid=10160573458725618&height=720&width=720&ext=1544297462&hash=AeSplfcWLrLP7ExH","width":540}}
                _this.storage.set(_defaults__WEBPACK_IMPORTED_MODULE_5__["STORAGE"].AUTH, _this.authData);
                _this.storage.set(_defaults__WEBPACK_IMPORTED_MODULE_5__["STORAGE"].PROFILE, profile);
                res(profile);
            }).catch(function (ex) {
                rej(ex);
                //this.error.handle(ex,rej,'ERROR.FACEBOOK_ME');
            });
        });
    };
    AuthService.prototype.loginGoogle = function () {
        return new Promise(function (res, rej) {
            res();
            alert('TODO Login with google');
        });
    };
    AuthService.prototype.logout = function () {
        this.storage.clear();
        this.router.navigate(['']);
    };
    AuthService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_ionic_storage__WEBPACK_IMPORTED_MODULE_1__["Storage"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], angular_6_social_login__WEBPACK_IMPORTED_MODULE_3__["AuthService"], _api_service__WEBPACK_IMPORTED_MODULE_4__["ApiService"]])
    ], AuthService);
    return AuthService;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




// import * as $ from 'jquery';
//
// window['$'] = $;
// window['jQuery'] = $;
if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Development\pihh\drinks-app-bar-management-system\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map